﻿# TODO: Translation updated at 2017-11-05 17:34

translate spanish strings:

    # omake.rpy:83
    old "{b}F.D.N.:{/b} 1978/09/29\n"
    new "{b}F.D.N:{/b} 29/09/1978\n"

    # omake.rpy:84
    old "{b}A.P.:{/b} Shinjuku, Tokyo\n"
    new "{b}A.P.:{/b} Shinjuku, Tokyo\n"

    # omake.rpy:85
    old "{b}Height:{/b} 5.4ft\n"
    new "{b}Altura:{/b} 165cm\n"

    # omake.rpy:86
    old "{b}Weight:{/b} 136 pounds\n"
    new "{b}Peso:{/b} 136 libras\n"

    # omake.rpy:87
    old "{b}Measurements:{/b} 74-64-83\n"
    new "{b}Medidas: {/b} 74-64-83\n"

    # omake.rpy:88
    old "{b}Blood type:{/b} A\n"
    new "{b}Tipo de sangre:{/b} A\n"

    # omake.rpy:89
    old "{b}Favourite manga:{/b} High School Samurai\n"
    new "{b}Manga favorito:{/b} High School Samurai\n"

    # omake.rpy:90
    old "{b}Favourite videogame:{/b} Lead of Fighters ‘96\n"
    new "{b}Videojuego favorito:{/b} Lead of Fighters ´96\n"

    # omake.rpy:91
    old "{b}Favourite food:{/b} American hamburgers\n"
    new "{b}Comida favorita:{/b} Hamburguesas americanas\n"

    # omake.rpy:92
    old "A young boy from Tokyo who has just moved to the village. At first, he thinks he's going to miss the urban life he knew before. But meeting Sakura will quickly change his mind...\nHe is a nice guy, determined, and sometimes a little bit crazy. He likes computers, mangas and loves to have fun with his friends. He is not afraid to face problems, especially when the sake of his friends is involved. He is quite uncertain on big decisions so he usually lets his instinct (or the player!) leading his decisions most of the time...\nHe got an older sister that is married and still lives in Tokyo."
    new "Un chico de Tokyo quien acaba de mudarse al pueblo. Al principio, él piensa que va a extrañar la vida urbana que conocía antes. Pero conociendo a Sakura hará cambiar rápidamente de opinión...\nÉl es un buen chico, determinado, y a veces un poco loco. Le gustan las computadoras, mangas y ama tener diversión con sus amigas. Él no teme enfrentar probleamas, especialmente cuando el bien de sus amistades está involucrada. Él es bastante incierto en las grandes decisiones así que usualmente deja que su instinto (¡o el jugador!) guíe sus decisiones la mayoría del tiempo...\nÉl tiene una hermana mayor que esta casada y aún vive en Tokyo."

    # omake.rpy:115
    old "{b}D.O.B.:{/b} 1979/02/28\n"
    new "{b}F.D.N.:{/b} 28/02/1979\n"

    # omake.rpy:116
    old "{b}P.O.B.:{/b} Kameoka, Kyoto\n"
    new "{b}A.P.:{/b} Kameoka, Kyoto\n"

    # omake.rpy:117
    old "{b}Height:{/b} 5.1ft\n"
    new "{b}Altura:{/b} 157cm\n"

    # omake.rpy:118
    old "{b}Weight:{/b} 121 pounds\n"
    new "{b}Peso{/b} 121 libras\n"

    # omake.rpy:119
    old "{b}Measurements:{/b} Unknown\n"
    new "{b}Medidas:{/b} Desconocidas\n"

    # omake.rpy:120
    old "{b}Blood type:{/b} AB\n"
    new "{b}Tipo de sangre:{/b} AB\n"

    # omake.rpy:121
    old "{b}Favourite manga:{/b} Uchuu Tenshi Moechan\n"
    new "{b}Manga favorito:{/b} Uchuu Tenshi Moechan\n"

    # omake.rpy:122
    old "{b}Favourite videogame:{/b} Taiko no Masuta EX 4’\n"
    new "{b}Videojuego favorito:{/b} Taiko no Masuta EX 4’\n"

    # omake.rpy:123
    old "{b}Favourite food:{/b} Beef yakitori\n"
    new "{b}Comida favorita:{/b} Beef yakitori\n"

    # omake.rpy:124
    old "Sakura is a member of the school's manga club and she has a very deep secret that makes of her a mysterious girl...\nShe is very shy but incredibly pretty. She was the idol of the school until a strange rumor about her started to spread. She likes classical music and plays violin sometimes in the night at her window..."
    new "Sakura es un miembro del club de manga de la escuela y ella tiene un muy profundo secreto que la hace una misteriosa chica...\nElla es muy tímida pero increíblemente bonita. Ella era la idol de la escuela hasta que un extraño rumor sobre ella comenzó a esparcirse. A ella le gusta la música clásica y a veces toca el violin en las noches en su ventana..."

    # omake.rpy:147
    old "{b}D.O.B.:{/b} 1979/08/05\n"
    new "{b}F.D.N.:{/b} 05/08/1979\n"

    # omake.rpy:148
    old "{b}P.O.B.:{/b} The Village, Osaka\n"
    new "{b}A.P.:{/b} El Pueblo, Osaka\n"

    # omake.rpy:149
    old "{b}Height:{/b} 5ft\n"
    new "{b}Altura:{/b} 152cm\n"

    # omake.rpy:150
    old "{b}Weight:{/b} 110 pounds\n"
    new "{b}Peso:{/b} 110 libras\n"

    # omake.rpy:151
    old "{b}Measurements:{/b} 92-64-87\n"
    new "{b}Medidas:{/b} 92-64-87\n"

    # omake.rpy:152
    old "{b}Blood type:{/b} O\n"
    new "{b}Tipo de sangre:{/b} O\n"

    # omake.rpy:153
    old "{b}Favourite manga:{/b} Rosario Maiden\n"
    new "{b}Manga favorito:{/b} Rosario Maiden\n"

    # omake.rpy:154
    old "{b}Favourite videogame:{/b} Super Musashi Galaxy Fight\n"
    new "{b}Videojuego favorito:{/b} Super Musashi Galaxy Fight\n"

    # omake.rpy:155
    old "{b}Favourite food:{/b} Takoyaki\n"
    new "{b}Comida favorita:{/b} Takoyaki\n"

    # omake.rpy:156
    old "Rika is the founder of the manga club.\nShe got very bad experiences with boys and she sees them as perverts since then. Rika cosplays as a hobby and her best and favourite cosplay is the heroine of the Domoco-chan anime. She have strange eyes minnows that makes every boys dreamy. She speaks in the Kansai dialect like most of the people originating from the Village.\nShe secretly have a little crush on Sakura..."
    new "Rika es la fundadora del club de manga.\nElla tuvo muy malas experiencias con los chicos y ella los ve como pervertidos desde entonces. Rika hace cosplays como un pasatiempo y su mejor cosplay y favorito de todos es la heroína del anime Domoco-chan. Ella tiene ojos extraños que hacen que todos los chicos sueñen. Ella habla en el dialecto Kansai como la mayoría de las personas originarias del pueblo.\nElla secretamente está atraída por Sakura... "

    # omake.rpy:179
    old "{b}D.O.B.:{/b} 1980/10/11\n"
    new "{b}F.D.N.:{/b} 11/10/1980\n"

    # omake.rpy:180
    old "{b}P.O.B.:{/b} Ginoza, Okinawa\n"
    new "{b}A.P.:{/b} Ginoza, Okinawa\n"

    # omake.rpy:181
    old "{b}Height:{/b} 4.5ft\n"
    new "{b}Altura:{/b} 140cm\n"

    # omake.rpy:182
    old "{b}Weight:{/b} 99 pounds\n"
    new "{b}Peso:{/b} 99 libras\n"

    # omake.rpy:183
    old "{b}Measurements:{/b} 71-51-74\n"
    new "{b}Medidas:{/b} 71-51-74\n"

    # omake.rpy:184
    old "{b}Blood type:{/b} B\n"
    new "{b}Tipo de sangre:{/b} B\n"

    # omake.rpy:185
    old "{b}Favourite manga:{/b} Nanda no Ryu\n"
    new "{b}Manga favorito:{/b} Nanda no Ryu\n"

    # omake.rpy:186
    old "{b}Favourite videogame:{/b} Pika Pika Rocket\n"
    new "{b}Videojuego favorito:{/b} Pika Pika Rocket\n"

    # omake.rpy:187
    old "{b}Favourite food:{/b} Chanpuruu\n"
    new "{b}Comida favorita:{/b} Chanpuruu\n"

    # omake.rpy:188
    old "Nanami lives alone with her big brother Toshio after their parents disappeared.\nShe's a quiet introvert girl at first glance, but once she's with her friends, she's a cute energy bomb. She has a natural talent for videogames, which made of her the champion of the Osaka prefecture in numerous videogames."
    new "Nanami vive sola con su hermano mayor Toshio después de que sus padres desaparecieran.\nElla es una chica callada e introvertida a primera vista, pero una vez que ella esta con sus amigos, ella es una linda bomba de energía. Ella tiene un talento natural para los videojuegos, lo cual la hizo campeona de la prefectura de Osaka en numerosos videojuegos."
# TODO: Translation updated at 2018-04-07 20:30

translate spanish strings:

    # omake.rpy:85
    old "Opening song \"TAKE MY HEART\""
    new "Canción de apertura \"TAKE MY HEART\""

    # omake.rpy:86
    old "Performed by Mew Nekohime\nLyrics by Max le Fou and Masaki Deguchi\nComposed and sequenced by Max le Fou\n© {a=http://www.maxlefou.com/}JMF{/a} 2018"
    new "Interpretado por Mew Nekohime\nLetras de Max le Fou y Masaki Deguchi\nCompuesto y secuenciado por by Max le Fou\n© {a=http://www.maxlefou.com/}JMF{/a} 2018"

    # omake.rpy:92
    old "{b}Japanese lyrics:{/b}\n"
    new "{b}Letras japonesas:{/b}\n"

    # omake.rpy:108
    old "{b}Romaji:{/b}\n"
    new "{b}Romaji:{/b}\n"

    # omake.rpy:124
    old "{b}Translation:{/b}\n"
    new "{b}Traducción:{/b}\n"

    # omake.rpy:125
    old "When you take my hand, I feel I could fly"
    new "Cuando tomas mi mano, siento que puedo volar"

    # omake.rpy:126
    old "When I dive into your eyes, I feel I could drown in happiness\n"
    new "Cuando me sumerjo en tus ojos, siento que podría ahogarme en felicidad\n"

    # omake.rpy:127
    old "We sure look different"
    new "Seguro que nos vemos diferentes"

    # omake.rpy:128
    old "But despite that, my heart beats loud\n"
    new "Pero a pesar de eso, mi corazón late fuerte\n"

    # omake.rpy:129
    old "A boy and a girl"
    new "Un niño y una niña"

    # omake.rpy:130
    old "I'm just a human"
    new "Solo soy un humano"

    # omake.rpy:131
    old "I hope you don't mind"
    new "Espero que no te moleste"

    # omake.rpy:132
    old "I can't control my feelings"
    new "No puedo controlar mis sentimientos"

    # omake.rpy:133
    old "Come to me, take my heart"
    new "Ven a mí, toma mi corazón"

    # omake.rpy:134
    old "I will devote myself to you"
    new "Me dedicaré a ti"

    # omake.rpy:135
    old "No matter what, I love you"
    new "No importa qué, te amo"

    # omake.rpy:168
    old "{b}D.O.B.:{/b} 1978/09/29\n"
    new "{b}D.O.B.:{/b} 1978/09/29\n"

    # omake.rpy:169
    old "{b}P.O.B.:{/b} Shinjuku, Tokyo\n"
    new "{b}P.O.B.:{/b} Shinjuku, Tokyo\n"
# TODO: Translation updated at 2019-04-27 10:49

translate spanish strings:

    # omake.rpy:278
    old "Chapter 1"
    new "Capítulo 1"

    # omake.rpy:278
    old "Complete the first chapter"
    new "Completa el primer capítulo"

    # omake.rpy:278
    old "Chapter 2"
    new "Capítulo 2"

    # omake.rpy:278
    old "Complete the second chapter"
    new "Completa el segundo capítulo"

    # omake.rpy:278
    old "Chapter 3"
    new "Capítulo 3"

    # omake.rpy:278
    old "Complete the third chapter"
    new "Completa el tercer capítulo"

    # omake.rpy:278
    old "Chapter 4"
    new "Capítulo 4"

    # omake.rpy:278
    old "Complete the fourth chapter"
    new "Completa el cuarto capítulo"

    # omake.rpy:278
    old "Chapter 5"
    new "Capítulo 5"

    # omake.rpy:278
    old "Complete the fifth chapter"
    new "Completa el quinto capítulo"

    # omake.rpy:278
    old "Finally together"
    new "Finally together"

    # omake.rpy:278
    old "Finish the game for the first time"
    new "Finaliza el juego por primera vez"

    # omake.rpy:278
    old "The perfume of rice fields"
    new "El perfume de los campos de arroz"

    # omake.rpy:278
    old "Get a kiss from Sakura"
    new "Obtén un beso de Sakura"

    # omake.rpy:278
    old "It's not that I like you, baka!"
    new "¡No es que me gustes, baka!"

    # omake.rpy:278
    old "Get a kiss from Rika"
    new "Obtén un beso de Rika"

    # omake.rpy:278
    old "A new kind of game"
    new "Un nuevo tipo de juego"

    # omake.rpy:278
    old "Get a kiss from Nanami"
    new "Obtén un beso de Nanami"

    # omake.rpy:278
    old "It's a trap!"
    new "¡It's a trap!"

    # omake.rpy:278
    old "Find the truth about Sakura"
    new "Encuentra la verdad sobre Sakura"

    # omake.rpy:278
    old "Good guy"
    new "Buen chico"

    # omake.rpy:278
    old "Tell Sakura the truth about the yukata"
    new "Dile a Sakura la verdad sobre el yukata"

    # omake.rpy:278
    old "It's all about the Pentiums, baby"
    new "Jugando en un Pentium"

    # omake.rpy:278
    old "Choose to game at the beginning of the game."
    new "Elige jugar al principio del juego"

    # omake.rpy:278
    old "She got me!"
    new "¡Ella me atrapó!"

    # omake.rpy:278
    old "Help Rika with the festival"
    new "Ayuda a Rika con el festival"

    # omake.rpy:278
    old "Grope!"
    new "¡Grope!"

    # omake.rpy:278
    old "Grope Rika (accidentally)"
    new "A tientas Rika (accidentalmente)"

    # omake.rpy:278
    old "A good prank"
    new "Una buena broma"

    # omake.rpy:278
    old "Prank your friends with Nanami"
    new "Haz bromas a tus amigos con Nanami"

    # omake.rpy:278
    old "I'm not little!"
    new "¡No soy pequeña!"

    # omake.rpy:278
    old "Help Sakura tell the truth to Nanami"
    new "Ayuda a Sakura a decirle la verdad a Nanami"

    # omake.rpy:278
    old "Big change of life"
    new "Gran cambio de vida"

    # omake.rpy:278
    old "Complete Sakura's route"
    new "Completa la ruta de Sakura"

    # omake.rpy:278
    old "City Rat"
    new "Rata de la ciudad"

    # omake.rpy:278
    old "Complete Rika's route"
    new "Completa la ruta de Rika"

    # omake.rpy:278
    old "That new girl"
    new "Esa nueva chica"

    # omake.rpy:278
    old "Complete Nanami's route"
    new "Completa la ruta de Nanami"

    # omake.rpy:278
    old "Knee-Deep into the 34D"
    new "Knee-Deep into the 34D"

    # omake.rpy:278
    old "Find the secret code in the game"
    new "Encuentra el código secreto en el juego"
