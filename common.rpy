﻿# TODO: Translation updated at 2017-11-05 17:34

translate spanish strings:

    # 00action_file.rpy:26
    old "{#weekday}Monday"
    new "{#weekday}Lunes"

    # 00action_file.rpy:26
    old "{#weekday}Tuesday"
    new "{#weekday}Martes"

    # 00action_file.rpy:26
    old "{#weekday}Wednesday"
    new "{#weekday}Miércoles"

    # 00action_file.rpy:26
    old "{#weekday}Thursday"
    new "{#weekday}Jueves"

    # 00action_file.rpy:26
    old "{#weekday}Friday"
    new "{#weekday}Viernes"

    # 00action_file.rpy:26
    old "{#weekday}Saturday"
    new "{#weekday}Sábado"

    # 00action_file.rpy:26
    old "{#weekday}Sunday"
    new "{#weekday}Domingo"

    # 00action_file.rpy:37
    old "{#weekday_short}Mon"
    new "{#weekday_short}Lun"

    # 00action_file.rpy:37
    old "{#weekday_short}Tue"
    new "{#weekday_short}Mar"

    # 00action_file.rpy:37
    old "{#weekday_short}Wed"
    new "{#weekday_short}Mié"

    # 00action_file.rpy:37
    old "{#weekday_short}Thu"
    new "{#weekday_short}Jue"

    # 00action_file.rpy:37
    old "{#weekday_short}Fri"
    new "{#weekday_short}Vie"

    # 00action_file.rpy:37
    old "{#weekday_short}Sat"
    new "{#weekday_short}Sáb"

    # 00action_file.rpy:37
    old "{#weekday_short}Sun"
    new "{#weekday_short}Dom"

    # 00action_file.rpy:47
    old "{#month}January"
    new "{#month}Enero"

    # 00action_file.rpy:47
    old "{#month}February"
    new "{#month}Febrero"

    # 00action_file.rpy:47
    old "{#month}March"
    new "{#month}Marzo"

    # 00action_file.rpy:47
    old "{#month}April"
    new "{#month}Abril"

    # 00action_file.rpy:47
    old "{#month}May"
    new "{#month}Mayo"

    # 00action_file.rpy:47
    old "{#month}June"
    new "{#month}Junio"

    # 00action_file.rpy:47
    old "{#month}July"
    new "{#month}Julio"

    # 00action_file.rpy:47
    old "{#month}August"
    new "{#month}Agosto"

    # 00action_file.rpy:47
    old "{#month}September"
    new "{#month}Septiembre"

    # 00action_file.rpy:47
    old "{#month}October"
    new "{#month}Octubre"

    # 00action_file.rpy:47
    old "{#month}November"
    new "{#month}Noviembre"

    # 00action_file.rpy:47
    old "{#month}December"
    new "{#month}Diciembre"

    # 00action_file.rpy:63
    old "{#month_short}Jan"
    new "{#month_short}Ene"

    # 00action_file.rpy:63
    old "{#month_short}Feb"
    new "{#month_short}Feb"

    # 00action_file.rpy:63
    old "{#month_short}Mar"
    new "{#month_short}Mar"

    # 00action_file.rpy:63
    old "{#month_short}Apr"
    new "{#month_short}Abr"

    # 00action_file.rpy:63
    old "{#month_short}May"
    new "{#month_short}May"

    # 00action_file.rpy:63
    old "{#month_short}Jun"
    new "{#month_short}Jun"

    # 00action_file.rpy:63
    old "{#month_short}Jul"
    new "{#month_short}Jul"

    # 00action_file.rpy:63
    old "{#month_short}Aug"
    new "{#month_short}Ago"

    # 00action_file.rpy:63
    old "{#month_short}Sep"
    new "{#month_short}Sep"

    # 00action_file.rpy:63
    old "{#month_short}Oct"
    new "{#month_short}Oct"

    # 00action_file.rpy:63
    old "{#month_short}Nov"
    new "{#month_short}Nov"

    # 00action_file.rpy:63
    old "{#month_short}Dec"
    new "{#month_short}Dic"

    # 00action_file.rpy:235
    old "%b %d, %H:%M"
    new "%b %d, %H:%M"

    # 00action_file.rpy:825
    old "Quick save complete."
    new "Guardado rápido completado."

    # 00director.rpy:693
    old "The interactive director is not enabled here."
    new "El control interactivo no esta habilitado aquí."

    # 00director.rpy:1480
    old "Done"
    new "Hecho"

    # 00director.rpy:1488
    old "(statement)"
    new "(declaración)"

    # 00director.rpy:1489
    old "(tag)"
    new "(etiqueta)"

    # 00director.rpy:1490
    old "(attributes)"
    new "(atributos)"

    # 00director.rpy:1491
    old "(transform)"
    new "(transformar)"

    # 00director.rpy:1516
    old "(transition)"
    new "(transición)"

    # 00director.rpy:1528
    old "(channel)"
    new "(canal)"

    # 00director.rpy:1529
    old "(filename)"
    new "(nombre de archivo)"

    # 00director.rpy:1554
    old "Change"
    new "Cambio"

    # 00director.rpy:1556
    old "Add"
    new "Añadir"

    # 00director.rpy:1559
    old "Cancel"
    new "Cancelar"

    # 00director.rpy:1562
    old "Remove"
    new "Remover"

    # 00director.rpy:1595
    old "Statement:"
    new "Declaración:"

    # 00director.rpy:1616
    old "Tag:"
    new "Etiqueta:"

    # 00director.rpy:1632
    old "Attributes:"
    new "Atributos:"

    # 00director.rpy:1650
    old "Transforms:"
    new "Transformaciones:"

    # 00director.rpy:1669
    old "Behind:"
    new "Detrás:"

    # 00director.rpy:1688
    old "Transition:"
    new "Transición:"

    # 00director.rpy:1706
    old "Channel:"
    new "Canal:"

    # 00director.rpy:1724
    old "Audio Filename:"
    new "Nombre del archivo de audio:"

    # 00gui.rpy:240
    old "Are you sure?"
    new "¿Estás seguro?"

    # 00gui.rpy:241
    old "Are you sure you want to delete this save?"
    new "¿Está seguro de querer eliminar estos datos?"

    # 00gui.rpy:242
    old "Are you sure you want to overwrite your save?"
    new "¿Está seguro de querer sobreescribir estos datos?"

    # 00gui.rpy:243
    old "Loading will lose unsaved progress.\nAre you sure you want to do this?"
    new "Cargar la partida va a hacer que pierdas los datos no guardados.\n¿Está seguro de querer hacerlo?"

    # 00gui.rpy:244
    old "Are you sure you want to quit?"
    new "¿Está seguro de querer salir?"

    # 00gui.rpy:245
    old "Are you sure you want to return to the main menu?\nThis will lose unsaved progress."
    new "¿Está seguro de querer regresar al menú principal?\nEste progreso no guardado se perderá."

    # 00gui.rpy:246
    old "Are you sure you want to end the replay?"
    new "¿Está seguro de que quiere terminar la reproducción?"

    # 00gui.rpy:247
    old "Are you sure you want to begin skipping?"
    new "¿Está seguro de que quiere saltar?"

    # 00gui.rpy:248
    old "Are you sure you want to skip to the next choice?"
    new "¿Está seguro de que quiere saltar hasta la próxima decisión?"

    # 00gui.rpy:249
    old "Are you sure you want to skip unseen dialogue to the next choice?"
    new "¿Está seguro de que quiere saltar diálogos no vistos hasta la próxima decisión?"

    # 00keymap.rpy:254
    old "Failed to save screenshot as %s."
    new "Error en guardar la captura de pantalla como %s."

    # 00keymap.rpy:266
    old "Saved screenshot as %s."
    new "Captura de pantalla guardado como %s."

    # 00library.rpy:146
    old "Self-voicing disabled."
    new "Voz automática deshabilitado."

    # 00library.rpy:147
    old "Clipboard voicing enabled. "
    new "Portapapeles de voz habilitado."

    # 00library.rpy:148
    old "Self-voicing enabled. "
    new "Voz automática habilitado."

    # 00library.rpy:183
    old "Skip Mode"
    new "Modo salto"

    # 00library.rpy:266
    old "This program contains free software under a number of licenses, including the MIT License and GNU Lesser General Public License. A complete list of software, including links to full source code, can be found {a=https://www.renpy.org/l/license}here{/a}."
    new "Este programa contiene software libre con un número de licencias, incluyendo la Licencia MIT y la Licencia menor para público general GNU. Una lista completa del software, incluyendo enlaces a las fuentes completas de código, puede ser encontrado {a=https://www.renpy.org/l/license}aquí{/a}."

    # 00preferences.rpy:442
    old "Clipboard voicing enabled. Press 'shift+C' to disable."
    new "Portapapeles de voz habilitados. Presione 'shift+C' para deshabilitar."

    # 00preferences.rpy:444
    old "Self-voicing would say \"[renpy.display.tts.last]\". Press 'alt+shift+V' to disable."
    new "La voz automática dirá \"[renpy.display.tts.last]\". Presione 'alt+shift+V' para deshabilitar."

    # 00preferences.rpy:446
    old "Self-voicing enabled. Press 'v' to disable."
    new "Voz automática habilitada. Presione 'v' para deshabilitar."

    # 00iap.rpy:217
    old "Contacting App Store\nPlease Wait..."
    new "Contactando la tienda de App\n Por favor espere..."

    # 00updater.rpy:373
    old "The Ren'Py Updater is not supported on mobile devices."
    new "El actualizador de Ren'Py no es soportado en aparatos móviles."

    # 00updater.rpy:492
    old "An error is being simulated."
    new "Un error esta siendo simulado."

    # 00updater.rpy:668
    old "Either this project does not support updating, or the update status file was deleted."
    new "Puede ser que este proyecto no soporta la actualización, o el archivo del estado de actualización fue borrado."

    # 00updater.rpy:682
    old "This account does not have permission to perform an update."
    new "Esta cuenta no tiene permiso para realizar una actualización."

    # 00updater.rpy:685
    old "This account does not have permission to write the update log."
    new "Esta cuenta no tiene permiso para escribir el registro de la actualización."

    # 00updater.rpy:710
    old "Could not verify update signature."
    new "No se pudo verificar la firma de la actualización."

    # 00updater.rpy:981
    old "The update file was not downloaded."
    new "El archivo de la actualización no fue descargada."

    # 00updater.rpy:999
    old "The update file does not have the correct digest - it may have been corrupted."
    new "El archivo de la actualización no tiene el correcto proceso - puede ser que haya sido corrompido."

    # 00updater.rpy:1055
    old "While unpacking {}, unknown type {}."
    new "Mientras desempacando {}, tipo desconocido {}."

    # 00updater.rpy:1402
    old "Updater"
    new "Actualizador"

    # 00updater.rpy:1409
    old "An error has occured:"
    new "Un error ha ocurrido."

    # 00updater.rpy:1411
    old "Checking for updates."
    new "Comprobando por actualizaciones."

    # 00updater.rpy:1413
    old "This program is up to date."
    new "Este programa está para actualizar."

    # 00updater.rpy:1415
    old "[u.version] is available. Do you want to install it?"
    new "[u.version] está disponible. ¿Quiere instalarlo?"

    # 00updater.rpy:1417
    old "Preparing to download the updates."
    new "Preparando para descargar las actualizaciones."

    # 00updater.rpy:1419
    old "Downloading the updates."
    new "Descargando las actualizaciones."

    # 00updater.rpy:1421
    old "Unpacking the updates."
    new "Desempacando las actualizaciones."

    # 00updater.rpy:1423
    old "Finishing up."
    new "Terminando."

    # 00updater.rpy:1425
    old "The updates have been installed. The program will restart."
    new "Las actualizaciones han sido instaladas. El programa se va a reiniciar."

    # 00updater.rpy:1427
    old "The updates have been installed."
    new "Las actualizaciones han sido instaladas."

    # 00updater.rpy:1429
    old "The updates were cancelled."
    new "Las actualizaciones fueron canceladas."

    # 00updater.rpy:1444
    old "Proceed"
    new "Proceder"

    # 00gallery.rpy:573
    old "Image [index] of [count] locked."
    new "Imagen [index] de [count] bloqueado."

    # 00gallery.rpy:593
    old "prev"
    new "previo"

    # 00gallery.rpy:594
    old "next"
    new "siguiente"

    # 00gallery.rpy:595
    old "slideshow"
    new "presentación"

    # 00gallery.rpy:596
    old "return"
    new "regresar"

    # 00gltest.rpy:64
    old "Graphics Acceleration"
    new "Aceleración de Gráficas"

    # 00gltest.rpy:70
    old "Automatically Choose"
    new "Seleccionar Automáticamente"

    # 00gltest.rpy:75
    old "Force Angle/DirectX Renderer"
    new "Forzar Angle/DirectX Renderer"

    # 00gltest.rpy:79
    old "Force OpenGL Renderer"
    new "Forzar OpenGL Renderer"

    # 00gltest.rpy:83
    old "Force Software Renderer"
    new "Forzar Software Renderer"

    # 00gltest.rpy:89
    old "NPOT"
    new "NPOT"

    # 00gltest.rpy:93
    old "Enable"
    new "Habilitado"

    # 00gltest.rpy:124
    old "Changes will take effect the next time this program is run."
    new "Los cambios van a tener efecto la próxima vez que este programa se ejecute."

    # 00gltest.rpy:134
    old "Return"
    new "Regresar"

    # 00gltest.rpy:156
    old "Performance Warning"
    new "Advertencia de Rendimiento"

    # 00gltest.rpy:161
    old "This computer is using software rendering."
    new "Esta computadora esta usando software de procesamiento."

    # 00gltest.rpy:163
    old "This computer is not using shaders."
    new "Esta computadora no esta usando sombreadores."

    # 00gltest.rpy:165
    old "This computer is displaying graphics slowly."
    new "Esta computadora esta visualizando gráficos lentamente."

    # 00gltest.rpy:167
    old "This computer has a problem displaying graphics: [problem]."
    new "Esta computadora tiene un problema visualizando gráficos: [problem]"

    # 00gltest.rpy:172
    old "Its graphics drivers may be out of date or not operating correctly. This can lead to slow or incorrect graphics display. Updating DirectX could fix this problem."
    new "Los motores gráficos pueden estar desactualizados o no operando correctamente. Esto puede llevar a visualizar gráficos lenta o incorrectamente. Actualizar DirectX podría arreglar este problema."

    # 00gltest.rpy:174
    old "Its graphics drivers may be out of date or not operating correctly. This can lead to slow or incorrect graphics display."
    new "Los motores gráficos pueden estar desactualizados o no operando correctamente. Esto puede llevar a visualizar gráficos lenta o incorrectamente."

    # 00gltest.rpy:179
    old "Update DirectX"
    new "Actualizar DirectX"

    # 00gltest.rpy:185
    old "Continue, Show this warning again"
    new "Continuar, Mostrar esta advertencia otra vez"

    # 00gltest.rpy:189
    old "Continue, Don't show warning again"
    new "Continuar, No mostrar la advertencia otra vez"

    # 00gltest.rpy:207
    old "Updating DirectX."
    new "Actualizando DirectX."

    # 00gltest.rpy:211
    old "DirectX web setup has been started. It may start minimized in the taskbar. Please follow the prompts to install DirectX."
    new "la configuración web de DirectX ha sido iniciado. Puede ser minimizado en la barra de tareas. Por favor sigue las instrucciones para instalar DirectX."

    # 00gltest.rpy:215
    old "{b}Note:{/b} Microsoft's DirectX web setup program will, by default, install the Bing toolbar. If you do not want this toolbar, uncheck the appropriate box."
    new "{b}Nota:{/b} El programa de configuración web DirectX de Microsoft va a, por defecto, instalar la barra de herramienta Bing. Si usted no quiere esta barra de herramiento, desmarque la caja apropiada."

    # 00gltest.rpy:219
    old "When setup finishes, please click below to restart this program."
    new "Cuando la configuración termine, por favor haga click abajo para reiniciar este programa."

    # 00gltest.rpy:221
    old "Restart"
    new "Reiniciar"

    # 00gamepad.rpy:32
    old "Select Gamepad to Calibrate"
    new "Selecciona el control de mando para calibrar"

    # 00gamepad.rpy:35
    old "No Gamepads Available"
    new "No hay controles de mando disponibles."

    # 00gamepad.rpy:54
    old "Calibrating [name] ([i]/[total])"
    new "Calibrando [name] ([i]/[total])"

    # 00gamepad.rpy:58
    old "Press or move the [control!r] [kind]."
    new "Presiona o mueve el [control!r] [kind]."

    # 00gamepad.rpy:66
    old "Skip (A)"
    new "Saltar (A)"

    # 00gamepad.rpy:69
    old "Back (B)"
    new "Regresar (B)"

    # _errorhandling.rpym:523
    old "Open"
    new "Abrir"

    # _errorhandling.rpym:525
    old "Opens the traceback.txt file in a text editor."
    new "Abriendo el archivo traceback.txt en un editor de texto."

    # _errorhandling.rpym:527
    old "Copy"
    new "Copiar"

    # _errorhandling.rpym:529
    old "Copies the traceback.txt file to the clipboard."
    new "Copiando el archivo traceback.txt al portapapeles."

    # _errorhandling.rpym:556
    old "An exception has occurred."
    new "Una excepción ha ocurrido."

    # _errorhandling.rpym:576
    old "Rollback"
    new "Reversión."

    # _errorhandling.rpym:578
    old "Attempts a roll back to a prior time, allowing you to save or choose a different choice."
    new "Intentando una reversión a un tiempo previo, permitiendo que usted guarde o elija una diferente decisión."

    # _errorhandling.rpym:581
    old "Ignore"
    new "Ignorar"

    # _errorhandling.rpym:585
    old "Ignores the exception, allowing you to continue."
    new "Ignorar la excepción, permitiendote continuar.."

    # _errorhandling.rpym:587
    old "Ignores the exception, allowing you to continue. This often leads to additional errors."
    new "Ignorar la excepción, permitiendote continuar. Esto a menudo genera errores adicionales."

    # _errorhandling.rpym:591
    old "Reload"
    new "Recargar"

    # _errorhandling.rpym:593
    old "Reloads the game from disk, saving and restoring game state if possible."
    new "Recargar el juego desde el disco, guardando y restaurando el estado del juego si fuera posible."

    # _errorhandling.rpym:596
    old "Console"
    new "Consola"

    # _errorhandling.rpym:598
    old "Opens a console to allow debugging the problem."
    new "Abriendo una consola para permitir la depuración del problema."

    # _errorhandling.rpym:608
    old "Quits the game."
    new "Salir del juego."

    # _errorhandling.rpym:632
    old "Parsing the script failed."
    new "El análisis sintáctico del script falló."

    # _errorhandling.rpym:658
    old "Opens the errors.txt file in a text editor."
    new "Abriendo el archivo errors.txt en un editor de texto."

    # _errorhandling.rpym:662
    old "Copies the errors.txt file to the clipboard."
    new "Copiando el archivo de errors.txt al portapapeles."

# TODO: Translation updated at 2018-04-07 20:30

translate spanish strings:

    # 00action_file.rpy:344
    old "Save slot %s: [text]"
    new "Save slot %s: [text]"

    # 00action_file.rpy:417
    old "Load slot %s: [text]"
    new "Load slot %s: [text]"

    # 00action_file.rpy:459
    old "Delete slot [text]"
    new "Delete slot [text]"

    # 00action_file.rpy:539
    old "File page auto"
    new "File page auto"

    # 00action_file.rpy:541
    old "File page quick"
    new "File page quick"

    # 00action_file.rpy:543
    old "File page [text]"
    new "File page [text]"

    # 00action_file.rpy:733
    old "Next file page."
    new "Next file page."

    # 00action_file.rpy:797
    old "Previous file page."
    new "Previous file page."

    # 00action_file.rpy:876
    old "Quick save."
    new "Quick save."

    # 00action_file.rpy:895
    old "Quick load."
    new "Quick load."

    # 00action_other.rpy:344
    old "Language [text]"
    new "Language [text]"

    # 00library.rpy:150
    old "bar"
    new "bar"

    # 00library.rpy:151
    old "selected"
    new "selected"

    # 00library.rpy:152
    old "viewport"
    new "viewport"

    # 00library.rpy:153
    old "horizontal scroll"
    new "horizontal scroll"

    # 00library.rpy:154
    old "vertical scroll"
    new "vertical scroll"

    # 00library.rpy:155
    old "activate"
    new "activate"

    # 00library.rpy:156
    old "deactivate"
    new "deactivate"

    # 00library.rpy:157
    old "increase"
    new "increase"

    # 00library.rpy:158
    old "decrease"
    new "decrease"

    # 00preferences.rpy:207
    old "display"
    new "display"

    # 00preferences.rpy:219
    old "transitions"
    new "transitions"

    # 00preferences.rpy:228
    old "skip transitions"
    new "skip transitions"

    # 00preferences.rpy:230
    old "video sprites"
    new "video sprites"

    # 00preferences.rpy:239
    old "show empty window"
    new "show empty window"

    # 00preferences.rpy:248
    old "text speed"
    new "text speed"

    # 00preferences.rpy:256
    old "joystick"
    new "joystick"

    # 00preferences.rpy:256
    old "joystick..."
    new "joystick..."

    # 00preferences.rpy:263
    old "skip"
    new "skip"

    # 00preferences.rpy:266
    old "skip unseen [text]"
    new "skip unseen [text]"

    # 00preferences.rpy:271
    old "skip unseen text"
    new "skip unseen text"

    # 00preferences.rpy:273
    old "begin skipping"
    new "begin skipping"

    # 00preferences.rpy:277
    old "after choices"
    new "after choices"

    # 00preferences.rpy:284
    old "skip after choices"
    new "skip after choices"

    # 00preferences.rpy:286
    old "auto-forward time"
    new "auto-forward time"

    # 00preferences.rpy:300
    old "auto-forward"
    new "auto-forward"

    # 00preferences.rpy:310
    old "auto-forward after click"
    new "auto-forward after click"

    # 00preferences.rpy:319
    old "automatic move"
    new "automatic move"

    # 00preferences.rpy:328
    old "wait for voice"
    new "wait for voice"

    # 00preferences.rpy:337
    old "voice sustain"
    new "voice sustain"

    # 00preferences.rpy:346
    old "self voicing"
    new "self voicing"

    # 00preferences.rpy:355
    old "clipboard voicing"
    new "clipboard voicing"

    # 00preferences.rpy:364
    old "debug voicing"
    new "debug voicing"

    # 00preferences.rpy:373
    old "emphasize audio"
    new "emphasize audio"

    # 00preferences.rpy:382
    old "rollback side"
    new "rollback side"

    # 00preferences.rpy:392
    old "gl powersave"
    new "gl powersave"

    # 00preferences.rpy:398
    old "gl framerate"
    new "gl framerate"

    # 00preferences.rpy:401
    old "gl tearing"
    new "gl tearing"

    # 00preferences.rpy:413
    old "music volume"
    new "music volume"

    # 00preferences.rpy:414
    old "sound volume"
    new "sound volume"

    # 00preferences.rpy:415
    old "voice volume"
    new "voice volume"

    # 00preferences.rpy:416
    old "mute music"
    new "mute music"

    # 00preferences.rpy:417
    old "mute sound"
    new "mute sound"

    # 00preferences.rpy:418
    old "mute voice"
    new "mute voice"

    # 00preferences.rpy:419
    old "mute all"
    new "mute all"

    # _compat\gamemenu.rpym:198
    old "Empty Slot."
    new "Empty Slot."

    # _compat\gamemenu.rpym:355
    old "Previous"
    new "Previous"

    # _compat\gamemenu.rpym:362
    old "Next"
    new "Next"

    # _compat\preferences.rpym:428
    old "Joystick Mapping"
    new "Joystick Mapping"

    # _developer\developer.rpym:38
    old "Developer Menu"
    new "Developer Menu"

    # _developer\developer.rpym:43
    old "Interactive Director (D)"
    new "Interactive Director (D)"

    # _developer\developer.rpym:45
    old "Reload Game (Shift+R)"
    new "Reload Game (Shift+R)"

    # _developer\developer.rpym:47
    old "Console (Shift+O)"
    new "Console (Shift+O)"

    # _developer\developer.rpym:49
    old "Variable Viewer"
    new "Variable Viewer"

    # _developer\developer.rpym:51
    old "Image Location Picker"
    new "Image Location Picker"

    # _developer\developer.rpym:53
    old "Filename List"
    new "Filename List"

    # _developer\developer.rpym:57
    old "Show Image Load Log (F4)"
    new "Show Image Load Log (F4)"

    # _developer\developer.rpym:60
    old "Hide Image Load Log (F4)"
    new "Hide Image Load Log (F4)"

    # _developer\developer.rpym:95
    old "Nothing to inspect."
    new "Nothing to inspect."

    # _developer\developer.rpym:223
    old "Return to the developer menu"
    new "Return to the developer menu"

    # _developer\developer.rpym:383
    old "Rectangle: %r"
    new "Rectangle: %r"

    # _developer\developer.rpym:388
    old "Mouse position: %r"
    new "Mouse position: %r"

    # _developer\developer.rpym:393
    old "Right-click or escape to quit."
    new "Right-click or escape to quit."

    # _developer\developer.rpym:425
    old "Rectangle copied to clipboard."
    new "Rectangle copied to clipboard."

    # _developer\developer.rpym:428
    old "Position copied to clipboard."
    new "Position copied to clipboard."

    # _developer\developer.rpym:447
    old "Type to filter: "
    new "Type to filter: "

    # _developer\developer.rpym:575
    old "Textures: [tex_count] ([tex_size_mb:.1f] MB)"
    new "Textures: [tex_count] ([tex_size_mb:.1f] MB)"

    # _developer\developer.rpym:579
    old "Image cache: [cache_pct:.1f]% ([cache_size_mb:.1f] MB)"
    new "Image cache: [cache_pct:.1f]% ([cache_size_mb:.1f] MB)"

    # _developer\developer.rpym:589
    old "✔ "
    new "✔ "

    # _developer\developer.rpym:592
    old "✘ "
    new "✘ "

    # _developer\developer.rpym:597
    old "\n{color=#cfc}✔ predicted image (good){/color}\n{color=#fcc}✘ unpredicted image (bad){/color}\n{color=#fff}Drag to move.{/color}"
    new "\n{color=#cfc}✔ predicted image (good){/color}\n{color=#fcc}✘ unpredicted image (bad){/color}\n{color=#fff}Drag to move.{/color}"

    # _developer\inspector.rpym:38
    old "Displayable Inspector"
    new "Displayable Inspector"

    # _developer\inspector.rpym:61
    old "Size"
    new "Size"

    # _developer\inspector.rpym:65
    old "Style"
    new "Style"

    # _developer\inspector.rpym:71
    old "Location"
    new "Location"

    # _developer\inspector.rpym:122
    old "Inspecting Styles of [displayable_name!q]"
    new "Inspecting Styles of [displayable_name!q]"

    # _developer\inspector.rpym:139
    old "displayable:"
    new "displayable:"

    # _developer\inspector.rpym:145
    old "        (no properties affect the displayable)"
    new "        (no properties affect the displayable)"

    # _developer\inspector.rpym:147
    old "        (default properties omitted)"
    new "        (default properties omitted)"

    # _developer\inspector.rpym:185
    old "<repr() failed>"
    new "<repr() failed>"

    # _layout\classic_load_save.rpym:170
    old "a"
    new "a"

    # _layout\classic_load_save.rpym:179
    old "q"
    new "q"

    # 00gltest.rpy:70
    old "Renderer"
    new "Renderer"

    # 00gltest.rpy:131
    old "Powersave"
    new "Powersave"

    # 00gltest.rpy:145
    old "Framerate"
    new "Framerate"

    # 00gltest.rpy:149
    old "Screen"
    new "Screen"

    # 00gltest.rpy:153
    old "60"
    new "60"

    # 00gltest.rpy:157
    old "30"
    new "30"

    # 00gltest.rpy:163
    old "Tearing"
    new "Tearing"

# TODO: Translation updated at 2019-04-23 10:19

translate spanish strings:

    # 00accessibility.rpy:76
    old "Font Override"
    new "Font Override"

    # 00accessibility.rpy:80
    old "Default"
    new "Default"

    # 00accessibility.rpy:84
    old "DejaVu Sans"
    new "DejaVu Sans"

    # 00accessibility.rpy:88
    old "Opendyslexic"
    new "Opendyslexic"

    # 00accessibility.rpy:94
    old "Text Size Scaling"
    new "Text Size Scaling"

    # 00accessibility.rpy:100
    old "Reset"
    new "Reset"

    # 00accessibility.rpy:105
    old "Line Spacing Scaling"
    new "Line Spacing Scaling"

    # 00accessibility.rpy:117
    old "Self-Voicing"
    new "Self-Voicing"

    # 00accessibility.rpy:121
    old "Off"
    new "Off"

    # 00accessibility.rpy:125
    old "Text-to-speech"
    new "Text-to-speech"

    # 00accessibility.rpy:129
    old "Clipboard"
    new "Clipboard"

    # 00accessibility.rpy:133
    old "Debug"
    new "Debug"

    # 00director.rpy:1481
    old "⬆"
    new "⬆"

    # 00director.rpy:1487
    old "⬇"
    new "⬇"

    # 00preferences.rpy:430
    old "font transform"
    new "font transform"

    # 00preferences.rpy:433
    old "font size"
    new "font size"

    # 00preferences.rpy:441
    old "font line spacing"
    new "font line spacing"

    # _developer\developer.rpym:63
    old "Image Attributes"
    new "Image Attributes"

    # _developer\developer.rpym:90
    old "[name] [attributes] (hidden)"
    new "[name] [attributes] (hidden)"

    # _developer\developer.rpym:94
    old "[name] [attributes]"
    new "[name] [attributes]"

    # _developer\developer.rpym:154
    old "Hide deleted"
    new "Hide deleted"

    # _developer\developer.rpym:154
    old "Show deleted"
    new "Show deleted"

    # _errorhandling.rpym:542
    old "Copy BBCode"
    new "Copy BBCode"

    # _errorhandling.rpym:544
    old "Copies the traceback.txt file to the clipboard as BBcode for forums like https://lemmasoft.renai.us/."
    new "Copies the traceback.txt file to the clipboard as BBcode for forums like https://lemmasoft.renai.us/."

    # _errorhandling.rpym:546
    old "Copy Markdown"
    new "Copy Markdown"

    # _errorhandling.rpym:548
    old "Copies the traceback.txt file to the clipboard as Markdown for Discord."
    new "Copies the traceback.txt file to the clipboard as Markdown for Discord."

    # _errorhandling.rpym:683
    old "Copies the errors.txt file to the clipboard as BBcode for forums like https://lemmasoft.renai.us/."
    new "Copies the errors.txt file to the clipboard as BBcode for forums like https://lemmasoft.renai.us/."

    # _errorhandling.rpym:687
    old "Copies the errors.txt file to the clipboard as Markdown for Discord."
    new "Copies the errors.txt file to the clipboard as Markdown for Discord."

