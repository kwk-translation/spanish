﻿# TODO: Translation updated at 2017-11-05 17:34

translate spanish strings:

    # screens.rpy:253
    old "Back"
    new "Regresar"

    # screens.rpy:254
    old "History"
    new "Historia" #I think you mean Story, but I could be wrong.

    # screens.rpy:255
    old "Skip"
    new "Saltar"

    # screens.rpy:256
    old "Auto"
    new "Auto"

    # screens.rpy:257
    old "Save"
    new "Guardar"

    # screens.rpy:258
    old "Q.save"
    new "Guar. Rápido"

    # screens.rpy:259
    old "Q.load"
    new "Carg. Rápido"

    # screens.rpy:260
    old "Settings"
    new "Configuración"

    # screens.rpy:302
    old "New game"
    new "Nuevo juego"

    # screens.rpy:309
    old "Update Available!"
    new "¡Actualización Disponible!"

    # screens.rpy:320
    old "Load"
    new "Cargar"

    # screens.rpy:323
    old "Bonus"
    new "Extras"

    # screens.rpy:329
    old "End Replay"
    new "Finalizar reproducción"

    # screens.rpy:333
    old "Main menu"
    new "Menú"

    # screens.rpy:335
    old "About"
    new "Acerca de"

    # screens.rpy:337
    old "Please donate!"
    new "¡Por favor dona!"

    # screens.rpy:342
    old "Help"
    new "Ayuda"

    # screens.rpy:346
    old "Quit"
    new "Salir"

    # screens.rpy:572
    old "Version [config.version!t]\n"
    new "Versión [config.version!t]\n"

    # screens.rpy:578
    old "Ren'Py version: [renpy.version_only].\n\n[renpy.license!t]"
    new "Ren'Py versión:  [renpy.version_only].\n\n[renpy.license!t]"

    # screens.rpy:618
    old "Page {}"
    new "Página {}"

    # screens.rpy:618
    old "Automatic saves"
    new "Guardados automáticos"

    # screens.rpy:618
    old "Quick saves"
    new "Guardados rápidos"

    # screens.rpy:660
    old "{#file_time}%A, %B %d %Y, %H:%M"
    new "{#file_time}%A, %B %d %Y, %H:%M"

    # screens.rpy:660
    old "empty slot"
    new "ranura vacía"

    # screens.rpy:677
    old "<"
    new "<"

    # screens.rpy:680
    old "{#auto_page}A"
    new "{#auto_page}A"

    # screens.rpy:683
    old "{#quick_page}Q"
    new "{#quick_page}Q"

    # screens.rpy:689
    old ">"
    new ">"

    # screens.rpy:751
    old "Display"
    new "Visualización"

    # screens.rpy:752
    old "Windowed"
    new "Ventana"

    # screens.rpy:753
    old "Fullscreen"
    new "Pantalla completa"

    # screens.rpy:757
    old "Rollback Side"
    new "Reversión lateral"

    # screens.rpy:758
    old "Disable"
    new "Deshabilitar"

    # screens.rpy:759
    old "Left"
    new "Izquierda"

    # screens.rpy:760
    old "Right"
    new "Derecha"

    # screens.rpy:765
    old "Unseen Text"
    new "Texto no visto"

    # screens.rpy:766
    old "After choices"
    new "Después decisiones"

    # screens.rpy:767
    old "Transitions"
    new "Transiciones"

    # screens.rpy:782
    old "Text speed"
    new "Velocidad de texto"

    # screens.rpy:786
    old "Auto forward"
    new "Auto avance"

    # screens.rpy:793
    old "Music volume"
    new "Volumen de la Música"

    # screens.rpy:800
    old "Sound volume"
    new "Volumen del sonido"

    # screens.rpy:806
    old "Test"
    new "Prueba"

    # screens.rpy:810
    old "Voice volume"
    new "Volumen de voz"

    # screens.rpy:821
    old "Mute All"
    new "Silenciar todo"

    # screens.rpy:832
    old "Language"
    new "Idioma"

    # screens.rpy:958
    old "The dialogue history is empty."
    new "El diálogo de la historia está vacio."

    # screens.rpy:1023
    old "Keyboard"
    new "Teclado"

    # screens.rpy:1024
    old "Mouse"
    new "Ratón"

    # screens.rpy:1027
    old "Gamepad"
    new "Control de mando"

    # screens.rpy:1040
    old "Enter"
    new "Entrar"

    # screens.rpy:1041
    old "Advances dialogue and activates the interface."
    new "Avances del diálogo y activación del interfaz."

    # screens.rpy:1044
    old "Space"
    new "Espacio"

    # screens.rpy:1045
    old "Advances dialogue without selecting choices."
    new "Avances del diálogo sin seleccionar decisiones."

    # screens.rpy:1048
    old "Arrow Keys"
    new "Teclas de Flecha"

    # screens.rpy:1049
    old "Navigate the interface."
    new "Navegando el interfaz"

    # screens.rpy:1052
    old "Escape"
    new "Escapar"

    # screens.rpy:1053
    old "Accesses the game menu."
    new "Accediendo al menú del juego."

    # screens.rpy:1056
    old "Ctrl"
    new "Ctrl"

    # screens.rpy:1057
    old "Skips dialogue while held down."
    new "Saltar los diálogos mientras se mantenga presionado."

    # screens.rpy:1060
    old "Tab"
    new "Tab"

    # screens.rpy:1061
    old "Toggles dialogue skipping."
    new "Saltándose diálogos conmutados "

    # screens.rpy:1064
    old "Page Up"
    new "Re. pág" # or Re. pág

    # screens.rpy:1065
    old "Rolls back to earlier dialogue."
    new "Retroceder al diálogo previo."

    # screens.rpy:1068
    old "Page Down"
    new "Av. pág" # or Av. pág

    # screens.rpy:1069
    old "Rolls forward to later dialogue."
    new "Avanzar al diálogo posterior"

    # screens.rpy:1073
    old "Hides the user interface."
    new "Esconder el interfaz de usuario."

    # screens.rpy:1077
    old "Takes a screenshot."
    new "Tomar una captura de pantalla."

    # screens.rpy:1081
    old "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."
    new "Alternar apoyo {a=https://www.renpy.org/l/voicing}self-voicing{/a}."

    # screens.rpy:1087
    old "Left Click"
    new "Click Izquierdo"

    # screens.rpy:1091
    old "Middle Click"
    new "Click del medio"

    # screens.rpy:1095
    old "Right Click"
    new "Click Derecho"

    # screens.rpy:1099
    old "Mouse Wheel Up\nClick Rollback Side"
    new "Rueda del ratón hacia arriba\nClick Reversión lateral"

    # screens.rpy:1103
    old "Mouse Wheel Down"
    new "Rueda del ratón hacia abajo"

    # screens.rpy:1110
    old "Right Trigger\nA/Bottom Button"
    new "Gatillo Derecho\nA/Botón inferior"

    # screens.rpy:1114
    old "Left Trigger\nLeft Shoulder"
    new "Gatillo Izquierdo\nHombro Izquierdo"

    # screens.rpy:1118
    old "Right Shoulder"
    new "Hombro Derecho"

    # screens.rpy:1122
    old "D-Pad, Sticks"
    new "Almohadilla-D, palancas"

    # screens.rpy:1126
    old "Start, Guide"
    new "Iniciar, guía"

    # screens.rpy:1130
    old "Y/Top Button"
    new "Y/Botón Superior"

    # screens.rpy:1133
    old "Calibrate"
    new "Calibrar"

    # screens.rpy:1198
    old "Yes"
    new "Si"

    # screens.rpy:1199
    old "No"
    new "No"

    # screens.rpy:1245
    old "Skipping"
    new "Saltando"

    # screens.rpy:1466
    old "Menu"
    new "Menú"

    # screens.rpy:1522
    old "Picture gallery"
    new "Gallería de imágenes"

    # screens.rpy:1523
    old "Music player"
    new "Reproductor de Música"

    # screens.rpy:1543
    old "Musics and pictures"
    new "Música e imágenes"

    # screens.rpy:1508
    old "Bonus chapters"
    new "Capítulos extras"

    # screens.rpy:1511
    old "1 - A casual day at the club"
    new "1 - Un día casual en el club"

    # screens.rpy:1516
    old "2 - Questioning sexuality (Sakura's route)"
    new "2 - Cuestionando la sexualidad (Ruta de Sakura)"

    # screens.rpy:1521
    old "3 - Headline news"
    new "3 - Encabezados de las noticias"

    # screens.rpy:1526
    old "4a - A picnic at the summer (Sakura's route)"
    new "4a - Un día de campo en el Verano (Ruta de Sakura)"

    # screens.rpy:1531
    old "4b - A picnic at the summer (Rika's route)"
    new "4b - Un día de campo en el Verano (Ruta de Rika)"

    # screens.rpy:1536
    old "4c - A picnic at the summer (Nanami's route)"
    new "4c - Un día de campo en el Verano (Ruta de Nanami)"

    # char profiles
    old "Characters profiles"
    new "Perfiles de los personajes"

    # screens.rpy:1536
    old "Opening song lyrics"
    new "Letras de canciones de apertura"
# TODO: Translation updated at 2019-04-23 10:19

translate spanish strings:

    # screens.rpy:1611
    old "Max le Fou - Taichi's Theme"
    new "Max le Fou - Tema de taichi"

    # screens.rpy:1613
    old "Max le Fou - Sakura's Waltz"
    new "Max le Fou - Vals de sakura"

    # screens.rpy:1615
    old "Max le Fou - Rika's theme"
    new "Max le Fou - Tema de Rika"

    # screens.rpy:1617
    old "Max le Fou - Of Bytes and Sanshin"
    new "Max le Fou - De Bytes y Sanshin"

    # screens.rpy:1619
    old "Max le Fou - Time for School"
    new "Max le Fou - Tiempo para la escuela"

    # screens.rpy:1621
    old "Max le Fou - Sakura's secret"
    new "Max le Fou - El secreto de Sakura"

    # screens.rpy:1623
    old "Max le Fou - I think I'm in love"
    new "Max le Fou - Creo que estoy enamorado"

    # screens.rpy:1625
    old "Max le Fou - Darkness of the Village"
    new "Max le Fou - Oscuridad de la aldea"

    # screens.rpy:1627
    old "Max le Fou - Love always win"
    new "Max le Fou - El amor siempre gana"

    # screens.rpy:1629
    old "Max le Fou - Ondo"
    new "Max le Fou - Ondo"

    # screens.rpy:1631
    old "Max le Fou - Happily Ever After"
    new "Max le Fou - Feliz para siempre"

    # screens.rpy:1633
    old "J.S. Bach - Air Orchestral suite #3"
    new "J.S. Bach - Suite # 3 de Air Orchestral"

    # screens.rpy:1635
    old "Mew Nekohime - TAKE MY HEART (TV Size)"
    new "Mew Nekohime - TAKE MY HEART (TV Size)"
# TODO: Translation updated at 2019-04-27 10:49

translate spanish strings:

    # screens.rpy:1552
    old "Achievements"
    new "Logros"
