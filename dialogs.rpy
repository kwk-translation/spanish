﻿# TODO: Translation updated at 2018-03-19 11:55

# game/dialogs.rpy:3
translate spanish scene1_acf2102c:

    # centered "{size=+12}Summer of the 9th year of Heisei era\n(1997){/size}"
    centered "{size=+12}Verano del 9no año de la era Heisei\n(1997){/size}"

# game/dialogs.rpy:6
translate spanish scene1_dd8c4dd5:

    # "Life has a way of changing. Often drastically without any warnings whatsoever."
    "La vida tiene sus cambios. A menudo, drasticamente sin ningún aviso."

# game/dialogs.rpy:7
translate spanish scene1_0497b746:

    # "At first, a day may seem like any other."
    "A primera vista, un dia puede verse como cualquier otro."

# game/dialogs.rpy:8
translate spanish scene1_1ab3b689:

    # "But by tomorrow, it can all change...{p}from where you live, to who you are as a person."
    "Pero mañana, todo puede cambiar...{p}No solo dónde vives, sino también quién eres como persona."

# game/dialogs.rpy:9
translate spanish scene1_522a76b6:

    # "Recently, I've experienced such a large change."
    "Recientemente he experimentado ese gran cambio."

# game/dialogs.rpy:10
translate spanish scene1_011ea937:

    # "My dad wanted to start his own grocery shop. So my parents and I moved to a small village near Osaka."
    "Mi papá quería comenzar su propia tienda de abarrotes. Así que mis padres y yo nos mudamos a un pequeño pueblo cerca de Osaka."

# game/dialogs.rpy:11
translate spanish scene1_2b371705:

    # "I thought I would have a hard time readjusting."
    "He tenido un tiempo difícil para reajustarme."

# game/dialogs.rpy:12
translate spanish scene1_a26a5f2b:

    # "The move from Tokyo to a small lost village, far from a big city with shops and fast-food restaurants..."
    "La mudanza de Tokyo a un pequeño pueblo perdido, lejos de la gran ciudad con tiendas y restaurantes de comida rápida..."

# game/dialogs.rpy:13
translate spanish scene1_fa8a1f18:

    # "Being a 18-year-old student, it would be difficult. I was used to the excitement of the big city."
    "Es duro para un estudiante de 18 años de edad, como yo. Estaba acostumbrado a la emoción de la gran ciudad."

# game/dialogs.rpy:14
translate spanish scene1_8d6f1c40:

    # "However, even before my first day of school, my life would change."
    "Sin embargo, mi vida pronto cambio... Antes de mi primer día de clases."

# game/dialogs.rpy:19
translate spanish scene1_d7a7c4a2:

    # centered "{size=+35}CHAPTER 1\nWhere everything started{fast}{/size}"
    centered "{size=+35}CAPÍTULO 1\nDonde todo comenzo{fast}{/size}"

# game/dialogs.rpy:26
translate spanish scene1_5bb0286e:

    # "It's sunday evening, after dinner."
    "Es un Domingo en la noche, después de la cena."

# game/dialogs.rpy:27
translate spanish scene1_a651e12e:

    # "My stuff is all unpacked and placed in my new room."
    "Mis cosas estan todas desempacadas y colocadas en mi nuevo cuarto."

# game/dialogs.rpy:28
translate spanish scene1_54a8b155:

    # "I love this new look!"
    "¡Amo este nueva apariencia!"

# game/dialogs.rpy:29
translate spanish scene1_8bb96761:

    # "It sure is smaller than my previous bedroom, but it's not like I needed that much space."
    "Es por seguro que es más pequeño que mi cuarto anterior, pero no es como si necesite mucho espacio."

# game/dialogs.rpy:30
translate spanish scene1_acf53d85:

    # "As long as I have room to sleep, play games, watch anime, and geek out, it's all good."
    "Mientras que tenga un cuarto para dormir, jugar, ver anime y estar de ocio, todo esta bien."

# game/dialogs.rpy:31
translate spanish scene1_7d722416:

    # "I'm not sure what I should do before going to bed."
    "No estoy seguro de qué es lo que debería de hacer antes de ir a la cama."

# game/dialogs.rpy:32
translate spanish scene1_b2b74adf:

    # "I could study a bit, so I don't look too dumb at school tomorrow..."
    "Tal vez debería de estudiar un poco, y así no me vería tan tonto mañana en la escuela..."

# game/dialogs.rpy:33
translate spanish scene1_ad0d9f75:

    # "But I also want to rest and have a bit of fun..."
    "Pero también quiero descansar y tener un poco de diversión..."

# game/dialogs.rpy:48
translate spanish scene1_e05b5690:

    # "I should play a short game, or I'll stay up too long. So I decide to play some {i}Tetranet online{/i}."
    "No debo de jugar por mucho tiempo o estaré despierto hasta muy tarde. Así que decidí jugar algo de {i}Tetranet online{/i}."

# game/dialogs.rpy:49
translate spanish scene1_7170a1ff:

    # "It's a puzzle game with blocks. The online version lets you attack your opponents and flood them with their own blocks."
    "Es un juego de rompecabezas con bloques. La versión en línea te deja lanzar ataques a tus oponentes e inundarlos con sus bloques."

# game/dialogs.rpy:50
translate spanish scene1_8dd01706:

    # "After a few minutes, I found myself in a game room. One of the players named 'NAMINAMI' had two stars in front of their name."
    "Después de 10 minutos jugando, estuve depierto, enfrentándome a otro juegador con el nombre de \"NAMINAMI\" con dos estrellas enfrente de su nombre."

# game/dialogs.rpy:51
translate spanish scene1_962ec706:

    # "Two stars meant that they had won hundreds of games online. Since three stars are for the game developers, I guess that means I'm up against a pro of this game."
    "Esas dos estrellas significa que nunca ha sido derrotado después de 100 juegos en línea."
    "Ya que las 3 estrellas son para los desarrolladores del juego, supongo que él o ella es una clase de campeón mundial de este juego. "

# game/dialogs.rpy:52
translate spanish scene1_a71313b4:

    # "Judging by the low ping, I guess they live around the same prefecture I live in. Small world..."
    "El bajo desempeño de su internet me deja asumir que él o ella vive en los alrededores de la prefectura donde yo vivo. El mundo es pequeño... "

# game/dialogs.rpy:53
translate spanish scene1_e0301d9f:

    # "In the dozens of games we played, NAMINAMI continued to beat everyone in the game room. But I was always the last one to fall."
    "En la docena de juegos que jugué, NAMINAMI derrotó a todos los que estaban en la sala. Pero yo era siempre el que caía de último."

# game/dialogs.rpy:55
translate spanish scene1_a40571de:

    # write "NAMINAMI> GG dude, u pretty much rock at this game!{fast}"
    write "NAMINAMI> Buen juego amigo, ¡tu si que sabes cómo jugar a este juego!{fast}"

# game/dialogs.rpy:57
translate spanish scene1_7c6f0607:

    # write "%(stringhero)s>{fast} Nah i'm not that good"
    write "%(stringhero)s>{fast} Nah, no soy tan bueno."

# game/dialogs.rpy:59
translate spanish scene1_0d6dddc9:

    # write "NAMINAMI> yes u r. i had trouble winning against u.{fast}"
    write "NAMINAMI> Si que lo eres, tuve problemas ganando contra ti.{fast}"

# game/dialogs.rpy:61
translate spanish scene1_87ab3c60:

    # write "NAMINAMI> care 4 a rematch? just u and me?{fast}"
    write "NAMINAMI> ¿Te importaría una revancha? ¿solo tu y yo?{fast}"

# game/dialogs.rpy:63
translate spanish scene1_828e7297:

    # write "%(stringhero)s>{fast} Maybe next time. It's getting late, i have school tomorrow."
    write "%(stringhero)s>{fast} Tal vez en otro momento. Se esta haciendo tarde y tengo clases mañana."

# game/dialogs.rpy:65
translate spanish scene1_0d8de120:

    # write "NAMINAMI> ur rite, me 2 anyway. hope 2 c u again soon!{fast}"
    write "NAMINAMI> Tienes razón, yo también tengo clases. ¡Espero verte pronto otra vez!{fast}"

# game/dialogs.rpy:66
translate spanish scene1_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:73
translate spanish scene1_ee02f0bc:

    # "I decide to watch some old episodes of the anime based on my favorite manga, {i}High School Samurai{/i}."
    "Miré algunos viejos episodios de anime basados en mi manga favorito, {i}High School Samurai{/i}."

# game/dialogs.rpy:74
translate spanish scene1_d91b4fcd:

    # "It's the story about a guy who lived in the mountains, learning the ways of Bushido. Then, all of a sudden, he must go to a new town to learn about life in the city."
    "Es la historia sobre un chico que vivió en las montañas, aprendiendo los caminos del Bushido, entonces debe ir al poblado para aprender la vida de la ciudad."

# game/dialogs.rpy:75
translate spanish scene1_e72d0922:

    # "He resolves a lot of his problems with his samurai skills and his heroic determination."
    "Él resolvió muchos problemas con sus habilidades de samurai y su determinación heróica."

# game/dialogs.rpy:80
translate spanish scene1_a19cfb71:

    # hero "I decide to study. Keep up the great work, %(stringhero)s!"
    hero "Debería estudiar más. ¡Debes seguir trabajando, %(stringhero)s!"

# game/dialogs.rpy:82
translate spanish scene1_ad5fbe82:

    # "I studied until very late at night."
    "Estudié hasta muy tarde."

# game/dialogs.rpy:84
translate spanish scene1_2f455d53:

    # "I don't know how long I stayed up, but I was so tired, I ended up falling asleep..."
    "No sé cuándo me fui a la cama pero estaba tan cansado, que apenas podía estar despierto..."

# game/dialogs.rpy:89
translate spanish scene1_cb77d3d7:

    # "*{i}Riiiiiiiiiiiing{/i}*"
    "*{i}Riiiiiiiiiiiing{/i}*"

# game/dialogs.rpy:91
translate spanish scene1_4037429a:

    # "I switched off the alarm clock and went back to sleep again."
    "Apague la alarma del reloj y me fui a dormir otra vez."

# game/dialogs.rpy:92
translate spanish scene1_548721a3:

    # hero "I'm so tired..."
    hero "Estoy tan cansado..."

# game/dialogs.rpy:93
translate spanish scene1_2a13d6dc:

    # "Mom" "%(stringhero)s!! Wake up!!! You'll be late for school!!!"
    "Mamá" "¡¡%(stringhero)s!! ¡¡Despierta!! ¡¡¡Vas a llegar tarde a la escuela!!!"

# game/dialogs.rpy:94
translate spanish scene1_c83937b9:

    # hero "Huh... What time is it..."
    hero "Ah... ¿Qué hora es?..."

# game/dialogs.rpy:95
translate spanish scene1_fc1e319f:

    # "Wait... 8:02AM?!{p} School starts at 8:30AM!!!"
    "Espera... 8:02AM.{p}Y yo comienzo a las 8:30AM."

# game/dialogs.rpy:97
translate spanish scene1_4217b105:

    # hero "Dangit!"
    hero "¡Maldita sea!"

# game/dialogs.rpy:99
translate spanish scene1_62c6f195:

    # hero "I stayed up and played too much! I'm late!"
    hero "¡He jugado demasiado! ¡Estoy atrasado!"

# game/dialogs.rpy:101
translate spanish scene1_b8605e00:

    # hero "I watched too much anime! I'm late!"
    hero "¡Me desvelé demasiado! ¡Estoy atrasado!"

# game/dialogs.rpy:103
translate spanish scene1_a476aee9:

    # hero "I studied too much! I'm late!"
    hero "¡Estudié demasiado! ¡Estoy atrasado!"

# game/dialogs.rpy:106
translate spanish scene1_0ff90bcf:

    # "I jumped out of bed, put on my brand new school uniform, and ran down the stairs."
    "Salté de la cama, me puse mi uniforme nuevo de la escuela y baje corriendo las escaleras."

# game/dialogs.rpy:108
translate spanish scene1_5c1508de:

    # "I ate my breakfast faster than usual."
    "Me comí mi desayuno más rápido de lo usual."

# game/dialogs.rpy:109
translate spanish scene1_6a44f1a3:

    # "I took a map with a route to school and left after saying goodbye to my parents."
    "Tomé el mapa con una ruta a la escuela y me fuí después de despedirme de mis padres."

# game/dialogs.rpy:112
translate spanish scene1_ad63def5:

    # "I followed the map and dashed through the streets."
    "Seguí el mapa y corrí a través de las calles."

# game/dialogs.rpy:113
translate spanish scene1_00e59ce7:

    # "This town looks pretty unique."
    "Este pueblo luce bastante único."

# game/dialogs.rpy:114
translate spanish scene1_689e5094:

    # "It's a strange town, like a mix between a suburb and a rural village."
    "Es un extraño pueblo, una mezcla entre un pueblo rural y área sub-urbana."

# game/dialogs.rpy:115
translate spanish scene1_a64e7e8b:

    # "Summer had just arrived. The cicadas were crying. And it sure was hot out here..."
    "El verano acaba de llegar, las cigarras estaban estridulando como a menudo lo hacen en Japón, y esta caluroso aquí afuera."

# game/dialogs.rpy:116
translate spanish scene1_71061539:

    # "I noticed only a few people on the streets."
    "Noté solo unas pocas personas en las calles."

# game/dialogs.rpy:117
translate spanish scene1_1d275b47:

    # "Some of them were kind enough to help me find my way to the school..."
    "Algunos de ellos eran lo suficientemente amables para ayudarme a encontrar mi camino a la escuela..."

# game/dialogs.rpy:118
translate spanish scene1_e123d412:

    # "Then, as I was nearing an intersection..."
    "Entonces, de repente, al estar cerca de una intersección..."

# game/dialogs.rpy:119
translate spanish scene1_da3e8686:

    # "I see a girl in a school uniform."
    "Veo una chica con el uniforme de la escuela."

# game/dialogs.rpy:121
translate spanish scene1_72d48eb1:

    # hero "Well, since village only has one school...that must be where she's heading!"
    hero "Debido a que el pueblo solo tiene una escuela, es ahí donde debe estar dirigiéndose"

# game/dialogs.rpy:122
translate spanish scene1_018ee7a3:

    # hero "And if that is where she's going, maybe she could help me out!"
    hero "Y si ahí es donde se esta dirigiendo, tal vez ella me puede ayudar!"

# game/dialogs.rpy:123
translate spanish scene1_a95f40e0:

    # "I approached her carefully, as I didn't want to scare her."
    "Me le acerque cuidadosamente, como si no quisiera asustarla."

# game/dialogs.rpy:124
translate spanish scene1_b0e8ce5c:

    # "She looked beautiful; long black hair, thin body, and beautiful legs..."
    "Ella se veía hermosa; largo cabello negro, un cuerpo delgado y hermosas piernas..."

# game/dialogs.rpy:125
translate spanish scene1_d501d47f:

    # "I hesitated for a moment before saying anything..."
    "Dudé por un momento antes de decir algo..."

# game/dialogs.rpy:126
translate spanish scene1_9195ee62:

    # hero "Ahem... Excuse me, miss?"
    hero "Ahem... ¿Disculpeme, señorita?"

# game/dialogs.rpy:131
translate spanish scene1_951b2409:

    # "She turned around to face me."
    "Ella se volteó hacía mí."

# game/dialogs.rpy:132
translate spanish scene1_b7723325:

    # "Oh wow! She was insanely pretty!"
    "¡Oh por Dios! ¡Ella era increíblemente bonita!"

# game/dialogs.rpy:133
translate spanish scene1_64e26830:

    # "Her eyes were blue.{p}A wonderful, deep, ocean blue. They were like the clear waters you'd find in Hawaii!"
    "Sus ojos eran azules.{p}Un maravilloso y profundo océano azul. ¡Y digo un océano cristalino, como esos en Hawaii!"

# game/dialogs.rpy:134
translate spanish scene1_a28c7063:

    # "She looked at me, confused, and a bit timid."
    "Ella me ve confundida... Y tímida."

# game/dialogs.rpy:136
translate spanish scene1_40d86de1:

    # s "Yes?"
    s "¿S-si?"

# game/dialogs.rpy:137
translate spanish scene1_013ed851:

    # "Wow! Her voice sounds so adorable!"
    "¡Wow! ¡Su voz suena tan lindo!"

# game/dialogs.rpy:138
translate spanish scene1_ed63afd9:

    # "She is so cute, that I was at a loss for words."
    "Ella es tan linda que estuve sin palabras."

# game/dialogs.rpy:139
translate spanish scene1_d015b36e:

    # hero "Uhhh... I'm new in the village, and I'm trying to find my school."
    hero "Uhhh... Soy nuevo en el pueblo y estoy tratando de encontrar mi escuela."

# game/dialogs.rpy:140
translate spanish scene1_759f29b6:

    # hero "And since you're wearing a uniform... I thought you could..."
    hero "Y ya que estas usando un uniforme... Pensé que tu podrías..."

# game/dialogs.rpy:141
translate spanish scene1_9c182eb6:

    # "The girl stared at me with her big, majestic, blue eyes..."
    "La chica me mira con sus grandes y maravillosos ojos azules..."

# game/dialogs.rpy:142
translate spanish scene1_e85ec319:

    # "Finally, she smiled."
    "Pero finalmente ella me sonríe."

# game/dialogs.rpy:144
translate spanish scene1_208138e6:

    # s "Of course I can!"
    s "¡Por su puesto que puedo!"

# game/dialogs.rpy:145
translate spanish scene1_d94b34fa:

    # s "I'm going there right now. You can follow me!"
    s "¡Me estoy diriendo allí ahora mismo. Puedes seguirme!"

# game/dialogs.rpy:146
translate spanish scene1_453ef329:

    # hero "That would be great! Thank you!"
    hero "¡Eso sería genial! ¡Gracias!"

# game/dialogs.rpy:150
translate spanish scene1_ef8271d0:

    # "We ended up walking to school together."
    "Entonces caminamos juntos a la escuela."

# game/dialogs.rpy:151
translate spanish scene1_e8a9e895:

    # "Darn, looks like it's my lucky day. {p}I'm walking to school with a beautiful girl on the first day!!!"
    "Vaya, parece que es mi día de suerte...{p}¡Mi primer día de escuela y voy ahí con una hermosa chica!"

# game/dialogs.rpy:152
translate spanish scene1_cb9f5aad:

    # s "You said you're new in town, right?"
    s "Dijiste que eres nuevo en el pueblo, ¿verdad?"

# game/dialogs.rpy:153
translate spanish scene1_36317276:

    # hero "Hmm?"
    hero "¿Hmm?"

# game/dialogs.rpy:155
translate spanish scene1_ecbbbe32:

    # "I had been daydreaming, so I didn't notice that she was talking to me."
    "He estado soñando despierto, así que no noté que ella me estaba hablando."

# game/dialogs.rpy:156
translate spanish scene1_d86baf24:

    # "She started to blush."
    "Ella comenzó a ruborizarse."

# game/dialogs.rpy:158
translate spanish scene1_0408e4ff:

    # s "Oh... I'm sorry! I didn't mean to surprise you!"
    s "Oh...¡Discúlpame! ¡No era mi intención asustarte!"

# game/dialogs.rpy:159
translate spanish scene1_004a1338:

    # "She's just so adorable! I smiled to myself."
    "Ella es tan linda. Sonreí para mí mismo."

# game/dialogs.rpy:160
translate spanish scene1_02ac9e9f:

    # hero "You're not!{p}Actually, I just arrived last Friday. My family just moved here from Tokyo."
    hero "¡No, no lo hiciste!{p}En realidad, llegué el pasado Viernes. Mi familia acaba de mudarse aquí desde Tokyo."

# game/dialogs.rpy:162
translate spanish scene1_b0c354bc:

    # s "You're from Tokyo?"
    s "¿Eres de Tokyo?"

# game/dialogs.rpy:163
translate spanish scene1_7273bcfc:

    # s "It seems like a wonderful city! I wish I could visit it someday!"
    s "¡Es una maravillosa ciudad! ¡Desearía visitarlo algún día!"

# game/dialogs.rpy:164
translate spanish scene1_e87c5166:

    # hero "I bet... I noticed there are fewer distractions here than in Tokyo."
    hero "Lo apuesto...Desde que llegamos aquí, no he encontrado ningunas distraciones como en Tokyo."

# game/dialogs.rpy:166
translate spanish scene1_6ab8e3af:

    # s "Mmhmm, it's true. The village is kinda quiet; so there's usually nothing to do..."
    s "Mmm, es verdad. El pueblo es algo tranquilo; así que por lo general no hay nada que hacer..."

# game/dialogs.rpy:167
translate spanish scene1_f5cb34b6:

    # s "When we want to enjoy ourselves, we usually go to the nearest city."
    s "Cuando queremos divertirnos, usualmente nos vamos a la ciudad más cercana."

# game/dialogs.rpy:169
translate spanish scene1_f471c6c0:

    # s "It's only a short train ride away, so it's okay!"
    s "Pero tenemos un tren que nos lleva hasta allí, ¡así que esta bien!"

# game/dialogs.rpy:170
translate spanish scene1_6136ebba:

    # hero "There's a train that takes you to the city nearby!?"
    hero "¿Un tren? ¿¡Para llegar a la ciudad más cercana!?"

# game/dialogs.rpy:171
translate spanish scene1_bbdac77d:

    # hero "I hope there's a lot of stuff to do there."
    hero "Espero que hayan muchas cosas ahí."

# game/dialogs.rpy:172
translate spanish scene1_aa2bb7ad:

    # s "There is, don't worry! There are fast food restaurants, shops, arcades..."
    s "Las hay, ¡no te preocupes! Hay restaurantes de comida rápida, tiendas, salones recreativos..."

# game/dialogs.rpy:173
translate spanish scene1_cd44299c:

    # hero "You like arcade games?"
    hero "¿Te gustan los juegos de las maquinitas?"

# game/dialogs.rpy:175
translate spanish scene1_69578672:

    # s "Very much so!"
    s "¡Si y mucho!"

# game/dialogs.rpy:176
translate spanish scene1_13bf5fa0:

    # s "I'm especially a fan of music and rhythm games, but I enjoy shooting games too!"
    s "Especialmente soy una fan de los juegos de música y de ritmo, ¡pero disfruto de juegos de disparos también!"

# game/dialogs.rpy:177
translate spanish scene1_0fef1f71:

    # hero "Shooting games?"
    hero "¿Juegos de disparos?"

# game/dialogs.rpy:178
translate spanish scene1_ad5c53dd:

    # "Wow... I couldn't imagine such a cute and timid girl shooting zombies or soldiers in an arcade game..."
    "Vaya... No puedo imaginarme a una chica linda y tímida como ella disparando zombis o soldados en un salón recreativo..."

# game/dialogs.rpy:179
translate spanish scene1_45a2a82d:

    # hero "By the way, what grade are you in?"
    hero "De cualquier modo, ¿a qué grado vas?"

# game/dialogs.rpy:181
translate spanish scene1_68018eef:

    # s "I'm in the second section."
    s "Estoy en la segunda sección."

# game/dialogs.rpy:182
translate spanish scene1_4c52535b:

    # hero "Uh...Second section?"
    hero "Uh...¿Segunda sección?"

# game/dialogs.rpy:183
translate spanish scene1_3b1fd997:

    # s "You see, our school is the only school in the village."
    s "Verás, es la única escuela en el pueblo."

# game/dialogs.rpy:184
translate spanish scene1_7e47a0e7:

    # s "There are students of many different ages and grades at our school, but there's an unbalanced amount of students for the usual grade system."
    s "Y los estudiantes son de diferentes edades y hay una cantidad desbalanceada de estudiantes para el usual sistema de grados."

# game/dialogs.rpy:185
translate spanish scene1_a82424ad:

    # s "So they made different classes, called sections, consisting of 25 students each, which are made up of students of different ages and grades."
    s "Así que ellos hicieron diferentes clases, llamadas secciones, de 25 estudiantes cada uno."

# game/dialogs.rpy:186
translate spanish scene1_b83c9923:

    # hero "That sounds fun... I wonder where I'll be assigned to..."
    hero "Eso suena divertido... Me pregunto a donde seré asignado..."

# game/dialogs.rpy:187
translate spanish scene1_696b7e4d:

    # s "Well, there are only five sections."
    s "Bueno, hay 5 secciones."

# game/dialogs.rpy:188
translate spanish scene1_029cc297:

    # hero "So I'll have a 1-in-5 chance of being in the same section as you!"
    hero "Entonces tengo de 1 a 5 probabilidades de estar en tu misma sección..."

# game/dialogs.rpy:189
translate spanish scene1_c959a464:

    # "I chuckled a bit and she giggled, a little embarrassed."
    "Me reí un poco y ella también pero algo avergonzada."

# game/dialogs.rpy:191
translate spanish scene1_c152d24c:

    # s "My name is Sakura... And yours?"
    s "Mi nombre es Sakura... ¿Y el tuyo?"

# game/dialogs.rpy:192
translate spanish scene1_180ea3e3:

    # hero "Name's %(stringhero)s."
    hero "Mi nombre es %(stringhero)s "

# game/dialogs.rpy:194
translate spanish scene1_abeffde2:

    # s "%(stringhero)s..."
    s "%(stringhero)s..."

# game/dialogs.rpy:196
translate spanish scene1_77296229:

    # s "Nice to meet you, %(stringhero)s-san!"
    s "Mucho gusto en conocerte, ¡%(stringhero)s-san!"

# game/dialogs.rpy:197
translate spanish scene1_6ec72dd8:

    # hero "Pleased to meet you as well, Sakura-san!"
    hero "Un gusto conocerte a ti también, ¡Sakura-san!"

# game/dialogs.rpy:198
translate spanish scene1_548f585d:

    # "She gave me a sweet smile."
    "Ella mostro una dulce sonrisa."

# game/dialogs.rpy:199
translate spanish scene1_4466f621:

    # "Gosh, if a smile could kill, I think I'd be dead right now..."
    "Oh por Dios, si una sonrisa pudiera matar, pienso que ya estaría muerto ahora..."

# game/dialogs.rpy:201
translate spanish scene1_1a8c3634:

    # "Deep down, I really hoped to be in the same class as her."
    "En lo profundo de mi corazón, realmente espere estar en las misma clase que ella."

# game/dialogs.rpy:202
translate spanish scene1_2fcc0bdf:

    # "I wanted to see her again and learn more about her... {image=heart.png}"
    "Quería verla otra vez y aprender más sobre ella... {image=heart.png}"

# game/dialogs.rpy:210
translate spanish scene1_df51f81c:

    # "I can't believe it!"
    "¡No lo puedo creer!"

# game/dialogs.rpy:211
translate spanish scene1_06cbe8a5:

    # "I've been assigned to the second section!{p}I'm so happy!"
    "¡He sido asignado en la segunda sección!{p}¡Estoy tan feliz!"

# game/dialogs.rpy:212
translate spanish scene1_a8b3f3cd:

    # "However, I wonder if it's a good section."
    "Sin embargo, me pregunto si es una buena sección."

# game/dialogs.rpy:213
translate spanish scene1_3c0ab1de:

    # "I wouldn't like to be in a classroom full of jerks or something..."
    "No quisiera estar en una clase llena de payasos o algo así..."

# game/dialogs.rpy:214
translate spanish scene1_9a96bdeb:

    # "If that's the case, I just hope I'll be near Sakura-san..."
    "Si ese es el caso, solo espero estar cerca de Sakura-san..."

# game/dialogs.rpy:216
translate spanish scene1_1111d955:

    # "*{i}Bump{/i}*"
    "*{i}Golpe{/i}*"

# game/dialogs.rpy:217
translate spanish scene1_89c4aa2c:

    # "While I was daydreaming again, I bumped into someone."
    "Mientras sueño despierto otra vez, me topé con alguien."

# game/dialogs.rpy:218
translate spanish scene1_65939e34:

    # "It was a girl with pigtails."
    "Era una linda chica con un cabello azul claro."

# game/dialogs.rpy:222
translate spanish scene1_7c921ed5:

    # r "Hey, you! Watch where you're going!"
    r "¡Oye, tú! ¡Mira por donde andas!"

# game/dialogs.rpy:223
translate spanish scene1_5359da65:

    # hero "Sorry... It was my fault..."
    hero "Perdón... Fue mi culpa..."

# game/dialogs.rpy:225
translate spanish scene1_b17d24d0:

    # r "Wait... You're new here, aren't ya?"
    r "Espera... Tú eres el nuevo aquí, ¿cierto?"

# game/dialogs.rpy:227
translate spanish scene1_39364aa2:

    # r "Hmph, well whatever, I'll see you later."
    r "Hmph, te veré más tarde..."

# game/dialogs.rpy:229
translate spanish scene1_b67db50c:

    # "She ran down the hall, probably heading to her classroom."
    "Ella corrido por el corredor, probablemente dirigiéndose a su salón de clases."

# game/dialogs.rpy:230
translate spanish scene1_e2ba9dde:

    # "That girl was kinda rude... But rather pretty-looking..."
    "Esa chica fue ruda... Pero con una apariencia bastante linda..."

# game/dialogs.rpy:231
translate spanish scene1_cd4a6e19:

    # "She reminds me of the main female character of the anime Domoco-chan because of her hair... And from what I could tell, her personality matched as well..."
    "Ella me recuerda a la protagonista del anime Domoco-chan por su cabello... Y por lo que pude ver, su personalidad también..."

# game/dialogs.rpy:234
translate spanish scene1_41041ae3:

    # "Here it is... Second section's classroom."
    "Aquí es... Salón de la segunda sección."

# game/dialogs.rpy:235
translate spanish scene1_201315b4:

    # "Some of students were already here."
    "Algunos estudiantes ya estaban aquí."

# game/dialogs.rpy:236
translate spanish scene1_8928782e:

    # "Sakura-san was right, there are students of all ages here."
    "Sakura-san estaba en lo correcto, todos ellos son de diferentes edades."

# game/dialogs.rpy:237
translate spanish scene1_563881d0:

    # "The youngest looked around 8 years old and the oldest, about 18."
    "El más joven aparentaba tener unos 8 años de edad y el mayor de todos unos 18."

# game/dialogs.rpy:239
translate spanish scene1_75dbcf3d:

    # "I saw Sakura-san sitting by the window."
    "Miré a Sakura-san sentada por la ventana."

# game/dialogs.rpy:240
translate spanish scene1_3f9d91b2:

    # "She smiled as soon as saw me."
    "Ella sonrió cuando me vio."

# game/dialogs.rpy:242
translate spanish scene1_05ec4d50:

    # s "Ah, %(stringhero)s-san! Over here! There's a desk available here!"
    s "Ah, ¡%(stringhero)s-san! ¡Por aquí! ¡Hay un escritorio disponible aquí!"

# game/dialogs.rpy:243
translate spanish scene1_a4ac0809:

    # "She pointed the empty desk behind her."
    "Ella apunto al escritorio vacío detrás de ella."

# game/dialogs.rpy:244
translate spanish scene1_c00ae698:

    # "How lucky! A desk near her!"
    "¡Qué suerte! ¡Un escritorio cerca de ella!"

# game/dialogs.rpy:245
translate spanish scene1_95960ba8:

    # "This really is my lucky day!"
    "¡Este es realmente mi día de suerte!"

# game/dialogs.rpy:247
translate spanish scene1_c25aa128:

    # hero "I'm so excited we're in the same section!"
    hero "¡Estoy tan emocionado estamos en la misma sección!"

# game/dialogs.rpy:248
translate spanish scene1_7882f4c9:

    # s "So am I!"
    s "¡Yo también lo estoy!"

# game/dialogs.rpy:249
translate spanish scene1_ef153560:

    # hero "Our classmates really are made up of different ages!"
    hero "Es gracioso cómo hay compañeros de tan diversos edades."

# game/dialogs.rpy:250
translate spanish scene1_198a38e2:

    # s "Just like I told you!"
    s "¡Justo como te dije!"

# game/dialogs.rpy:251
translate spanish scene1_44c33509:

    # hero "Actually,{w} this whole town seems pretty special..."
    hero "En realidad...{w} Todo el pueblo luce tan especial..."

# game/dialogs.rpy:252
translate spanish scene1_7a669bce:

    # hero "I mean, the school system, the way the streets are, the kind-hearted townfolk..."
    hero "Quiero decir, este sistema escolar,... La forma que están hechas las calles,... Las amables personas del pueblo..."

# game/dialogs.rpy:253
translate spanish scene1_3b8a00b5:

    # s "Do you like it?"
    s "¿Te gusta?"

# game/dialogs.rpy:254
translate spanish scene1_874f89c2:

    # hero "So far...Yeah, I think I do like it."
    hero "Hasta ahora... Pienso que si. Me gusta." # ME GUSTAS TU lol

# game/dialogs.rpy:255
translate spanish scene1_d703df5c:

    # s "I hope you'll enjoy your time here, %(stringhero)s-san!"
    s "Espero que disfrutes tu estadía aquí, ¡%(stringhero)s-san!"

# game/dialogs.rpy:256
translate spanish scene1_0afeb16b:

    # hero "Thank you, Sakura-senpai."
    hero "Gracias, Sakura-senpai."

# game/dialogs.rpy:257
translate spanish scene1_034d31d6:

    # hero "So, what are we learning in class today?"
    hero "Entonces, ¿qué clases vamos a tener hoy?"

# game/dialogs.rpy:258
translate spanish scene1_21c1af0a:

    # s "Math."
    s "Matemáticas."

# game/dialogs.rpy:259
translate spanish scene1_a9d114f6:

    # hero "Oh geez..."
    hero "Yeowch..."

# game/dialogs.rpy:260
translate spanish scene1_0940f6fc:

    # "Looks like my luck couldn't last all day..."
    "Parece que mi suerte no puede durar todo el día."

# game/dialogs.rpy:261
translate spanish scene1_07506d68:

    # s "You don't like math?"
    s "¿No te gusta las matemáticas?"

# game/dialogs.rpy:262
translate spanish scene1_280571a1:

    # hero "Well, I'm not very good at it."
    hero "Pues, no soy muy bueno con ello."

# game/dialogs.rpy:263
translate spanish scene1_21fba3a7:

    # s "I'm pretty good with math. I can help you, if you want!"
    s "Soy bastante buena con las matemáticas. ¡Puedo ayudarte, si tu quieres!"

# game/dialogs.rpy:264
translate spanish scene1_0181a8b6:

    # hero "That sounds perfect!"
    hero "¡Eso suena perfecto!"

# game/dialogs.rpy:265
translate spanish scene1_8b4aa3e9:

    # hero "Thanks a lot, Sakura-senpai!"
    hero "¡Muchas gracias, Sakura-senpai!"

# game/dialogs.rpy:266
translate spanish scene1_1e4821cf:

    # s "Teehee! You're welcome!"
    s "¡Teehee! ¡De nada!"

# game/dialogs.rpy:268
translate spanish scene1_a8275802:

    # hero "Well, since I stayed up all night studying, it shouldn't be that hard."
    hero "Después de todo, me la pasé estudiando anoche. No debería ser tan difícil."

# game/dialogs.rpy:269
translate spanish scene1_0df50adc:

    # s "I doubt we'll start with something too difficult anyway. But I'll be here if you need me!"
    s "Dudo que comencemos con algo difícil de cualquier manera. ¡Pero estaré allí si me necesitas!"

# game/dialogs.rpy:273
translate spanish scene1_6220dd69:

    # "Class started without anything noteworthy happening."
    "Las clases comenzaron sin nada digno de atención."

# game/dialogs.rpy:274
translate spanish scene1_5181e117:

    # "Math ended up being pretty easy. It was planned for every level, probably because of all the different ages in class."
    "Para ser honesto, la matemática fue bastante fácil. Estaba planeado para todos los niveles, probablemente debido a  los diferentes edades en la clase."

# game/dialogs.rpy:276
translate spanish scene1_a1cb44f3:

    # "Or it could be that all that studying really paid off..."
    "O tal vez estudiar lo hace menos difícil de lo que pensé..."

# game/dialogs.rpy:277
translate spanish scene1_3523ed26:

    # "The lessons felt like they were going on for awhile though..."
    "Las lecciones simplemente se veían extensas..."

# game/dialogs.rpy:287
translate spanish scene3_2db6f6fb:

    # "The school bell rang to announce the end of classes."
    "La campana de la escuela sonó para anunciar el fin de las clases."

# game/dialogs.rpy:288
translate spanish scene3_9a7755fc:

    # "I was surprised by the noise. It looked more like a bell for firemen. This is really not a urban school!"
    "Estaba sorprendido por el ruido. Parecía más como una alarma de incendios. ¡Realmente esto no es una escuela urbana!"

# game/dialogs.rpy:289
translate spanish scene3_99288ad6:

    # "Sakura and I decided to take and eat our lunches together in the classroom, as well as some other classmates."
    "Yo y Sakura comimos el almuerzo juntos en la clase, además de otros compañeros."

# game/dialogs.rpy:290
translate spanish scene3_d58c404f:

    # "During lunch, I asked Sakura-san if there were clubs here like in normal schools."
    "Durante el almuerzo, le pregunté a Sakura-san si había clubes como las escuelas normales."

# game/dialogs.rpy:292
translate spanish scene3_0e1303f7:

    # s "Yes, for example, I'm in the manga club."
    s "Si, de hecho estoy en el club de manga."

# game/dialogs.rpy:293
translate spanish scene3_5d47cb20:

    # s "I like it a lot, plus the leader of the club is a cosplayer."
    s "Me gusta, además fue fundado por una cosplayer."

# game/dialogs.rpy:294
translate spanish scene3_aae30260:

    # hero "A manga club!?"
    hero "¿¡Un club de manga!?"

# game/dialogs.rpy:295
translate spanish scene3_04e97040:

    # hero "Awesome! I love manga!"
    hero "¡Asombroso! ¡me encanta el manga!"

# game/dialogs.rpy:297
translate spanish scene3_6ec6ef6f:

    # s "You do?{p}That's great!"
    s "¿Te gusta?{p}¡Eso es grandioso!"

# game/dialogs.rpy:298
translate spanish scene3_f9c203d9:

    # s "You're more than welcome if you want to join us, %(stringhero)s-san!"
    s "Eres más que bienvenido si quieres unirtenos, ¡%(stringhero)s-san!"

# game/dialogs.rpy:299
translate spanish scene3_c7e8dac1:

    # hero "That would be awesome!"
    hero "¡Eso sería asombroso!"

# game/dialogs.rpy:300
translate spanish scene3_c0f4f625:

    # hero "I just hope the club leader will let me join..."
    hero "Solo espero que el fundador me deje unirme."

# game/dialogs.rpy:302
translate spanish scene3_fc29c8d9:

    # s "I'm sure she will! We're always looking for new members, especially right now!"
    s "¡Estoy segura que ella lo hará! Siempre necesitamos de nuevos miembros, especialmente ahora."

# game/dialogs.rpy:303
translate spanish scene3_3d9d0b13:

    # hero "'She'?"
    hero "'¿Ella?'"

# game/dialogs.rpy:304
translate spanish scene3_ea9b5b89:

    # s "Yup!"
    s "¡Sip!"

# game/dialogs.rpy:305
translate spanish scene3_cca5acb7:

    # s "You know, a lot of girls enjoy anime and manga."
    s "Sabes, muchas chicas disfrutan del anime y el manga."

# game/dialogs.rpy:306
translate spanish scene3_10b77e4e:

    # hero "Yeah, I know..."
    hero "Si, lo sé..."

# game/dialogs.rpy:307
translate spanish scene3_65b45dcd:

    # "Actually, I have an older sister who loved shoujo anime and manga when we were younger."
    "En realidad, tengo una hermana mayor quien amó el shoujo anime y manga cuando eramos más jovenes."

# game/dialogs.rpy:308
translate spanish scene3_b56e0491:

    # "But that was a long time ago. She's married now and is living in Tokyo with her husband."
    "Pero eso es pasado ahora. Hoy ella esta casada y viviendo en Tokyo con su esposo."

# game/dialogs.rpy:309
translate spanish scene3_b784ebc5:

    # "I wonder how she is doing... I should e-mail her when I get back home."
    "Me pregunto cómo le estará yendo... Debería mandarle un correo cuando vuelva a casa."

# game/dialogs.rpy:310
translate spanish scene3_2494a3a5:

    # hero "How many members are in the club?"
    hero "¿Cuántos miembros hay en el club?"

# game/dialogs.rpy:312
translate spanish scene3_cb2e3f44:

    # s "Actually, we only have three members right now..."
    s "En realidad solo somos tres..."

# game/dialogs.rpy:313
translate spanish scene3_76a2a0d6:

    # hero "Three? That's all?"
    hero "¿Tres? ¿Eso es todo?"

# game/dialogs.rpy:314
translate spanish scene3_88a7dbf3:

    # s "Yeah... Three girls."
    s "Si... Tres chicas."

# game/dialogs.rpy:315
translate spanish scene3_1f17e8f8:

    # s "It's rather hard to find new members, considering the only manga shop around is in the city..."
    s "En realidad, es algo difícil encontrar nuevos miembros, considerando que la única tienda de manga cercana esta en la ciudad..."

# game/dialogs.rpy:316
translate spanish scene3_9f89b658:

    # s "Usually, the people that join aren't too invested to get into new manga to read so they end up leaving the club."
    s "Usualmente, las personas que se unen se aburren buscando nuevos mangas por lo que al final dejan el club."

# game/dialogs.rpy:317
translate spanish scene3_84b8bd32:

    # hero "I see."
    hero "Ya veo."

# game/dialogs.rpy:318
translate spanish scene3_dd98eff8:

    # "Holy crap! Joining a manga club with only three girls!?"
    "¡Por Dios! ¡Unirse a un club de manga con solo 3 chicas!"

# game/dialogs.rpy:319
translate spanish scene3_d931c57f:

    # "Seems like my luck is back! I feel like the hero of a harem anime or a visual novel!"
    "Mi suerte está de vuelta, o eso parece... ¡Siento como si fuera el héroe de un anime harem o de una novela visual!"

# game/dialogs.rpy:321
translate spanish scene3_454939b4:

    # s "So, shall we go?"
    s "Entonces, ¿nos vamos?"

# game/dialogs.rpy:322
translate spanish scene3_0dab1b57:

    # hero "Sure! You lead the way, Sakura-senpai!"
    hero "¡Claro! ¡guía el camino, Sakura-senpai!"

# game/dialogs.rpy:324
translate spanish scene3_452bb69b:

    # s "Let's go!"
    s "¡Vamos!"

# game/dialogs.rpy:329
translate spanish scene3_afe022e1:

    # "I followed her through the halls. We climbed up the stairs and then entered a small classroom."
    "La seguí mientras caminamos por los corredores, arriba de las escaleras, y dentro de un pequeño salón de clases."

# game/dialogs.rpy:331
translate spanish scene3_31abf4ae:

    # "The room only had a few desks and they were all placed together to make a big table."
    "El salón tenía unas cuantos escritorios, todos puestos para formar una gran mesa."

# game/dialogs.rpy:332
translate spanish scene3_1b03234c:

    # "There were some manga posters on the walls and a bookshelf full of manga and artbooks."
    "Había algunos carteles de manga en las paredes y un estantería lleno de manga y libros de arte."

# game/dialogs.rpy:333
translate spanish scene3_bfcd2b6d:

    # "There was also a small TV with a NATO GEO gaming console and a VCR plugged in it."
    "También había una pequeña TV con una consola de juego NATO GEO y un VCR conectado a ella."

# game/dialogs.rpy:335
translate spanish scene3_f22a2c8a:

    # "Two girls were already sitting at the table. One of them was completely focused on her handheld console."
    "Dos chicas estaban sentadas en la mesa. Una de ellas estaba completamente concentrada en su consola portátil."

# game/dialogs.rpy:336
translate spanish scene3_85e454f4:

    # "I instantly recognized the other one. She was the same girl that I had bumped into this morning!"
    "Instantáneamente reconocí a la otra, era la misma que había chocado en esa mañana."

# game/dialogs.rpy:340
translate spanish scene3_074e44c0:

    # r "Sakura-chan! How are you?"
    r "¡Sakura-chan! ¿Cómo estas?"

# game/dialogs.rpy:342
translate spanish scene3_bedfc3ef:

    # s "Rika-chan! Nanami-chan!"
    s "¡Rika-chan! ¡Nanami-chan!"

# game/dialogs.rpy:343
translate spanish scene3_8ac194aa:

    # s "I found a new member for the club!"
    s "¡He encontrado a un nuevo miembro para el club!"

# game/dialogs.rpy:344
translate spanish scene3_a4e3e823:

    # hero "Huh... Hello there?"
    hero "Huh... ¿Hola?"

# game/dialogs.rpy:346
translate spanish scene3_7fdb9008:

    # r "You!"
    r "¡Tú!"

# game/dialogs.rpy:348
translate spanish scene3_0ca8552f:

    # s "You know him already?"
    s "¿Ya lo conocías?"

# game/dialogs.rpy:350
translate spanish scene3_771d8927:

    # r "Yeah, he bumped into me in the hallway this morning!"
    r "¡Si, el chocó conmigo en el corredor esta mañana!"

# game/dialogs.rpy:351
translate spanish scene3_56462fb4:

    # hero "Hey, I already told you that I'm sorry! I was daydreaming and I didn't see you!"
    hero "¡Oye, ya te dije que lo siento! ¡Estaba soñando despierto y no te vi!"

# game/dialogs.rpy:352
translate spanish scene3_4b30e319:

    # r "Heh... I bet you were daydreaming about girls, weren't you!?"
    r "Je... Soñando despierto sobre chicas, ¡no es así!"

# game/dialogs.rpy:353
translate spanish scene3_d003c469:

    # hero "Yikes!"
    hero "¡Yikes!"

# game/dialogs.rpy:354
translate spanish scene3_67cc7e93:

    # "Well... she was kind of right..."
    "Bueno, en realidad, ella tenía algo de razón..."

# game/dialogs.rpy:355
translate spanish scene3_7ee0bf30:

    # "I felt a little embarrassed."
    "Me sentí avergonzado."

# game/dialogs.rpy:356
translate spanish scene3_b9df3e31:

    # "I noticed Nanami stopped playing and sneaked over silently to join us."
    "Noté que Nanami silenciosamente dejó de jugar para unírsenos."

# game/dialogs.rpy:359
translate spanish scene3_2385f465:

    # s "Don't pay attention to her sourness. All boys are perverts to Rika-chan, but it's not in a mean way!"
    s "No le prestes atención a su aspereza. Todos los chicos son pervertidos para Rika-chan ¡pero no es de una mala manera!"

# game/dialogs.rpy:360
translate spanish scene3_bdadc851:

    # hero "I see. I just hope she doesn't kill me in my sleep."
    hero "Ya veo. Solo espero que ella no me mate cuando duerma."

# game/dialogs.rpy:361
translate spanish scene3_85dc831b:

    # r "Hmph!"
    r "¡Hmph!"

# game/dialogs.rpy:363
translate spanish scene3_0160ed38:

    # s "He likes anime and manga, plus he seems quite motivated. Don't you want to give him a chance?"
    s "A él le gusta el anime y el manga, además parece bastante motivado. ¿No quieres darle una oportunidad?"

# game/dialogs.rpy:364
translate spanish scene3_bfbf7878:

    # s "We need more boys in this club too. Some people already think we're just a girls only club."
    s "Necesitamos más hombres en este club también. Algunas personas ya piensan que somos un club solo para chicas."

# game/dialogs.rpy:368
translate spanish scene3_49d8fa00:

    # r "Alright, fine, I accept. Welcome to the club, Mr...?"
    r "Esta bien, acepto. Bienvenido al club, ¿señor....?"

# game/dialogs.rpy:369
translate spanish scene3_d2163b95:

    # hero "%(stringhero)s. Nice to meet you."
    hero "%(stringhero)s. Gusto en conocerte."

# game/dialogs.rpy:370
translate spanish scene3_da243e9b:

    # r "Yeah, likewise."
    r "Si, igualmente."

# game/dialogs.rpy:371
translate spanish scene3_48579522:

    # "Rika spoke with a Kansai dialect. Just like most of the village's townfolk."
    "Rika estaba hablando con un dialecto Kansai. Justo como la mayoría de la gente del pueblo."

# game/dialogs.rpy:376
translate spanish scene3_d37e267a:

    # "I saw Nanami just looking at me curiously, so Sakura spoke."
    "Estaba viendo que Nanami me miraba curiosamente, entonces Sakura habló."

# game/dialogs.rpy:377
translate spanish scene3_1901ff6f:

    # s "So you already met Rika-chan. Here we have our little Nanami-chan! She entered the club just two weeks ago!"
    s "Entonces ella es Rika-chan, y aquí, la pequeña Nanami-chan. Ella entró al club hace dos semanas."

# game/dialogs.rpy:379
translate spanish scene3_35deb10d:

    # n "Hey, I'm not little! I'm 16!"
    n "¡Oye, no soy pequeña! ¡Tengo 16!"

# game/dialogs.rpy:380
translate spanish scene3_a2b33e03:

    # "It's true, she looks a lot younger than she is. But she was still pretty cute looking."
    "Es verdad que se ve más joven de lo que aparenta. Ella se veía linda."

# game/dialogs.rpy:381
translate spanish scene3_15aa8440:

    # r "She's small but strong! She doesn't brag much about it but she's good at video games. And I mean... {b}very{/b} good!"
    r "¡Pequeña pero fuerte! Ella no alardea mucho sobre ello pero ella es buena en los videojuegos. Y digo...¡{b}muy{/b} buena!"

# game/dialogs.rpy:383
translate spanish scene3_28d5cd6d:

    # s "She's the prefecture's champion in a lot of games! {i}Lead of Fighter '96, Pika Rocket Online,{/i}... Even {i}Tetranet Online{/i}!"
    s "¡Ella es la campeona de la prefectura en muchos juegos! {i}Lead of Fighter'96, Pika Rocket Online,{/i}... ¡Incluso {i}Tetranet Online{/i}!"

# game/dialogs.rpy:384
translate spanish scene3_a50ea391:

    # hero "Cool!"
    hero "¡Genial!"

# game/dialogs.rpy:386
translate spanish scene3_a580dff0:

    # hero "Wait wait wait...{p}Isn't your username... NAMINAMI?"
    hero "Espera espera espera...{p}¿No es tu nombre de usuario... NAMINAMI?"

# game/dialogs.rpy:388
translate spanish scene3_582ff77a:

    # n "Huh?! How did you know?"
    n "¡¿Huh?! ¿Cómo lo sabes?"

# game/dialogs.rpy:389
translate spanish scene3_8aa6fe25:

    # hero "I think we played {i}Tetranet Online{/i} last night!"
    hero "¡Jugamos  {i}Tetranet Online{/i} anoche!"

# game/dialogs.rpy:391
translate spanish scene3_88e0bb32:

    # n "Wait, you're that %(stringhero)s-dude I had a hard time beating?"
    n "Espera, ¿¿tu eres ese tipo llamado %(stringhero)s con quien tuve un momento difícil para derrotarlo??"

# game/dialogs.rpy:392
translate spanish scene3_3052c7b9:

    # "{i}Dude{/i}?"
    "¿{i}Tipo{/i}?"

# game/dialogs.rpy:393
translate spanish scene3_0fd6ee48:

    # hero "Yeah that was me!"
    hero "Uhhh... Si, era yo."

# game/dialogs.rpy:394
translate spanish scene3_e6f7ec96:

    # hero "What a small world..."
    hero "El país de seguro es pequeño."

# game/dialogs.rpy:395
translate spanish scene3_a43e5a9b:

    # n "Let's meet up here after club activities. I challenge you to {i}Lead of Fighters '96{/i}!"
    n "Encontremonos aquí después de las actividades del club. ¡Te reto a {i}Lead of Fighters '96{/i}!"

# game/dialogs.rpy:396
translate spanish scene3_050d4a5f:

    # "Her eyes were sparkling, full of energy, like she was ready for a real challenge. It was pretty adorable."
    "Sus ojos estaban brillando enérgicamente y con aire desafiante. Fue lindo."

# game/dialogs.rpy:397
translate spanish scene3_6376ce6f:

    # "I'm sure I'll get my ass kicked again..."
    "Seguramente seré abatido otra vez..."

# game/dialogs.rpy:398
translate spanish scene3_958d04dd:

    # "But whatever...She was really cute, so..."
    "Pero no importa. Ella es linda..."

# game/dialogs.rpy:399
translate spanish scene3_65819df0:

    # hero "Challenge accepted!"
    hero "¡Reto aceptado!"

# game/dialogs.rpy:403
translate spanish scene3_5de027f9:

    # n "So you're the dude Rika-nee was talking about... Nice to meet you!"
    n "Así que eres tu el tipo quien Rika-nee estaba hablando... ¡Gusto en conocerte!"

# game/dialogs.rpy:404
translate spanish scene3_3052c7b9_1:

    # "{i}Dude{/i}?"
    "¿{i}Tipo{/i}?"

# game/dialogs.rpy:405
translate spanish scene3_396612f8:

    # hero "Nice to meet you too."
    hero "Gusto en conocerte también."

# game/dialogs.rpy:406
translate spanish scene3_dd9f61de:

    # hero "Hehe, sorry to say this, but you really don't look like a video game champion."
    hero "Jeje, perdón por mencionarlo, pero no pareces una campeona de los videojuegos."

# game/dialogs.rpy:407
translate spanish scene3_9458441a:

    # n "One of these days, I'll show you how skilled I am!"
    n "¡Te demostraré lo habilidosa que soy algún día!"

# game/dialogs.rpy:408
translate spanish scene3_54ab12a6:

    # "Her eyes were sparkling, full of energy. It was pretty adorable."
    "Sus ojos estaban brillando energícamente y con aire desafiante. Era linda."

# game/dialogs.rpy:409
translate spanish scene3_40cb84ff:

    # hero "Sure, you're on anytime!"
    hero "¡Claro, cuando quieras!"

# game/dialogs.rpy:411
translate spanish scene3_bddf279b:

    # "Then I focused my attention on Rika."
    "Entonces concentre mi atención en Rika."

# game/dialogs.rpy:412
translate spanish scene3_fff0f085:

    # "Now that I've seen her up close, I noticed that her eyes were unusual."
    "Ahora que la he visto de cerca, noté que sus ojos son inusuales."

# game/dialogs.rpy:413
translate spanish scene3_3429571b:

    # "One eye was blue and another was green."
    "Eran de color azul y verde."

# game/dialogs.rpy:414
translate spanish scene3_14f143f3:

    # "The guy whose allowed to dive into those eyes will be one lucky guy."
    "El hombre que ella le permita sumergirse en ellos tendrá mucha suerte."

# game/dialogs.rpy:415
translate spanish scene3_540f6b1e:

    # "But given her personality, is there any guy who will even get that chance?"
    "¿Pero habrá algún hombre que tendrá esa oportunidad, si a ella no le gusta los chicos?"

# game/dialogs.rpy:416
translate spanish scene3_1eaf8eb5:

    # hero "If you don't mind me saying, Rika-san,...you look like Domoco-chan."
    hero "Si no te importa que te pregunte, Rika-san... Tu te ves como Domoco-chan."

# game/dialogs.rpy:418
translate spanish scene3_24649718:

    # "Rika-san smiled. She looked really pretty with one."
    "Rika-san sonrió, ella se veía bastante linda."

# game/dialogs.rpy:419
translate spanish scene3_63ae1e58:

    # r "Yeah! I love cosplaying as her, she's one of my favorite anime characters!"
    r "¡Si! ¡Me gusta hacer cosplay de ella, es mi favorito!"

# game/dialogs.rpy:420
translate spanish scene3_e1b3e1b9:

    # r "You've seen the show?"
    r "¿Haz visto el programa?"

# game/dialogs.rpy:421
translate spanish scene3_dc1e9f8c:

    # hero "I love it. It's a really funny anime!"
    hero "Me encanta. ¡Es un anime realmente gracioso!"

# game/dialogs.rpy:422
translate spanish scene3_7f21dac0:

    # s "Rika-chan can do a really good imitation of Domoco-chan!"
    s "¡Rika-chan puede hacer una buena imitación de Domoco-chan!"

# game/dialogs.rpy:423
translate spanish scene3_7020cc49:

    # r "Watch this!{p}{i}Pipirupiru pépérato!{/i}{p}How is it?"
    r "¡Observa esto!{p}{i}Pipirupiru pépérato!{/i}{p}¿Cómo estuvo?"

# game/dialogs.rpy:424
translate spanish scene3_30d87b21:

    # hero "That was pretty great! It looked just like the real thing!"
    hero "¡Eso fue grandioso! ¡Se veía justo como la real!"

# game/dialogs.rpy:425
translate spanish scene3_87588078:

    # n "Very true!"
    n "¡Muy cierto!"

# game/dialogs.rpy:426
translate spanish scene3_e0982078:

    # r "I'm glad you liked it!"
    r "¡Me da gusto que te haya gustado!"

# game/dialogs.rpy:429
translate spanish scene3_ed04f436:

    # r "Now it's time for the question every member has to answer before they join the club!"
    r "¡Ahora es tiempo para la pregunta que todo miembro tiene que responder antes de unirse al club!"

# game/dialogs.rpy:430
translate spanish scene3_322a4cd3:

    # r "Go for it, Sakura!"
    r "¡Ve por ello, Sakura!"

# game/dialogs.rpy:431
translate spanish scene3_5a20a3eb:

    # hero "What's with that face? It's creeping me out..."
    hero "¿Por qué esa cara? Da miedo..."

# game/dialogs.rpy:432
translate spanish scene3_041b4444:

    # s "Don't worry, %(stringhero)s-san. It's not a hard question."
    s "No te preocupes, %(stringhero)s-san. No es una pregunta difícil."

# game/dialogs.rpy:433
translate spanish scene3_9ae95b50:

    # s "Here it is:{p}What is your all-time favorite manga?"
    s "Aquí va:{p} ¿cuál es tu manga favorito?"

# game/dialogs.rpy:434
translate spanish scene3_29f0a12c:

    # hero "My all-time favorite one? Hm..."
    hero "¿Mi favorito de todos los tiempos? Hm...."

# game/dialogs.rpy:435
translate spanish scene3_2df7fab7:

    # "I started to think."
    "Comencé a pensar."

# game/dialogs.rpy:436
translate spanish scene3_27a6f658:

    # "There are a few that I really like. I enjoy a lot of ecchi manga, but if I say that, the girls would look at me funny... Especially Rika-san."
    "Hay unos pocos que me gustan. Disfruto mucho de los mangas ecchi, pero si digo eso, tal vez esas chicas me mirarían raro... Especialmente Rika-san."

# game/dialogs.rpy:437
translate spanish scene3_6275c604:

    # "But it's always good to be honest with girls as well..."
    "Pero ser honesto con las chicas también es bueno..."

# game/dialogs.rpy:441
translate spanish scene3_152e1c76:

    # "Let's tell the truth..."
    "Vamos a decirles la verdad..."

# game/dialogs.rpy:442
translate spanish scene3_f2942a1c:

    # "After all, maybe they don't know about this one..."
    "Después de todo, tal vez ellas no saben sobre este..."

# game/dialogs.rpy:443
translate spanish scene3_923bf6bc:

    # hero "I love {i}High School Samurai{/i}! That's my favorite one!"
    hero "¡Me encanta {i}High School Samurai{/i}! ¡¡Es grandioso!!"

# game/dialogs.rpy:448
translate spanish scene3_459d8d4d:

    # "They started to look funny, as I expected."
    "Ellas empezaron a mirarme raro, como lo esperaba."

# game/dialogs.rpy:449
translate spanish scene3_ea8e9dca:

    # "Rika-san looked pissed off again and Sakura-chan was blushing red."
    "Rika se miraba molesta otra vez y Sakura-chan se torno rojiza."

# game/dialogs.rpy:450
translate spanish scene3_10363d18:

    # "Nanami was about to burst into tears of laughter."
    "Nanami estaba apunto de partirse de la risa."

# game/dialogs.rpy:451
translate spanish scene3_cebb2b3d:

    # "Aw crap, maybe I made the wrong choice..."
    "Yeowch, tal vez tomé la decisión equivocada..."

# game/dialogs.rpy:452
translate spanish scene3_d83f0622:

    # r "You damn pervert! You're just like every other guy!"
    r "¡Ja! Eres realmente un chico. ¡Un verdadero pervertido!"

# game/dialogs.rpy:453
translate spanish scene3_244ff0c7:

    # n "Hahaha! You sure have guts, dude! Confessing something like that in front of Rika-nee!"
    n "¡Jajaja! ¡De seguro tienes agallas! ¡Confesando algo como eso a Rika-nee!"

# game/dialogs.rpy:454
translate spanish scene3_e1554201:

    # s "..."
    s "..."

# game/dialogs.rpy:456
translate spanish scene3_e08d3442:

    # "I was hanging my head in shame, but suddenly Sakura-san spoke."
    "Comencé a sentirme avergonzado, pero de pronto Sakura-san habló."

# game/dialogs.rpy:457
translate spanish scene3_76909f2d:

    # s "Actually, Rika-chan..."
    s "En realidad, Rika-chan..."

# game/dialogs.rpy:458
translate spanish scene3_4570740d:

    # s "{i}High School Samurai{/i} isn't that bad at all."
    s "{i}High School Samurai{/i} no es malo para nada."

# game/dialogs.rpy:459
translate spanish scene3_6bb1ac2f:

    # s "You see, the hero isn't a typical boy like most harem manga. He's actually very heroic!"
    s "Verás, el héroe no es el usual chico de los mangas harem. Él se parece más a un héroe real."

# game/dialogs.rpy:460
translate spanish scene3_b2661f33:

    # s "He has a great sense of honor and all the monologues he gives are very righteous and noble."
    s "Él tiene un gran sentido del honor y todos los monólogos que dice son correctos y épicos."

# game/dialogs.rpy:461
translate spanish scene3_8effa9e6:

    # s "I admire heroes like that."
    s "Admiro un héroe como ese."

# game/dialogs.rpy:462
translate spanish scene3_f902c5fb:

    # s "And by the way, the hero himself isn't really a pervert, since every ecchi scene where he's involved happens accidentally."
    s "Y por cierto, el héroe no es realmente un pervertido, ya que todas las escenas ecchi donde esta involucrado pasa accidentalmente."

# game/dialogs.rpy:464
translate spanish scene3_0ddbdce1:

    # r "Really?"
    r "¿En serio?"

# game/dialogs.rpy:465
translate spanish scene3_68f8b283:

    # n "I've seen some episodes. That guy reminds me of {i}Nanda no Ryu{/i}, who's always resolving conflicts with great punchlines and fighting when necessary."
    n "He visto algunos episodios. Ese chico me recuerda a {i}Nanda no Ryu{/i}, siempre resolviendo conflictos con frases geniales y peleando si fuera necesario."

# game/dialogs.rpy:466
translate spanish scene3_ced41922:

    # n "Sure, it's ecchi sometimes, but it's actually a nice show to watch."
    n "Claro que es ecchi a veces, pero es agradable de ver."

# game/dialogs.rpy:467
translate spanish scene3_9816aa54:

    # r "Well... I guess it's okay, then..."
    r "Bueno... Supongo que esta bien entonces..."

# game/dialogs.rpy:471
translate spanish scene3_eca282df:

    # "Let's not take any risks..."
    "No tomemos ningún riesgo..."

# game/dialogs.rpy:472
translate spanish scene3_6ba925d2:

    # hero "I love {i}Rosario Maiden{/i}. It's a great manga!"
    hero "Me encanta {i}Rosario Maiden{/i}. ¡Es un gran manga!"

# game/dialogs.rpy:477
translate spanish scene3_21a5901b:

    # "The three girls stared at me."
    "Las tres chicas se quedaron mirandome."

# game/dialogs.rpy:478
translate spanish scene3_7f0ae732:

    # "Finally, they all smiled. Rika-san spoke."
    "Finalmente, Rika-san habló y todas ellas sonrieron."

# game/dialogs.rpy:483
translate spanish scene3_79487764:

    # r "I love that one too!"
    r "Me encanta ese también."

# game/dialogs.rpy:484
translate spanish scene3_ca38b9e6:

    # r "The art style is superb and the story is great. It's a pretty great series."
    r "Las gráficas son increíbles, la historia grandiosa... Es un buen manga."

# game/dialogs.rpy:485
translate spanish scene3_c24a4394:

    # s "I like it too."
    s "Me gusta ese manga también."

# game/dialogs.rpy:486
translate spanish scene3_3d29180a:

    # s "It's not my favorite but I like it."
    s "No es mi favorita pero me gusta."

# game/dialogs.rpy:487
translate spanish scene3_1324b4b2:

    # s "I especially love the dolls. I wish I could have one like that to take home!"
    s "Especialmente me gustan mucho las muñecas. ¡Desearía tener uno así en casa!"

# game/dialogs.rpy:488
translate spanish scene3_29de2f7c:

    # hero "Yeah! They're really cute! Especially the green one!"
    hero "¡Si! ¡Ellas son muy lindas! ¡Especialmente la de verde!"

# game/dialogs.rpy:489
translate spanish scene3_42f3f6f5:

    # s "The green one is kinda mean but it's still my favorite one!"
    s "¡La de verde es algo mala pero es mi favorita!"

# game/dialogs.rpy:490
translate spanish scene3_bf59c037:

    # n "I'm not too into the dolls. I prefer the protagonist. He's so handsome!"
    n "Yo no estoy en eso de las muñecas. Prefiero al protagonista. ¡Es lindo!"

# game/dialogs.rpy:491
translate spanish scene3_f7e6d2b6:

    # r "I prefer the red doll. She's just so... dominant... hehe! {image=heart.png}"
    r "Yo prefiero la muñeca de roja. Ella es simplemente... Dominante... ¡Jeje! {image=heart.png}"

# game/dialogs.rpy:492
translate spanish scene3_40c59aa9:

    # "Rika-san gave me an evil look. What the heck could she be thinking? I'm confused and a bit scared."
    "Rika-san me guiñó. Me pregunto qué es lo que esta pensando. Estoy confundido."

# game/dialogs.rpy:493
translate spanish scene3_745f17e5:

    # "But it looks like she's starting to like me."
    "Pero parece que le estoy comenzando a agradar."

# game/dialogs.rpy:494
translate spanish scene3_d79ab191:

    # "Well, at least I hope so..."
    "Bueno, eso espero..."

# game/dialogs.rpy:502
translate spanish scene3_66851b4f:

    # "It was almost time to head home."
    "La noche llegó y todos necesitabamos ir a casa."

# game/dialogs.rpy:504
translate spanish scene3_a7a81522:

    # "Rika-chan and Sakura-chan left to head home. I ended up staying with Nanami-chan, ready to challenge her."
    "Rika y Sakura se fueron de la escuela y yo estuve con Nanami, listo para retarla."

# game/dialogs.rpy:509
translate spanish scene3_e5863c5e:

    # n "So, are you ready, dude?"
    n "Entonces, ¿estas listo?"

# game/dialogs.rpy:510
translate spanish scene3_30e27e13:

    # hero "Of course I am. I was born ready!"
    hero "Por su puesto que lo estoy. ¡Nací listo!"

# game/dialogs.rpy:511
translate spanish scene3_fa127982:

    # n "I meant, ready to get your butt kicked?"
    n "Quiero decir, ¿listo para que tu trasero sea pateado?"

# game/dialogs.rpy:512
translate spanish scene3_b87b3a25:

    # hero "Only if you're ready to get yours kicked first!"
    hero "¡Solo si tú estas lista para que el tuyo sea pateado primero!"

# game/dialogs.rpy:513
translate spanish scene3_7cf5de36:

    # "I pretty much know she's going to win the game. But I'm not gonna let win without a challenge. Although, she'll still call me a {i}dude{/i} afterwards."
    "Estoy muy seguro que ella va a ganar el juego. Pero no voy a dejar que ella me derrote y me llame un {i}tipo{/i} depués."

# game/dialogs.rpy:514
translate spanish scene3_c3b34a29:

    # "I gotta defend my pride, eh?"
    "Debo defender mi orgullo, ¿eh?"

# game/dialogs.rpy:518
translate spanish scene3_d0713ddb:

    # "She turned on the TV and the NATO GEO after inserting Lead of Fighters '96 cartridge."
    "Ella encendió la TV y el NATO GEO después de colocar el cartucho de Lead of Fighters '96."

# game/dialogs.rpy:519
translate spanish scene3_3716d683:

    # hero "It's pretty amazing the club has this gaming console here! That's one of the most expensive systems available!"
    hero "¡Es increíble que tengas esta consola de juegos aquí! ¡Esa joya es una de los más caros sistemas disponible!"

# game/dialogs.rpy:520
translate spanish scene3_8ed428db:

    # n "Actually, it's mine. I lent to the club to use."
    n "Es mía. Se lo  presté al club."

# game/dialogs.rpy:521
translate spanish scene3_8a31c669:

    # n "Are you familiar with the {i}Lead of Fighters{/i} series?"
    n "¿Conoces las series de {i}Lead of Fighters{/i}?"

# game/dialogs.rpy:522
translate spanish scene3_165263c8:

    # hero "Yeah, actually this game is my favorite one so far."
    hero "La verdad este juego de la serie es mi favorita hasta ahora."

# game/dialogs.rpy:523
translate spanish scene3_5ff36a65:

    # hero "It's my favorite game too, but I could only play it in the arcades in Tokyo."
    hero "Incluso puedo decir que es mi juego favorito de todos los tiempos pero solo pude jugarlo en las maquinitas en Tokyo."

# game/dialogs.rpy:524
translate spanish scene3_facc5c5d:

    # n "I see."
    n "Ya veo."

# game/dialogs.rpy:525
translate spanish scene3_9be30a14:

    # n "So, who's your favorite character?"
    n "Entonces, ¿Cuál es tu personaje favorito?"

# game/dialogs.rpy:526
translate spanish scene3_913605a7:

    # n "I usually like to pick the cute girls."
    n "Normalmente elijo chicas lindas."

# game/dialogs.rpy:527
translate spanish scene3_3cba88d7:

    # n "In most fighting games they rarely look threating, but when you know how to play with them, they can be deadly!"
    n "En los juegos de pelea ellas raramente son una amenaza pero cuando sabes como usarlas, ¡ellas pueden ser mortales!"

# game/dialogs.rpy:528
translate spanish scene3_978b1237:

    # hero "I'm not sure what my favorite is... I usually pick randomly."
    hero "No estoy seguro... Yo usualmente elijo por la impresión que me dan."

# game/dialogs.rpy:529
translate spanish scene3_7e18c9db:

    # n "If you're a beginner, you should take this guy, here. {w}He's good for be-{nw}"
    n "Si eres un principiante, deberías escoger este de aquí. {w}Él es bueno para prin-{nw}"

# game/dialogs.rpy:530
translate spanish scene3_0d60b236:

    # hero "Hey I'm not a beginner! I was one of the best at my previous school!"
    hero "¡Oye, yo no soy ningún principiante! ¡Era uno de los mejores en mi anterior escuela!"

# game/dialogs.rpy:531
translate spanish scene3_72095650:

    # n "Oh yeah? Let's see about that!"
    n "¡Vamos a ver si es cierto!"

# game/dialogs.rpy:532
translate spanish scene3_704fb29f:

    # "So we selected our fighters and started the virtual brawl."
    "Así que elegimos a nuestros peleadores y comenzamos a batallar virtualmente."

# game/dialogs.rpy:533
translate spanish scene3_c8bd5427:

    # n "So you're from Tokyo?"
    n "Entonces, ¿eres de Tokyo?"

# game/dialogs.rpy:534
translate spanish scene3_6e3125fc:

    # hero "Yeah. I moved here because my parents wanted to open a store in a quiet, peaceful village."
    hero "Sip. Me mudé aquí a causa de mis padres quienes querían abrir una tienda en un tranquilo y pacífico pueblo."

# game/dialogs.rpy:535
translate spanish scene3_352bd956:

    # n "A store? You mean...the grocery store at the G. street?"
    n "¿Una tienda? Quieres decir... ¿Es la tienda de abarrotes en la Calle G.?"

# game/dialogs.rpy:536
translate spanish scene3_cf6c4656:

    # hero "How do you know?"
    hero "¿Cómo lo supiste?"

# game/dialogs.rpy:537
translate spanish scene3_3b26e73f:

    # n "I... heard it was looking for new owners."
    n "Yo... Escuché que estaban buscando nuevos propietarios."

# game/dialogs.rpy:538
translate spanish scene3_9bf86260:

    # n "My brother worked there for a while, back then."
    n "Mi hermano trabajó ahi por un tiempo."

# game/dialogs.rpy:540
translate spanish scene3_28e4c374:

    # "{i}Bing, poof, kick, bzzzz\nK O !!!{/i}"
    "{i}Bing, poof, patada, bzzzz\n¡¡¡K. O. !!!{/i}"

# game/dialogs.rpy:541
translate spanish scene3_cd3c8571:

    # "Of course, she beat the crap out of me."
    "Por supuesto que ella me venció."

# game/dialogs.rpy:542
translate spanish scene3_7944ba15:

    # "I'm not sure if I lost because of her skills or because we're talking at the same time.{p}It might be both..."
    "No estoy seguro si fue debido a sus habilidades o porque estábamos hablando al mismo tiempo.{p} O tal vez por ambos..."

# game/dialogs.rpy:543
translate spanish scene3_66644a5e:

    # "We chose different fighters and started a new fight."
    "Elegimos diferentes peleadores y empezamos una nueva ronda."

# game/dialogs.rpy:544
translate spanish scene3_b7629213:

    # hero "So you have a brother?"
    hero "Entonces, ¿tienes un hermano?"

# game/dialogs.rpy:545
translate spanish scene3_c1b9564a:

    # n "Yep, an older brother.{p}He's 27."
    n "Sip, un hermano mayor,{p}él tiene 27."

# game/dialogs.rpy:546
translate spanish scene3_6b9e255a:

    # hero "Is he any good at games?"
    hero "¿Es bueno en los videojuegos?"

# game/dialogs.rpy:547
translate spanish scene3_25066c43:

    # n "Not really."
    n "No realmente."

# game/dialogs.rpy:548
translate spanish scene3_456c3557:

    # n "I don't think it's from his generation."
    n "Pienso que no es de su generación."

# game/dialogs.rpy:549
translate spanish scene3_5f66a4a4:

    # hero "That's possible. My sister is almost as old as him and it's not her thing either."
    hero "Eso es posible. Mi hermana tiene casí la misma edad y los videojuegos no son lo suyo."

# game/dialogs.rpy:550
translate spanish scene3_a2e58feb:

    # n "Oh yeah, your sister... What's she doing?"
    n "Ah si, tu hermana... ¿Qué es lo que hace?"

# game/dialogs.rpy:551
translate spanish scene3_e1b6d146:

    # hero "She works as a banker."
    hero "Ella trabaja en un banco."

# game/dialogs.rpy:552
translate spanish scene3_30b55ef7:

    # n "That sounds boring."
    n "Eso suena aburrido."

# game/dialogs.rpy:553
translate spanish scene3_a5630de6:

    # hero "I know right! I don't understand how she can do something like that without getting bored."
    hero "¡Verdad que si! No entiendo cómo puede hacer eso y no aburrirse."

# game/dialogs.rpy:555
translate spanish scene3_28e4c374_1:

    # "{i}Bing, poof, kick, bzzzz\nK O !!!{/i}"
    "{i}Bing, poof, patada, bzzzz\n¡¡¡K O !!!{/i}"

# game/dialogs.rpy:556
translate spanish scene3_821cdbe8:

    # "And yet again, I lost."
    "Y otra vez, perdí."

# game/dialogs.rpy:557
translate spanish scene3_b0f56878:

    # hero "Alright. No more Mister Nice Guy, I'm gonna fight for real!"
    hero "Esta bien. No más señor amable, ¡voy a pelear de verdad!"

# game/dialogs.rpy:558
translate spanish scene3_7a9ca5b7:

    # n "It's about time..."
    n "Ya era tiempo..."

# game/dialogs.rpy:559
translate spanish scene3_9f960747:

    # "Nanami gave me a mischievous grin.{p}That looked cute but kinda creepy too."
    "Nanami me dio una sonrisa pícara.{p}Se miraba linda pero algo espeluznante."

# game/dialogs.rpy:560
translate spanish scene3_e6598e64:

    # "She probably knew I was already giving it my all..."
    "Ella probablemente ya sabía que lo estaba dando todo..."

# game/dialogs.rpy:561
translate spanish scene3_ac835df6:

    # "We played through the next round without a single word."
    "Jugamos la siguiente ronda sin hablar."

# game/dialogs.rpy:562
translate spanish scene3_a3279e7e:

    # "I was finally was gaining an advantage! She stayed less and less calm as I was slowly winning."
    "Finalmente estaba teniendo la ventaja. Ella se estaba impacientando mientras ganaba lentamente."

# game/dialogs.rpy:563
translate spanish scene3_289633fa:

    # "And then finally, using a secret combo I knew well..."
    "Y finalmente, usando un combo secreto que conocía bien..."

# game/dialogs.rpy:565
translate spanish scene3_28e4c374_2:

    # "{i}Bing, poof, kick, bzzzz\nK O !!!{/i}"
    "{i}Bing, poof, patada, bzzzz\n¡¡¡K O !!!{/i}"

# game/dialogs.rpy:566
translate spanish scene3_ed61300c:

    # "Nanami stared at me with a sad face and began to cry like a small child."
    "Nanami me miró con una cara triste y comenzó a llorar como una pequeña niña."

# game/dialogs.rpy:567
translate spanish scene3_15a85014:

    # "It didn't look real at all though. There were no tears or anything, she was just moaning like a baby."
    "Aunque no parecía real. No había lagrimas o algo, ella solo estaba gimoteando como un bebé."

# game/dialogs.rpy:568
translate spanish scene3_decd73c2:

    # hero "Hey hey, calm down! I'm sorry... I'll let you win next time!"
    hero "¡Oye oye, calmate! Lo siento... ¡Te dejaré ganar la próxima vez!"

# game/dialogs.rpy:569
translate spanish scene3_ab688811:

    # "She instantly stopped crying and made her cute/creepy grin."
    "Ella instantáneamente dejó de llorar e hizo su linda-espeluznante sonrisa."

# game/dialogs.rpy:570
translate spanish scene3_c6891b22:

    # n "Works everytime! {image=heart.png}"
    n "¡Eso funciona todo el tiempo!  {image=heart.png}"

# game/dialogs.rpy:571
translate spanish scene3_56b514db:

    # hero "Oh man..."
    hero "¡Eres una!"

# game/dialogs.rpy:572
translate spanish scene3_2d2950d5:

    # "I started laughing."
    "Pero en vez de estar enojado, comencé a reír."

# game/dialogs.rpy:573
translate spanish scene3_f2d7328e:

    # "Nanami made a goofy pouting face which made me laugh even more."
    "Nanami hizo un puchero lo cual me hizo reír mucho más."

# game/dialogs.rpy:574
translate spanish scene3_05ab44b2:

    # n "Let's fight again!"
    n "¡Vamos a pelear otra vez!"

# game/dialogs.rpy:575
translate spanish scene3_241aaaf7:

    # hero "Alright, alright, little one!"
    hero "¡Esta bien, esta bien, pequeña!"

# game/dialogs.rpy:576
translate spanish scene3_52c6a554:

    # n "I'm not little!"
    n "¡No soy pequeña!"

# game/dialogs.rpy:581
translate spanish scene3_176ddd63:

    # "We played for a while, but eventually we both needed to get home."
    "Seguimos jugando por un tiempo, entonces nos teníamos que ir a casa."

# game/dialogs.rpy:582
translate spanish scene3_ce50b0ab:

    # n "That was fun! I hope we can do this again soon!"
    n "¡Eso fue divertido! ¡Espero hacer esto otra vez y pronto!"

# game/dialogs.rpy:583
translate spanish scene3_24edf3d9:

    # hero "Anytime, Nanami-chan."
    hero "Cuando quieras, Nanami-chan."

# game/dialogs.rpy:585
translate spanish scene3_7074277a:

    # n "Oh by the way, here's my e-mail."
    n "Oh, por cierto, aquí esta mi correo electrónico."

# game/dialogs.rpy:586
translate spanish scene3_cb953bdb:

    # "She gave me a tiny piece of paper with an e-mail written on it. There was a small heart where the @ sign was supposed to be."
    "Ella me dio un pequeño papel con un correo escrito en ella y un pequeño corazón alrededor de la @."

# game/dialogs.rpy:587
translate spanish scene3_a99fbe92:

    # "I blushed a bit."
    "Me ruboricé un poco."

# game/dialogs.rpy:588
translate spanish scene3_9f566895:

    # n "It's my personal e-mail... Just in case..."
    n "Es mi correo personal... Solo por si acaso..."

# game/dialogs.rpy:589
translate spanish scene3_712996f5:

    # hero "Thanks, Nanami-chan."
    hero "Gracias, Nanami-chan."

# game/dialogs.rpy:592
translate spanish scene3_05df8eb4:

    # n "See you tomorrow, %(stringhero)s-senpai! {image=heart.png}"
    n "Nos vemos mañana, ¡%(stringhero)s-senpai! {image=heart.png}"

# game/dialogs.rpy:595
translate spanish scene3_759e6781:

    # "She left the school, hopping happily like a kid on the street."
    "Ella se fue de la escuela, dando saltitos de felicidad como un niño en la calle."

# game/dialogs.rpy:596
translate spanish scene3_37a9b710:

    # "Dang, for a gaming champion...she's really something."
    "Vaya que ella es linda para ser una campeona de videojuegos..."

# game/dialogs.rpy:599
translate spanish scene3_68c44298:

    # "The sun was starting to set."
    "La noche estaba al caer."

# game/dialogs.rpy:600
translate spanish scene3_6f9822aa:

    # "Rika-chan and Nanami-chan take different routes to get home, so only Sakura-chan and I ended up walking home together."
    "Rika y Nanami tomaron diferentes caminos a los nuestros, así que Sakura y yo regresamos a nuestras casas juntos."

# game/dialogs.rpy:606
translate spanish scene3_563d938f:

    # s "So you like seinen manga?"
    s "Así que te gustan los mangas seinen, ¿cierto?"

# game/dialogs.rpy:607
translate spanish scene3_04f08300:

    # hero "Well yes..."
    hero "Bueno, si..."

# game/dialogs.rpy:608
translate spanish scene3_14e7e4b7:

    # hero "On the same subject, you said {i}Rosario Maiden{/i} wasn't your favorite. Which one is your favorite, then?"
    hero "Hablando sobre esto, dijiste que {i}Rosario Maiden{/i} no era tu favorito. ¿Cuál es tu favorito entonces?"

# game/dialogs.rpy:609
translate spanish scene3_41af6c01:

    # s "In the seinen type? It's {i}Desu Motto{/i}."
    s "¿En el tipo seinen? Es {i}Desu Motto{/i}."

# game/dialogs.rpy:610
translate spanish scene3_a0282b9a:

    # hero "You like {i}Desu Motto{/i}?"
    hero "¿Te gusta {i}Desu Motto{/i}?"

# game/dialogs.rpy:611
translate spanish scene3_157e9fd5:

    # "I was kinda surprised."
    "Estaba sorprendido."

# game/dialogs.rpy:612
translate spanish scene3_3593807d:

    # "{i}Desu Motto{/i} is a story about a living book that eats human guts. It's full of all kinds of blood and gore, more so than in {i}Nanda no Ryu{/i}."
    "{i}Desu Motto{/i} es una historia sobre un libro viviente que come intestinos humanos, con grandes explosiones de sangre, más grandes que en {i}Nanda no Ryu{/i}."

# game/dialogs.rpy:613
translate spanish scene3_976cd68f:

    # "And there's a magical girl who pursues this book with a chainsaw in order to destroy it."
    "Y hay una chica mágica quien persigue ese libro con una motosierra para destruirlo."

# game/dialogs.rpy:614
translate spanish scene3_e7ee8e3e:

    # "Anyway, it's not the kind of thing girls her age would watch and like."
    "De cualquier manera, no es tipo de cosas que una chica como ella debería de mirar y tomarle gusto."

# game/dialogs.rpy:615
translate spanish scene3_2ae24650:

    # "Appearances can be surprising, sometimes..."
    "Las apariencias pueden ser sorprendentes a veces..."

# game/dialogs.rpy:616
translate spanish scene3_28bb09a7:

    # hero "I like it too, but there's too much gore for me."
    hero "También me gusta, pero lo encuentro muy sangriento para mí."

# game/dialogs.rpy:617
translate spanish scene3_ed0c1175:

    # hero "I can't stand too much blood in an anime. I remember being traumatized by {i}Elfic Leaves{/i}..."
    hero "No puedo soportar ver mucha sangre en un anime. Recuerdo haber sido traumatizado por {i}Elfic Leaves{/i}..."

# game/dialogs.rpy:618
translate spanish scene3_58b557f4:

    # "{i}Elfic Leaves{/i} is known to be the most violent and brutal anime in all of japanimation history. It's so gory, it was never broadcast on TV and it only exists on VHS tapes."
    "{i}Elfic Leaves{/i} fue conocido como el más violento y sanguinario anime en toda la historia de la animación japonesa. Tan sangriento que nunca se ha vuelto a transmitir en la TV y solo existe en cintas VHS."

# game/dialogs.rpy:619
translate spanish scene3_64b818b7:

    # "I remember watching it by accident, about four years ago. It was on a VHS tape in my sister's room..."
    "Recuerdo haberlo visto por accidente, hace 4 años. Encontré una cinta en el cuarto de mi hermana..."

# game/dialogs.rpy:621
translate spanish scene3_74bc980e:

    # s "I can understand that. That one is really hardcore."
    s "Entiendo. Ese es uno realmente extremo."

# game/dialogs.rpy:622
translate spanish scene3_fe827394:

    # s "Though I think it's okay, since they're only drawings. I don't think I could stand it either if it was real."
    s "Aunque pienso que está bien, debido a que solo son dibujos. Pienso que no lo podría soportar si fuera real."

# game/dialogs.rpy:623
translate spanish scene3_189e996b:

    # hero "Same for me."
    hero "Lo mismo para mí."

# game/dialogs.rpy:625
translate spanish scene3_1f00d77e:

    # "Sakura stared at me for a moment."
    "Sakura me miró intensamente por un momento."

# game/dialogs.rpy:626
translate spanish scene3_24848926:

    # "It's like she was wondering about something... Or that she came a conclusion about something..."
    "Como si se estuviera preguntándose algo... O que ella se ha percatado de algo..."

# game/dialogs.rpy:627
translate spanish scene3_47159e79:

    # s "...{w}{i}Rosario Maiden{/i} isn't actually favorite manga ever, right?"
    s "...{w}{i}Rosario Maiden{/i} no es tu favorito de todos los tiempos, ¿verdad?"

# game/dialogs.rpy:628
translate spanish scene3_ef3bce8f:

    # "Gah!!"
    "¡¡Guh!!"

# game/dialogs.rpy:629
translate spanish scene3_bb685cb0:

    # "How'd she make an incredible guess like that?!"
    "¡Esta chica es increíblemente buena adivinando!"

# game/dialogs.rpy:630
translate spanish scene3_e633313f:

    # "I guess I couldn't hide it from her anymore..."
    "Supongo que no lo podía ocultarlo más..."

# game/dialogs.rpy:631
translate spanish scene3_ae8949a7:

    # hero "You got me..."
    hero "Me has descubierto..."

# game/dialogs.rpy:632
translate spanish scene3_8137fe91:

    # hero "My favorite is {i}High School Samurai{/i}..."
    hero "Mi favorito es {i}High School Samurai{/i}..."

# game/dialogs.rpy:634
translate spanish scene3_5c82e516:

    # s "Really?"
    s "¿En serio?"

# game/dialogs.rpy:637
translate spanish scene3_d2de1953:

    # s "So you like {i}High School Samurai{/i}?"
    s "¿Entonces te gusta {i}High School Samurai{/i}?"

# game/dialogs.rpy:638
translate spanish scene3_3d802b0c:

    # "I turned my head away from her a bit."
    "Giré mi cara a otro lado."

# game/dialogs.rpy:639
translate spanish scene3_6449a5e4:

    # hero "Thanks again for saving me back there... Looks like Rika-san doesn't like that kind of manga and anime."
    hero "Gracias nuevamente por salvarme... Parece que Rika-san no le gusta ese tipo de manga y anime."

# game/dialogs.rpy:641
translate spanish scene3_bd58aae1:

    # s "It's okay. Rika-chan probably already forgot about it."
    s "Esta bien. Rika-chan probablemente ya lo haya olvidado."

# game/dialogs.rpy:642
translate spanish scene3_5798ec89:

    # s "And to be honest...{p}I really enjoy {i}High School Samurai{/i}!"
    s "Y para ser honesta...{p}¡Yo también disfruto de {i}High School Samurai{/i}!"

# game/dialogs.rpy:643
translate spanish scene3_b3ba8ed1:

    # hero "You do?!"
    hero "¿De veras?"

# game/dialogs.rpy:644
translate spanish scene3_d207e06f:

    # s "Yes!"
    s "¡Si!"

# game/dialogs.rpy:647
translate spanish scene3_2098e30f:

    # s "It's a great manga... The hero is really handsome."
    s "Es un gran manga... El héroe es muy atractivo."

# game/dialogs.rpy:648
translate spanish scene3_2f77db13:

    # s "And... the girls are very cute, too..."
    s "Y... Las chicas son muy lindas también..."

# game/dialogs.rpy:649
translate spanish scene3_1ae45319:

    # hero "Ah?"
    hero "¿Ah?"

# game/dialogs.rpy:650
translate spanish scene3_22eeaf4a:

    # s "Yes, especially the main one. I wish I could look like her someday..."
    s "Si, especialmente la protagonista. Desearía parecerme a ella algún día..."

# game/dialogs.rpy:651
translate spanish scene3_1171cfe9:

    # hero "You're already pretty cute though, Sakura-san."
    hero "Aunque tú eres en realidad tan linda como ella, Sakura-san."

# game/dialogs.rpy:652
translate spanish scene3_aa49989e:

    # "I said that without thinking."
    "Lo dije sin pensarlo."

# game/dialogs.rpy:653
translate spanish scene3_f463b1f2:

    # "I was on the verge of taking my words back until she replied."
    "Estaba apunto de retractarme hasta que ella respondió."

# game/dialogs.rpy:655
translate spanish scene3_1c22c637:

    # s "...Do you really think so?"
    s "...Tú...¿Tú lo piensas?"

# game/dialogs.rpy:656
translate spanish scene3_b836a32c:

    # "Her face turned red out of embarrassment."
    "Se ha sonrojado de la vergüenza."

# game/dialogs.rpy:657
translate spanish scene3_87546ecc:

    # "So cute!"
    "¡Es tan linda!"

# game/dialogs.rpy:658
translate spanish scene3_4baf7e22:

    # "It was so cute that I felt a little embarrassed too."
    "Tan linda que me sentí avergonzado también."

# game/dialogs.rpy:659
translate spanish scene3_1c0b1c3e:

    # hero "Well y-yeah... You're a pretty girl, Sakura-san..."
    hero "Bueno s-si... Eres realmente una chica bonita, Sakura-san..."

# game/dialogs.rpy:660
translate spanish scene3_557b9c7c:

    # s ". . ."
    s ". . ."

# game/dialogs.rpy:661
translate spanish scene3_5d8f36c3:

    # s "Thank you... %(stringhero)s-kun..."
    s "Gracias... %(stringhero)s-kun..."

# game/dialogs.rpy:662
translate spanish scene3_2758fdb5:

    # "She hesitated for awhile before thanking me."
    "Ella titubeó un largo tiempo antes de agradecerme."

# game/dialogs.rpy:663
translate spanish scene3_d3083c53:

    # "Was that her being timid? Probably."
    "¿Estaba siendo tímida? Probablemente."

# game/dialogs.rpy:668
translate spanish scene3_88d97835:

    # "We finally reached the crossroads of our respective houses."
    "Finalmente llegamos a la intersección de nuestras respectivas casas."

# game/dialogs.rpy:669
translate spanish scene3_95f36d4e:

    # hero "Sakura-chan, since I don't know the path to school very well, do you mind if we go to school together every day?"
    hero "Sakura-chan, ya que no conozco muy bien el camino a la escuela, ¿te importaría si fueramos a la escuela juntos diariamente?"

# game/dialogs.rpy:670
translate spanish scene3_c1d7d0f9:

    # s "Sure! I'll wait for you here tomorrow."
    s "¡Claro! Te esperaré aquí mañana."

# game/dialogs.rpy:671
translate spanish scene3_139aa776:

    # hero "Alright, thank you, Sakura-chan!"
    hero "Bien, muchas gracias, ¡Sakura-chan!"

# game/dialogs.rpy:672
translate spanish scene3_3257668c:

    # s "You're welcome..."
    s "De nada..."

# game/dialogs.rpy:674
translate spanish scene3_00bab8bf:

    # s "See you tomorrow, %(stringhero)s-kun!"
    s "¡Nos vemos mañana, %(stringhero)s-kun!"

# game/dialogs.rpy:675
translate spanish scene3_6215ba95:

    # hero "See you!"
    hero "¡Nos vemos!"

# game/dialogs.rpy:678
translate spanish scene3_a46fbd6c:

    # "I watched her walk down the street."
    "La miré caminando por la calle."

# game/dialogs.rpy:679
translate spanish scene3_5e5c6211:

    # "Her hips were swaying a little with each step, her hair moving with the summer wind."
    "Su pequeño trasero estaba balanceándose un poco en cada paso, con su cabello moviéndose por el viento del verano."

# game/dialogs.rpy:681
translate spanish scene3_862f5cfa:

    # "The cicadas were crying, giving an atmosphere of something dreamy and peaceful with the hot summer weather."
    "Las cigarras cantaban, creando una atmósfera pacífica y de ensueño acompañado del calor del verano."

# game/dialogs.rpy:684
translate spanish scene3_82417432:

    # "Before going to bed, I decided to get a breath of fresh air in front of the house."
    "Antes de ir a la cama, decidí tomar un poco del aire nocturno frente a la casa."

# game/dialogs.rpy:685
translate spanish scene3_be4997dc:

    # "I must admit, this place is beautiful."
    "Este lugar es hermoso, lo admito.."

# game/dialogs.rpy:686
translate spanish scene3_ba75a6ad:

    # "For sure, finding everything you want isn't as convenient as in Tokyo..."
    "Claro que encontrar todo lo que quieres no es tan conveniente como en Tokyo..."

# game/dialogs.rpy:687
translate spanish scene3_d9ac4ff7:

    # "But gee,... It's so good to breath real air instead of smog..."
    "Pero... Es tan bueno respirar aire real en vez de un aire contaminado..."

# game/dialogs.rpy:688
translate spanish scene3_8534ca42:

    # ". . ."
    ". . ."

# game/dialogs.rpy:691
translate spanish scene3_f6793b5b:

    # "What's that?..."
    "¿Qué es eso?"

# game/dialogs.rpy:692
translate spanish scene3_41539cb7:

    # "Sounds like someone is playing the violin."
    "Parece como alguien tocando el violin en el pueblo."

# game/dialogs.rpy:693
translate spanish scene3_f3b91659:

    # "It's kinda appropriate for a night like this..."
    "Que apropiado para una noche como esta..."

# game/dialogs.rpy:694
translate spanish scene3_8534ca42_1:

    # ". . ."
    ". . ."

# game/dialogs.rpy:695
translate spanish scene3_f07946fa:

    # "I think I'm starting enjoying my new life here."
    "Estoy pensando en que comienzo a disfrutar de mi nueva vida, al fin."

# game/dialogs.rpy:719
translate spanish scene4_840498c7:

    # centered "{size=+35}CHAPTER 2\nA fine date{fast}{/size}"
    centered "{size=+35}CAPÍTULO 2\nUna fina cita{fast}{/size}"

# game/dialogs.rpy:730
translate spanish scene4_46a27380:

    # write "Dear sister."
    write "Querida hermana."

# game/dialogs.rpy:732
translate spanish scene4_d033b41a:

    # write "How are things?{w} Mom, dad, and I have arrived at our new home.{w} Things here are way different than in Tokyo. It's peaceful, but there's nothing to do in the village.{w} You need to take the train to find something to do in the nearest town."
    write "¿Cómo van las cosas?{w} Yo y mis padres hemos llegado a nuestro nuevo hogar.{w} Las cosas son tan diferentes de Tokyo. Es pacífico y no hay nada que hacer en el pueblo.{w} Uno necesita tomar el tren para encontrar algo que hacer en la ciudad más cercana."

# game/dialogs.rpy:734
translate spanish scene4_dcc0ac1c:

    # write "I just came back from my first day at my new school. I've already made three friends!"
    write "Acabo de regresar de mi primer día de clases. Hice tres amigas ahí."

# game/dialogs.rpy:736
translate spanish scene4_9b62289a:

    # write "Things have changed a lot, but I think I'll get used to it soon enough."
    write "Las cosas han cambiado mucho, pero pienso que me adaptaré pronto."

# game/dialogs.rpy:737
translate spanish scene4_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:739
translate spanish scene4_66476017:

    # write "We're all part of a manga club at school.{w} Since they're all girls, can you recommend any shoujo manga that I can read and talk with them about?"
    write "Mis tres amigas y yo estamos en el club de manga en la escuela.{w} Ya que son mujeres, ¿hay algún manga shoujo que pueda hablar con ellas?"

# game/dialogs.rpy:741
translate spanish scene4_c54efb3e:

    # write "I hope you'll visit us someday soon."
    write "Espero que nos visites en nuestra nueva casa algún día."

# game/dialogs.rpy:743
translate spanish scene4_f60fe613:

    # write "Your dear brother{p}%(stringhero)s"
    write "Tu querido hermano{p}%(stringhero)s"

# game/dialogs.rpy:744
translate spanish scene4_76b2fe88_1:

    # nvl clear
    nvl clear

# game/dialogs.rpy:751
translate spanish scene4_0b847f9b:

    # "The week passed by quickly and it's already Friday."
    "La semana pasó y ahora es Viernes."

# game/dialogs.rpy:752
translate spanish scene4_a3a675c2:

    # "Spending time with the manga club is great."
    "Esa semana con las chicas fue grandioso."

# game/dialogs.rpy:753
translate spanish scene4_58d076ca:

    # "Sakura is a little shy, but very sweet.{p}Rika still thinks I'm a pervert..."
    "Sakura-chan siempre fue linda y tímida.{p}Rika-chan siempre estaba pensando que yo era un pervertido..."

# game/dialogs.rpy:755
translate spanish scene4_8367dc19:

    # "Nanami is playful and competitive. Playing video games with her is always fun."
    "Nanami y yo aún jugamos videojuegos juntos, en su mayoría en línea después de la escuela."

# game/dialogs.rpy:757
translate spanish scene4_0cbaa7bb:

    # "Nanami and I even started to play games online a little bit!"
    "Descubrí que Nanami esta metida en los videojuegos así que comenzamos a jugar juntos un poco en línea."

# game/dialogs.rpy:758
translate spanish scene4_4995ec74:

    # "I know it sounds weird that the only friends I've made are three girls. But I already feel like I know I can count on them."
    "Sé que suena raro que los amigos en mi nuevo pueblo son solo tres mujeres. Pero sé que puedo contar con ellas."

# game/dialogs.rpy:759
translate spanish scene4_99d1ceb5:

    # "I think it's better to have a few close friends you can count on than a large group of friends that don't know each other very well."
    "Es mejor tener pocos amigos con quienes puedas contar que tener muchos amigos que no te traten bien, ¿no lo crees?"

# game/dialogs.rpy:760
translate spanish scene4_6dc4476d:

    # "I've learned a lot about manga, anime and the village itself thanks to them."
    "Aprendí mucho sobre el manga, anime y el pueblo mismo gracias a ellas..."

# game/dialogs.rpy:764
translate spanish scene4_bd1aa541:

    # "On my way to school, I was walking towards the crossroads where I usually join Sakura."
    "Este Viernes, estaba caminando hacia la intersección donde usualmente me junto con Sakura-chan."

# game/dialogs.rpy:766
translate spanish scene4_e8c9ca4f:

    # "As I got closer to her, I noticed that she wasn't alone."
    "Estaba algo lejos de ella cuando noté que no estaba sola."

# game/dialogs.rpy:767
translate spanish scene4_5108d11f:

    # "When I got closer, I could see three rough looking guys around her."
    "Cuando me acerqué, ví a 3 chicos malos alrededor de ella."

# game/dialogs.rpy:768
translate spanish scene4_6898b2d9:

    # "It was like they were the typical bad boys you see in some manga or anime, with the same exact goofy hairstyles and everything."
    "Los típicos chicos malos que puedes ver en algunos mangas y animes, con el peinado y todo eso."

# game/dialogs.rpy:769
translate spanish scene4_f2f59080:

    # "I vaguely knew who they were. They were from our school, but I remember Rika-chan telling me these guys never show up to school."
    "Los conocía vagamente, ellos eran de nuestra escuela. Rika-chan me dijo que esos chicos no llegan a la escuela muy a menudo..."

# game/dialogs.rpy:771
translate spanish scene4_b5bd2c54:

    # "Bad guy 1" "Go on! Say it!"
    "Chico 1" "¡Vamos! ¡dilo!"

# game/dialogs.rpy:772
translate spanish scene4_2c3b1b19:

    # "One of the guys shouted at Sakura. I instinctively hid myself behind a wall."
    "El tipo le gritó tan fuerte a Sakura-chan, que instintivamente me escondí detrás de una pared."

# game/dialogs.rpy:773
translate spanish scene4_b57e6ca3:

    # "Another guy who looked like their leader forcefully grabbed Sakura by her collar."
    "El líder agarró con fuerza a Sakura-chan por el cuello."

# game/dialogs.rpy:774
translate spanish scene4_bf711db5:

    # "Bad guy 1" "You're guy, we all know it! So say it! Tell us what you really are!"
    "Chico 1" "Eres un chico, ¡todos lo sabemos! ¡Así que dilo! ¡Te reto a que lo digas enfrente de mí!"

# game/dialogs.rpy:775
translate spanish scene4_b765fd13:

    # "Bad guy 2" "Yeah, stop playing the cute little girl!"
    "Chico 2" "¡Si, deja de estar jugando a ser una linda y pequeña chica!"

# game/dialogs.rpy:776
translate spanish scene4_dc755d36:

    # "Bad guy 3" "I'm sure you're just a sick pervert that disguises himself into a girl to peek at other girls in the toilets, right!?"
    "Chico 3" "Estoy seguro que eres solo un pervertido que se disfraza de chica para echar un vistazo a las otras chicas en los baños, ¿¡cierto!?"

# game/dialogs.rpy:777
translate spanish scene4_7849a2e2:

    # "Bad guy 2" "Yeah, you're disgusting!"
    "Chico 2" "¡Si, eres desagradable!"

# game/dialogs.rpy:778
translate spanish scene4_8006ab79:

    # "What the hell are they talking about?!"
    "¿¡De qué diablos están hablando!?"

# game/dialogs.rpy:779
translate spanish scene4_e45090ba:

    # "Anyway, Sakura is trouble! I have to do something!"
    "¡De cualquier modo, Sakura-chan esta en problemas! ¡Debo de hacer algo!"

# game/dialogs.rpy:780
translate spanish scene4_af06121a:

    # "I'm not very good at fighting... I don't have the strength of a super hero..."
    "No soy muy bueno peleando... No tengo la fuerza de un super héroe..."

# game/dialogs.rpy:781
translate spanish scene4_77b9ebe0:

    # "But I don't care! Sakura-chan must be saved!"
    "¡Pero no me importa! ¡Sakura-chan debe ser salvada!"

# game/dialogs.rpy:782
translate spanish scene4_d3847689:

    # "Be strong like the {i}High School Samurai{/i}, %(stringhero)s!"
    "¡Se fuerte como el {i}High School Samurai{/i}, %(stringhero)s!"

# game/dialogs.rpy:783
translate spanish scene4_38f6654f:

    # "{size=+10}Chaaaaarge!!!!!{/size}"
    "{size=+10}¡¡¡¡A la carga!!!!!{/size}"

# game/dialogs.rpy:784
translate spanish scene4_e9b169e1:

    # "I ran between the group and started to shout at the guys."
    "Corrí a donde estaban y les grité a los chicos malos."

# game/dialogs.rpy:785
translate spanish scene4_03cc4ea3:

    # hero "Hey! Leave her alone!"
    hero "¡Oigan! ¡Dejenla sola!"

# game/dialogs.rpy:786
translate spanish scene4_21ec0b43:

    # "Bad guy 2" "Huh? Who's that little brat?"
    "Chico 2" "¿Ah? ¿Quién es ese mocoso?"

# game/dialogs.rpy:787
translate spanish scene4_5a5a90ca:

    # "Bad guy 3" "Who are you? His boyfriend? You a faggot!?"
    "Chico 3" "¿Quién eres? ¿Su novio? ¿¡Eres un maricón!?"

# game/dialogs.rpy:789
translate spanish scene4_62845cb0:

    # "Bad guy 1" "Pfft! Whatever, let's go, guys. I had enough fun anyway."
    "Chico 1" "¡Pfft! Vamonos, chicos, ya tuvimos suficiente diversión de todas maneras."

# game/dialogs.rpy:790
translate spanish scene4_02769f02:

    # "B.g. 2 & 3" "What?"
    "C. 2 & 3" "¿Qué?"

# game/dialogs.rpy:791
translate spanish scene4_9d93e425:

    # "Bad guy 1" "I said let's go!!!"
    "Chico 1" "¡¡¡Dije que nos vamos!!!"

# game/dialogs.rpy:792
translate spanish scene4_e19b48ff:

    # "Bad guy 1" "There's way more fun shit to do than kicking the asses of these faggots!"
    "Chico 1" "¡Hay cosas mucho más divertidos que patear el trasero de estos maricones!"

# game/dialogs.rpy:793
translate spanish scene4_aadc99c4:

    # "I was ready for a fight but fortunately, they ended up walking away, arguing amongst themselves."
    "Estaba listo para pelear pero al final, ellos se fueron sin pelea alguna."

# game/dialogs.rpy:794
translate spanish scene4_65210d1a:

    # "I doubt I scared them. Maybe they really thought that with me in the way, harassing Sakura wasn't fun anymore."
    "No pienso que los haya asustado. Tal vez pensaron, que en mi presencia, molestar a Sakura-chan ya no era divertido."

# game/dialogs.rpy:795
translate spanish scene4_5b2dcd53:

    # "Sakura collapsed on her knees and sat on the ground. She stayed motionless, looking down."
    "Soltaron a Sakura-chan y se fueron caminando. Ella cayó sentada y estaba inmóvil, mirando hacia abajo."

# game/dialogs.rpy:797
translate spanish scene4_14ee51d1:

    # "I watched them walk away. The cicadas were crying."
    "Los miré caminado mientras que las cigarras cantaban."

# game/dialogs.rpy:799
translate spanish scene4_5714df67:

    # "I got on my knees to reach out to Sakura. She looked dead inside."
    "Me arrodillé para mirar a Sakura-chan. Ella parecía conmocionada."

# game/dialogs.rpy:800
translate spanish scene4_dcd2ce5e:

    # hero "Sakura-chan, are you okay? Are you hurt?"
    hero "Sakura-chan, ¿estás bien?"

# game/dialogs.rpy:801
translate spanish scene4_82675361:

    # "She didn't say a word. I took my bottle of water and gave it to her."
    "Ella no dijo nada. Tomé mi botella de agua y se la dí."

# game/dialogs.rpy:802
translate spanish scene4_44a2c36a:

    # hero "Here, drink. Try to calm down."
    hero "Aquí tienes, bebe un poco. Tranquilizate."

# game/dialogs.rpy:803
translate spanish scene4_ecea0509:

    # s "T... thank you..."
    s "G...Gracias..."

# game/dialogs.rpy:804
translate spanish scene4_0196f5ab:

    # "She drank from the bottle slowly. Tears were running down her face. She was definitely traumatized by what just happened."
    "Ella bebió de la botella lentamente. Vi que le salían unas lágrimas de sus grandes ojos azules. Probablemente el incidente que tuvo la haya traumatizado."

# game/dialogs.rpy:805
translate spanish scene4_e638db20:

    # "Or maybe it was just the refreshing feeling of drinking water, bringing her back to reality. I couldn't tell..."
    "O tal vez solo haya sido la repentina frescura del agua. Quien lo puede decir..."

# game/dialogs.rpy:806
translate spanish scene4_76b6a439:

    # hero "It's okay, they're gone... They won't bother you anymore..."
    hero "Esta bien, ya se fueron... Ellos ya no te molestarán... "

# game/dialogs.rpy:807
translate spanish scene4_46f4b04d:

    # "I stayed with Sakura in the area for a while, waiting for her to recover."
    "Estuvimos alrededor del área por un momento, esperando para que Sakura-chan se recupere."

# game/dialogs.rpy:808
translate spanish scene4_2bd9d9ea:

    # "Why did those guys bullied her like that?"
    "¿Porqué esos tipos la acosaban así?"

# game/dialogs.rpy:809
translate spanish scene4_50b0bbf2:

    # "There's no way an innocent girl like her could be a guy in disguise! No way!"
    "¡No hay forma de que una linda chica como ella pueda ser un chico disfrazado! ¡No hay manera!"

# game/dialogs.rpy:810
translate spanish scene4_4c3197ed:

    # "Maybe they picked on her because she has some tomboyish tastes or something...?"
    "Supongo que la molestaron porque ella tiene algún gusto poco femenino o algo."

# game/dialogs.rpy:811
translate spanish scene4_a20e210b:

    # "Whatever, that was disgusting of them!"
    "Pero como sea, ¡eso fue poco racional de ellos!"

# game/dialogs.rpy:812
translate spanish scene4_3664d290:

    # "In fact, nothing they said made any sense... I still don't get it..."
    "De hecho, nada tiene sentido en esa historia... No lo entiendo..."

# game/dialogs.rpy:813
translate spanish scene4_846b18ad:

    # "Finally, after Sakura calmed down a bit, we got up and went to school."
    "Finalmente nos levantamos y nos fuimos a la escuela."

# game/dialogs.rpy:817
translate spanish scene4_c8300016:

    # "We silently made our way to school together."
    "Recorrimos nuestro camino a la escuela sin decir una sola palabra."

# game/dialogs.rpy:818
translate spanish scene4_291de2b0:

    # "I was worried. I think I'll need the help of Rika-chan."
    "Estaba preocupado. Pienso que necesitaré la ayuda de Rika-chan."

# game/dialogs.rpy:825
translate spanish scene4_2eae0bd6:

    # hero "...and that's what happened this morning."
    hero "... Y eso fue que pasó esta mañana."

# game/dialogs.rpy:826
translate spanish scene4_4ddab44b:

    # "I didn't bother telling Rika what the bullies actually said to Sakura. Just all the rest."
    "No le dije a Rika lo que los abusadores le dijeron a Sakura. Solo todo lo demás."

# game/dialogs.rpy:827
translate spanish scene4_6d41b106:

    # "Anyway, I'm pretty sure what they were saying doesn't even matter. They just made up a reason to bully her."
    "Estoy muy seguro que lo que dijeron no era nada importante. Solo la primera razón que encontraron para acosarla."

# game/dialogs.rpy:828
translate spanish scene4_60f0cbde:

    # n "Those bastards!"
    n "¡Esos bastardos!"

# game/dialogs.rpy:829
translate spanish scene4_cf610c40:

    # r "They were lucky that I wasn't there! I'm pretty good at trashing boys!"
    r "¡Tuvieron suerte de que yo no estaba ahí! ¡Soy muy buena para destrozar chicos!"

# game/dialogs.rpy:830
translate spanish scene4_ab36d9dc:

    # s "It's okay, Rika-chan. They ran away when %(stringhero)s-kun came to rescue me."
    s "Esta bien, Rika-chan. Ellos huyeron cuando %(stringhero)s-kun vino a rescatarme."

# game/dialogs.rpy:832
translate spanish scene4_9f03aed3:

    # s "In fact, without his help, I don't know what would have happened..."
    s "De hecho, sin su ayuda, no sé lo que hubiera pasado..."

# game/dialogs.rpy:833
translate spanish scene4_188ed7b1:

    # s "Thank you... Thank you, %(stringhero)s-kun!"
    s "Gracias... ¡Muchas gracias, %(stringhero)s-kun!"

# game/dialogs.rpy:834
translate spanish scene4_c1b029c3:

    # "Sakura bowed to me. I wasn't sure what to say."
    "Sakura-chan me reverenció . Me sentí un poco avergonzado."

# game/dialogs.rpy:838
translate spanish scene4_03382661:

    # r "Well, I guess I have to thank you for helping Sakura-chan %(stringhero)s."
    r "Bien, supongo que te tengo que agradecer por rescatar a mi amiga, %(stringhero)s."

# game/dialogs.rpy:839
translate spanish scene4_77f35da3:

    # hero "It's okay, it's okay. After all, it's my duty to save damsels in distress!"
    hero "No te preocupes. Era mi deber salvar una damisela en peligro."

# game/dialogs.rpy:840
translate spanish scene4_aadcce64:

    # "Sakura laughed and finally smiled."
    "Sakura sonrió."

# game/dialogs.rpy:841
translate spanish scene4_f25fd7bb:

    # hero "Sakura-chan..."
    hero "Sakura-chan..."

# game/dialogs.rpy:842
translate spanish scene4_75a10854:

    # hero "I promise you right now."
    hero "Te prometo aquí mismo."

# game/dialogs.rpy:843
translate spanish scene4_e7c0608a:

    # hero "I'll dedicate myself to escorting you to and from school everyday!"
    hero "¡Me dedicaré a ir a casa y a la escuela contigo todos los días! ¡de la escuela a tu casa y de tu casa a la escuela!"

# game/dialogs.rpy:845
translate spanish scene4_6d4d7801:

    # s "Really? You promise?"
    s "¿En serio? ¿Lo prometes?"

# game/dialogs.rpy:847
translate spanish scene4_c89aabf6:

    # s "Oh, thank you, %(stringhero)s-kun! {image=heart.png}"
    s "¡Oh, gracias, %(stringhero)s-kun! {image=heart.png}"

# game/dialogs.rpy:849
translate spanish scene4_6bd67288:

    # r "Hmph! Fine, I'll accept this for now...only because I live the exact opposite direction from her house!"
    r "¡Hmph! ¡Lo acepto solo porque vivo exactamente al lado contrario de su casa!"

# game/dialogs.rpy:850
translate spanish scene4_12240494:

    # r "If I could, I would already be escorting her everyday!"
    r "Si pudiera, ¡yo la escoltaría así todos los días!"

# game/dialogs.rpy:852
translate spanish scene4_87cde803:

    # n "Same for me..."
    n "Lo mismo para mí..."

# game/dialogs.rpy:853
translate spanish scene4_641491a3:

    # r "I just know I can count on you, %(stringhero)s."
    r "Solo puedo contar contigo, %(stringhero)s."

# game/dialogs.rpy:854
translate spanish scene4_b799ab6b:

    # "I smiled."
    "Me reí."

# game/dialogs.rpy:855
translate spanish scene4_ea6ddde8:

    # "Rika really does seem like a tsundere but I found it pretty lovable."
    "Rika-chan realmente se miraba como una tsundere pero lo encontré tierno."

# game/dialogs.rpy:856
translate spanish scene4_4401cc13:

    # "I felt even better when I saw Sakura's bright smile."
    "Me sentí mejor cuando miré la brillante sonrisa de Sakura-chan."

# game/dialogs.rpy:861
translate spanish scene4_77f4ec72:

    # r "Anyways..."
    r "De cualquier manera, gente."

# game/dialogs.rpy:862
translate spanish scene4_72233eeb:

    # r "I have an important announcement for the members of the club!"
    r "¡Tengo un importante anuncio para los miembros del club!"

# game/dialogs.rpy:863
translate spanish scene4_40d86de1:

    # s "Yes?"
    s "¿Si?"

# game/dialogs.rpy:864
translate spanish scene4_5ebd74f9:

    # hero "Yes, ma'am!"
    hero "Si, ¡señora!"

# game/dialogs.rpy:865
translate spanish scene4_2dd18f3e:

    # n "What is it?"
    n "¿Qué es?"

# game/dialogs.rpy:866
translate spanish scene4_8afe26e4:

    # r "Two weeks from now, the summer festival will be happening in the village. And the club must meet together on this day and enjoy the festival!"
    r "En dos semanas, habrá un festival de verano en el pueblo y ¡el club debe festejarlo juntos ese día!"

# game/dialogs.rpy:867
translate spanish scene4_b181bb98:

    # hero "A summer festival? Really?"
    hero "¿Un festival de verano? ¿En serio?"

# game/dialogs.rpy:868
translate spanish scene4_7ea87b78:

    # "I've heard about japanese festivals like the Matsuri."
    "He escuchado sobre festivales japoneses como el Matsuri."

# game/dialogs.rpy:869
translate spanish scene4_0a708135:

    # "They're very popular in older villages. People do things like put on yukatas and visit food stands. There's also a lot games, traditional dances and even a fireworks display at the very end."
    "Estos son muy populares en viejos pueblos. Las personas usan yukatas y visitan los puestos de comida. También hay juegos, danza tradicional y fuegos artificiales al final."

# game/dialogs.rpy:870
translate spanish scene4_2339b27a:

    # "I've only see them in anime. I've always wanted to go to a real one!"
    "Yo solo los he visto en algunos animes. Siempre he querido ver uno real."

# game/dialogs.rpy:871
translate spanish scene4_97a399b0:

    # r "You never went to any summer festival? You city rat!"
    r "Nunca haz ido a un festival de verano? ¡Tu rata de la ciudad!"

# game/dialogs.rpy:872
translate spanish scene4_e1074e1b:

    # hero "Hey, shut up!"
    hero "Oye, ¡cierra tu boca!"

# game/dialogs.rpy:873
translate spanish scene4_b091f4a6:

    # s "You'll come to realize, the summer festival of this village is absolutely amazing."
    s "Te darás cuenta que el festival de Verano del pueblo es grandioso."

# game/dialogs.rpy:874
translate spanish scene4_15e427d2:

    # s "Sounds like it'll be a lot of fun!"
    s "¡Esto será muy divertido!"

# game/dialogs.rpy:875
translate spanish scene4_0fcfce33:

    # hero "And no need to take the train this time, right?"
    hero "Y no hay necesidad de tomar el tren esta vez, ¿cierto?"

# game/dialogs.rpy:876
translate spanish scene4_970c7b2d:

    # s "True! *{i}giggles{/i}*"
    s "¡Es verdad! *{i}risitas{/i}*"

# game/dialogs.rpy:877
translate spanish scene4_6e7d7bc8:

    # r "I hope you have your yukata, city rat!"
    r "Espero que tengas tu yukata, ¡rata de la ciudad!"

# game/dialogs.rpy:878
translate spanish scene4_7c767f63:

    # hero "Hey! I do!"
    hero "¡Oye! ¡claro que lo tengo!"

# game/dialogs.rpy:879
translate spanish scene4_2322fa79:

    # "Actually, I didn't. I just wanted her to quit nagging me."
    "En realidad no lo tengo, pero quería que me dejara de molestar..."

# game/dialogs.rpy:886
translate spanish scene4_5805cafa:

    # "As I promised with Sakura-chan, I escorted her on the way home, after school."
    "Como lo prometí a Sakura-chan, la escolté a su casa después de la escuela."

# game/dialogs.rpy:887
translate spanish scene4_6713403c:

    # "Ah geez...It'll be a problem if I don't have a yukata before the summer festival. Rika would never let it go."
    "Ahora tengo un problema. Le dije a Rika-chan que tengo una yukata para el festival, pero en realidad no lo tengo."

# game/dialogs.rpy:888
translate spanish scene4_df7e7b2d:

    # "I have to find one as quickly as possible."
    "Entonces debo de encontrar uno tan rápido como sea posible."

# game/dialogs.rpy:889
translate spanish scene4_20088daa:

    # "I know wearing a yukata isn't mandatory for summer festivals, but I just want to show that tsundere that I'm not the city rat she thinks I am."
    "Sabía que los yukatas no eran obligatorios para los festivales de verano, pero solo quiero demostrarle a esa chica tsundere que no soy la rata de la ciudad que cree que soy."

# game/dialogs.rpy:890
translate spanish scene4_2100c01f:

    # hero "Sakura-chan?"
    hero "¿Sakura-chan?"

# game/dialogs.rpy:891
translate spanish scene4_40d86de1_1:

    # s "Yes?"
    s "¿Si?"

# game/dialogs.rpy:892
translate spanish scene4_bf7e25cb:

    # hero "Are you free tomorrow?"
    hero "¿Estas libre mañana?"

# game/dialogs.rpy:893
translate spanish scene4_9b65d1b6:

    # s "Yes, why?"
    s "Si, ¿porqué?"

# game/dialogs.rpy:894
translate spanish scene4_b543397f:

    # hero "Well..."
    hero "Bueno..."

# game/dialogs.rpy:895
translate spanish scene4_d5ca9051:

    # hero "I'm not familiar with the big city yet..."
    hero "No estoy familiarizado con la gran ciudad aún..."

# game/dialogs.rpy:896
translate spanish scene4_fb7dbaed:

    # hero "And I'd like to go there tomorrow..."
    hero "Pero quisiera ir mañana ahí..."

# game/dialogs.rpy:897
translate spanish scene4_55bf8a2d:

    # hero "So I was wondering if you would..."
    hero "Así que estaba preguntándome si tu querias..."

# game/dialogs.rpy:898
translate spanish scene4_4cf796da:

    # hero "... come... with me...?"
    hero "... ¿Ir... Conmigo...?"

# game/dialogs.rpy:899
translate spanish scene4_0dc65489:

    # "Those last few words were hard to say. It's like I'm asking her out on a date!"
    "Las últimas palabras fueron difíciles de decir. Era como si le preguntara que saliéramos a una cita."

# game/dialogs.rpy:900
translate spanish scene4_02bf7f98:

    # "No... Wait... I'm totally asking her on a date right now!"
    "No... Espera... ¡Estoy preguntándole por una cita ahora mismo!"

# game/dialogs.rpy:902
translate spanish scene4_01b0fe5a:

    # s "Of course! I'd love to come with you to the city! {image=heart.png}"
    s "¡Por su puesto! ¡me encantaría ir contigo a la ciudad! {image=heart.png}"

# game/dialogs.rpy:903
translate spanish scene4_c2837dd9:

    # hero "Really? Thanks!"
    hero "¿En serio? ¡Gracias!"

# game/dialogs.rpy:904
translate spanish scene4_12d152e0:

    # hero "I couldn't think of a better guide than you!"
    hero "¡No podía pensar de una mejor guía para la ciudad!"

# game/dialogs.rpy:905
translate spanish scene4_4b122407:

    # "We giggled. She blushed."
    "Nos reímos. Ella se sonrojó."

# game/dialogs.rpy:906
translate spanish scene4_2e510646:

    # hero "I'll see you at the train station at 9am."
    hero "Te veré en la estación del tren a las 9am."

# game/dialogs.rpy:907
translate spanish scene4_4131ec23:

    # "Perfect! Just according to keikaku... I mean, the plan."
    "¡Perfecto! Todo según el keikaku... Digo, el plan."

# game/dialogs.rpy:908
translate spanish scene4_2f4016d4:

    # "Not only will this tour in the city inform me of where all the important shops are, but I also can find a yukata!"
    "No solo esta visita en la ciudad va hacer que conozca donde están todas las tiendas importantes ¡sino que también podré encontrar y comprar una yukata!"

# game/dialogs.rpy:910
translate spanish scene4_de73d541:

    # "I think she just saved me again."
    "Pienso que ella me ha salvado otra vez."

# game/dialogs.rpy:912
translate spanish scene4_586d150a:

    # "I think she just saved me."
    "Pienso que me acaba de salvar."

# game/dialogs.rpy:913
translate spanish scene4_8713b332:

    # "I can't wait to go to the city with Sakura tomorrow!"
    "No puedo esperar a ir a la ciudad con Sakura-chan mañana..."

# game/dialogs.rpy:922
translate spanish scene5_83f03a3f:

    # "The next morning."
    "A la mañana siguiente."

# game/dialogs.rpy:923
translate spanish scene5_432ea515:

    # "I was at the train station in the village. I'm not even sure that it was a station. It's so small and old."
    "Estaba en la estación del tren del pueblo. No estaba seguro si era la estación, ya que se miraba muy viejo."

# game/dialogs.rpy:924
translate spanish scene5_55ba7152:

    # "And so small... I didn't know there were such small train stations in the country..."
    "Y tan pequeño... No sabía que había estaciones tan pequeños en el país..."

# game/dialogs.rpy:925
translate spanish scene5_ee63449f:

    # s "%(stringhero)s-kuuuuuuuuuuuuun!!!"
    s "¡¡¡%(stringhero)s-kuuuuuuuuuuuuun!!!"

# game/dialogs.rpy:926
translate spanish scene5_09ca9e90:

    # "Sakura arrived."
    "Sakura-chan llegó."

# game/dialogs.rpy:927
translate spanish scene5_f274dee3:

    # "She was wearing a small blue summer dress, a striped shirt, and cute laced shoes that didn't hide her tiny feet."
    "Ella estaba vistiendo un pequeño vestido azul y una blusa con rayas. También lindos zapatos de encaje que no escondían mucho sus pequeños pies."

# game/dialogs.rpy:928
translate spanish scene5_535385a4:

    # "I usually find school uniforms attractive, but I have to admit that the casual outfit she was wearing suits her so much."
    "Usualmente encuentro el uniforme de la escuela sexi, pero tengo que admitir que la ropa casual que ella estaba vistiendo le quedaba bien, ¡se veía más sexi!"

# game/dialogs.rpy:931
translate spanish scene5_7eeab041:

    # s "Good morning, %(stringhero)s-kun!{p}Sorry for the wait!"
    s "Buenos días, %(stringhero)s-kun!{p}¡Perdón por la espera!"

# game/dialogs.rpy:932
translate spanish scene5_a5707cad:

    # hero "Good morning, Sakura-chan!"
    hero "Buenos días, ¡Sakura-chan!"

# game/dialogs.rpy:934
translate spanish scene5_8736489d:

    # s "Ready to visit the city?"
    s "¿Lista para visitar la ciudad?"

# game/dialogs.rpy:935
translate spanish scene5_eef83bf6:

    # s "It's not as big as Tokyo for sure, but there's a lot more there than the village!"
    s "No es tan grande como Tokyo; ¡pero es mucho más grande que el pueblo!"

# game/dialogs.rpy:936
translate spanish scene5_fc3b562c:

    # hero "I guess you'll be my tour guide, senpai!"
    hero "Estoy listo para el recorrido, ¡senpai!"

# game/dialogs.rpy:937
translate spanish scene5_c2497b21:

    # "I laughed a bit but I kinda realized something..."
    "Me reí un poco pero estaba avergonzado."

# game/dialogs.rpy:938
translate spanish scene5_2326053d:

    # "If I go to the city to buy a yukata, she'll obviously notice it."
    "Si iba a ir para comprar una yukata, ella lo notaría."

# game/dialogs.rpy:939
translate spanish scene5_1b0c84a6:

    # "But if I don't, Rika will torment me at the festival."
    "Pero si no lo hago, Rika se burlará de mí en el festival."

# game/dialogs.rpy:943
translate spanish scene5_2100c01f:

    # hero "Sakura-chan?"
    hero "¿Sakura-chan?"

# game/dialogs.rpy:944
translate spanish scene5_40d86de1:

    # s "Yes?"
    s "¿Si?"

# game/dialogs.rpy:952
translate spanish scene5_20b30901:

    # hero "Yesterday...I lied. I don't have any yukata."
    hero "Mentí ayer... No tengo ninguna yukata."

# game/dialogs.rpy:953
translate spanish scene5_adb520a7:

    # hero "In fact, I was planning to buy one in the city while visiting it with you..."
    hero "De hecho, estaba planeando comprar uno en la ciudad mientras la visitaba contigo..."

# game/dialogs.rpy:955
translate spanish scene5_584030bd:

    # s "Is that so?"
    s "¿Así?"

# game/dialogs.rpy:957
translate spanish scene5_ef055bc2:

    # s "It's okay! I'll help you to find one!"
    s "Jeje, ¡no hay problema! ¡Te ayudaré a encontrar uno!"

# game/dialogs.rpy:958
translate spanish scene5_404c741c:

    # hero "Really? That's a relief! Thanks!"
    hero "¿En serio? ¡Muchas gracias!"

# game/dialogs.rpy:959
translate spanish scene5_12d1e584:

    # hero "I said I had one to avoid Rika-chan nagging me."
    hero "Dije que tengo uno para evitar que Rika-chan me fastidiara."

# game/dialogs.rpy:960
translate spanish scene5_a7f1d53d:

    # s "Rika-chan likes to give you a hard time, but I'm sure she means well!"
    s "Lo entiendo. ¡Pienso que haría lo mismo si fuera tu! *{i}risitas{/i}*"

# game/dialogs.rpy:961
translate spanish scene5_fb14ae9d:

    # s "Our Rika-chan is very unique!"
    s "¡Nuestra Rika-chan es única!"

# game/dialogs.rpy:962
translate spanish scene5_fe794b00:

    # "I sighed with relief and laughed with Sakura."
    "Suspiré de alivio y me reí con Sakura-chan."

# game/dialogs.rpy:963
translate spanish scene5_a45c2161:

    # "I'm glad she's so understanding!"
    "¡Pienso que me ha salvado de nuevo!"

# game/dialogs.rpy:967
translate spanish scene5_e1b13f3b:

    # "Hmm... no... It's bad if I tell her..."
    "Mmm... No... Sería malo si le cuento..."

# game/dialogs.rpy:968
translate spanish scene5_7857c1c2:

    # "I'll just go out to have fun with her today. Once I see a yukata shop, I'll take note and I'll will come back tomorrow alone to buy it."
    "Solo tendré el recorrido con ella. Una vez que vea una tienda de yukata, lo anotaré y volveré solo mañana para comprarlo."

# game/dialogs.rpy:969
translate spanish scene5_8ba6ee94:

    # "Yeah, that should be a good idea..."
    "Si, buena idea..."

# game/dialogs.rpy:971
translate spanish scene5_1e69c51f:

    # s "%(stringhero)s-kun, are you okay?"
    s "%(stringhero)s-kun, ¿estás bien?"

# game/dialogs.rpy:972
translate spanish scene5_fcde3cfe:

    # hero "Oh, yes, nevermind... I was daydreaming again..."
    hero "Oh, si, no te preocupes... Estaba soñando despierto otra vez..."

# game/dialogs.rpy:979
translate spanish scene5_c85106ec:

    # "The train arrived and we boarded it towards the city."
    "El tren llegó a su destino y nos bajamos para ir a la ciudad."

# game/dialogs.rpy:980
translate spanish scene5_97c5ca7d:

    # "The city was three stations ahead. About 30 minutes of travel time. Sakura and I made small talk on the train."
    "Estaba 3 estaciones adelante. Casi 30 minutos de viaje."

# game/dialogs.rpy:982
translate spanish scene5_66ca24f3:

    # "We finally arrived at the city."
    "Finalmente llegamos a la ciudad."

# game/dialogs.rpy:983
translate spanish scene5_0a3e36c2:

    # "It really is pretty big. Just as Sakura said."
    "Era grande. Justo como Sakura-chan lo dijo."

# game/dialogs.rpy:985
translate spanish scene5_0c293bd8:

    # s "Here we are!"
    s "¡Aquí estamos!"

# game/dialogs.rpy:986
translate spanish scene5_5e4b4e2f:

    # s "The shopping area isn't too far. Only two streets away."
    s "El área comercial no está lejos. Solo dos calles de distancia."

# game/dialogs.rpy:987
translate spanish scene5_75ad6856:

    # hero "Great! Let's get going, senpai!"
    hero "Bien. ¿Nos vamos, senpai?"

# game/dialogs.rpy:988
translate spanish scene5_9e4c1aaf:

    # "Sakura smiled and we started walking through the busy streets."
    "Sakura sonrió y comenzamos a caminar en las calles."

# game/dialogs.rpy:992
translate spanish scene5_1b70d467:

    # "After some time, we finally reached a street full of a variety of different shops."
    "Después de un tiempo, finalmente llegamos a una calle llena de diferentes tiendas."

# game/dialogs.rpy:993
translate spanish scene5_454785ef:

    # "Clothing, multimedia, restaurants, arcades,..."
    "Ropa, multimedia, restaurantes, salones recreativos..."

# game/dialogs.rpy:994
translate spanish scene5_a2720e0f:

    # "They had everything here!"
    "¡Había de todo!"

# game/dialogs.rpy:995
translate spanish scene5_4f036fa9:

    # "I hasn't been shopping since I moved from Tokyo. I was pretty excited."
    "Estaba emocionado. ¡No había ido de compras desde nuestro mudanza!"

# game/dialogs.rpy:997
translate spanish scene5_0b0fbfd9:

    # "But first things first, I have to find a yukata shop."
    "Pero debo de encontrar una yukata primero que nada."

# game/dialogs.rpy:998
translate spanish scene5_69e0143d:

    # "So we started with the clothes shops."
    "Así que comenzamos con las tiendas de ropa."

# game/dialogs.rpy:1001
translate spanish scene5_8534ca42:

    # ". . ."
    ". . ."

# game/dialogs.rpy:1002
translate spanish scene5_aceff1a2:

    # "Argh!!"
    "¡¡Argh!!"

# game/dialogs.rpy:1003
translate spanish scene5_249d4d18:

    # "There's absolutely no yukata under the price of ¥200,000!"
    "¡Definitivamente no hay una yukata debajo de los ¥200,000!"

# game/dialogs.rpy:1004
translate spanish scene5_6a82deb5:

    # "It's definitely too expensive for the budget I had."
    "Es demasiado caro para el presupuesto que tengo."

# game/dialogs.rpy:1006
translate spanish scene5_3101ced6:

    # "As I was checking the prices, I noticed Sakura seemed to be thinking about something."
    "Sakura parecía pensar sobre algo mientras verificaba los precios."

# game/dialogs.rpy:1007
translate spanish scene5_01c5b3f7:

    # "She looked kinda sorry that I couldn't afford a yukata for myself."
    "Ella se miraba afligida de que yo no pudiera costearme una yukata por mi cuenta."

# game/dialogs.rpy:1008
translate spanish scene5_0ddddc7d:

    # hero "Nah, forget it.{p}After all, it's not like my life depends on getting one."
    hero "Nah, olvidalo.{p}Después de todo, no es que realmente necesite uno."

# game/dialogs.rpy:1009
translate spanish scene5_a727d569:

    # hero "Although...I just hope Rika forgot about it already. I should brace myself for anything..."
    hero "Y tal vez Rika-chan ya lo haya olvidado..."

# game/dialogs.rpy:1010
translate spanish scene5_41a74897:

    # hero "Let's go somewhere else for now..."
    hero "¡Solo deberíamos de continuar nuestro recorrido!"

# game/dialogs.rpy:1011
translate spanish scene5_5c76b703:

    # s "Hmm... Okay..."
    s "Hmm... Esta bien..."

# game/dialogs.rpy:1016
translate spanish scene5_041a4807:

    # s "So, where do you want to go? Is there anything you want to see?"
    s "Entonces, ¿por dónde comenzamos? ¿Quieres ver algo?"

# game/dialogs.rpy:1017
translate spanish scene5_72bc6d39:

    # hero "How about checking out manga in the bookstores?"
    hero "¿Qué tal si vemos los mangas en las librerías?"

# game/dialogs.rpy:1018
translate spanish scene5_b6d74bf5:

    # hero "That could be useful for the club!"
    hero "¡Eso puede ser útil para el club!"

# game/dialogs.rpy:1020
translate spanish scene5_fa9a9f16:

    # s "Nice idea! Let's go!"
    s "¡Buena idea! ¡Vamos!"

# game/dialogs.rpy:1024
translate spanish scene5_2cd8d6de:

    # "The manga bookstore was gigantic!"
    "¡La librería de manga era enorme!"

# game/dialogs.rpy:1025
translate spanish scene5_3f7f2473:

    # "It was three floors full of manga as far as the eye could see!"
    "¡Tres pisos llenos de manga!"

# game/dialogs.rpy:1026
translate spanish scene5_17b06ea6:

    # "I start by browsing through the shonen harem manga, since it's my favorite genre."
    "Comencé por mirar por los mangas shonen harem, ya que es mi género favorito."

# game/dialogs.rpy:1027
translate spanish scene5_54cf672e:

    # "I found all the volumes of {i}High School Samurai{/i}, but I had them already."
    "Encontré todos los volúmenes de {i}High School Samurai{/i}, pero ya los tenía."

# game/dialogs.rpy:1028
translate spanish scene5_e98d6302:

    # "Then I saw a series I didn't anything know, called {i}Uchuu Tenshi Moechan{/i}. I found the manga cover to be quite attractive. As I was reaching out to grab a volume..."
    "Entonces vi una serie que no conocía llamado {i}Uchuu Tenshi Moechan{/i}. Encontré los dibujos atractivos y estaba extendiendo mi brazo para tomarlo."

# game/dialogs.rpy:1030
translate spanish scene5_2c810467:

    # "My hand touched another which went towards the same book. We both retracted our hands back quickly."
    "Entonces mi mano tocó otra mano el cual iba hacia la misma dirección. Retraímos nuestras manos rápidamente."

# game/dialogs.rpy:1031
translate spanish scene5_931b518d:

    # hero "Oh, I'm so sorry..."
    hero "Oh, perdona..."

# game/dialogs.rpy:1032
translate spanish scene5_56a93df9:

    # s "Sorry, my bad..."
    s "Perdón, fue mi culpa..."

# game/dialogs.rpy:1033
translate spanish scene5_8534ca42_1:

    # ". . ."
    ". . ."

# game/dialogs.rpy:1034
translate spanish scene5_cc9a5909:

    # "!!!"
    "!!!"

# game/dialogs.rpy:1036
translate spanish scene5_85b2e7cc:

    # "Sakura!?"
    "¿¡Sakura!?"

# game/dialogs.rpy:1037
translate spanish scene5_a3335b11:

    # "I was a little caught off guard."
    "Estaba un poco sorprendido."

# game/dialogs.rpy:1039
translate spanish scene5_960abe9a:

    # "Well only a little, since I knew that she liked {i}High School Samurai{/i} .{w} But I never imagined that she would like shonen harem manga that were aimed towards guys!"
    "Bueno, solo un poco, ya que sabía que le gustaba {i}High School Samurai{/i};{w} pero no me imaginé ¡que le gustaría los mangas shonen harem que normalmente son para chicos!."

# game/dialogs.rpy:1040
translate spanish scene5_adf46165:

    # hero "Heh so...you must really like that genre huh, Sakura-chan?"
    hero "Je,¿te gustan esos, Sakura-chan?"

# game/dialogs.rpy:1041
translate spanish scene5_cd229797:

    # s "Well... y-yes..."
    s "Bueno... S-si..."

# game/dialogs.rpy:1042
translate spanish scene5_0735783a:

    # "She was blushing... Looks like it was embarrassing for her to talk about it."
    "Se sonrojó... Parece que era embarazoso para ella hablar sobre esto."

# game/dialogs.rpy:1043
translate spanish scene5_40d8c136:

    # s "I... I like these kinds of manga... The girls in them are very attractive..."
    s "Me... Me gustan esos tipos de mangas... Las chicas en estos mangas son lindas..."

# game/dialogs.rpy:1044
translate spanish scene5_cde483c5:

    # s "I wish I was as beautiful as the women in these manga..."
    s "Desearía ser tan bonita como ellas"

# game/dialogs.rpy:1045
translate spanish scene5_6ce4f6f3:

    # "That's kinda adorable."
    "Que adorable."

# game/dialogs.rpy:1046
translate spanish scene5_b7cf4dac:

    # "Honestly though, she's far prettier than any girl I've seen in any shonen manga!"
    "¡Pero ella ES mucho más bonita que todas las chicas de todos los mangas shonen!"

# game/dialogs.rpy:1047
translate spanish scene5_7658f518:

    # "If I wasn't such a nervous wreck half the time, I'd say it again to her face..."
    "Si no fuera tan tímido, le diría eso otra vez..."

# game/dialogs.rpy:1048
translate spanish scene5_f50ba84f:

    # "But I just couldn't... I lacked the courage to say it out loud."
    "Pero no pude... Estaba demasiado avergonzado para decirlo esta vez."

# game/dialogs.rpy:1049
translate spanish scene5_e3f223bb:

    # "My mind started to wander off about touching her hand by accident."
    "Mis dedos sintieron su suave mano por accidente..."

# game/dialogs.rpy:1050
translate spanish scene5_25f48c38:

    # "It was so soft... It seemed almost surreal..."
    "Tan suave... Que se veía casi irreal..."

# game/dialogs.rpy:1051
translate spanish scene5_875908a3:

    # "I realized that I was daydreaming, and quickly regained my composure."
    "Traté de tomar control de mí mismo de nuevo."

# game/dialogs.rpy:1052
translate spanish scene5_aa64fa57:

    # hero "Oh...Uhh...So do you know what the story is about?"
    hero "Oh... Entonces, ¿de qué se trata la historia?"

# game/dialogs.rpy:1054
translate spanish scene5_dc5fd421:

    # s "Oh, it's a nice one."
    s "Oh, es una buena."

# game/dialogs.rpy:1055
translate spanish scene5_9b8c5139:

    # s "It's the story about a boy from Earth who is kidnapped by female aliens because they think he is a god that will save their planet."
    s "Es la historia sobre un chico de la Tierra quien es secuestrado por chicas alienígenas porque ellas piensan que él es el Dios que salvará su planeta."

# game/dialogs.rpy:1056
translate spanish scene5_85fb97ae:

    # s "They all want to go out with him but he's very shy; plus he has to fight against an horrible and powerful entity..."
    s "Todas ellas quieren salir con él pero él es muy tímido; además tiene que pelear contra una horrible y poderosa entidad..."

# game/dialogs.rpy:1057
translate spanish scene5_f86e719b:

    # s "But fortunately, the girls have super powers and he fights alongside the girls..."
    s "Pero afortunadamente, las chicas tiene algunos poderes y él solo tiene que entrar a las batallas con las chicas..."

# game/dialogs.rpy:1058
translate spanish scene5_ea859548:

    # s "It's kinda funny and heroic at same time!"
    s "¡Es algo gracioso y épico al mismo tiempo!"

# game/dialogs.rpy:1059
translate spanish scene5_b293cdd7:

    # hero "That sounds pretty entertaining!"
    hero "¡Suena bien!"

# game/dialogs.rpy:1060
translate spanish scene5_fa4d34a0:

    # hero "Maybe I should start reading it..."
    hero "Tal vez debería comenzar a leerlo..."

# game/dialogs.rpy:1062
translate spanish scene5_40a4c6ba:

    # s "I can lend you the first few volumes at school if you want!"
    s "¡Te puedo prestar el primer volúmen en la escuela si quieres!"

# game/dialogs.rpy:1063
translate spanish scene5_7fe42341:

    # hero "That would be great! Thanks, Sakura-chan!"
    hero "¡Eso sería grandioso! ¡Gracias, Sakura-chan!"

# game/dialogs.rpy:1067
translate spanish scene5_86f22ee4:

    # "After looking all around the store, we finally ended up leaving the shop."
    "Finalmente salimos de la tienda."

# game/dialogs.rpy:1068
translate spanish scene5_b7801019:

    # "She bought the new volume of {i}Moechan{/i} while I got the first volume of {i}OGT{/i}, a seinen that I heard of a long time ago."
    "Ella tomó el nuevo volúmen de {i}Moechan{/i} mientras que yo estaba sujetando el primer volúmen de {i}OGT{/i}, un seinen que lo he escuchado hace ya tiempo. "

# game/dialogs.rpy:1071
translate spanish scene5_5ebb4f12:

    # s "So, what else do you want to do?"
    s "Entonces, ¿quieres hacer algo más?"

# game/dialogs.rpy:1072
translate spanish scene5_f7ff6212:

    # hero "How about the arcade?"
    hero "¡Claro! ¿Qué tal los salónes recreativos?"

# game/dialogs.rpy:1073
translate spanish scene5_d8b99f39:

    # hero "I'm curious to see how good you are at shooting games."
    hero "Tengo curiosidad de ver que tan buena eres en disparar."

# game/dialogs.rpy:1075
translate spanish scene5_5b8b0c8c:

    # s "Teehee! Alright, let's go! But I won't hold back!"
    s "¡Teehee! ¡Esta bien, vamos!"

# game/dialogs.rpy:1079
translate spanish scene5_fb2b36bb:

    # "We played a game of {i}Silent Crisis{/i} together."
    "Jugamos a un juego de {i}Silent Crisis{/i} juntos."

# game/dialogs.rpy:1080
translate spanish scene5_e30e02a0:

    # "She was definitely a pro at it!"
    "¡Ella era definitivamente buena en esto!"

# game/dialogs.rpy:1081
translate spanish scene5_6f318a21:

    # "She knew exactly where the zombies spawned and shot them before they could even attack!"
    "Ella sabía exactamente donde iban a aparecer los zombis y ¡les disparaba antes de que pudieran atacar!"

# game/dialogs.rpy:1082
translate spanish scene5_1c41f171:

    # "I ended up using all my credits since I died over and over, while she didn't lose a single life at all!"
    "¡Yo usé todos mis créditos mientras que ella no perdió ninguna vida para nada!"

# game/dialogs.rpy:1083
translate spanish scene5_ac2581f9:

    # hero "Wow... Sakura-chan, you're definitely better than I am..."
    hero "Wow... Sakura-chan, eres definitivamente mejor que yo..."

# game/dialogs.rpy:1084
translate spanish scene5_509bc659:

    # hero "You know the game by heart or something?"
    hero "¿Te sabes el juego de memoria o algo?"

# game/dialogs.rpy:1085
translate spanish scene5_7ab00054:

    # "She was concentrated on the game."
    "Ella estaba concentrada en el juego."

# game/dialogs.rpy:1086
translate spanish scene5_924b98a0:

    # s "Not really... In fact, it's only the third time I played this one."
    s "No realmente... De hecho, es solo la tercera vez que lo juego."

# game/dialogs.rpy:1087
translate spanish scene5_8c2adcf7:

    # s "I think it's my reflexes that make me good at these types of games."
    s "Pienso que son mis reflejos que me hacen buena en este juego."

# game/dialogs.rpy:1089
translate spanish scene5_7846d5b4:

    # "???" "Hey guys! You're here too?"
    "???" "¡Oigan chicos! ¿Ustedes también están aquí?"

# game/dialogs.rpy:1095
translate spanish scene5_43c67a2f:

    # hero "Oh! Nanami-chan!"
    hero "Ooh. ¡Nanami-chan!"

# game/dialogs.rpy:1096
translate spanish scene5_640b15c3:

    # s "Hey Nana-chan!"
    s "¡Oh Nana-chan!"

# game/dialogs.rpy:1097
translate spanish scene5_bccc3e1d:

    # hero "What are you doing here?"
    hero "¿Qué estás haciendo aquí?"

# game/dialogs.rpy:1099
translate spanish scene5_dbdeb2ab:

    # n "Duh, playing arcade games, you dork!"
    n "Duh, jugando las maquinitas, ¡tontín!"

# game/dialogs.rpy:1100
translate spanish scene5_86073654:

    # hero "Eh? I'm a dork now?"
    hero "¿Soy un tontín ahora?"

# game/dialogs.rpy:1101
translate spanish scene5_a555e2b6:

    # n "Yes, you're a dork dude! {image=heart.png}"
    n "Si, ¡eres un tontín amigo! {image=heart.png}"

# game/dialogs.rpy:1102
translate spanish scene5_040d24b0:

    # "Sakura laughed so much at the lame joke, she almost took a hit in the game."
    "Sakura se reía tanto de la broma que casí le dan un golpe en el juego."

# game/dialogs.rpy:1103
translate spanish scene5_013529c0:

    # "Still, it's incredible how she can do multiple things at the same time. I heard it's an ability that girls naturally have.{p}I kinda envy them."
    "Es increíble que ella pueda hacer múltiples cosas a la vez."
    "Escuché que es una habilidad que las chicas tienen naturalmente.{p}Las envidio."

# game/dialogs.rpy:1105
translate spanish scene5_38444d54:

    # "Nanami put a token in the arcade machine and pressed the button labeled 'Player 2'."
    "Nanami puso una ficha en la maquinita y presionó el botón de inicio del Jugador 2."

# game/dialogs.rpy:1107
translate spanish scene5_a015009c:

    # n "Looks like you could use some back up!"
    n "Dejame ayudarte. Te ves como si necesitaras apoyo."

# game/dialogs.rpy:1108
translate spanish scene5_4bf0d20d:

    # "Then Nanami started to shoot at the virtual mobs in the game with Sakura."
    "Entonces Nanami comenzó a disparar a la horda virtual en el juego con Sakura."

# game/dialogs.rpy:1110
translate spanish scene5_2bae2874:

    # n "Hey, %(stringhero)s-senpai, do you know that some video game experts are saying that the golden age of arcade games is coming to an end?"
    n "Oye, %(stringhero)s-senpai, ¿sabías que algunos expertos de los juegos dijeron que vivimos en la era dorada de las maquinitas?"

# game/dialogs.rpy:1111
translate spanish scene5_e7066814:

    # hero "Really?"
    hero "¿En serio?"

# game/dialogs.rpy:1112
translate spanish scene5_b3e58373:

    # hero "But arcade games seem pretty recent, though..."
    hero "Aunque los juegos de maquinitas son bastante recientes..."

# game/dialogs.rpy:1113
translate spanish scene5_2f385a6e:

    # n "It's because of the Internet and online gaming. It's taking more and more room in the gaming domain."
    n "Es por la causa del internet. Esta tomando más y más en el dominio de las videojuegos. Especialmente debido al juego en línea."

# game/dialogs.rpy:1114
translate spanish scene5_c0e7820a:

    # n "Soon there won't be as many arcades as there are now. Some are even afraid that arcade games will just end up being boring and flat compared to what you can find online."
    n "Pronto ya no habrá tantas maquinitas como las hay actualmente. O los nuevos juegos van a ser más aburridos y planos."

# game/dialogs.rpy:1115
translate spanish scene5_34b00fac:

    # n "At least rhythm games that need specific peripherals like {i}Para Para Revolution{/i} will be alright!"
    n "O al menos los juegos de ritmo que necesitan periféricos específicos como {i}Para Para Revolution{/i}."

# game/dialogs.rpy:1116
translate spanish scene5_5b2c12b3:

    # s "Oooh I love that game!"
    s "Oooh ¡amo este juego!"

# game/dialogs.rpy:1117
translate spanish scene5_b5fe983b:

    # hero "Ah..."
    hero "Ah..."

# game/dialogs.rpy:1118
translate spanish scene5_7c35731c:

    # hero "Well, what about making arcade games that can be played online?"
    hero "Bien, ¿Qué tal sobre juegos de maquinitas que puedan jugarse en línea?"

# game/dialogs.rpy:1119
translate spanish scene5_b075ad7f:

    # n "Hmm..."
    n "Hmm..."

# game/dialogs.rpy:1120
translate spanish scene5_0817fef1:

    # n "Hey, maybe you're getting at something, senpai!"
    n "¡Oye, tal vez estas llegando a algo, senpai!"

# game/dialogs.rpy:1121
translate spanish scene5_88898146:

    # s "I'm not sure that would work for rhythm games..."
    s "No estoy seguro que vaya a funcionar para los juegos de ritmo..."

# game/dialogs.rpy:1122
translate spanish scene5_25648fa4:

    # "While Nanami was talking about the history of arcade games, she was still doing an amazing job shooting at monsters on the screen."
    "Mientras que Nanami estába aún hablando de la historia de los juegos de maquinitas, ella aún estaba disparando a la pantalla."

# game/dialogs.rpy:1127
translate spanish scene5_70915e9e:

    # "After a while, Sakura finally lost her last life. Even Nanami was struggling to stay alive."
    "Después de un tiempo, Sakura finalmente perdió su última vida y Nanami estába jugando sola."

# game/dialogs.rpy:1128
translate spanish scene5_1c665d0e:

    # "Nanami was pretty good, but not as good as when she's playing fighting or puzzle games..."
    "Ella era buena, pero sorprendentemente no tan buena como cuando estábamos jugando juegos de pelea o de rompecabezas..."

# game/dialogs.rpy:1129
translate spanish scene5_c09efcd8:

    # hero "Looks like we found a game she's not a master of!"
    hero "Parece que encontramos un juego donde ella no es tan buena en ello."

# game/dialogs.rpy:1130
translate spanish scene5_b4b28eb9:

    # s "Well, nobody's perfect, right?"
    s "Nadie es perfecto, ¿verdad?"

# game/dialogs.rpy:1133
translate spanish scene5_ca00aade:

    # s "You know,..."
    s "Sabes..."

# game/dialogs.rpy:1134
translate spanish scene5_29a8910b:

    # s "Nana-chan...{w}she... She is more fragile than she looks..."
    s "Nana-chan...{w}Ella... Ella es más frágil de lo que aparenta..."

# game/dialogs.rpy:1135
translate spanish scene5_6d59e0e0:

    # hero "What do you mean?"
    hero "¿Qué quieres decir?"

# game/dialogs.rpy:1136
translate spanish scene5_af6cfb46:

    # s "Well, something happened to her that makes her pretty introverted and nervous around people she doesn't know..."
    s "Pues, le pasó algo que la hizo bastante introvertida e insegura con las personas que ella no conoce..."

# game/dialogs.rpy:1137
translate spanish scene5_eb73848e:

    # hero "What happened?"
    hero "¿Qué pasó?"

# game/dialogs.rpy:1139
translate spanish scene5_557b9c7c:

    # s ". . ."
    s ". . ."

# game/dialogs.rpy:1140
translate spanish scene5_b475ba93:

    # s "I shouldn't tell you more, I'm sorry for bringing it up. I prefer she tells you herself..."
    s "No te puedo contar más, lo siento. Prefiero que ella te lo cuente por sí misma..."

# game/dialogs.rpy:1143
translate spanish scene5_8811f347:

    # s "But on a more positive note,. {p}it's the first time I've seen her speak so much with someone she's only known for a few days."
    s "Pero hay algo seguro:{p}Es la primera vez que la veo hablando tanto con alguien que conoce hace solo unos pocos días."

# game/dialogs.rpy:1144
translate spanish scene5_f97de379:

    # s "That's a pretty good sign for you!"
    s "¡Es una muy buena señal para ti!"

# game/dialogs.rpy:1145
translate spanish scene5_291c83e9:

    # s "Maybe you'll be one of her best friends!"
    s "¡Tal vez te conviertas en uno de sus mejores amigos!"

# game/dialogs.rpy:1146
translate spanish scene5_723e1b93:

    # hero "Does she has alot of best friends?"
    hero "¿Ella tiene muchos mejores amigos?"

# game/dialogs.rpy:1148
translate spanish scene5_94da3bfe:

    # s "So far there's only Rika and me."
    s "Hasta ahora solo estamos Rika y yo."

# game/dialogs.rpy:1150
translate spanish scene5_9a43a0d7:

    # s "I'm sure she'll tell you herself someday, if she trusts you enough."
    s "Ella te lo dirá algún día, tal vez, si ella confía en ti lo suficiente."

# game/dialogs.rpy:1151
translate spanish scene5_6a223ad6:

    # s "Anyways, don't mind her if she doesn't speak too much. Befriending her takes time."
    s "De cualquier manera, no le des importancia si ella no habla mucho. Hacerse amigo de ella toma tiempo."

# game/dialogs.rpy:1152
translate spanish scene5_b6cbfe55:

    # hero "I understand...{p}Does she trust a lot of people?"
    hero "Entiendo...{p}¿Ella confía mucho en las personas?"

# game/dialogs.rpy:1153
translate spanish scene5_84d423c2:

    # s "So far, she only trusts her big brother, Rika and me."
    s "Hasta ahora solo sé que confía en su hermano mayor, yo y Rika."

# game/dialogs.rpy:1154
translate spanish scene5_ed700bc4:

    # s "I'm sure you'll be next on the list sooner or later!"
    s "Estoy segura de que tu turno vendrá tarde o temprano."

# game/dialogs.rpy:1156
translate spanish scene5_e668a3b6:

    # hero "I see..."
    hero "Ya veo..."

# game/dialogs.rpy:1157
translate spanish scene5_0f2a7110:

    # hero "I'll do my best to be a good friend to her."
    hero "¡Haré mi mejor esfuerzo!"

# game/dialogs.rpy:1159
translate spanish scene5_e866394b:

    # s "That's great to hear, %(stringhero)s-kun!"
    s "¡Gracias, %(stringhero)s-kun!"

# game/dialogs.rpy:1160
translate spanish scene5_d01d2141:

    # s "I'm sure someday you and her will be best friends!"
    s "Estoy segura de que algún día ella te apreciará más."

# game/dialogs.rpy:1161
translate spanish scene5_e741d135:

    # "For sure, but I wonder what Nanami thinks of me right now..."
    "De veras, me pregunto que es lo que Nanami piensa sobre mí ahora..."

# game/dialogs.rpy:1162
translate spanish scene5_4464bb7e:

    # "And I'm wondering what Sakura thinks of me as well..."
    "Y me pregunto que es lo que Sakura piensa de mí también..."

# game/dialogs.rpy:1167
translate spanish scene5_2f43b611:

    # "After Nanami finished playing, we ate some yakitori at a fast food restaurant together."
    "Después de eso, comimos algo de yakitori en el restaurante de comida rápida."

# game/dialogs.rpy:1168
translate spanish scene5_b84a1878:

    # "Then we boarded the train to return to the village."
    "Entonces regresamos al pueblo."

# game/dialogs.rpy:1169
translate spanish scene5_b6fe7fc0:

    # "The three of us had a pretty fun day together."
    "Tuvimos una buena mañana y tarde, nosotros tres.. "

# game/dialogs.rpy:1171
translate spanish scene5_841a64f2:

    # "Nanami waved goodbye to us as she left to get back home."
    "Nanami nos dejó para volver a su casa."

# game/dialogs.rpy:1173
translate spanish scene5_be1b33c3:

    # "Sakura and I were alone once again."
    "Yo y Sakura estábamos solos, como al comienzo de nuestra cita."

# game/dialogs.rpy:1175
translate spanish scene5_4fcb2681:

    # "Sakura seemed to be deep in thought about something again."
    "Sakura se veía pensando sobre algo durante nuestra cita."

# game/dialogs.rpy:1177
translate spanish scene5_5dcdb866:

    # "As we arrived at Sakura's house, Sakura spoke."
    "Y ella aún estába muy pensativa cuando estábamos cerca de su casa."

# game/dialogs.rpy:1178
translate spanish scene5_007ceac2:

    # s "%(stringhero)s-kun..."
    s "%(stringhero)s-kun..."

# game/dialogs.rpy:1179
translate spanish scene5_a4db12cc:

    # hero "What's up?"
    hero "¿Si?"

# game/dialogs.rpy:1181
translate spanish scene5_0f62d32d:

    # s "I think I have an idea! Just wait here a minute!"
    s "¡Tengo una idea! ¡Solo espera aquí un minuto!"

# game/dialogs.rpy:1183
translate spanish scene5_91bb60f0:

    # "She ran into her house and came back with something. It looked like a set neatly wrapped of clothes."
    "Ella se fue corriendo a su casa y volvió con algo azul. Se veía como un conjunto de ropa."

# game/dialogs.rpy:1185
translate spanish scene5_8941c000:

    # s "Here... Please, take it..."
    s "Aquí tienes... Tómalo..."

# game/dialogs.rpy:1186
translate spanish scene5_e8a32052:

    # s "It's a bit small but you can make it bigger with some touch-ups."
    s "Es un poco pequeño pero puedes agrandarlo con unos pequeños toques."

# game/dialogs.rpy:1187
translate spanish scene5_a7e82bdc:

    # "I unwrapped the set of clothes. {p}I couldn't believe it!"
    "Desenvolví la cosa azul.{p}¡No lo puedo creer!"

# game/dialogs.rpy:1188
translate spanish scene5_b12f3dbd:

    # "It was a blue yukata!"
    "¡Era una yukata!"

# game/dialogs.rpy:1189
translate spanish scene5_10b50536:

    # "I chuckled for a second. For a second, I thought it was one of her own yukata. I was laughing imagining how I would look like in a yukata made for girls."
    "Me reí. Pensé que era una de sus propias yukatas. Estaba imaginando cómo me vería con una yukata para chicas."

# game/dialogs.rpy:1190
translate spanish scene5_d616579f:

    # "But after a closer look, I notice that it was really a yukata for men, with the right cuttings and colors!"
    "Pero después de mirar más de cerca, noté que era una yukata para hombres, ¡con los correctos cortes y colores!"

# game/dialogs.rpy:1191
translate spanish scene5_ac50db4a:

    # hero "Whoa... For real? I can borrow this?"
    hero "Wow... ¿De verdad? ¿puedo... tomarlo prestado?"

# game/dialogs.rpy:1193
translate spanish scene5_3e45378b:

    # s "You can keep it if you want, %(stringhero)s-kun, it's okay!"
    s "Incluso te lo puedes quedar, %(stringhero)s-kun, ¡esta bien!"

# game/dialogs.rpy:1194
translate spanish scene5_68f22fc9:

    # hero "Gee...thank you... Thank you so much!"
    hero "Oh... Gracias... ¡Muchísimas gracias!"

# game/dialogs.rpy:1195
translate spanish scene5_882e22b6:

    # hero "Well... I gotta go... See you at school, Sakura-chan!"
    hero "Bien... Tengo que irme... Te veré en la escuela, ¡Sakura-chan!"

# game/dialogs.rpy:1196
translate spanish scene5_1a4bf588:

    # s "Have a nice weekend, %(stringhero)s-kun!"
    s "Ten un buen fin de semana, ¡%(stringhero)s-kun!"

# game/dialogs.rpy:1199
translate spanish scene5_bb555e9c:

    # "Sakura waved goodbye and went back into her house."
    "Sakura volvió a entrar a su casa."

# game/dialogs.rpy:1200
translate spanish scene5_020b0a9c:

    # "She really has a habit of helping me out in a jam!"
    "¡Ella realmente tiene un hábito de salvarme la vida!"

# game/dialogs.rpy:1201
translate spanish scene5_66a17c47:

    # "But it felt kinda strange."
    "Pero esto era extraño."

# game/dialogs.rpy:1202
translate spanish scene5_51ab6c7b:

    # "Whose yukata is this?"
    "¿Cómo obtuvo esta yukata?"

# game/dialogs.rpy:1203
translate spanish scene5_4735f789:

    # "It looked like it's sized for her..."
    "Se veía como fuera hecha para su talla..."

# game/dialogs.rpy:1204
translate spanish scene5_960d182a:

    # "Maybe it was her father's from when he was young..."
    "Oh... Tal vez era una yukata de su padre cuando era joven..."

# game/dialogs.rpy:1205
translate spanish scene5_cb071560:

    # "But...It looks brand new..."
    "Pero... Parece nuevo..."

# game/dialogs.rpy:1206
translate spanish scene5_0074c533:

    # "They couldn't have bought one by mistake..."
    "No pudieron comprarlo por error, ¿o si?..."

# game/dialogs.rpy:1212
translate spanish scene5_8c3a38cf:

    # hero "So... I'll see you later at school!"
    hero "Entonces... ¡Te veré en la escuela!"

# game/dialogs.rpy:1213
translate spanish scene5_c2d0f8e4:

    # s "See you around %(stringhero)s-kun!"
    s "Te veo luego ¡%(stringhero)s-kun!"

# game/dialogs.rpy:1214
translate spanish scene5_c0d35692:

    # s "I had a lot of fun with you today!"
    s "¡He tenido un montón de diversión contigo!"

# game/dialogs.rpy:1215
translate spanish scene5_da65aa5d:

    # hero "Likewise!"
    hero "¡Igualmente!"

# game/dialogs.rpy:1218
translate spanish scene5_bb555e9c_1:

    # "Sakura waved goodbye and went back into her house."
    "Ella se despide y corre para su casa."

# game/dialogs.rpy:1219
translate spanish scene5_c8d8f45f:

    # "Thanks to her, I knew how to get to the city and where the shops were."
    "Gracias a ella, sé como llegar a la ciudad y donde estaban las tiendas."

# game/dialogs.rpy:1220
translate spanish scene5_67c032e7:

    # "During my visit, I spotted some yukata shops."
    "En mi visita, encontré algunas tiendas de yukatas."

# game/dialogs.rpy:1221
translate spanish scene5_5854a13b:

    # "Tomorrow, I'll go there alone and I'll buy myself a yukata."
    "Mañana, iré solo y me compraré una yukata."

# game/dialogs.rpy:1222
translate spanish scene5_bb429b41:

    # "I'll catch that loudmouth Rika by surprise at the festival!"
    "¡Esa pequeña bocazas Rika-chan lo verá en el festival!"

# game/dialogs.rpy:1224
translate spanish scene5_a20cefa7:

    # "..."
    "..."

# game/dialogs.rpy:1225
translate spanish scene5_667ec4fd:

    # "Oh well... Let's go home, now..."
    "Oh bueno... Vamos a casa ahora..."

# game/dialogs.rpy:1246
translate spanish scene7_237e24d1:

    # centered "{size=+35}CHAPTER 3\nSakura's secret{fast}{/size}"
    centered "{size=+35}CÁPITULO 3\nEl secreto de Sakura{fast}{/size}"

# game/dialogs.rpy:1251
translate spanish scene7_58905c5b:

    # "Another week has passed."
    "Otra semana ha pasado."

# game/dialogs.rpy:1252
translate spanish scene7_19ce328a:

    # "School was great, with Rika, Nanami and Sakura around."
    "La escuela era grandiosa, con Rika-chan, Nanami-chan y Sakura-chan. "

# game/dialogs.rpy:1253
translate spanish scene7_2fc12cae:

    # "During these last two weeks, I really started to feel that I was a part of the village now."
    "Durante esas 2 semanas, realmente comencé a sentir que era parte del pueblo ahora."

# game/dialogs.rpy:1254
translate spanish scene7_2c9e68a3:

    # "I'm happy, but I'm unsure if I have feelings for Sakura..."
    "Estoy feliz pero no sé aún si tengo sentimientos por Sakura o no..."

# game/dialogs.rpy:1255
translate spanish scene7_909fa517:

    # "Or maybe even for Nanami..."
    "O tal vez incluso por Nanami..."

# game/dialogs.rpy:1257
translate spanish scene7_4dd65759:

    # "One thing's for sure, Nanami and I talk and play games more and more.{p}I think she really likes me."
    "Una cosa es segura, ella y yo estamos hablando y jugando más y más.{p}Pienso que a ella le agrado."

# game/dialogs.rpy:1258
translate spanish scene7_ef8f393e:

    # "But I still don't know what that thing Sakura told me about her was about..."
    "Pero aún no sé lo que es esa cosa que Sakura me contó..."

# game/dialogs.rpy:1259
translate spanish scene7_28d00c59:

    # "Well... At least I don't have any feelings for Rika...No way!"
    "Bueno... Al menos, ¡no puedo tener ningún sentimiento por Rika-chan! ¡De ninguna manera!"

# game/dialogs.rpy:1261
translate spanish scene7_10129566:

    # r "Hey, %(stringhero)s!"
    r "Oye, ¡%(stringhero)s!"

# game/dialogs.rpy:1266
translate spanish scene7_57e4be97:

    # "Yikes!!! Speak of the devil herself..."
    "¡¡¡Yikes!!! Hablando del diablo..."

# game/dialogs.rpy:1267
translate spanish scene7_391047cd:

    # hero "Oh, hey, Rika-chan..."
    hero "Oh, hola, Rika-chan..."

# game/dialogs.rpy:1268
translate spanish scene7_9c2ccd47:

    # r "Do you have some free time next weekend?"
    r "¿Tienes algo de tiempo libre el próximo fin de semana?"

# game/dialogs.rpy:1269
translate spanish scene7_158b0faf:

    # hero "This weekend?"
    hero "¿Esta semana?"

# game/dialogs.rpy:1270
translate spanish scene7_3a013f55:

    # r "No, you idiot. The weekend after this one!"
    r "No, idiota. ¡El fin de semana después de este!"

# game/dialogs.rpy:1271
translate spanish scene7_9d4e7e0f:

    # hero "Oh... Sure I guess. What do you need?"
    hero "Oh... Seguro. ¿Porqué?"

# game/dialogs.rpy:1272
translate spanish scene7_acfd623e:

    # r "Well, since you're a manly man that has muscles..."
    r "Bueno, ya que eres un hombre y por serlo tienes músculos..."

# game/dialogs.rpy:1273
translate spanish scene7_67864244:

    # r "I thought, maybe you could make yourself useful and help me prepare for festival!"
    r "Pensé que tal vez me podrías ayudar en ¡preparar cosas para el festival!"

# game/dialogs.rpy:1274
translate spanish scene7_e98e7579:

    # "Argh! Why did I agree before knowing why?!"
    "¡Argh! ¿¡Porqué dije \"seguro\" antes de conocer el porqué!? ¡Ahora estoy atascado!"

# game/dialogs.rpy:1275
translate spanish scene7_df0ced9f:

    # "Looks like I don't have a choice now..."
    "Parece que no tengo otra elección ahora..."

# game/dialogs.rpy:1283
translate spanish scene7_8b156e71:

    # hero "Alright, fine, I'll help."
    hero "¡Esta bien, claro!"

# game/dialogs.rpy:1285
translate spanish scene7_10eea5f6:

    # r "Great!"
    r "¡Grandioso!"

# game/dialogs.rpy:1287
translate spanish scene7_6f5d2035:

    # r "I'll tell you where to meet later!"
    r "¡Te diré nuestro lugar de reunión después!"

# game/dialogs.rpy:1288
translate spanish scene7_9766f460:

    # hero "Rika-chan, I didn't know you were from a temple."
    hero "Rika-chan, no sabía que eras del templo."

# game/dialogs.rpy:1289
translate spanish scene7_82bf0a2d:

    # r "My parents are shinto priests. Sometimes I help them for festivals and other temple related stuff."
    r "Mis padres son sacerdotes shinto. Algunas veces les ayudo para los festivales y cosas así."

# game/dialogs.rpy:1290
translate spanish scene7_c90a7240:

    # hero "Do you live in the temple with your parents?"
    hero "¿Y tu no vives en el templo con tus padres?"

# game/dialogs.rpy:1292
translate spanish scene7_a6c17ff8:

    # r "My..."
    r "Mis..."

# game/dialogs.rpy:1293
translate spanish scene7_95b777d9:

    # "Her face fell dark."
    "Su rostro se puso sombrío y perdió su sonrisa."

# game/dialogs.rpy:1294
translate spanish scene7_51eeeb24:

    # r "My parents divorced."
    r "Mis padres se divorciaron."

# game/dialogs.rpy:1295
translate spanish scene7_95db867f:

    # r "I live with my mother and my father guards the temple."
    r "Vivo con mi madre y mi padre cuida del templo."

# game/dialogs.rpy:1297
translate spanish scene7_5fbbef96:

    # r "But they're still on good terms and they still have their faith in the shinto order, so it's okay."
    r "Pero ellos aún están en buenos términos y ellos aún tienen su fé en la orden del shinto, así que está bien."

# game/dialogs.rpy:1298
translate spanish scene7_868b412a:

    # hero "Ah, I'm sorry, I didn't know. It's good they're on positive terms though."
    hero "Ah, bueno."

# game/dialogs.rpy:1299
translate spanish scene7_0d2419ec:

    # r "Yeah...I guess so. Anyway, I'll meet you at the club, see ya!"
    r "Muy bien, te veo en el club, ¡nos vemos!"

# game/dialogs.rpy:1301
translate spanish scene7_36df1f8b:

    # "I guess I learned something new about Rika. Still...I hope she won't be a total jerk towards me this weekend..."
    "Oh bueno.. Espero que ella no sea una total cretina hacía mí este fin de semana..."

# game/dialogs.rpy:1308
translate spanish scene7_219621f1:

    # s "%(stringhero)s-kun!!"
    s "¡¡%(stringhero)s-kun!!"

# game/dialogs.rpy:1309
translate spanish scene7_51a60250:

    # hero "Sakura-chan! How goes?"
    hero "¡Sakura-chan! ¿Cómo estas?"

# game/dialogs.rpy:1310
translate spanish scene7_f4d7051e:

    # "Sakura bowed at me. As she rose back up, I noticed her face was flushed."
    "Sakura se reverencia ante mí. Su rostro se ve enrojecido."

# game/dialogs.rpy:1312
translate spanish scene7_85835f50:

    # s "%(stringhero)s-kun,...are you doing anything this weekend?"
    s "%(stringhero)s-kun,... ¿Harás algo este fin de semana?"

# game/dialogs.rpy:1314
translate spanish scene7_31d438a9:

    # hero "The next weekend?"
    hero "¿El próximo fin de semana?"

# game/dialogs.rpy:1315
translate spanish scene7_9d85daa8:

    # hero "I'm sorry, I'm helping Rika-chan set up stuff for the festival."
    hero "No puedo. Ayudaré a Rika-chan a establecer las cosas para el festival."

# game/dialogs.rpy:1316
translate spanish scene7_77ee7366:

    # s "Huh?"
    s "¿Ah?"

# game/dialogs.rpy:1317
translate spanish scene7_8480da2c:

    # s "Oh... No, I mean, the weekend of this week"
    s "Oh... No, quiero decir, el fin de semana de esta semana."

# game/dialogs.rpy:1318
translate spanish scene7_cb5a59ac:

    # hero "Oh whoops. Guess my mind was wandering to far into the future... Hm..."
    hero "Oooh... Hm..."

# game/dialogs.rpy:1319
translate spanish scene7_d0aa7f64:

    # hero "I'm not doing anything at all."
    hero "Nada en especial..."

# game/dialogs.rpy:1320
translate spanish scene7_2217ef70:

    # hero "Do you want to go out to the city again?"
    hero "¿Excepto sacarte a una cita?"

# game/dialogs.rpy:1322
translate spanish scene7_1d124fd8:

    # "Sakura's face lit up and she started to giggled."
    "Sakura se rie."

# game/dialogs.rpy:1323
translate spanish scene7_0f5eeead:

    # s "I...I was about to ask you the same question! {image=heart.png}"
    s "¡Esa era la cosa que yo quería preguntarte! {image=heart.png}"

# game/dialogs.rpy:1324
translate spanish scene7_9e2e5ae5:

    # hero "Of course, I'd love to go out with you to the city!"
    hero "Por supuesto, ¡siempre me gusta salir contigo a la ciudad!"

# game/dialogs.rpy:1325
translate spanish scene7_22e33626:

    # hero "Yeah, I'd like to have even more fun than last time!"
    pass

# game/dialogs.rpy:1327
translate spanish scene7_c31b4dec:

    # s "Same here! I'll wait for you at the train station at 9AM!"
    s "¡Grandioso! Te esperaré en la estación del tren a las 9AM."

# game/dialogs.rpy:1328
translate spanish scene7_5409a3f5:

    # hero "Alright! I'll see you then!"
    hero "¡Esta bien!"

# game/dialogs.rpy:1330
translate spanish scene7_a7db0154:

    # "Ah, it's almost time for class..."
    "Ah, tiempo para las clases..."

# game/dialogs.rpy:1334
translate spanish scene7_13719c4f:

    # "The school day dragged on and on... Maybe I just couldn't wait to hang out with Sakura."
    "El día pasó rápidamente, como siempre... O tal vez no podía esperar para la cita..."

# game/dialogs.rpy:1335
translate spanish scene7_85ae4e7c:

    # "The next day came, and I arrived at the train station in the morning to go to the city with Sakura."
    "Entonces la cita con Sakura llegó a la mañana siguiente..."

# game/dialogs.rpy:1341
translate spanish SH1_e0b76a41:

    # "We took the train to the city. We started our day out eating at an American fast food place because we were already hungry."
    "Esta vez comenzamos la cita con la comida rápida americana porque ya teníamos hambre."

# game/dialogs.rpy:1342
translate spanish SH1_d68b3fb1:

    # "After that, we decided to try karaoke."
    "Luego de eso, fuímos al karaoke."

# game/dialogs.rpy:1344
translate spanish SH1_0c6feee4:

    # "Sakura's voice was absolutely mesmerizing."
    "La voz de Sakura era increíblemente hermosa."

# game/dialogs.rpy:1345
translate spanish SH1_a925770d:

    # hero "You could be a famous pop idol, Sakura-chan."
    hero "Podrías ser una muy popular ídolo pop, Sakura-chan."

# game/dialogs.rpy:1346
translate spanish SH1_ee5a9ea8:

    # s "Thank you... But I don't want to be one, though..."
    s "Gracias... Pero no quiero ser una..."

# game/dialogs.rpy:1347
translate spanish SH1_b809126d:

    # hero "Eh? Why is that?"
    hero "¿Eh? ¿Porqué es eso?"

# game/dialogs.rpy:1348
translate spanish SH1_93f8ec4b:

    # s "Well..."
    s "Bueno..."

# game/dialogs.rpy:1349
translate spanish SH1_3098f4a6:

    # "She hesitated a bit before replying."
    "Ella vaciló un largo tiempo antes de responder."

# game/dialogs.rpy:1350
translate spanish SH1_25b5d3d5:

    # s "I don't think I'd like the lifestyle of a pop idol..."
    s "Pienos que no me gustaría el estilo de vida de una ídolo popular..."

# game/dialogs.rpy:1351
translate spanish SH1_9c5f10fe:

    # s "It's so much work and you don't have a lot of time for yourself."
    s "Es mucho trabajo, y no tienes mucho tiempo para ti misma cuando haces eso."

# game/dialogs.rpy:1352
translate spanish SH1_67650f2f:

    # hero "Yeah, it must be rough..."
    hero "Si, entiendo..."

# game/dialogs.rpy:1353
translate spanish SH1_759de575:

    # hero "By the way, what would you would like to do in the future?"
    hero "Por cierto, ¿Qué te gustaría hacer en el futuro?"

# game/dialogs.rpy:1354
translate spanish SH1_9028f52b:

    # s "I don't really know yet..."
    s "No lo sé, todavía..."

# game/dialogs.rpy:1355
translate spanish SH1_36411a29:

    # s "Maybe I'll be a part of a symphony orchestra if I can."
    s "Tal vez ser parte de una orquesta sinfónica si pudiera..."

# game/dialogs.rpy:1356
translate spanish SH1_ba98acc9:

    # hero "Do you play any instruments?"
    hero "¿Tocas algún instrumento?"

# game/dialogs.rpy:1357
translate spanish SH1_cc1fe723:

    # s "Yes, the violin."
    s "Si, el violín."

# game/dialogs.rpy:1358
translate spanish SH1_5632a1a6:

    # "I suddenly remembered..."
    "De pronto recordé..."

# game/dialogs.rpy:1363
translate spanish SH1_1075e8d3:

    # "So the violin I hear sometimes at night... That must be her..."
    "Así que el violín que escuchaba de vez en cuando en las noches... Era ella..."

# game/dialogs.rpy:1368
translate spanish SH1_6ae339be:

    # hero "I think I hear you playing the violin during the evening from time to time."
    hero "¿Así que eres la que tocaba el violín durante las noches?"

# game/dialogs.rpy:1369
translate spanish SH1_ccbcdf1d:

    # "She looked down, kinda embarrassed."
    "Ella miró abajo, avergonzada."

# game/dialogs.rpy:1370
translate spanish SH1_457a5f49:

    # s "Oh... You heard me playing?"
    s "Oh... ¿Me escuchaste tocando?"

# game/dialogs.rpy:1371
translate spanish SH1_0027dd08:

    # hero "Yeah... You play so perfectly... It's amazing!"
    hero "Si... Tocaste tan perfectamente... ¡Me asombraste!"

# game/dialogs.rpy:1372
translate spanish SH1_55a77664:

    # s "W-why T-t-thank you..."
    s "G-g-gracias..."

# game/dialogs.rpy:1373
translate spanish SH1_7b7e5c26:

    # hero "I hope I can watch you play someday."
    hero "Espero verte tocando algún día."

# game/dialogs.rpy:1374
translate spanish SH1_32eac59f:

    # s "I'd be happy to play for you, %(stringhero)s-kun!"
    s "Estaría feliz de tocar para ti, ¡%(stringhero)s-kun!"

# game/dialogs.rpy:1378
translate spanish SH1_3e846a41:

    # "I tried to sing the next song, but I was horribly awful at singing. At least it made Sakura laugh."
    "Después de algunas canciones, donde yo era horrible cantando (y que al menos hizo reír a Sakura-chan)."

# game/dialogs.rpy:1379
translate spanish SH1_7f6f8e16:

    # "When we were done, we headed back to to the city's train station."
    "Estábamos a punto de volver a la estación de la ciudad."

# game/dialogs.rpy:1383
translate spanish SH1_884ab08c:

    # "As we were about to cross the road..."
    "Repentinamente, cuando íbamos a cruzar el camino."

# game/dialogs.rpy:1384
translate spanish SH1_5b2451aa:

    # "A car, ignoring the red light, kept driving and almost hit Sakura!"
    "Un carro pasó muy rápido cerca de nosotros, cruzando en luz roja."

# game/dialogs.rpy:1385
translate spanish SH1_d2261bcd:

    # "Sakura quickly stepped back but lost her balance and was about to fall on her back."
    "Sakura retrocedió rápidamente y cayó de espaldas."

# game/dialogs.rpy:1387
translate spanish SH1_eb7a286f:

    # "I managed to catch her just in time and she fell into arms."
    "La agarré justo a tiempo y cayó en mis brazos."

# game/dialogs.rpy:1388
translate spanish SH1_e5780a08:

    # "She looked into my eyes and I got lost in hers."
    "Ella me miró con sus grandes y hermosos ojos azules."

# game/dialogs.rpy:1389
translate spanish SH1_ac769ce2:

    # "Time stopped."
    "El tiempo paró."

# game/dialogs.rpy:1390
translate spanish SH1_e75587c1:

    # "As we stared at each other, we forgot all about the car incident."
    "Mientras nos mirábamos el uno al otro, casí nos olvidamos sobre el incidente del carro."

# game/dialogs.rpy:1391
translate spanish SH1_0657ef81:

    # s "%(stringhero)s....-kun...."
    s "%(stringhero)s....-kun...."

# game/dialogs.rpy:1392
translate spanish SH1_c6dc2ed4:

    # hero "....Sakura-chan..."
    hero "....Sakura-chan..."

# game/dialogs.rpy:1394
translate spanish SH1_ae0ed22a:

    # "Her eyes slowly shut, her face closed in on mine, her small mouth beckoned for something... It was her lips that seemed to be waiting for me."
    "Ella cerró sus ojos, dirigiéndome su pequeña boca sin cerrarla por completo... Sus labios parecían esperar por mí. "

# game/dialogs.rpy:1395
translate spanish SH1_07a9d021:

    # "Oh my gosh... I think she wants me to kiss her! I can't believe it! What do I do!?"
    "Oh por Dios... ¡Pienso que ella quiere que la bese! ¡No lo puedo creer!"

# game/dialogs.rpy:1397
translate spanish SH1_4b8ffc21:

    # "I could hear my heart beating loudly. It felt so loud, I was almost sure that the people around us could hear it too."
    "Mi corazón estába latiendo fuertemente... Tan fuerte que estába seguro que la gente alrededor nuestro podía escucharlo también."

# game/dialogs.rpy:1398
translate spanish SH1_fe20f7a5:

    # "This was it. It's now or never. My face slowly approached hers..."
    "Me acerqué a su rostro, lentamente..."

# game/dialogs.rpy:1399
translate spanish SH1_8538723d:

    # "Very slowly..."
    "Muy lentamente..."

# game/dialogs.rpy:1400
translate spanish SH1_f1ab825c:

    # "I could feel her breath on my face."
    "Casi podía sentir su aliento en mi rostro."

# game/dialogs.rpy:1405
translate spanish SH1_16f58986:

    # "And the second my nose touched hers...{p}She turned her face away, completely embarrassed, and stood up almost immediately."
    "Al segundo que mi nariz tocó el suyo....{p}Ella volteó su rostro, avergonzada, y se levantó."

# game/dialogs.rpy:1406
translate spanish SH1_56dcd521:

    # s "I'm... I'm okay..."
    s "Estoy... Estoy bien..."

# game/dialogs.rpy:1410
translate spanish SH1_557b9c7c:

    # s ". . ."
    s ". . ."

# game/dialogs.rpy:1411
translate spanish SH1_b64fd408:

    # s "Let's go home..."
    s "Vámonos a casa..."

# game/dialogs.rpy:1416
translate spanish SH1_32abdb6f:

    # "On the train ride home, I was in a complete daze..."
    "Estaba aturdido mientas regresábamos a casa..."

# game/dialogs.rpy:1417
translate spanish SH1_06b56af7:

    # "I thought she wanted me to kiss her... But in the end, she refused..."
    "Estaba muy seguro que ella quería que la besara... Pero al final, fuí rechazado..."

# game/dialogs.rpy:1418
translate spanish SH1_a20cefa7:

    # "..."
    "..."

# game/dialogs.rpy:1419
translate spanish SH1_23dda12a:

    # "Dammit, stop being paranoid, %(stringhero)s!...{p}Maybe it's just because it was her first time. She might not have felt ready..."
    "Oh, deja de ser paranoico, ¡%(stringhero)s!...{p}Tal vez es porque es su primera vez y ella es tan tímida que no se sintió lista para ello..."

# game/dialogs.rpy:1420
translate spanish SH1_e1f82eca:

    # "I felt kinda happy inside though. At least it means that she's attracted to me... I think..."
    "Aunque me sentía feliz por dentro. Porque eso significaba que ella estaba atraída por mí..."

# game/dialogs.rpy:1421
translate spanish SH1_a3dbd16d:

    # "But I also felt just as confused..."
    "Pero también me siento confundido..."

# game/dialogs.rpy:1422
translate spanish SH1_246f930a:

    # "As we sat together in silence, I occasionally looked over to Sakura."
    "Miraba a Sakura de vez en cuando.."

# game/dialogs.rpy:1423
translate spanish SH1_acd10f88:

    # "She didn't look angry. But she seemed really embarrassed still."
    "Ella no se veía enojada. Se miraba avergonzada."

# game/dialogs.rpy:1424
translate spanish SH1_045668b1:

    # "But, most of all, she looked upset about something. She just sat there lost in her thoughts like she was profoundly thinking about something."
    "Pero sobre todo, ella parecía estar profundamente pensativa sobre algo."

# game/dialogs.rpy:1427
translate spanish SH1_3c386e89:

    # "Sakura was completely quiet along the way."
    "Sakura estaba callada por completo durante el camino."

# game/dialogs.rpy:1428
translate spanish SH1_849b7889:

    # "I didn't know what to say...or what to do."
    "Estaba avergonzado..."

# game/dialogs.rpy:1429
translate spanish SH1_c312d8ea:

    # "The atmosphere was uncomfortable."
    "La atmósfera era incómodo desde el incidente del carro."

# game/dialogs.rpy:1430
translate spanish SH1_59826635:

    # "Suddenly, she broke the silence."
    "Repentinamente, ella rompió el silencio."

# game/dialogs.rpy:1431
translate spanish SH1_1eeb4b66:

    # s "...%(stringhero)s-kun?"
    s "...¿%(stringhero)s-kun?"

# game/dialogs.rpy:1432
translate spanish SH1_4edce7c8:

    # hero "Yes?"
    hero "¿Si?"

# game/dialogs.rpy:1434
translate spanish SH1_782acf27:

    # s "I...{p}........................{p}I have something I want to tell you..."
    s "Yo...{p}........................{p}Tengo algo que contarte..."

# game/dialogs.rpy:1435
translate spanish SH1_d2dcc110:

    # hero "What is it?"
    hero "¿Qué es?"

# game/dialogs.rpy:1436
translate spanish SH1_719ce3f8:

    # "I followed her as she entered a rice field."
    "Mientras que conversábamos, ella entró a un arrozal. La seguí... "

# game/dialogs.rpy:1437
translate spanish SH1_d7b94be6:

    # "The path inside was small, surrounded by big rice plants."
    "El camino era pequeño y rodeado de grandes plantas de arroz."

# game/dialogs.rpy:1438
translate spanish SH1_481e4154:

    # "It was completely desolate. There was nobody around us. Only the chants of the cicadas could be heard."
    "Era un camino vació. No había nadie alrededor nuestro... Solo el canto de las cigarras se podían escuchar."

# game/dialogs.rpy:1440
translate spanish SH1_8329e10f:

    # s "This is something very difficult to tell you...{p}Painfully difficult..."
    s "Es algo difícil contártelo...{p}Muy difícil..."

# game/dialogs.rpy:1441
translate spanish SH1_c02f0d8f:

    # s "This isn't something you might not even comprehend, but..."
    s "Tan difícil para mí contártelo a ti... como lo es para ti entenderlo..."

# game/dialogs.rpy:1442
translate spanish SH1_7cf20b6f:

    # hero "This sounds pretty serious."
    hero "Eso suena serio..."

# game/dialogs.rpy:1443
translate spanish SH1_a4c7d906:

    # "I could feel a sort of anxiety building up from inside me."
    "Sentí mi corazón latir."

# game/dialogs.rpy:1444
translate spanish SH1_ec713d94:

    # "I've never seen Sakura behave this seriously before."
    "Nunca vi a Sakura-chan tan determinada antes."

# game/dialogs.rpy:1445
translate spanish SH1_49f22c99:

    # s "I'm about to tell you this because I trust you."
    s "Estoy por decirte esto porque confío en ti. "

# game/dialogs.rpy:1446
translate spanish SH1_afd8f97b:

    # "I smiled, but I was nervous."
    "Sonreí."

# game/dialogs.rpy:1447
translate spanish SH1_5066f9b8:

    # hero "Y-you can trust me, Sakura-chan. You're my friend. You, Rika and Nanami are my best friends and I'll never betray any of you. Never."
    hero "Y puedes hacerlo, Sakura-chan. Eres mi amiga. Tu, Rika y Nanami son mis mejores amigas y nunca las traicionaré a ninguna de ustedes. Nunca."

# game/dialogs.rpy:1448
translate spanish SH1_9b73dceb:

    # "I mean that, from the bottom of my heart."
    "Sinceramente lo hago desde el fondo de mi corazón."

# game/dialogs.rpy:1449
translate spanish SH1_64b40e00:

    # s "Do you promise? Never?"
    s "¿Lo prometes? ¿Nunca?"

# game/dialogs.rpy:1450
translate spanish SH1_b7fb0fa9:

    # hero "Forever and ever."
    hero "Para siempre y por siempre."

# game/dialogs.rpy:1451
translate spanish SH1_ac397e42:

    # "This time I smiled with a little more confidence."
    "Sonreí otra vez."

# game/dialogs.rpy:1452
translate spanish SH1_43dc30fb:

    # "She gave a faint smile but reverted back to her serious face after."
    "Ella me devolvió la sonrisa levemente pero manteniendo su cara seria inmediatamente después."

# game/dialogs.rpy:1453
translate spanish SH1_ee01b059:

    # s "It's... So hard for me to tell you...{p}Besides my parents, only Rika-chan knows about it.{p}Not even Nanami-chan knows..."
    s "Es... Tan difícil decirtelo..."
    s "En realidad, además de mis padres, solo Rika-chan conoce sobre ello.{p}Ni siquiera Nanami-chan lo conoce...Aún..."

# game/dialogs.rpy:1454
translate spanish SH1_a56d7a94:

    # s "Everyone else seems to have just heard rumors about it... or aren't sure at all..."
    s "Todo los demás que parecen saberlo solo han escuchado rumores sobre ello... O no están seguros..."

# game/dialogs.rpy:1455
translate spanish SH1_d0d64bc6:

    # hero "Oh..."
    hero "Oh..."

# game/dialogs.rpy:1456
translate spanish SH1_2fcf5cc9:

    # "I was confused... What was she trying to say?"
    "Estaba confundido...¿Qué es lo que esta tratando de decir?"

# game/dialogs.rpy:1457
translate spanish SH1_0ebf712b:

    # "Obviously, she and Rika-chan knew a secret.{p}And Sakura-chan was about to tell me this secret."
    "Obviamente, ella y Rika-chan saben el secreto.{p}Y Sakura-chan estaba a punto de decirme este secreto."

# game/dialogs.rpy:1458
translate spanish SH1_4df61edb:

    # "I feel happy. That means she must feel I'm a close enough friend and that she really trusts me."
    "Me siento feliz. Eso significa que ella me ha aceptado como un mejor amigo y confía en mí."

# game/dialogs.rpy:1460
translate spanish SH1_32e60ae0:

    # "She stopped after walking a small distance from me, facing away from me."
    "Ella se detuvo después de caminar tres metros de mí, de espaldas."

# game/dialogs.rpy:1461
translate spanish SH1_a0dd6cdb:

    # "Her body covered the sun and I saw only her shadow in the distance."
    "Ella estaba enfrente del sol así que solo podía mirar su sombra."

# game/dialogs.rpy:1463
translate spanish SH1_e1554201:

    # s "..."
    s "..."

# game/dialogs.rpy:1464
translate spanish SH1_007ceac2:

    # s "%(stringhero)s-kun..."
    s "%(stringhero)s-kun..."

# game/dialogs.rpy:1465
translate spanish SH1_d5eced1a:

    # s "Do you remember the day when those thugs bullied me?"
    s "¿Recuerdas el día cuando esos rufianes me acosaron?"

# game/dialogs.rpy:1466
translate spanish SH1_d19c5e51:

    # hero "Yes, I remember."
    hero "Si, lo recuerdo."

# game/dialogs.rpy:1467
translate spanish SH1_3b33aa23:

    # s "Do you remember what they said to me?"
    s "¿Recuerdas lo que me dijeron?"

# game/dialogs.rpy:1468
translate spanish SH1_ec6b7626:

    # hero "Yeah, I remember."
    hero "Si, claramente."

# game/dialogs.rpy:1469
translate spanish SH1_b28fcf52:

    # hero "They were trying to make you admit that you were a boy, or something like that."
    hero "Ellos trataron de hacerte admitir que eres un chico, algo como eso."

# game/dialogs.rpy:1470
translate spanish SH1_bf9bdd8b:

    # s "Yes."
    s "Si."

# game/dialogs.rpy:1471
translate spanish SH1_b5a2fb72:

    # s "It's because they..."
    s "Eso es porque ellos..."

# game/dialogs.rpy:1472
translate spanish SH1_9fce5758:

    # s "They guessed it... For some reason they knew it..."
    s "Ellos lo supusieron... Por alguna razón que ellos conocen..."

# game/dialogs.rpy:1473
translate spanish SH1_bbc36fab:

    # hero "I... What are you trying to say?..."
    hero "Yo... ¿Qué es lo tratas de decir?..."

# game/dialogs.rpy:1474
translate spanish SH1_007ceac2_1:

    # s "%(stringhero)s-kun..."
    s "%(stringhero)s-kun..."

# game/dialogs.rpy:1483
translate spanish SH1_e4a4445c:

    # s "I {b}really{/b} am a boy."
    s "Yo {b}realmente{b} soy un chico."

# game/dialogs.rpy:1484
translate spanish SH1_16bb48d0:

    # hero "...W-what?"
    hero "...¿Qué?"

# game/dialogs.rpy:1505
translate spanish SH1_47f1e757:

    # centered "{size=+35}CHAPTER 4\nBig decisions{fast}{/size}"
    centered "{size=+35}CÁPITULO 4\nGrandes decisiones{fast}{/size}"

# game/dialogs.rpy:1515
translate spanish SH1_bb09a9d7:

    # hero "You... You're... a boy!?"
    hero "Tu... Eres... ¿¡Un chico!?"

# game/dialogs.rpy:1516
translate spanish SH1_a82ece96:

    # "My mind went completely numb for a second."
    "No sabía en que pensar."

# game/dialogs.rpy:1517
translate spanish SH1_7bdf1789:

    # "I couldn't believe it."
    "No lo podía creer."

# game/dialogs.rpy:1518
translate spanish SH1_cd989119:

    # "No, she couldn't be a boy!{p} That's impossible!!!"
    "No, ¡ella no podía ser un chico!{p}¡¡¡Es imposible!!!"

# game/dialogs.rpy:1519
translate spanish SH1_9f02c3f2:

    # "I started to nervously laugh to myself. There's no way. It was just a joke..."
    "Comencé a reírme para mi mismo, nerviosamente. No lo podía creer. De seguro era una broma."

# game/dialogs.rpy:1520
translate spanish SH1_1faf5341:

    # hero "Haha it's a joke...No way...No, you can't be a boy, Sakura-chan!"
    hero "No, no puedes ser un chico, ¡Sakura-chan!"

# game/dialogs.rpy:1521
translate spanish SH1_b293358c:

    # "Sakura-chan didn't say a thing."
    "Sakura-chan no dijo una sola palabra."

# game/dialogs.rpy:1522
translate spanish SH1_b2a3712a:

    # "She suddenly turned around, and approached me."
    "Ella repentinamente se volteó y se me acerco."

# game/dialogs.rpy:1523
translate spanish SH1_fcff274b:

    # s "...If you don't believe me, take a look for yourself!"
    s "...Si no me crees, ¡toma un vistazo a esto!"

# game/dialogs.rpy:1528
translate spanish SH1_93439998:

    # "She suddenly lifted her skirt up, showing me her panties!"
    "Ella de pronto se levantó su falda, ¡enseñandome sus bragas!"

# game/dialogs.rpy:1529
translate spanish SH1_6a7c30f8:

    # "I gasped out of embarrassment and shock. I took a look around, but fortunately we were still alone in the rice fields."
    "Grité de la vergüenza y la sorpresa. Miré alrededor, pero afortunadamente aún estabamos solos en los arrozales."

# game/dialogs.rpy:1530
translate spanish SH1_6db43b58:

    # "I tried my hardest to not look at Sakura and covered my eyes."
    "Difícilmente traté de no mirar a Sakura y cubrir mis ojos."

# game/dialogs.rpy:1531
translate spanish SH1_7987ae0e:

    # hero "No, Sakura-chan, I can't look, this is just inappropriate!"
    hero "No, Sakura-chan, no puedo mirar, ¡es demasiado embarazoso!"

# game/dialogs.rpy:1532
translate spanish SH1_ea014391:

    # s "Please, just look at me!"
    s "¡Por favor hazlo!"

# game/dialogs.rpy:1533
translate spanish SH1_14a15247:

    # "I peeked a little and could see her eyes intensely glaring at me. She was dead serious."
    "Sus lindos ojos azules estaban mirándome. Ella estaba realmente seria."

# game/dialogs.rpy:1534
translate spanish SH1_453421f4:

    # "My eyes slowly scrolled down to look below her waistline."
    "Entonces lentamente miré abajo a su cintura."

# game/dialogs.rpy:1535
translate spanish SH1_952d1725:

    # "She was wearing blue-colored panties..."
    "Ella estaba usando unas lindas bragas azules de chica."

# game/dialogs.rpy:1536
translate spanish SH1_ede13101:

    # "But then, I noticed that...in her panties...there was...something.{p}Something that shouldn't have been there."
    "Pero en sus bragas, había... Algo.{p}Algo que no debería estar ahí."

# game/dialogs.rpy:1537
translate spanish SH1_dbbe62c0:

    # "...But the evidence was right there... "
    "...La evidencia estaba ahí..."

# game/dialogs.rpy:1538
translate spanish SH1_a97eb7ed:

    # "I couldn't believe my eyes but the truth was there..."
    "Mis ojos no lo podían creer pero la verdad estaba ahí..."

# game/dialogs.rpy:1540
translate spanish SH1_6ae69507:

    # "She was really a HE!!!"
    "¡¡¡Ella era realmente un ÉL!!!"

# game/dialogs.rpy:1541
translate spanish SH1_20403ffb:

    # "I started to shake."
    "Comencé a temblar."

# game/dialogs.rpy:1542
translate spanish SH1_99484c94:

    # "So the cute girl I was following for school.{p}The wonderful girl who was talking about manga and anime with Rika and Nanami..."
    "Entonces la linda chica que estaba siguiendo a la escuela.{p}La maravillosa chica quien estaba hablando de mangas y animes con Rika-chan..."

# game/dialogs.rpy:1543
translate spanish SH1_0e81ba72:

    # "The girl I was infatuated with for almost two weeks...{p}The girl I was about to kiss today..."
    "La chica con quien salí por casi dos semanas...{p}La chica que estaba a punto de besarla hoy..."

# game/dialogs.rpy:1544
translate spanish SH1_a2b0302d:

    # "That girl... Was a boy! A guy! A dude! A male!!!!!"
    "Esa chica... ¡Era un chico! ¡Un tío! ¡Un tipo! ¡¡¡¡¡¡Un hombre!!!!!!"

# game/dialogs.rpy:1545
translate spanish SH1_51a16ccf:

    # "I stood there, motionless."
    "Estuve parado, inmóvil."

# game/dialogs.rpy:1546
translate spanish SH1_0cbdd6b2:

    # hero "You... yo-yo-yo-yo-you're a..."
    hero "Tu...T-t-t-t-t-t-t-tu eres un..."

# game/dialogs.rpy:1547
translate spanish SH1_29fb07f0:

    # hero "{size=+10}You're a boy?!{/size}"
    hero "{size=+10}¡¿Eres un chico?!{/size}"

# game/dialogs.rpy:1548
translate spanish SH1_37a5e581:

    # "I ended up shouted at her. I couldn't believe it."
    "Grité. No lo podía creer."

# game/dialogs.rpy:1549
translate spanish SH1_024d5fe2:

    # "My outburst made her...I mean him a bit scared. He stepped back a little."
    "Mi estruendo la... Quiero decir lo asustó un poco. Retrocedió un poco."

# game/dialogs.rpy:1550
translate spanish SH1_12900b8c:

    # hero "Sorry... Sorry, Sakura-chan, I didn't mean to scare you..."
    hero "Lo siento... Lo siento, Sakura-chan, no tenía intención de asustarte..."

# game/dialogs.rpy:1551
translate spanish SH1_a613c480:

    # hero "Huh...no, I... I mean, Sakura-kun...? Huh... Aaaaargh!!"
    hero "Huh no, Yo... Quiero decir, Sakura-kun... Huh... ¡¡Aaaaargh!!"

# game/dialogs.rpy:1552
translate spanish SH1_32d25ac1:

    # "My mind felt like it split into a million pieces."
    "Me sentí completamente confundido y atolondrado."

# game/dialogs.rpy:1553
translate spanish SH1_e0277eeb:

    # hero "It's... incredible... You..."
    hero "Es... Increíble...Tú..."

# game/dialogs.rpy:1554
translate spanish SH1_00287bf2:

    # hero "But... You know, I..."
    hero "Pero... Sabes, yo..."

# game/dialogs.rpy:1559
translate spanish SH1_a783e872:

    # "As if reading my mind, Sakura turned around, facing the sun again, and he... or she... started to calmly speak."
    "Asumiendo mis preguntas, Sakura se volteó, dirigiéndose al sol, y él... O ella... Comenzó a hablar calmadamente."

# game/dialogs.rpy:1561
translate spanish SH1_b0e013d4:

    # s "I was born with a very rare... {w}Well, I don't know if it's a genital disorder or something..."
    s "Yo nací con un muy raro...{w}Bien, no sé si es un desorden genital o algo..."

# game/dialogs.rpy:1562
translate spanish SH1_41913518:

    # s "Technically, I inherited all the chromosomes of my mother, but I got a Y chromosome anyway."
    s "Técnicamente, yo heredé todas las cromosomas de mi madre, pero tuve un cromosoma Y de todas maneras."

# game/dialogs.rpy:1563
translate spanish SH1_f7622dac:

    # s "It made me a very girlish boy. So girlish that even my brain is one of a girl."
    s "Eso me hizo un chico muy femenino. Tan femenino que incluso mi cerebro es el de una chica."

# game/dialogs.rpy:1564
translate spanish SH1_20638c61:

    # s "I was a normal boy when I was young, but my mind and body transformed me as I grew older."
    s "Era un chico normal cuando era más joven, pero mi mente y cuerpo se transformaron a medida que crecía."

# game/dialogs.rpy:1565
translate spanish SH1_1cb3b079:

    # s "I was more attracted by dolls than little cars, and I preferred talking with girls than playing soccer with the boys..."
    s "Estaba más atraído por muñecas que pequeños carros, y prefería hablar con chicas que jugar fútbol con los chicos..."

# game/dialogs.rpy:1566
translate spanish SH1_425c4b72:

    # s "When I was 11-years-old, my dad started to get exhausted of this... \"joke\", that change inside of me... So he started getting me back into \"male stuff\"..."
    s "Cuando tenía 11 años, mi papá comenzó a cansarse de esta \"comedia\", que cambiaba dentro de mí... Entonces él comenzó a introducirme de nuevo a las \"cosas másculinas\"..."

# game/dialogs.rpy:1567
translate spanish SH1_c69a2dcb:

    # s "He bought me outfits and books usually made for boys..."
    s "Él me compraba trajes y libros usualmente hechos para chicos..."

# game/dialogs.rpy:1568
translate spanish SH1_31185f69:

    # s "But as time passed by...he started to drink more... and his alcohol addiction grew. Ge started to become more violent."
    s "Pero como pasaba el tiempo y su adicción al alcohol crecía, él comenzó a ser más violento."

# game/dialogs.rpy:1569
translate spanish SH1_c8d0b471:

    # s "He even tried to cut my hair once."
    s "Él incluso trató de cortar mi cabello una vez."

# game/dialogs.rpy:1570
translate spanish SH1_f480d83d:

    # s "But my mother protected me. She has always accepted me for who I am, no matter what."
    s "Pero mi madre me protegió. Ella siempre me aceptó y a mis decisiones, no importando qué."

# game/dialogs.rpy:1571
translate spanish SH1_64529bc1:

    # s "I can't help it. {p}I feel like a girl inside, that's all."
    s "No puedo evitarlo.{p}Me siento como una chica por dentro, eso es todo."

# game/dialogs.rpy:1572
translate spanish SH1_f674ff4c:

    # s "Actually, my father is really pissed off at me. My mother tries to protect me but my father becomes crazy when I'm around..."
    s "En realidad, mi padre está realmente enfadado conmigo. Mi madre trata de protegerme pero mi padre enloquece cuando estoy cerca..."

# game/dialogs.rpy:1573
translate spanish SH1_b935cc96:

    # "I nodded, taking in her...his monologue slowly, digesting his story..."
    "Asentí, asimilando su monólogo lentamente, digiriendo su historia..."

# game/dialogs.rpy:1575
translate spanish SH1_0317f7d7:

    # "I didn't know what to do..."
    "No sabía que hacer..."

# game/dialogs.rpy:1576
translate spanish SH1_03c57d51:

    # "His...her life must have been pretty tough growing up...but..."
    "De cualquier forma, él... Ella no estaba muy feliz sobre su vida..."

# game/dialogs.rpy:1577
translate spanish SH1_20a5c853:

    # hero "So you want to be a girl. No matter what everybody thinks, and whatever your own body thinks, you want to be a girl and that's all."
    hero "Entonces tú quieres ser una chica. No importando lo que piensen los demás, y cualquier cosa que tu propio cuerpo piense, tú quieres ser una chica y eso es todo."

# game/dialogs.rpy:1578
translate spanish SH1_10c7cae4:

    # s "Yes...more than anything."
    s "Si..."

# game/dialogs.rpy:1579
translate spanish SH1_71387f7a:

    # hero "Hmm..."
    hero "Hmm..."

# game/dialogs.rpy:1580
translate spanish SH1_60e224a2:

    # "I thought for a moment...and I finally smiled and accepted it."
    "Lo pensé por un momento, pero finalmente sonreí y lo acepté."

# game/dialogs.rpy:1581
translate spanish SH1_c35613b6:

    # "After all... Maybe she's a boy, but Sakura is still Sakura."
    "Oye después de todo... Tal vez ella es un chico, pero Sakura es aún Sakura, ¿no lo crees?"

# game/dialogs.rpy:1582
translate spanish SH1_de287c57:

    # hero "Hmm okay... It's okay for me..."
    hero "Hmm esta bien... Esta bien para mí..."

# game/dialogs.rpy:1583
translate spanish SH1_5c82e516:

    # s "Really?"
    s "¿En serio?"

# game/dialogs.rpy:1584
translate spanish SH1_456228e3:

    # hero "Yeah! If you're happy like this, then I'm happy for you as well!"
    hero "¡Si! como sea... Si tú eres feliz así, ¡estoy feliz por ti también!"

# game/dialogs.rpy:1585
translate spanish SH1_923bc3f8:

    # "Sakura was still facing away from me, she still seemed a little unsatisfied."
    "Sakura aún estaba de espaldas."

# game/dialogs.rpy:1586
translate spanish SH1_8a085455:

    # s "You must have some questions at least, don't you?"
    s "Tú tienes preguntas, verdad."

# game/dialogs.rpy:1587
translate spanish SH1_bc81f33c:

    # "Sheesh...she's right, I do have a lot of questions. It's scary how he...uh...she can read my mind."
    "Eso no era una pregunta. ¡Me asusta cómo él o ella pueda leer mi mente en este momento!"

# game/dialogs.rpy:1588
translate spanish SH1_9614bee5:

    # "She spoke softly."
    "Sentí una sonrisa en su voz."

# game/dialogs.rpy:1589
translate spanish SH1_3caf349f:

    # s "Go ahead, please. You're my friend, it's normal to have questions about this. And you have the right to know more."
    s "Adelante, por favor. Eres mi amigo, es normal tener preguntas sobre esto y tienes el derecho a saber más."

# game/dialogs.rpy:1590
translate spanish SH1_64036f15:

    # "I calmed myself down a bit and gave Sakura a smile."
    "Sonreí."

# game/dialogs.rpy:1591
translate spanish SH1_9877ea0f:

    # hero "Okay...{p}So you prefer that everyone say 'she' to you instead of 'he'?"
    hero "Esta bien...{p}¿Entonces prefieres que todos te digan 'ella' en vez de 'él'?"

# game/dialogs.rpy:1592
translate spanish SH1_bf9bdd8b_1:

    # s "Yes."
    s "Si."

# game/dialogs.rpy:1593
translate spanish SH1_caa3249e:

    # hero "I see... Alright..."
    hero "Ya veo... Esta bien..."

# game/dialogs.rpy:1594
translate spanish SH1_20b19b0b:

    # "The problem of she and he is all solved now. It feels as confusion is already going away."
    "El problema de ella y él esta resuelto ahora. Sonreí mientras la confusión comenzaba a irse."

# game/dialogs.rpy:1595
translate spanish SH1_9ef84186:

    # hero "And, well... What about your... uhh... b... breasts?"
    hero "Y, bien... ¿Qué hay sobre tus... uhh.. S... Senos?"

# game/dialogs.rpy:1596
translate spanish SH1_28e89323:

    # hero "I see that y-your chest is... Well..."
    hero "Veo que t-tus pechos son... Bueno..."

# game/dialogs.rpy:1597
translate spanish SH1_7622ed66:

    # s "Mmm? Oh... {p}Just a bra with some cotton inside..."
    s "¿Mmm? Oh...{p}Es solo un sostén con algodón dentro..."

# game/dialogs.rpy:1598
translate spanish SH1_38942254:

    # hero "Oh...why do that?"
    hero "¿Y porqué esos?"

# game/dialogs.rpy:1599
translate spanish SH1_18c07f8f:

    # s "I've always wanted real breasts and would like to wear a bra..."
    s "Siempre he querido tener senos reales y usar un sostén..."

# game/dialogs.rpy:1600
translate spanish SH1_f56274ef:

    # s "They don't look big as Rika-chan's but it's enough to make me feel like I have breasts."
    s "Ellos no se miran tan grandes como los de Rika-chan pero son suficientes par hacerme sentir que tengo senos."

# game/dialogs.rpy:1601
translate spanish SH1_972441ce:

    # hero "Oh, on the same subject...Have you ever thought about doing... surgery?"
    hero "Oh, hablando sobre esto... ¿Haz pensado alguna vez sobre hacerte... Cirugía?"

# game/dialogs.rpy:1602
translate spanish SH1_a711c810:

    # s "Yes, I've thought about it..."
    s "Pensé sobre ello..."

# game/dialogs.rpy:1603
translate spanish SH1_a18ea8c8:

    # s "But I don't want to... I'm too afraid of surgery!"
    s "Pero no quiero... ¡Estoy demasiado asustada de la cirugía!"

# game/dialogs.rpy:1604
translate spanish SH1_27bb92e4:

    # "The way she said that sounded pretty cute but she seemed pretty frustrated."
    "Tan linda."

# game/dialogs.rpy:1605
translate spanish SH1_b5f35068:

    # "...huh? What the heck am I saying? It's a guy!"
    "...¿Ah? ¿Qué diablos estoy diciendo? ¡Es un chico!"

# game/dialogs.rpy:1606
translate spanish SH1_d25154cc:

    # "I can't fall in love with a dude! Even if he looks like a girl!"
    "¡No puedo enamorarme de un chico! ¡Incluso si se mira como una chica!"

# game/dialogs.rpy:1607
translate spanish SH1_86c14a59:

    # "Even if he looks like...{w}such a wonderful girl...{w}who seems so fragile in the shadow..."
    "Incluso si él se mira...{w}Como una maravillosa chica...{w}Que parece tan frágil en la sombra..."

# game/dialogs.rpy:1608
translate spanish SH1_31273a30:

    # "Ugh dammit! What should I do?! I'm still confused!!!"
    "¡¿Maldición, qué es lo que debería hacer?! ¡¡¡Estoy perdido!!!"

# game/dialogs.rpy:1612
translate spanish SH1_7072f3b4:

    # "No..."
    "No..."

# game/dialogs.rpy:1613
translate spanish SH1_7eb4948e:

    # "No way... I can't! I am a pure and straight guy..."
    "No hay forma...¡No puedo! Soy un chico puro y heterosexual..."

# game/dialogs.rpy:1614
translate spanish SH1_5b52ab19:

    # "But whatever...{p}What's the big deal?"
    "Pero de cualquier forma...{p}¿Cuál es el problema?"

# game/dialogs.rpy:1615
translate spanish SH1_81380db8:

    # "It's a boy? Okay. So what? I'm just fixed on her gender."
    "¿Es un chico? Esta bien. ¿Entonces qué? Solo estoy fijado en su género..."

# game/dialogs.rpy:1616
translate spanish SH1_27c4c879:

    # "But that doesn't change much. I can't love her, but we're still friends!"
    "Pero eso no cambia mucho. ¡No puedo amarla, pero puedo estar como su amigo!"

# game/dialogs.rpy:1617
translate spanish SH1_d4af9d2c:

    # "With such a big secret between us, I feel like we're even closer friends now!"
    "Y con semejante gran secreto compartido, ¡siento que somos incluso amigos muy cercanos ahora!"

# game/dialogs.rpy:1618
translate spanish SH1_2b941147:

    # hero "Oh...Now I understand why you refused the kiss earlier..."
    hero "Ahora entiendo porque rechazaste el beso antes..."

# game/dialogs.rpy:1619
translate spanish SH1_508719ae:

    # hero "It's because you didn't want to break my heart, huh?"
    hero "Es porque no querías romper mi corazón, ¿verdad?"

# game/dialogs.rpy:1620
translate spanish SH1_e17592e6:

    # s "Yes, exactly..."
    s "Si, exactamente..."

# game/dialogs.rpy:1621
translate spanish SH1_1f18b742:

    # hero "You know, I feel happy, now."
    hero "Sabes, me siento feliz ahora."

# game/dialogs.rpy:1623
translate spanish SH1_5ab99daa:

    # "Sakura turns around to face me."
    "Sakura se voltea y me ve."

# game/dialogs.rpy:1624
translate spanish SH1_51437fd0:

    # s "Eh? Why is that?"
    s "¿Eh? ¿A qué se debe eso?"

# game/dialogs.rpy:1625
translate spanish SH1_d89b0eb5:

    # hero "Because you shared your secret with me. And that you didn't want to break my heart."
    hero "Porque tú compartiste tu secreto conmigo y no querías romper mi corazón."

# game/dialogs.rpy:1626
translate spanish SH1_1a3b0477:

    # hero "I feel like you must trust me a lot. And that means that you consider me a close friend..."
    hero "Siento que confías mucho en mí. Eso significa que me consideras como un amigo especial... Y eres muy amable..."

# game/dialogs.rpy:1627
translate spanish SH1_d9d841a7:

    # hero "I'm happy to be your friend, Sakura-chan."
    hero "Estoy feliz de ser tu amigo, Sakura-chan."

# game/dialogs.rpy:1629
translate spanish SH1_6caf3c5b:

    # "Sakura didn't say anything for a moment..."
    "Sakura no dice nada por un momento..."

# game/dialogs.rpy:1630
translate spanish SH1_bdc6ac8a:

    # "Finally she gave me a big smile. She looked like she was going to cry from happiness."
    "Finalmente me muestra una sonrisa."

# game/dialogs.rpy:1633
translate spanish SH1_3f0e1dcd:

    # s "Thank you, %(stringhero)s-kun!"
    s "Gracias, ¡%(stringhero)s-kun!"

# game/dialogs.rpy:1634
translate spanish SH1_9db5fc3e:

    # s "I'm so glad that you took the time to understand me! {p}And I'm happy to be your friend, too!"
    s "¡Estoy tan contenta de que me entiendas! {p}¡Y estoy feliz de ser tu amiga también!"

# game/dialogs.rpy:1635
translate spanish SH1_cc14d6a4:

    # hero "I promise, I'll keep your secret safe."
    hero "Y prometo que mantendré tu secreto."

# game/dialogs.rpy:1636
translate spanish SH1_639e5f3c:

    # "We held our pinky fingers together to seal the promise. We both gave each other a big smile."
    "Nos tomamos de nuestros dedos meñiques para sellar la promesa y nos sonreímos."

# game/dialogs.rpy:1637
translate spanish SH1_3c805b45:

    # "Maybe she's a he... but she's a friend first and foremost."
    "Tal vez ella es un él... Pero ella es ante todo una amiga."

# game/dialogs.rpy:1643
translate spanish SH1_c8994519:

    # "We reached her house talking happily along the way about a variety of things."
    "Llegamos a su casa en silencio."

# game/dialogs.rpy:1644
translate spanish SH1_fa80129a:

    # "We were enjoying our time, as the best of friends."
    "Disfrutábamos nuestro tiempo como mejores amigos."

# game/dialogs.rpy:1645
translate spanish SH1_d62f35ea:

    # hero "I wonder how Rika-chan will react when she realizes I know your secret too."
    hero "Me pregunto cómo reaccionará Rika-chan cuando ella se dé cuenta que sé \"eso\" también."

# game/dialogs.rpy:1647
translate spanish SH1_589baf2e:

    # s "Teehee! She will probably try to hit on you, then! {image=heart.png}"
    s "¡Teehee! ¡Ella probablemente intentará pegarte! {image=heart.png}"

# game/dialogs.rpy:1648
translate spanish SH1_d5bffef3:

    # hero "Gah! That's a bit cruel, Sakura-chan!"
    hero "¡Gah! ¡Eres mala, Sakura-chan!"

# game/dialogs.rpy:1650
translate spanish SH1_b112bb86:

    # hero "...Although you're probably right..."
    pass

# game/dialogs.rpy:1651
translate spanish SH1_2a493962:

    # "We both laughed."
    "Nos reímos un poco, juntos..."

# game/dialogs.rpy:1653
translate spanish SH1_7a9a62fd:

    # "Something crossed my mind though..."
    "Entonces, algo pasó en mi cabeza..."

# game/dialogs.rpy:1655
translate spanish SH1_2100c01f:

    # hero "Sakura-chan?"
    hero "¿Sakura-chan?"

# game/dialogs.rpy:1656
translate spanish SH1_282f01ee:

    # hero "You know..."
    hero "Sabes..."

# game/dialogs.rpy:1657
translate spanish SH1_e9e23b04:

    # hero "Nanami-chan...{p}She really seems to care about you. More than you think."
    hero "Nanami-chan...{p}Ella realmente se ve que le importas. Más de lo que crees."

# game/dialogs.rpy:1658
translate spanish SH1_82f05805:

    # hero "So... Maybe it's time for her to know the truth as well."
    hero "Así que... Tal vez es hora para ella de saber la verdad también."

# game/dialogs.rpy:1660
translate spanish SH1_37bc0b4d:

    # "Sakura started to think."
    "Sakura comenzó a pensar."

# game/dialogs.rpy:1661
translate spanish SH1_eef310db:

    # s "I know...{p}I really do want to tell her..."
    s "Lo sé...{p}Realmente quiero contarle..."

# game/dialogs.rpy:1662
translate spanish SH1_08cb90bc:

    # s "The fact is... I'm so scared of her reaction..."
    s "El hecho es... Estoy asustada de su reacción..."

# game/dialogs.rpy:1663
translate spanish SH1_337166d2:

    # hero "Hmmm...I can understand."
    hero "¿Porqué?"

# game/dialogs.rpy:1664
translate spanish SH1_93f8ec4b_1:

    # s "Well..."
    s "Bueno..."

# game/dialogs.rpy:1665
translate spanish SH1_d9c81ff9:

    # s "I don't know..."
    s "No lo sé..."

# game/dialogs.rpy:1666
translate spanish SH1_aac62100:

    # hero "There's no reason to be scared, Sakura-chan."
    hero "No hay razón para asustarse, Sakura-chan."

# game/dialogs.rpy:1667
translate spanish SH1_45627ed5:

    # hero "Listen..."
    hero "Escucha..."

# game/dialogs.rpy:1669
translate spanish SH1_ad02dfe3:

    # hero "I'll talk to Rika-chan. Next Monday, let's tell Nanami-chan when we're all together at the club."
    hero "Voy a hablar con Rika. El próximo lunes, le contaremos a Nanami todos juntos en el club."

# game/dialogs.rpy:1670
translate spanish SH1_82ea3c85:

    # hero "Rika and I will be there to support you."
    hero "Entonces Rika y yo estaremos ahí para apoyarte."

# game/dialogs.rpy:1671
translate spanish SH1_f63d0a16:

    # hero "Nanami-chan is your close friend, right? She has the right to know too. I'm sure she'll understand."
    hero "Nanami-chan es una amiga muy cercana, ¿verdad? Ella tiene el derecho a saber. Estoy seguro que ella entenderá."

# game/dialogs.rpy:1672
translate spanish SH1_c91ce444:

    # hero "You must take your life in your own hands, Sakura-chan!!!"
    hero "Debes tomar valor en tus manos, ¡Sakura-chan!"

# game/dialogs.rpy:1673
translate spanish SH1_938668c5:

    # "Sakura laughed at the lame line I said and finally smiled."
    "Sakura lo pensó un momento otra vez y finalmente sonrió."

# game/dialogs.rpy:1675
translate spanish SH1_757d342d:

    # s "Alright. Let's do this!"
    s "Esta bien. ¡Hagámoslo!"

# game/dialogs.rpy:1676
translate spanish SH1_6a86d9b7:

    # hero "On Monday then."
    hero "Preparate para el Lunes."

# game/dialogs.rpy:1677
translate spanish SH1_47e672b9:

    # hero "Don't forget: Rika and I will be there for you. You won't be alone!"
    hero "No lo olvides: Yo y Rika vamos a estar ahí para ti. ¡No vas a estar sola en esto!"

# game/dialogs.rpy:1678
translate spanish SH1_69496358:

    # s "Thank you, %(stringhero)s-kun!{p}I'll be as brave as I can!"
    s "Gracias, ¡%(stringhero)s-kun!{p}¡Estaré preparada!"

# game/dialogs.rpy:1681
translate spanish SH1_e029963b:

    # "She giggled and I waved goodbye to her as she went back to her house."
    "Ella se rie y se despide con la mano mientras regresa a su casa."

# game/dialogs.rpy:1682
translate spanish SH1_5feddf4e:

    # "She is really unique. I'm glad to have a friend like her."
    "Ella realmente es única. Estoy contento de tener una amiga como ella."

# game/dialogs.rpy:1683
translate spanish SH1_8534ca42:

    # ". . ."
    ". . ."

# game/dialogs.rpy:1684
translate spanish SH1_aed2ad78:

    # "Although, I was a kinda depressed... I was so in love with her..."
    "Aunque, estoy un poco triste... Estaba tan enamorado de ella..."

# game/dialogs.rpy:1685
translate spanish SH1_051a7c18:

    # "My heart was a bit broken..."
    "Mi corazón se rompió un poco..."

# game/dialogs.rpy:1686
translate spanish SH1_7af2582c:

    # "But strangely, I wasn't completely devastated."
    "Extrañamente, me sentí triste pero no devastado."

# game/dialogs.rpy:1687
translate spanish SH1_12bd407a:

    # "Despite the fact we can't be lovers, I feel a lot closer to her."
    "Y a pesar del hecho de que no podemos ser amantes, eso me hizo más cercana a ella."

# game/dialogs.rpy:1688
translate spanish SH1_46bb790b:

    # "Plus, it looked like she felt the same... she didn't look that sad either."
    "Además, suponiendo que ella estaba compartiendo el mismo sentimiento, ella no se veía tan triste tampoco..."

# game/dialogs.rpy:1689
translate spanish SH1_a20cefa7_1:

    # "..."
    "..."

# game/dialogs.rpy:1690
translate spanish SH1_8d8f367d:

    # "Nah... Everything is just fine.{p}Fine and even better."
    "Nah... Todo esta bien.{p}Bien e incluso mejor."

# game/dialogs.rpy:1696
translate spanish SH1_e3ea38e8:

    # "Yeah, okay... It's a guy..."
    "Si, esta bien... Es un chico..."

# game/dialogs.rpy:1697
translate spanish SH1_40db10a1:

    # "So what???"
    "¿¿¿Y qué???"

# game/dialogs.rpy:1698
translate spanish SH1_d6afd40a:

    # "Who said that love can only work with the opposite gender!?"
    "¿¡Quién dijo que el amor solo puede trabajar con el género opuesto!?"

# game/dialogs.rpy:1699
translate spanish SH1_4158b541:

    # "I loved Sakura since the start. I desired her since the beginning."
    "Amé a Sakura desde el inicio. La he querido desde el principio."

# game/dialogs.rpy:1700
translate spanish SH1_12080c5e:

    # "She told me that she's a boy, but I still love and want her! So be it!"
    "Ella me dijo que es un chico, ¡pero aún la amo! ¡Entonces que sea así!"

# game/dialogs.rpy:1701
translate spanish SH1_a337f545:

    # "I love Sakura-chan, gender doesn't matter!"
    "Amo a Sakura-chan, ¡no importando que es lo que sea él o ella!"

# game/dialogs.rpy:1702
translate spanish SH1_b5134f34:

    # hero "I understand, now, why you rejected me when I tried to kiss you..."
    hero "Comprendo ahora porque me rechazaste cuando traté de besarte..."

# game/dialogs.rpy:1703
translate spanish SH1_c7f3c846:

    # s "Yes..."
    s "Si..."

# game/dialogs.rpy:1704
translate spanish SH1_690e9f0a:

    # "I smiled and started to come closer to her."
    "Sonreí y comencé a acercarme a ella."

# game/dialogs.rpy:1705
translate spanish SH1_a66f34d1:

    # s "As a girl in soul, I prefer boys to girls,"
    s "Como una chica en el alma, prefiero a los chicos que a las chicas."

# game/dialogs.rpy:1706
translate spanish SH1_a5e73d0f:

    # s "But I was afraid to break our hearts if I do this..."
    s "Pero estaba temerosa de romper nuestros corazones si lo hacía..."

# game/dialogs.rpy:1707
translate spanish SH1_d4993a91:

    # "She felt that I was behind her so she turned around."
    "Ella sintió que estaba detrás de ella y entonces se volteó."

# game/dialogs.rpy:1708
translate spanish SH1_ef87d060:

    # s "I didn't want to lose my friend, and-"
    s "No quería perder a mi amigo y-"

# game/dialogs.rpy:1711
translate spanish SH1_5cc40c7f:

    # "I don't even let her finish her sentence."
    "No deje que terminara su oración."

# game/dialogs.rpy:1712
translate spanish SH1_747a95a0:

    # "I took her in my arms and kissed her."
    "Mientras que ella estaba hablando, me le acerqué, la tomé en mis brazos y la besé por sorpresa."

# game/dialogs.rpy:1713
translate spanish SH1_de5e4ab3:

    # "She tried to step back for a second, {p}but finally she gave in."
    "Ella trató de retroceder en el primer segundo, {p}pero al final cedió."

# game/dialogs.rpy:1714
translate spanish SH1_ddcc906a:

    # "Our lips were lovingly united."
    "Nuestros labios estaban unidos amorosamente."

# game/dialogs.rpy:1715
translate spanish SH1_96042711:

    # "Yes... I love her."
    "Si... La amo."

# game/dialogs.rpy:1716
translate spanish SH1_5b84a0a9:

    # "I don't care about what people say.{p}She's a girl and I love her. It's as simple as that."
    "No me importó lo que las personas podían decir.{p}Es una chica y la amo, eso es todo..."

# game/dialogs.rpy:1718
translate spanish SH1_21f57c0f:

    # "We stopped kissing and Sakura-chan looked me in the eyes."
    "Nos detuvimos y Sakura-chan me miró en los ojos."

# game/dialogs.rpy:1719
translate spanish SH1_7673b5e7:

    # "I found myself swimming into the endless ocean of beauty her gaze offered me..."
    "Estaba nadando en sus dos océanos que eran sus ojos..."

# game/dialogs.rpy:1720
translate spanish SH1_04263bd9:

    # s "... %(stringhero)s... -kun..."
    s "... %(stringhero)s... -kun..."

# game/dialogs.rpy:1721
translate spanish SH1_752a2eec:

    # hero "... Sa... Sakura-san..."
    hero "... Sa... Sakura-chan..."

# game/dialogs.rpy:1730
translate spanish SH1_aab57b73:

    # "We kissed again, but this time, even more intensely."
    "Nos besamos de nuevo, un beso más fuerte y tórrido."

# game/dialogs.rpy:1731
translate spanish SH1_d01f1362:

    # "I found my hands were running down her body. Her body was so womanly, so perfect."
    "Mis manos estaban bajando de su cuerpo. Tan femenina, tan perfecta..."

# game/dialogs.rpy:1732
translate spanish SH1_b11b14d6:

    # "Her fingers were ruffling my hair as she was held me tighter."
    "Sus dedos estaban en mi cabello y ella me sujetaba con más fuerza..."

# game/dialogs.rpy:1733
translate spanish SH1_fd13e895:

    # "We stayed in the rice fields for a long while. We just couldn't stop..."
    "Estuvimos por un largo tiempo en los arrozales. Silenciosamente, acurrucándonos y besándonos algunas veces... No podíamos parar... "

# game/dialogs.rpy:1734
translate spanish SH1_4237d00f:

    # "Even though my head was spinning a bit from the situation, I still couldn't stop giving her my love..."
    "Mi cabeza estaba dando vueltas por la situación pero aún no podía parar de dar mi amor a ella..."

# game/dialogs.rpy:1735
translate spanish SH1_a7d8f7db:

    # "The sun began to set and filled the sky with a beautiful shade of orange as the evening arrived."
    "El sol estaba poniéndose anaranjado mientras que la noche llegaba."

# game/dialogs.rpy:1739
translate spanish SH1_5b186bfb:

    # "It was getting late. I had to let Sakura return home. We held hands as we walked to her house."
    "Nos tomamos de las manos mientras regresábamos a su casa."

# game/dialogs.rpy:1740
translate spanish SH1_0163bcac:

    # "We were silent. There was no need for words. We were just enjoying every moment together."
    "Estábamos callados. No se necesitaba hablar. Estábamos disfrutando nuestro tiempo juntos, como amantes."

# game/dialogs.rpy:1741
translate spanish SH1_94a25b9b:

    # "My heart was still racing. I was overwhelmed with joy."
    "Mi corazón estaba latiendo tanto que era casi doloroso. Pero no me importo para nada."

# game/dialogs.rpy:1742
translate spanish SH1_32219c59:

    # "Suddenly, Sakura held my arm tighter."
    "Ella de pronto se agarró de mi brazo con fuerza."

# game/dialogs.rpy:1743
translate spanish SH1_15b1a829:

    # s "I wonder what Rika-chan will say when she finds out about us!"
    s "¡Me pregunto que es lo que Rika-chan va a decir cuando sepa sobre lo nuestro!"

# game/dialogs.rpy:1745
translate spanish SH1_164ae856:

    # s "Knowing her, she might try to kill you! Haha!"
    s "¡Teehee! ¡Tal vez ella a va tratar de matarte, conociéndola!"

# game/dialogs.rpy:1746
translate spanish SH1_d0bb62e3:

    # hero "...That's kinda scary to think about..."
    hero "¡Argh!"

# game/dialogs.rpy:1747
translate spanish SH1_d7c1b42b:

    # s "In fact, I've always wondered if she was in love with me or something..."
    s "De hecho, estaba siempre preguntándome si ella estaba enamorada de mí o algo..."

# game/dialogs.rpy:1748
translate spanish SH1_555aded3:

    # s "She is the Rika-chan you know since she knows my secret."
    s "Ella, ya sabes, ya que conoce el secreto."

# game/dialogs.rpy:1749
translate spanish SH1_e7066814:

    # hero "Really?"
    hero "¿En serio?"

# game/dialogs.rpy:1750
translate spanish SH1_2e63f4b7:

    # hero "Heh... To be honest... She kinda scares me more, now!"
    hero "Je... Para ser honesto... ¡Ella me asusta más, ahora!"

# game/dialogs.rpy:1751
translate spanish SH1_edeec3ff:

    # "Sakura-chan giggles."
    "Sakura-chan se rie."

# game/dialogs.rpy:1752
translate spanish SH1_c9ae305b:

    # s "Don't worry, I'll protect you! {image=heart.png}"
    s "No te preocupes, ¡te protegeré! {image=heart.png}"

# game/dialogs.rpy:1753
translate spanish SH1_9554a6c1:

    # "As we approached the front of her house, she rubbed her head against my shoulder."
    "Ella rozó su cabeza contra mi hombro mientras estábamos enfrente de su casa."

# game/dialogs.rpy:1755
translate spanish SH1_ee198ceb:

    # "I couldn't resist her touch. I took her in my arms and kissed her again."
    "No podía resistir su toque. La tomé en mis brazos y la besé de nuevo."

# game/dialogs.rpy:1756
translate spanish SH1_ab476564:

    # "She softly kissed me back."
    "Ella me besa de vuelta suavemente."

# game/dialogs.rpy:1758
translate spanish SH1_8944b322:

    # "When she stopped, she smiled, squeezed my butt and whispered in my ear."
    "Cuando terminó, ella sonrió, apretó mi trasero y susurró en mi oído."

# game/dialogs.rpy:1759
translate spanish SH1_cc8b4089:

    # s "See you tomorrow... sweetheart! {image=heart.png}"
    s "Te veré mañana... ¡Cariño! {image=heart.png}"

# game/dialogs.rpy:1760
translate spanish SH1_30f6ec48:

    # hero "Heh, you're acting more like a guy when you do stuff like that!"
    hero "Jeje, ¡actuas como un chico cuando avanzas así!"

# game/dialogs.rpy:1761
translate spanish SH1_f2fe27d5:

    # s "Meanie! {image=heart.png}"
    s "¡Maloso! {image=heart.png}"

# game/dialogs.rpy:1763
translate spanish SH1_31fe93a4:

    # "She sticked her tongue out at me before disappearing into her house..."
    "Ella sacó su lengua antes de correr a su casa y desaparecer."

# game/dialogs.rpy:1765
translate spanish SH1_f5ae9218:

    # "I... don't know what happened within me..."
    "Yo... No sabía lo que pasó dentro de mí..."

# game/dialogs.rpy:1766
translate spanish SH1_506f97de:

    # "I was flooding with energy and happiness!"
    "¡Estaba desbordado con energía y felicidad!"

# game/dialogs.rpy:1767
translate spanish SH1_9e8998ef:

    # "I ran along the streets."
    "Antes de irme a casa, corrí por las calles."

# game/dialogs.rpy:1768
translate spanish SH1_3f79e021:

    # "I ran, I ran, I ran. As fast as I could. Overwhelmed with joy."
    "Corrí, corrí, corrí. Tan rápido como puedo."

# game/dialogs.rpy:1769
translate spanish SH1_91e3f3ea:

    # "I wanted to sing. I wanted to laugh. I wanted to dance."
    "Quiero cantar. Quiero reír. Quiero bailar."

# game/dialogs.rpy:1770
translate spanish SH1_eb6be300:

    # "I felt completely crazy!!!"
    "¡¡¡Me sentía completamente loco!!!"

# game/dialogs.rpy:1771
translate spanish SH1_375cf97b:

    # "I had never felt that kind of euphoria before..."
    "Nunca he tenido ese tipo de euforia antes..."

# game/dialogs.rpy:1773
translate spanish SH1_e1e2560b:

    # "As I was running home, I passed by Nanami who was coming back from groceries."
    "Mientas que estaba corriendo a casa, pasé junto a Nanami quien estaba regresando de las compras."

# game/dialogs.rpy:1774
translate spanish SH1_6bbff298:

    # n "Hey, %(stringhero)s-senpai! What's cookin'?"
    n "Oye, ¡%(stringhero)s-senpai! ¿Qué se esta cocinando?"

# game/dialogs.rpy:1775
translate spanish SH1_bfe48993:

    # "I lifted her off the ground like a small kitten and spun with her around happily!"
    "¡La levanté como una gatita y la giré alrededor felizmente!"

# game/dialogs.rpy:1776
translate spanish SH1_0da09179:

    # hero "I'm so happy, Nanami-chan! So so so so so so {b}SO{/b} happy!"
    hero "¡Estoy tan feliz, Nanami-chan! ¡¡Tan tan tan tan tan tan {b}TAN{/b} feliz!!"

# game/dialogs.rpy:1778
translate spanish SH1_7e7bd642:

    # n "Eeeeek! W-what the heck!? W-w-why you're so happy? You're acting crazy!"
    n "¡Eeeeek! ¿P-p-porque estas tan feliz? ¡Pareces un loco!"

# game/dialogs.rpy:1779
translate spanish SH1_c2832101:

    # hero "It's because I..."
    hero "Es porque yo..."

# game/dialogs.rpy:1780
translate spanish SH1_192f06a3:

    # "I put back Nanami on the ground."
    "Puse de vuelta a Nanami en el suelo."

# game/dialogs.rpy:1781
translate spanish SH1_00ab9ecc:

    # hero "No, wait- I don't want to spoil the surprise."
    hero "No, espera... No quiero arruinar la sorpresa."

# game/dialogs.rpy:1782
translate spanish SH1_99c8156c:

    # hero "I'll tell you at school on Monday alongside Rika-chan!"
    hero "¡Te lo diré con Rika-chan el Lunes en la escuela!"

# game/dialogs.rpy:1784
translate spanish SH1_a79e612d:

    # n "Monday?! But that's too looooong!"
    n "¿¡El Lunes!? ¡Pero eso es demasiado tieeeeeempo!"

# game/dialogs.rpy:1785
translate spanish SH1_380da8df:

    # n "Now I really want to know!"
    n "¡Me hiciste querer saber ahora!"

# game/dialogs.rpy:1786
translate spanish SH1_5f2414ee:

    # hero "Patience, little one, patience!"
    hero "¡Paciencia, pequeña, paciencia!"

# game/dialogs.rpy:1788
translate spanish SH1_f5bde8dc:

    # "I continued running back home, screaming \"WOOOOOOOOOOOOO!\" out loud. I heard a scream from Nanami-"
    "Y mientras que estaba corriendo regreso a casa con un \"Banzaaaaaai!\", escuché un grito de Nanami, a lo lejos..."

# game/dialogs.rpy:1790
translate spanish SH1_84fa99ca:

    # n "{size=+15}I'M NOT LITTLE!{/size}"
    n "{size=+15}¡NO SOY PEQUEÑA!{/size}"

# game/dialogs.rpy:1792
translate spanish SH1_d69bfabe:

    # "My gosh, what a day...!"
    "Por Dios, QUE día..."

# game/dialogs.rpy:1793
translate spanish SH1_0d4f6adf:

    # "Sakura called earlier. I'm going on another date with her tomorrow!"
    "Me pregunto cómo van a ir las cosas mañana... Ya que Sakura decidió que vamos a tener otra cita mañana."

# game/dialogs.rpy:1794
translate spanish SH1_dece2bab:

    # "I was about to ask her out on a date myself...looks like she beat me to it."
    "Aunque no me importa, estaba queriendo preguntarle por una cita de todas maneras."

# game/dialogs.rpy:1795
translate spanish SH1_e91f1c64:

    # "Maybe we can read each other's minds now that we're lovers! Anyway...I better get some rest."
    "¡Da miedo cómo los amantes pueden predecir las mentes de sus parejas!"

# game/dialogs.rpy:1805
translate spanish sceneS1_b5e51d3c:

    # "I was waiting for Sakura at the train station of the village."
    "Estaba esperando por Sakura en la estación del tren del pueblo."

# game/dialogs.rpy:1806
translate spanish sceneS1_6f70efee:

    # "I was getting used to these trips, now. But I'm a bit tired today."
    "Estaba acostumbrándome a estos viajes ahora. Pero estoy un poco cansado el día de hoy."

# game/dialogs.rpy:1807
translate spanish sceneS1_6a4ff20d:

    # "I couldn't sleep at all last night...I was full of excitement thinking about our lovely date."
    "No pude dormir nada anoche... Tanta emoción, pensando sobre tener una encantadora cita con Sakura-chan."

# game/dialogs.rpy:1808
translate spanish sceneS1_32c4a28b:

    # "Then a joyful Sakura appeared! She was listening to music on a CD player."
    "Entonces Sakura apareció, alegremente. Ella estaba escuchando alguna música en un reproductor de CD."

# game/dialogs.rpy:1809
translate spanish sceneS1_cfc9f30e:

    # "When she saw me, a big smile filled her face. She turned off the CD player and walked toward me."
    "Cuando me vio, puso una gran sonrisa, apagó el reproductor y caminó hacia mí."

# game/dialogs.rpy:1811
translate spanish sceneS1_641c11ba:

    # s "%(stringhero)s-kun!! Sorry for the wait! {image=heart.png}"
    s "¡¡%(stringhero)s-kun!! ¡Perdón por la tardanza! {image=heart.png}"

# game/dialogs.rpy:1812
translate spanish sceneS1_a5707cad:

    # hero "Good morning, Sakura-chan!"
    hero "¡Buenos días, Sakura-chan!"

# game/dialogs.rpy:1814
translate spanish sceneS1_bd4c0f66:

    # "We embraced each other with a kiss."
    "Nos besamos."

# game/dialogs.rpy:1815
translate spanish sceneS1_a85140f3:

    # "Darn, I missed these sweet lips so much."
    "Dios, extrañe tanto esos dulces labios."

# game/dialogs.rpy:1816
translate spanish sceneS1_0e294c8c:

    # "I had a feeling she had put on a little more makeup on her face for the occasion."
    "Tengo la sensación de que se ha puesto algo de maquillaje en su rostro para la ocasión."

# game/dialogs.rpy:1818
translate spanish sceneS1_2c25f5b4:

    # hero "You look wonderful today Sakura-chan."
    hero "Te ves maravillosa hoy, Sakura-chan."

# game/dialogs.rpy:1819
translate spanish sceneS1_280a32f7:

    # "She was overflowing with happiness."
    "Eso era verdad... Ella estaba como rebosado de felicidad."

# game/dialogs.rpy:1820
translate spanish sceneS1_f3a61c54:

    # "I have never seen her so happy before."
    "Nunca la he visto tan feliz antes."

# game/dialogs.rpy:1822
translate spanish sceneS1_1bc30b12:

    # s "Oh, does that mean I'm not pretty everyday...?"
    s "Hmm, ¿eso significa que no soy bonita los demás días?"

# game/dialogs.rpy:1823
translate spanish sceneS1_103429d2:

    # hero "Ah! N-no, I mean, you're as pretty as usual... I mean you are always..."
    hero "¡Argh! No, quiero decir, eres tan bonita como lo usual... Quiero decir que lo eres siempre..."

# game/dialogs.rpy:1825
translate spanish sceneS1_ee5f4b9e:

    # "Sakura gave me a mischievous grin and gave me a quick kiss on the lips."
    "Sakura hizo una sonrisa traviesa y me dio un rápido beso en los labios."

# game/dialogs.rpy:1826
translate spanish sceneS1_7641d5e7:

    # s "Silly... I'm just messing with you! {image=heart.png}"
    s "Idiota... ¡Estoy jugando contigo! {image=heart.png}"

# game/dialogs.rpy:1827
translate spanish sceneS1_fe43f145:

    # hero "Gah! That's too cruel! {image=heart.png}"
    hero "¡Gah! ¡Eres malvada! {image=heart.png}"

# game/dialogs.rpy:1828
translate spanish sceneS1_73ac0c99:

    # "We laughed together as the train arrived in the station."
    "Nos reímos juntos mientras que el tren llegaba a la estación."

# game/dialogs.rpy:1833
translate spanish sceneS1_c93ea523:

    # "We sat on the train, going towards the grand city."
    "Estábamos sentados en el tren, yendo hacia la gran ciudad."

# game/dialogs.rpy:1834
translate spanish sceneS1_2af5fbd9:

    # "I was wondering about her CD player, so I decided to ask her."
    "Todavía estaba preguntándome, mientras que veía su reproductor de CD, así que finalmente le pregunté."

# game/dialogs.rpy:1835
translate spanish sceneS1_95d5b90a:

    # hero "What were you listening to on your CD player?"
    hero "¿Qué estabas escuchando en tu reproductor?"

# game/dialogs.rpy:1836
translate spanish sceneS1_d701c031:

    # s "Oh, some classical music."
    s "Oh, algo de música clásica."

# game/dialogs.rpy:1837
translate spanish sceneS1_9071067a:

    # hero "You like classical music?"
    hero "¿Te gusta la música clásica?"

# game/dialogs.rpy:1838
translate spanish sceneS1_5ad3424b:

    # s "I love it!"
    s "¡Me encanta!"

# game/dialogs.rpy:1839
translate spanish sceneS1_302806ba:

    # "I'm not especially a fan of that kind of music but I will admit that it's good to listen to sometimes. It can be very relaxing."
    "No soy especialmente un fan de esa clase de música pero admito que es bueno escucharlo a veces. Es muy relajante."

# game/dialogs.rpy:1840
translate spanish sceneS1_865dd143:

    # hero "What's your favorite song?"
    hero "¿Qué canción es tu favorita?"

# game/dialogs.rpy:1842
translate spanish sceneS1_ae336353:

    # "Sakura gave me one side of her headphones and started the CD player."
    "Sakura sonrió, me dio uno de los audiculares y encendió el reproductor."

# game/dialogs.rpy:1845
translate spanish sceneS1_8534ca42:

    # ". . ."
    ". . ."

# game/dialogs.rpy:1846
translate spanish sceneS1_083198ae:

    # hero "Oh, I know this song... but I don't remember its name..."
    hero "Oh, conozco esta canción... No recuerdo su nombre..."

# game/dialogs.rpy:1847
translate spanish sceneS1_ef73b932:

    # s "It's the {i}Air Orchestral suite #3{/i} of Bach."
    s "Es el {i}Air Orchestral suite #3{/i} de Bach."

# game/dialogs.rpy:1848
translate spanish sceneS1_00935374:

    # s "I don't know why... But I love to listen to this when I'm in a good mood..."
    s "No sé porqué... Pero amo escucharlo cuando estoy feliz..."

# game/dialogs.rpy:1849
translate spanish sceneS1_6bbb4eeb:

    # hero "You must be in a pretty good mood then, huh?"
    hero "¿Estás feliz ahora?..."

# game/dialogs.rpy:1850
translate spanish sceneS1_1833fef5:

    # "She doesn't reply. She just leans her head on my shoulder and closes her eyes."
    "Ella no respondió. Solo puso su cabeza en mi hombro y cerró sus ojos."

# game/dialogs.rpy:1851
translate spanish sceneS1_bb3922e5:

    # "I gently lean my head against her."
    "Gentilmente pongo mi cabeza en la suya."

# game/dialogs.rpy:1852
translate spanish sceneS1_54d8e14e:

    # "Her presence, her warmth, her strangely feminine smell, along with the music..."
    "Su presencia, su calidez, su olor extrañamente femenino, la música..."

# game/dialogs.rpy:1853
translate spanish sceneS1_b2bf1fcc:

    # "I wish this train would never stop and that we could stay like this forever."
    "Deseo que el tren nunca pare y que estemos así por siempre."

# game/dialogs.rpy:1858
translate spanish sceneS1_4bdd421c:

    # hero "...Some things make more sense now that you've told me your secret."
    hero "...Algunas cosas tienen sentido, ahora que me has contado tu secreto."

# game/dialogs.rpy:1860
translate spanish sceneS1_49c95ed0:

    # s "What do you mean?"
    s "¿Qué quieres decir?"

# game/dialogs.rpy:1861
translate spanish sceneS1_35d9c8b5:

    # hero "Well, I finally understand why you like shooting games and harem ecchi manga."
    hero "Finalmente entiendo porque te gustan los juegos de disparo y mangas harem ecchi."

# game/dialogs.rpy:1862
translate spanish sceneS1_1dcf5a89:

    # hero "You have some more boyish tastes in you, despite that the rest of you is pretty feminine."
    hero "Tienes algunos gustos masculinos, a pesar de que el resto de ti es femenino."

# game/dialogs.rpy:1863
translate spanish sceneS1_1ae549d3:

    # s "Yes, it's true."
    s "Si, es verdad."

# game/dialogs.rpy:1864
translate spanish sceneS1_64d14f00:

    # s "I think I got that from my father."
    s "Lo obtuve de mi padre."

# game/dialogs.rpy:1865
translate spanish sceneS1_75219ee1:

    # s "For my last few birthdays, my father bought me shooting games and ecchi manga."
    s "Él fue quien me compró algunos juegos de disparos y mangas ecchi en mis últimos cumpleaños."

# game/dialogs.rpy:1866
translate spanish sceneS1_0d2a822f:

    # s "I guess it just stuck with me..."
    s "Y se quedó conmigo..."

# game/dialogs.rpy:1867
translate spanish sceneS1_e668a3b6:

    # hero "I see..."
    hero "Ya veo..."

# game/dialogs.rpy:1868
translate spanish sceneS1_0676a71e:

    # hero "How it was when you were younger? Was it difficult?"
    hero "¿Cómo fue cuándo eras más pequeña? ¿Fue difícil?"

# game/dialogs.rpy:1870
translate spanish sceneS1_b6297f8f:

    # s "My childhood was kinda chaotic, yes."
    s "Mi niñez era algo caótica, si."

# game/dialogs.rpy:1871
translate spanish sceneS1_125b816d:

    # s "I was a boy like the others during my first years at the kindergarten."
    s "Era un chico como otros durante mis primeros años en el jardín de niños."

# game/dialogs.rpy:1872
translate spanish sceneS1_f7b3d45f:

    # s "But the time passed..."
    s "Pero el tiempo pasó..."

# game/dialogs.rpy:1873
translate spanish sceneS1_cc5f078f:

    # s "It was clear that I had more girlish tendencies. Very strong ones in fact..."
    s "Era más claro que tenía comportamientos femeninos. Uno muy fuerte..."

# game/dialogs.rpy:1876
translate spanish sceneS1_e1554201:

    # s "..."
    s "..."

# game/dialogs.rpy:1877
translate spanish sceneS1_146f1b24:

    # s "When I was going through puberty, I clearly expressed my desire to be considered as a real girl."
    s "Cuando mi pubertad llegó, claramente expresé mi deseo de considerarme como una chica real."

# game/dialogs.rpy:1878
translate spanish sceneS1_384c09e4:

    # s "I couldn't help it... My mind was locked in more feminine habits..."
    s "No podía evitarlo... Mi mente estaba encerrado en esto... Y mi voz no cambió a uno masculino mientras crecía."

# game/dialogs.rpy:1879
translate spanish sceneS1_eb52cb72:

    # s "It looked like my own body was confused and didn't know how to evolve. For example, my voice didn't deepen like a male."
    s "Es como si mi propio cuerpo estuviera confundido y no sabe cómo evolucionar."

# game/dialogs.rpy:1880
translate spanish sceneS1_167b4ba2:

    # s "So I let my hair grow, pierced my ears, started to wear skirts, used bras and even put stuff inside my bra to simulate real breasts..."
    s "Entonces deje mi cabello crecer, me perforé mis orejas, comencé a usar faldas, usé sostenes y me puse cosas dentro para simular senos..."

# game/dialogs.rpy:1881
translate spanish sceneS1_2476fd15:

    # s "When I looked at myself in a mirror dressed like a girl, I knew definitely that I was one. I wanted to be one... And I will be one."
    s "Cuando miré la chica que estaba enfrente del espejo por primera vez, sabía en definitiva que era una. Quería ser una... Y seré una."

# game/dialogs.rpy:1882
translate spanish sceneS1_e1554201_1:

    # s "..."
    s "..."

# game/dialogs.rpy:1883
translate spanish sceneS1_a678d317:

    # s "My mother accepted it well. She loves me more than anything and will accept anything as long as it makes me happy."
    s "Mi madre lo aceptó bien. Ella me ama más que nada y aceptará cualquier cosa mientras me haga feliz."

# game/dialogs.rpy:1884
translate spanish sceneS1_6bc7ecb8:

    # s "But other people didn't want to understand..."
    s "Pero las otras personas no querían comprenderlo..."

# game/dialogs.rpy:1885
translate spanish sceneS1_22ca470a:

    # s "My father, my classmates,..."
    s "Mi padre, mis compañeros..."

# game/dialogs.rpy:1886
translate spanish sceneS1_f7a73d77:

    # s "You get the picture...{p}Well to continue on, my first teenage years were horrible..."
    s "Ves el panorama supongo...{p}Bien para continuar, mis primeros años de adolescencia fueron horribles..."

# game/dialogs.rpy:1887
translate spanish sceneS1_e0339899:

    # hero "You mean here? In the village?"
    hero "¿Quieres decir aquí? ¿En el pueblo?"

# game/dialogs.rpy:1888
translate spanish sceneS1_e40cc235:

    # s "No.{p}We were living in Kyoto before, but we moved out here..."
    s "No.{p}Nosotros estábamos viviendo en Kyoto antes, pero nos tuvimos que mudarnos aquí..."

# game/dialogs.rpy:1889
translate spanish sceneS1_b830f777:

    # s "When we started our new life, I decided that I would fight to be considered as a girl in the new town."
    s "Cuando comenzamos nuestras nuevas vidas, decidí que pelearía para ser considerada como una chica en el nuevo pueblo."

# game/dialogs.rpy:1890
translate spanish sceneS1_e1a08602:

    # s "Only my parents and Rika-chan know the secret...{p}And you, of course."
    s "Y en realidad, solo mis padres y Rika-chan conocen el secreto..{p}Y tú, por su puesto."

# game/dialogs.rpy:1891
translate spanish sceneS1_fdbe45ca:

    # s "And Nanami-chan, on Monday."
    s "Y Nanami-chan, el Lunes."

# game/dialogs.rpy:1892
translate spanish sceneS1_e1554201_2:

    # s "..."
    s "..."

# game/dialogs.rpy:1893
translate spanish sceneS1_92a39690:

    # s "You know, I..."
    s "Sabes, yo..."

# game/dialogs.rpy:1894
translate spanish sceneS1_8e16ee4d:

    # s "I wish I was a real girl..."
    s "Desearía ser una chica de verdad..."

# game/dialogs.rpy:1895
translate spanish sceneS1_c10c0ef9:

    # s "I've always wanted to be a girl. {p}I've always wanted to have long hair.{p}I've always wanted to wear dresses..."
    s "Siempre he querido ser una chica. {p}Siempre he querido tener el cabello largo.{p}Siempre he querido usar vestidos..."

# game/dialogs.rpy:1896
translate spanish sceneS1_d8ba4cdd:

    # s "And I've always wanted... to have a boyfriend..."
    s "Y siempre he querido... Tener un novio..."

# game/dialogs.rpy:1897
translate spanish sceneS1_dbdb5e9d:

    # "I listened to her in complete silence."
    "La escuché, silenciosamente."

# game/dialogs.rpy:1898
translate spanish sceneS1_c2573e27:

    # "As she was remembering her childhood memories, I could see some tears were about to come out from her eyes."
    "Mientras que ella estaba recordando sus memorias de la niñez, algunas lágrimas estaban a punto de salir de sus ojos."

# game/dialogs.rpy:1899
translate spanish sceneS1_64770d3e:

    # "I put my hand on her shoulder and kiss her head."
    "Puse mi mano en su hombro y la bese en la cabeza."

# game/dialogs.rpy:1900
translate spanish sceneS1_f25fd7bb:

    # hero "Sakura-chan..."
    hero "Sakura-chan..."

# game/dialogs.rpy:1902
translate spanish sceneS1_1ad3847b:

    # hero "I know your secret. But I promise you... {p}To me, you'll always be a girl. A real girl..."
    hero "Sé tu secreto. Pero prometo...{p}Que para mí, serás siempre una chica. Una chica real..."

# game/dialogs.rpy:1904
translate spanish sceneS1_4ee0f788:

    # hero "I don't care about what other people think.{p}I don't care what your body thinks."
    hero "No me importa sobre lo que la gente vaya a pensar.{p}No me importa lo que tu cuerpo piense."

# game/dialogs.rpy:1905
translate spanish sceneS1_01d48485:

    # hero "I don't even care if your father hates me someday because of us going out together."
    hero "Incluso no me importa si tu padre me odie algún día porque salimos juntos."

# game/dialogs.rpy:1906
translate spanish sceneS1_f0c87851:

    # hero "Sakura-chan...You are a girl and I...{p}And I......{p}I........."
    hero "Sakura-chan... Eres una chica y yo...{p}Y yo......{p}Yo........."

# game/dialogs.rpy:1907
translate spanish sceneS1_2573e7bb:

    # "I never thought it would be so hard to say it, even if you want to express it so much at the same time."
    "Nunca pensé que sería tan difícil decirlo, incluso si quieres expresarlo tanto al mismo tiempo."

# game/dialogs.rpy:1908
translate spanish sceneS1_8dca2ab0:

    # "I forced myself to let it all out."
    "Me forcé a mí mismo en sacarlo."

# game/dialogs.rpy:1909
translate spanish sceneS1_5bec7fb2:

    # hero "I... Sakura-chan, I lo... I lo......{p}{size=+15}I love you, Sakura-chan!!!{/size}"
    hero "Yo... Sakura-chan, te... Te......{p}{size=+15}¡¡¡te amo, Sakura-chan!!!{/size}"

# game/dialogs.rpy:1910
translate spanish sceneS1_c352284f:

    # "I ended up loudly shouting that out."
    "¡Lo forcé tanto que grité!"

# game/dialogs.rpy:1911
translate spanish sceneS1_68b19ed1:

    # "Some people in the street were so surprised, they stopped to stare at us."
    "Algunas personas en la calle estaban sorprendidos y se detuvieron, mirándonos."

# game/dialogs.rpy:1912
translate spanish sceneS1_1496db50:

    # "I felt terribly embarrassed."
    "Me sentí terriblemente avergonzado."

# game/dialogs.rpy:1913
translate spanish sceneS1_7bb8b5bf:

    # "Even Sakura turned red with embarrassment."
    "Sakura se puso roja por la vergüenza también."

# game/dialogs.rpy:1914
translate spanish sceneS1_c3bbc64c:

    # "Suddenly I realized the deep sense of what I just said."
    "Repentinamente me di cuenta del profundo sentir que acabo de decir."

# game/dialogs.rpy:1915
translate spanish sceneS1_0c128ea7:

    # "Yes... Yes, I love her...{p}And I'm not afraid to say it!!!"
    "Si... Si, la amo...{p}¡¡¡Y no estoy asustado de decirlo!!!"

# game/dialogs.rpy:1916
translate spanish sceneS1_5d447c9e:

    # hero "Yes everyone!! I love her!!! You hear me??? I love HER!!!"
    hero "¡¡Si todo el mundo!! ¡¡¡La amo!!! ¿¿¿Me escuchas??? ¡¡¡LA amo!!!"

# game/dialogs.rpy:1917
translate spanish sceneS1_71bc4397:

    # "Strangers just stared at us awkwardly."
    "No creo que la gente tenga alguna idea cuando yo insistí en el \"la\"."

# game/dialogs.rpy:1918
translate spanish sceneS1_df63e596:

    # "Sakura was still blushing... But suddenly..."
    "Sakura estaba aún sonrojada... Pero de pronto..."

# game/dialogs.rpy:1919
translate spanish sceneS1_0f3f7530:

    # "She started to reply, in the same tone as me... Stuttering and trembling a bit but with the same conviction, and as loud as me!"
    "Ella comenzó a responder, en el mismo tono que el mío... Tartamudeando y temblando un poco pero con la misma convicción, ¡y tan fuerte como yo!"

# game/dialogs.rpy:1920
translate spanish sceneS1_c4c3f14c:

    # s "Y... Ye...Yes!!!"
    s "¡¡¡S...S...Si!!!"

# game/dialogs.rpy:1921
translate spanish sceneS1_d1b481d2:

    # s "{size=+10}Yes, I am a girl!...{p}I am a girl and I love you too, %(stringhero)s-kun!!!!{/size}"
    s "{size=+10}¡Si, soy una chica!!...{p}¡¡¡¡Soy una chica y te amo también, %(stringhero)s-kun!!!!{/size}"

# game/dialogs.rpy:1922
translate spanish sceneS1_6252a7c4:

    # "People continued to stare at us. But soon, they carried on with their lives, ignoring us."
    "La gente nos miró por un momento, antes de seguir con sus vidas, ignorándonos."

# game/dialogs.rpy:1923
translate spanish sceneS1_82c8f385:

    # "I rolled my eyes, appalled at the people."
    "Rodé mis ojos, consternado."

# game/dialogs.rpy:1924
translate spanish sceneS1_7e3455d5:

    # hero "Pfft... People..."
    hero "Ja... Gente..."

# game/dialogs.rpy:1925
translate spanish sceneS1_e9431aa5:

    # "Sakura made a strange face... She was holding her mouth while blushing...{p}She was... laughing???"
    "Sakura hizo una extraña cara... Ella estaba tapando su boca mientras se sonrojaba...{p}Ella estaba... ¿Riéndose?"

# game/dialogs.rpy:1927
translate spanish sceneS1_17b9939f:

    # "She starts to laugh loudly. It was a nice laugh. The sound filled me with comfort."
    "Ella comienza a reír fuertemente. Una linda risa sonando como una bonita melodia."

# game/dialogs.rpy:1929
translate spanish sceneS1_cf3fcfe1:

    # "I was a little surprised, but her laugh made me want to laugh as well."
    "Estaba sorprendido, pero su risa me hizo reír también."

# game/dialogs.rpy:1930
translate spanish sceneS1_5da66cff:

    # hero "Hahaha, what's so funny, Sakura-chan?"
    hero "¿Qué es tan gracioso, Sakura-chan?"

# game/dialogs.rpy:1931
translate spanish sceneS1_8da8d865:

    # "She tried to reply, but couldn't stop laughing."
    "Ella trató de responder mientras se reía."

# game/dialogs.rpy:1932
translate spanish sceneS1_b3fc2700:

    # s "It's... It's just I'm... I'm relieved now that I said it!!"
    s "Es... Es solo que estoy... ¡¡Estoy aliviada ahora que lo dije!!"

# game/dialogs.rpy:1933
translate spanish sceneS1_22593ea9:

    # s "And.... the way you rolled your eyes... It was too funny, I couldn't hold it!"
    s "Y... La manera que rodaste tus ojos... Fue demasiado gracioso, ¡No lo podía retener!"

# game/dialogs.rpy:1934
translate spanish sceneS1_917bb3ec:

    # hero "Hey, that's just how I naturally react!"
    hero "Oye, ¡que mala!"

# game/dialogs.rpy:1935
translate spanish sceneS1_1809902d:

    # s "Teeheehee! I'm so sorry, %(stringhero)s-kun..."
    s "¡Teeheehee! Lo siento, %(stringhero)s-kun..."

# game/dialogs.rpy:1936
translate spanish sceneS1_50395e0e:

    # "I rolled my eyes again without realizing and she started to laugh even louder..."
    "Rodé mis ojos otra vez sin darme cuenta y ella comenzó a reír más fuerte..."

# game/dialogs.rpy:1937
translate spanish sceneS1_36e4217d:

    # "I began to laugh as well."
    "No sé porqué, pero en ese entonces, estaba en el humor para reírme también."

# game/dialogs.rpy:1938
translate spanish sceneS1_3b7641fe:

    # "I was so happy."
    "Estaba tan feliz."

# game/dialogs.rpy:1940
translate spanish sceneS1_8efda62d:

    # "She stopped after a while, wiping away her tears of laughter."
    "Ella se detuvo después de un tiempo, quitándose sus lágrimas de la risa."

# game/dialogs.rpy:1941
translate spanish sceneS1_e15c0017:

    # s "I've never felt happiness like this."
    s "¡Estoy tan feliz ahora!"

# game/dialogs.rpy:1942
translate spanish sceneS1_1baf5e06:

    # s "Thanks to you, I feel that those sad memories have gone away now."
    s "Gracias a ti, siento que esos tristes momentos desaparecieron ahora."

# game/dialogs.rpy:1943
translate spanish sceneS1_b27b381b:

    # s "Thank you so much...%(stringhero)s-kun! Thank you!"
    s "Muchas gracias...¡%(stringhero)s-kun! ¡Gracias!"

# game/dialogs.rpy:1944
translate spanish sceneS1_0cb95e5a:

    # "She embraced me tightly like a plushie, like a little girl to her father, with an adorable smile on her lips. I could feel her heart beating against my chest..."
    "Ella me abrazó fuertemente como un peluche, como una niña pequeña a su padre, con una linda y profunda sonrisa en sus labios. Pude sentir sus latidos contra mi pecho..."

# game/dialogs.rpy:1950
translate spanish sceneS1_00f63e60:

    # "We had a lot of fun today in the city."
    "Tuvimos un montón de diversión hoy en la ciudad."

# game/dialogs.rpy:1951
translate spanish sceneS1_936e8fa2:

    # "Holding hands, we were on our way back to the station."
    "Estábamos regresando a la estación, tomándonos de las manos."

# game/dialogs.rpy:1952
translate spanish sceneS1_eb6c4ac4:

    # "But suddenly, Sakura had stopped."
    "Pero de pronto sentí que Sakura se detuvo."

# game/dialogs.rpy:1953
translate spanish sceneS1_e8cdcca5:

    # "I turned around to look at her, while still holding her hand."
    "Me volteé para mirarla, aún sujetando su mano."

# game/dialogs.rpy:1955
translate spanish sceneS1_e9b16d76:

    # "Her expression was blank, then turned to embarrassment. She was beet-root red."
    "Su expresión estaba en blanco, avergonzada... Y estaba roja."

# game/dialogs.rpy:1956
translate spanish sceneS1_27c88c8d:

    # hero "Something wrong?"
    hero "¿Pasa algo malo, cariño?"

# game/dialogs.rpy:1957
translate spanish sceneS1_0d15642b:

    # "She pointed toward a building near us..."
    "Ella apuntó a un edificio cerca de nosotros..."

# game/dialogs.rpy:1958
translate spanish sceneS1_1c82a33d:

    # "I looked to see, and it was...{w}{size=+5}love hotel!?!{/size}"
    "Es un...{w}{size=+5}¡¡¡Hotell!!!{/size}"

# game/dialogs.rpy:1959
translate spanish sceneS1_df826d7c:

    # "Whoa wait what!?"
    "¡¡¡Oh por Dios!!!"

# game/dialogs.rpy:1960
translate spanish sceneS1_58865132:

    # "Does she want to..."
    "Eso significa que ella quiere..."

# game/dialogs.rpy:1961
translate spanish sceneS1_57d99560:

    # hero "D-do you...you want to...go there?"
    hero "¿Tú... Tú quieres... ir ahí?"

# game/dialogs.rpy:1962
translate spanish sceneS1_be50fa6b:

    # "She nodded and looked down at the ground to hide her shyness. Her hand gripped me tighter."
    "Ella asintió y miró hacia el suelo, haciéndola más linda. Su mano estaba sujetando la mía con fuerza."

# game/dialogs.rpy:1964
translate spanish sceneS1_c7711bb7:

    # hero "S-sure, let's go."
    hero "C-Claro, vamos."

# game/dialogs.rpy:1968
translate spanish sceneS1_8534ca42_1:

    # ". . ."
    ". . ."

# game/dialogs.rpy:1969
translate spanish sceneS1_2c0af0e2:

    # "I woke up in the bed of the hotel."
    "Desperté en la cama del hotel."

# game/dialogs.rpy:1970
translate spanish sceneS1_1b6c4bc4:

    # "I think I doozed off for only 15 minutes or so. I was feeling pretty sleepy."
    "Pienso que me dormí solo por 15 minutos o algo así. Me sentí adormecido."

# game/dialogs.rpy:1971
translate spanish sceneS1_d36164f4:

    # "In my arms, Sakura was sleeping against me."
    "En mis brazos, Sakura estaba durmiendo sobre mí."

# game/dialogs.rpy:1972
translate spanish sceneS1_9f8a91dc:

    # "She held me tight..."
    "Ella me estaba agarrando con fuerza..."

# game/dialogs.rpy:1973
translate spanish sceneS1_f78bfd73:

    # "I guess she must have been exhausted, after so much fun in that room..."
    "Supongo que ella estaba tan cansada como yo, después de tanta diversión en ese cuarto."

# game/dialogs.rpy:1974
translate spanish sceneS1_2418a26e:

    # "She woke up still a little drowsy, and spoke with a weary voice,"
    "Ella se despertó un poco y me dijo con una voz algo cansada."

# game/dialogs.rpy:1975
translate spanish sceneS1_943f038e:

    # s "%(stringhero)s-kun?.."
    s "¿%(stringhero)s-kun?.."

# game/dialogs.rpy:1976
translate spanish sceneS1_36317276:

    # hero "Hmm?"
    hero "¿Hmm?"

# game/dialogs.rpy:1977
translate spanish sceneS1_e490aa43:

    # s "Was it... your first time too...?"
    s "¿Era... Tu primera vez también...?"

# game/dialogs.rpy:1978
translate spanish sceneS1_36fd2a62:

    # hero "Y-yeah..."
    hero "S-si..."

# game/dialogs.rpy:1979
translate spanish sceneS1_f2210395:

    # s "You... Do you regret it?"
    s "¿Tú... No te arrepientes?"

# game/dialogs.rpy:1980
translate spanish sceneS1_13b2e8fe:

    # hero "Me?... No... Why should I?"
    hero "¿Yo?... No... ¿Porqué debería?"

# game/dialogs.rpy:1981
translate spanish sceneS1_d723f1ce:

    # s "Nevermind..."
    s "Olvidalo..."

# game/dialogs.rpy:1982
translate spanish sceneS1_3191b879:

    # s "I'm just... I still can't believe I finally found a boy who accepts me the way I am..."
    s "Es solo que... Aún no puedo creer que finalmente encontré un chico que me acepte como soy..."

# game/dialogs.rpy:1983
translate spanish sceneS1_67fabef6:

    # s "You even gave me my first time..."
    s "Y alguien que me ha aceptado tanto, que incluso me haya dado su primera vez..."

# game/dialogs.rpy:1984
translate spanish sceneS1_f8751689:

    # hero "I'm happy to be your first...{p}And I'm happy that you were my first too."
    hero "Estoy feliz de ser tu primera vez...{p}Y estoy feliz de que eres mi primera vez también."

# game/dialogs.rpy:1985
translate spanish sceneS1_61308ad3:

    # s "I'm so happy %(stringhero)s-kun... This is almost like a dream..."
    s "Estoy tan feliz %(stringhero)s-kun... Es casi como un sueño..."

# game/dialogs.rpy:1986
translate spanish sceneS1_66805949:

    # "We lay there in peaceful silence together..."
    "No dijimos ni una palabra por un momento..."

# game/dialogs.rpy:1987
translate spanish sceneS1_0af1e620:

    # "I was completely crazy about her... "
    "Estaba completamente loco por ella..."

# game/dialogs.rpy:1988
translate spanish sceneS1_a71c4c3b:

    # "We got lost in each other's eyes."
    "Nos miramos profundamente en los ojos."

# game/dialogs.rpy:1989
translate spanish sceneS1_4a521333:

    # "Her eyes were filled with love and happiness. She smiled at me..."
    "Sus ojos estaban llenos de amor y felicidad. Ella me sonrió..."

# game/dialogs.rpy:1990
translate spanish sceneS1_dd054cb9:

    # "We ended up making love one more time... then some time later..."
    "Hicimos el amor una vez más, algunos minutos después..."

# game/dialogs.rpy:1991
translate spanish sceneS1_0dcaa626:

    # "We took a shower, left the hotel, and took the train home. I walked her home."
    "Entonces, después de una ducha, nos fuimos, tomamos un tren y regresamos a casa."

# game/dialogs.rpy:1995
translate spanish sceneS1_2724f4f7:

    # s "See you at school, honey!"
    s "¡Nos vemos en la escuela, cariño!"

# game/dialogs.rpy:1996
translate spanish sceneS1_90456e76:

    # hero "See you tomorrow, love..."
    hero "Nos vemos mañana, amor..."

# game/dialogs.rpy:1998
translate spanish sceneS1_adc5375c:

    # "She waved goodbye and entered her house."
    "Ella se despide y entra a su casa."

# game/dialogs.rpy:1999
translate spanish sceneS1_8c41d669:

    # "So much happened this weekend..."
    "Ese fin de semana pasó tan rápido..."

# game/dialogs.rpy:2000
translate spanish sceneS1_3eb2b85d:

    # "Sakura told me that she was a boy,{p}then we kissed and went on a date,{p}then we did \"it\"..."
    "Sakura me dijo que ella era un chico,{p}entonces nos besamos y salímos,{p}entonces hicimos \"eso\"..."

# game/dialogs.rpy:2001
translate spanish sceneS1_e36e1081:

    # "I'm so tired... So many things to wrap my head around..."
    "Estoy tan cansado... Tantas cosas sobre qué pensar..."

# game/dialogs.rpy:2002
translate spanish sceneS1_45a34284:

    # "I'd like to take the day off tomorrow, but I'll get to see Sakura tomorrow too!"
    "¡Me tomaría el día libre mañana si Sakura no fuera a la escuela!"

# game/dialogs.rpy:2010
translate spanish sceneS1_443e2e2a:

    # r "{size=+15}WHAAAAAAAAAT??????{/size}"
    r "{size=+15}¿¿¿¿¿¿QUÉEEEEEEEEEE??????{/size}"

# game/dialogs.rpy:2016
translate spanish sceneS1_d1f76e80:

    # r "You... Y-Y-Y-Y-You are lo-lo-lo-lovers?!!!"
    r "Ustedes...¡¡¡¿U-u-u-u-ustedes son a-a-a-amantes?!!! "

# game/dialogs.rpy:2017
translate spanish sceneS1_1182fed0:

    # s "Teehee!..."
    s "¡Teehee!..."

# game/dialogs.rpy:2018
translate spanish sceneS1_70b77763:

    # hero "Well, yeah..."
    hero "Bueno, si..."

# game/dialogs.rpy:2019
translate spanish sceneS1_bf2888c1:

    # n "Seriously, guys!!!!"
    n "¡¡¡¡En serio, chicos!!!!"

# game/dialogs.rpy:2020
translate spanish sceneS1_87964ca3:

    # r "B... B-B-B-Bu-Bu-Bu...."
    r "P... P-P-P-Pe-Pe-Pe...."

# game/dialogs.rpy:2021
translate spanish sceneS1_0e57396a:

    # r "I'm... I-I-I....I'm...."
    r "Estoy...E-e-e... Estoy..."

# game/dialogs.rpy:2023
translate spanish sceneS1_322f4f6b:

    # r "I'm so happy for both of you!!!"
    r "¡¡¡Estoy tan feliz por ambos!!!"

# game/dialogs.rpy:2025
translate spanish sceneS1_d35b9154:

    # r "No, wait. I'm not. Not at all."
    r "No, espera. No lo estoy."

# game/dialogs.rpy:2026
translate spanish sceneS1_b5c49fda:

    # r "%(stringhero)s is a damn pervert, he... He will..."
    r "%(stringhero)s es un pervertido, él...Él hará..."

# game/dialogs.rpy:2027
translate spanish sceneS1_5d72dbb5:

    # hero "Hey!!!"
    hero "¡¡¡Oye!!!"

# game/dialogs.rpy:2028
translate spanish sceneS1_e5583358:

    # n "{size=+10}Seriously, guys!!!!{/size}"
    n "{size=+10}¡¡¡¡En serio, chicos!!!!{/size}"

# game/dialogs.rpy:2029
translate spanish sceneS1_35b8ccde:

    # "Sakura looked at me and smiles brightly as she turned red."
    "Sakura me mira y sonrie mientras se sonroja."

# game/dialogs.rpy:2030
translate spanish sceneS1_124abe51:

    # "I looked right back at her and I smirked. She starts to laugh a little."
    "Yo la miré y me reí. Ella se rie también."

# game/dialogs.rpy:2032
translate spanish sceneS1_df3a5fe2:

    # "Rika sees this exchange and starts to stutter."
    "Rika mira esto y empieza a tartamudear."

# game/dialogs.rpy:2033
translate spanish sceneS1_6151b676:

    # r "No... N-N-No, d-d-d-don't tell me you... Don't tell me you already...."
    r "No... N-N-No, n-n-n-no me digan que ustedes... No me digan que ustedes ya...."

# game/dialogs.rpy:2034
translate spanish sceneS1_1d81a8c5:

    # hero "Hmm, alright, we won't tell you, then..."
    hero "Hmm, esta bien, no te diremos, entonces..."

# game/dialogs.rpy:2040
translate spanish sceneS1_cc4c3814:

    # r "{size=+15}EEEEEEEEHHHHHHHH!!!{/size}"
    r "{size=+15}¡¡¡EEEEEEEEHHHHHHHH!!!{/size}"

# game/dialogs.rpy:2042
translate spanish sceneS1_e4cfb08c:

    # n "{size=+15}SERIOUSLY, GUYS!!!!{/size}"
    n "{size=+15}¡¡¡¡EN SERIO, CHICOS!!!!{/size}"

# game/dialogs.rpy:2043
translate spanish sceneS1_0e6125af:

    # "Rika and Nanami were absolutely shocked."
    "Bien, ya lo haz entendido... Rika y Nanami estaban estupefactas..."

# game/dialogs.rpy:2044
translate spanish sceneS1_1067a846:

    # "Even after a few days passed, Rika still wasn't over it!"
    "¡Rika estuvo traumada por 3 días!"

# game/dialogs.rpy:2045
translate spanish sceneS1_5c7feca9:

    # "And Nanami randomly shouted \"Seriously, guys!!\" at us for the same amount of time!"
    "¡Y Nanami gritó de forma aleatoria \"¡¡En serio, chicos!!\" por mucho tiempo!"

# game/dialogs.rpy:2046
translate spanish sceneS1_9ed40c66:

    # "It was kinda fun to watch."
    "Eso fue divertido de ver."

# game/dialogs.rpy:2068
translate spanish secretnanami_d274ba9e:

    # "The next day, Nanami invited me to play videogames at her house"
    "Al día siguiente, Nanami me invitó a jugar videojuegos en su casa."

# game/dialogs.rpy:2070
translate spanish secretnanami_d7735442:

    # "Nanami's room was a bit messy."
    "El cuarto de Nanami se miraba un poco desordenada."

# game/dialogs.rpy:2071
translate spanish secretnanami_2bf0b11f:

    # "Her bed and computer setup took up most of the room. Her computer looked more powerful than the eMac I had in my bedroom."
    "Excepto por su cama, la parte más grande de su cuarto era su computadora. Algo más poderosa que el eMac en mi cuarto."

# game/dialogs.rpy:2072
translate spanish secretnanami_992fbe8e:

    # "There was also an old TV with several gaming consoles plugged in it."
    "También había una vieja TV con varias consolas de videojuegos conectados a ella."

# game/dialogs.rpy:2073
translate spanish secretnanami_2bd3e307:

    # "Random bags of snacks and clothes were scattered about. It was a mess, but kind of an impressive mess."
    "Eso era un desorden, pero un impresionate desorden."

# game/dialogs.rpy:2074
translate spanish secretnanami_2f4f2a6e:

    # "Nanami seemed a bit embarrassed as I was looking around the room."
    "Nanami estaba un poco ruborizada mientras miraba su cuarto. "

# game/dialogs.rpy:2076
translate spanish secretnanami_4a017f1e:

    # n "D-don't pay any attention to the mess, %(stringhero)s-senpai..."
    n "N-no le prestes atención al desorden, %(stringhero)s-senpai..."

# game/dialogs.rpy:2077
translate spanish secretnanami_7a1bf678:

    # hero "It's okay, I don't mind."
    hero "Esta bien, no me importa..."

# game/dialogs.rpy:2078
translate spanish secretnanami_9b653222:

    # hero "You have so much gaming stuff. It's impressive!"
    hero "Tú tienes muchas cosas de videojuegos, ¡es impresionante!"

# game/dialogs.rpy:2079
translate spanish secretnanami_2b844856:

    # hero "But there's barely any space. Doesn't it feel cramped for you at all?"
    hero "Pero el espacio es tan pequeño... ¿No te sientes apretada en semejante espacio reducido?"

# game/dialogs.rpy:2081
translate spanish secretnanami_d4d4357b:

    # n "Not at all!"
    n "¡Para nada!"

# game/dialogs.rpy:2082
translate spanish secretnanami_8d45096b:

    # n "Everything I could ever need is here!"
    n "¡Hay de todo lo que necesito aquí!"

# game/dialogs.rpy:2083
translate spanish secretnanami_3e97bcd0:

    # hero "Heh, well after all, you're small. Seems perfect for you."
    hero "Je, después de todo, eres pequeña. Eso debería estar bien."

# game/dialogs.rpy:2085
translate spanish secretnanami_db57c973:

    # n "I'm not that small!"
    n "¡No soy pequeña!"

# game/dialogs.rpy:2086
translate spanish secretnanami_2fc28d42:

    # "I laughed at her adorable reaction. I continued checking out the room."
    "Me reí en su lindura, mis ojos aún examinaban el cuarto."

# game/dialogs.rpy:2087
translate spanish secretnanami_51b37813:

    # hero "Your brother isn't here, today?"
    hero "¿Tu hermano no esta aquí, el día de hoy?"

# game/dialogs.rpy:2089
translate spanish secretnanami_d2016bd8:

    # n "Toshio-nii? No. He usually works on Sunday. He's only home on Mondays."
    n "¿Toshio? No. Él usualmente trabaja en Domingos. El solo descansa los Lunes."

# game/dialogs.rpy:2090
translate spanish secretnanami_b67b5fcc:

    # n "We have the house to ourselves until 6PM!"
    n "¡Tenemos la casa solo para nosotros hasta las 6PM!"

# game/dialogs.rpy:2091
translate spanish secretnanami_84b8bd32:

    # hero "I see."
    hero "Esta bien, ya veo..."

# game/dialogs.rpy:2093
translate spanish secretnanami_fec633fd:

    # "I noticed a framed picture near her computer desk."
    "Entonces noté una foto enmarcada cerca de su mesa de computadora."

# game/dialogs.rpy:2094
translate spanish secretnanami_03d84c1c:

    # "It looked like it was an old picture of Nanami as a child."
    "Era una vieja foto de Nanami a la edad de 12, aparentemente."

# game/dialogs.rpy:2095
translate spanish secretnanami_865a0544:

    # "Beside her, there was an older child. This must be her brother.{p}And behind the both of them, there was an adult couple. Probably her parents..."
    "Además de ella, había un niño mayor, su hermano supongo.{p}Y detrás de ellos, había una pareja adulta. Sin duda sus padres..."

# game/dialogs.rpy:2098
translate spanish secretnanami_d381eb8b:

    # "Nanami noticed I was looking at it and came closer. Her pleasant expression faded away."
    "Nanami notó que estaba mirándolo y se acercó. Su expresión se volvió más oscura."

# game/dialogs.rpy:2099
translate spanish secretnanami_3d2aa8bc:

    # n "That's...{w}the last picture I have of my parents."
    n "Es...{w}La última foto que tuve de mis padres..."

# game/dialogs.rpy:2100
translate spanish secretnanami_2462192a:

    # hero "You mean... They're gone? Are they divorced or something?"
    hero "Quieres decir...¿Ellos se fueron, se divorciaron o algo?"

# game/dialogs.rpy:2102
translate spanish secretnanami_3ac96ee3:

    # "She turned away from me. I faced her back, and she began to speak..."
    "Ella se volteó. Miré su espalda, escuchándola..."

# game/dialogs.rpy:2103
translate spanish secretnanami_03e340ee:

    # n "No..."
    n "No..."

# game/dialogs.rpy:2104
translate spanish secretnanami_ad2cdc06:

    # n "They are...{w}dead."
    n "Ellos están...{w}Muertos."

# game/dialogs.rpy:2106
translate spanish secretnanami_cc9f4dc4:

    # "I stayed motionless and was speechless. Shocked by the revelation."
    "Estuve inmóvil y sin habla, estupefacto por la revelación."

# game/dialogs.rpy:2107
translate spanish secretnanami_2c0862cf:

    # hero "I-... I'm so sorry to hear that, Nanami-chan... I didn't mean to bring up a sore subject..."
    hero "Yo... Lo siento tanto de escuchar eso, Nanami-chan..."

# game/dialogs.rpy:2109
translate spanish secretnanami_80110a90:

    # "Nanami sat on the floor, holding her legs."
    "Nanami se sentó en el piso, agarrando sus piernas."

# game/dialogs.rpy:2111
translate spanish secretnanami_eb85d77c:

    # n "You know...They were working at the grocery store your parents are managing today."
    n "Sabes... Ellos estaban trabajando en la tienda de abarrotes que tus padres administran hoy."

# game/dialogs.rpy:2112
translate spanish secretnanami_de2b803f:

    # hero "What...!?"
    hero "¿En serio?"

# game/dialogs.rpy:2113
translate spanish secretnanami_59529ad1:

    # "Just then, I remembered something. My parents got the shop because the previous owners hadn't been around for a few years."
    "Recuerdo que mis padres pudieron obtener la tienda porque los dueños anteriores se habían ido por años."

# game/dialogs.rpy:2114
translate spanish secretnanami_9fec9592:

    # "I didn't think they were actually gone..."
    "No pensé que ellos se fueran metafóricamente, a decir verdad... "

# game/dialogs.rpy:2115
translate spanish secretnanami_ee152b17:

    # "I sat next to Nanami."
    "Me senté cerca de Nanami."

# game/dialogs.rpy:2116
translate spanish secretnanami_b4b7b899:

    # n "It happened four years ago."
    n "Pasó hace 4 años."

# game/dialogs.rpy:2117
translate spanish secretnanami_f2bc7b70:

    # n "It... It was on my brother's birthday."
    n "Fue... Fue en el cumpleaños de mi hermano."

# game/dialogs.rpy:2118
translate spanish secretnanami_90a4c3c8:

    # n "They went to the city to get a surprise present for him..."
    n "Ellos fueron a la ciudad a conseguir un regalo sorpresa para él..."

# game/dialogs.rpy:2119
translate spanish secretnanami_1a38e7d1:

    # n "But they never came back..."
    n "Ellos nunca volvieron..."

# game/dialogs.rpy:2120
translate spanish secretnanami_728dd651:

    # n "I heard they were hit by a truck when they were coming back..."
    n "Escuché que ellos fueron golpeados por un camión cuando regresaban..."

# game/dialogs.rpy:2121
translate spanish secretnanami_28efb3c9:

    # n "Toshio-nii told me. He was the one to get the phone call about my parents."
    n "Toshio me lo dijo. Él fue el que tuvo la llamada telefónica sobre mis padres."

# game/dialogs.rpy:2123
translate spanish secretnanami_e75dc79b:

    # n "You know... My brother, he...{p}he changed a lot after that."
    n "Sabes... Mi hermano, él...{p}Él cambió mucho después de eso."

# game/dialogs.rpy:2124
translate spanish secretnanami_d070d077:

    # n "He blames himself for the death of our parents."
    n "Él piensa que él es responsable de la muerte de nuestros padres."

# game/dialogs.rpy:2125
translate spanish secretnanami_d8670523:

    # n "I told him that wasn't true. That it's not his fault."
    n "Le dije que eso no es verdad. Que no es su culpa."

# game/dialogs.rpy:2126
translate spanish secretnanami_65090fe7:

    # hero "That's right. It's not his fault... It was the fate..."
    hero "Eso es cierto. No es su culpa... Fue el destino..."

# game/dialogs.rpy:2127
translate spanish secretnanami_b6f21cea:

    # n "But he doesn't listen."
    n "Pero él no escuchó."

# game/dialogs.rpy:2128
translate spanish secretnanami_76cf9acc:

    # n "He's become a little unstable because of that. Sometimes, he scares me a bit."
    n "Él se volvió un poco loco a causa de eso. A veces, él me asusta un poco."

# game/dialogs.rpy:2129
translate spanish secretnanami_d1c2fbdd:

    # "I instantly thought about Sakura's relationship with her father. But Nanami corrected me immediately like she knew what I was thinking."
    "Inmediatamente pensé sobre la relación de Sakura con su padre. Pero Nanami me corrigió inmediatamente como si adivinara lo que estaba pensando."

# game/dialogs.rpy:2130
translate spanish secretnanami_d01d8042:

    # n "He doesn't beat me or argue with me... It's more like the opposite..."
    n "Él no me golpea, o discute conmigo... Es más como lo contrario..."

# game/dialogs.rpy:2131
translate spanish secretnanami_d40e7776:

    # n "He's become over-protective of me."
    n "Él se volvió sobre-protector hacia mí."

# game/dialogs.rpy:2132
translate spanish secretnanami_43d2f391:

    # n "He says we're each other's only family now and that we have to take good care of each other."
    n "Él dice que somos la única familia que tenemos ahora y que debemos cuidarnos bien del uno al otro."

# game/dialogs.rpy:2133
translate spanish secretnanami_f8c997a1:

    # hero "You don't have any grand-parents?"
    hero "¿Ustedes no tienen algún abuelo o abuela?"

# game/dialogs.rpy:2134
translate spanish secretnanami_8fcd3561:

    # n "My father's parents passed away before I was born."
    n "Los padres de mi padre estaban muertos antes de que yo naciera."

# game/dialogs.rpy:2135
translate spanish secretnanami_eb73e5cd:

    # n "My mother's parents are still alive... But they're all the way in Okinawa."
    n "Los padres de mi madre aún están vivos... Pero ellos residen en Okinawa."

# game/dialogs.rpy:2136
translate spanish secretnanami_4b6a67a0:

    # n "We write to them, from time to time..."
    n "Les escribimos, de vez en cuando..."

# game/dialogs.rpy:2137
translate spanish secretnanami_eb3d403f:

    # n "Afterwards, Toshio-nii took a job to afford the house rent and our other needs."
    n "Toshio tomó un trabajo después para costear la renta y las necesidades."

# game/dialogs.rpy:2138
translate spanish secretnanami_e668a3b6:

    # hero "I see..."
    hero "Ya veo..."

# game/dialogs.rpy:2139
translate spanish secretnanami_a20cefa7:

    # "..."
    "..."

# game/dialogs.rpy:2140
translate spanish secretnanami_0b1702b3:

    # "Darn, I don't know what to say."
    "Maldición, no sé qué decir."

# game/dialogs.rpy:2141
translate spanish secretnanami_12251861:

    # "Losing your parents at this age must be traumatic. I can barely imagine it."
    "Perder a los padres a esta edad debe ser traumático. Apenas lo imagino."

# game/dialogs.rpy:2142
translate spanish secretnanami_1d53eed8:

    # "I felt like Nanami was about to cry."
    "Sentí que Nanami estaba comenzando a llorar. Llorar con lágrimas reales."

# game/dialogs.rpy:2143
translate spanish secretnanami_966d23a4:

    # "I wrapped an arm around her shoulder and she started to sob silently in my arms."
    "Instintivamente la envolví con un brazo alrededor de sus hombros y ella comenzó a llorar silenciosamente en mis brazos."

# game/dialogs.rpy:2144
translate spanish secretnanami_ee5f81b5:

    # "She must have greatly loved her parents..."
    "Ella debe amar a sus padres enormemente..."

# game/dialogs.rpy:2146
translate spanish secretnanami_8b459908:

    # "After a while, she stopped crying. She wiped her tears and smiled at me."
    "Luego de un tiempo, ella paró de llorar, se limpió las lágrimas y me sonrió."

# game/dialogs.rpy:2150
translate spanish secretnanami_42d6bc4c:

    # n "Only a few people know the truth about my parents. Sakura-nee, Rika-nee... And now, you too."
    n "La gente piensa que nuestros padres regresaron a Okinawa, ya que Toshio es un adulto ahora.."

# game/dialogs.rpy:2151
translate spanish secretnanami_0fc2108d:

    # n "People think our parents have gone back to Okinawa, since Toshio-nii is now an adult."
    n "Solo unas pocas personas saben la verdad sobre mis padres. Sakura-nee, Rika-nee... Y ahora tú también.."

# game/dialogs.rpy:2156
translate spanish secretnanami_511241cb:

    # "So that's what Sakura was talking about at the arcade..."
    "Así que esto es lo qué Sakura estaba hablando en el salón recreativo..."

# game/dialogs.rpy:2161
translate spanish secretnanami_c3c4b0be:

    # hero "How are you feeling now? Are you okay?"
    hero "¿Cómo te sientes ahora? ¿Estás bien?"

# game/dialogs.rpy:2162
translate spanish secretnanami_8ed55284:

    # n "Yeah... I'm okay, %(stringhero)s-nii."
    n "Si... Estoy bien, %(stringhero)s-nii."

# game/dialogs.rpy:2164
translate spanish secretnanami_c259ee4e:

    # "I smiled and gently pat her head."
    "Sonreí y acaricié su cabeza gentilmente."

# game/dialogs.rpy:2165
translate spanish secretnanami_8f2bc02b:

    # hero "So, are we playing some games? You did invite me for that."
    hero "Entonces, ¿porqué no juganmos unos videojuegos? Supongo que me invitaste por eso."

# game/dialogs.rpy:2167
translate spanish secretnanami_7b9494dd:

    # n "Sure! Let me show you what I have."
    n "¡Claro! Dejame mostrarte qué es lo que tengo."

# game/dialogs.rpy:2169
translate spanish secretnanami_b5ed2554:

    # "Some time later, we were playing a fighting game called {i}Mortal Battle{/i}."
    "Un momento depués, estábamos jugando un juego de peleas llamado {i}Mortal Battle{/i}."

# game/dialogs.rpy:2170
translate spanish secretnanami_b69a371f:

    # "This was an American game known to have complicated and unconventional special attacks. It's hard to get used to it when you mostly play Japanese fighting games."
    "Ese juego americano era conocido por tener ataques especiales complicados y poco convenientes. Es difícil acostumbrarse cuando haz jugado mayormente juegos de pelea japoneses."

# game/dialogs.rpy:2171
translate spanish secretnanami_1145ba4f:

    # "But I ended up mastering one of the characters in a short amount of time."
    "Pero dominé uno de los personajes bastante bien."

# game/dialogs.rpy:2172
translate spanish secretnanami_c49f161e:

    # "Every time I selected this character, I ended up beating the crap out of Nanami's character."
    "Cada vez que lo usaba, derrotaba al personaje de Nanami."

# game/dialogs.rpy:2174
translate spanish secretnanami_073340cb:

    # n "Eeeeeh! How did you do that special attack?!"
    n "¡Eeeeeh! ¡¿Cómo haces ese ataque especial?!"

# game/dialogs.rpy:2175
translate spanish secretnanami_0883b3a9:

    # hero "It's not too hard. You just roll the stick like this, then like this and you press HK."
    hero "No es tan difícil. Solo giras la palanca así, luego así y presionas HK."

# game/dialogs.rpy:2177
translate spanish secretnanami_45d61fa6:

    # n "...I still don't get it..."
    n "...¿Cómo haces eso?"

# game/dialogs.rpy:2178
translate spanish secretnanami_27514623:

    # hero "Hold on, I'll show you."
    hero "Espera, te lo mostraré."

# game/dialogs.rpy:2183
translate spanish secretnanami_9d9c7a4c:

    # "I placed myself behind Nanami."
    "Me coloqué detrás de Nanami."

# game/dialogs.rpy:2184
translate spanish secretnanami_d11638d3:

    # hero "Don't move, I'll show you exactly how."
    hero "No te muevas, te mostraré el cómo exactamente."

# game/dialogs.rpy:2185
translate spanish secretnanami_98b0a82d:

    # n "O-okay!"
    n "¡E-esta bien!"

# game/dialogs.rpy:2186
translate spanish secretnanami_9b84d6a3:

    # "I placed my hands on Nanami's, holding the controller with her."
    "Tomé las manos de Nanami con las mías, sosteniendo el control con ella."

# game/dialogs.rpy:2187
translate spanish secretnanami_e5d45e92:

    # "I realized how small her hands were...but they were so soft and warm as well. I started to get a little nervous and blush."
    "Me sonrojé bastante, dándome cuenta cómo de suaves y calidos son sus pequeñas manos de jugadora."

# game/dialogs.rpy:2188
translate spanish secretnanami_b142513a:

    # "I felt like she was starting get just as nervous as well."
    "Sentí que ella estaba comenzando a sonrojarse también."

# game/dialogs.rpy:2189
translate spanish secretnanami_72fc66bd:

    # "I went into the training mode in the game."
    "En el juego, me fui al modo de entrenamiento."

# game/dialogs.rpy:2190
translate spanish secretnanami_068e1468:

    # "Behind the game's sound effects, I could faintly hear Nanami's soft breathing."
    "Detrás de los efectos de sonido del juego, podría escuchar tenuemente la suave respiración de Nanami."

# game/dialogs.rpy:2191
translate spanish secretnanami_dad13c6d:

    # "I started to feel the warmth of her back on my chest. I could feel myself losing focus..."
    "Comencé a sentir su calidez en mi pecho y un deseo comenzó a crecer en mí."

# game/dialogs.rpy:2192
translate spanish secretnanami_a3e2db4f:

    # hero "O-okay... S-so you... You roll the left stick like that..."
    hero "E-esta bien... E-entonces tú... Tú giras la palanca izquierda así..."

# game/dialogs.rpy:2193
translate spanish secretnanami_b73c31d9:

    # "I tried the special attack but it failed."
    "Traté el ataque especial pero falló."

# game/dialogs.rpy:2194
translate spanish secretnanami_f8130d09:

    # hero "W-wait, let me try again..."
    hero "E-espera, dejame tratar de nuevo..."

# game/dialogs.rpy:2195
translate spanish secretnanami_8fe679da:

    # "I couldn't concentrate with Nanami in my arms like this."
    "No podía concentrarme con Nanami en mis brazos así."

# game/dialogs.rpy:2197
translate spanish secretnanami_9f50bc6f:

    # "I could feel my heart beating against her back."
    "Mi corazón estaba latiendo con fuerza contra su espalda."

# game/dialogs.rpy:2198
translate spanish secretnanami_516207af:

    # "I tried to redo the special attack but I just couldn't do it correctly. My hands were shaking."
    "Traté de hacer el ataque especial de nuevo pero simplemente no lo pude hacerlo correctamente. Mis manos estaban temblando un poco."

# game/dialogs.rpy:2199
translate spanish secretnanami_9a928511:

    # "All my thoughts were floating towards Nanami."
    "Todos mis pensamientos estaban hacia Nanami."

# game/dialogs.rpy:2200
translate spanish secretnanami_11daa19b:

    # "Her soft smell, her warmth invading my hands and my chest against her back..."
    "Su suave aroma, su calidez invadiendo mis manos y mi pecho..."

# game/dialogs.rpy:2201
translate spanish secretnanami_702709c6:

    # "I... I think I'm falling in love with Nanami..."
    "Yo... Yo pienso que me estoy enamorando de Nanami..."

# game/dialogs.rpy:2202
translate spanish secretnanami_0290a533:

    # "Nanami turned to face to me, her face was glowing red, just like mine."
    "Nanami volteó su cara a la mía, su cara estaba roja como tomate, como el mío."

# game/dialogs.rpy:2203
translate spanish secretnanami_1126b44f:

    # "Her eyes were locked to mine. I got lost in her big emerald green eyes."
    "Su vista se conectó con la mía. Me sumergí en sus grandes ojos verde esmeralda."

# game/dialogs.rpy:2204
translate spanish secretnanami_1b126339:

    # "It was the coup de grace"
    "Era el golpe de gracia."

# game/dialogs.rpy:2205
translate spanish secretnanami_f5a66b6d:

    # n ".....%(stringhero)s-nii...."
    n ".....%(stringhero)s-nii...."

# game/dialogs.rpy:2206
translate spanish secretnanami_c7ff9e11:

    # hero "...Na....Nanami-chan..."
    hero "...Na....Nanami-chan..."

# game/dialogs.rpy:2210
translate spanish secretnanami_07a482ff:

    # "I felt myself moving toward her lips."
    "Atraido por ellos, mis labios tocaron los suyos."

# game/dialogs.rpy:2218
translate spanish secretnanami_6546765d:

    # "She came towards me as well, and our lips met."
    "Ella instantáneamente respondió, devolviendo el beso."

# game/dialogs.rpy:2219
translate spanish secretnanami_ad565af5:

    # "We both dropped the controller and embraced each other tightly, still kissing. Neither of us could stop at all."
    "Ambos dejamos caer el control y nos abrazamos el uno al otro fuertemente, aún besándonos. Ninguno de los dos podía parar."

# game/dialogs.rpy:2220
translate spanish secretnanami_52b5a252:

    # "We fell to the floor in our embrace, trying to avoid some of the mess around."
    "Nos tendimos en el piso con nuestro abrazo, tratando de evitar algo de desorden que estaba alrededor."

# game/dialogs.rpy:2221
translate spanish secretnanami_ca782044:

    # "Her kiss, her softness, her moaning of desire and her sweet smell were driving me crazy."
    "Su beso, su suavidad, sus gemidos de deseo y su dulce aroma me estaban volviendo loco."

# game/dialogs.rpy:2222
translate spanish secretnanami_474523d0:

    # "We kept going, sometimes stopping to pronounce each other's name..."
    "Seguimos haciéndolo, a veces parando para pronunciar el nombre del otro..."

# game/dialogs.rpy:2223
translate spanish secretnanami_5ca23c3c:

    # "After a while, she took my hand and gently dragged me to her bed."
    "Después de un tiempo, ella tomó mi mano y gentilmente me dirigió a su cama."

# game/dialogs.rpy:2224
translate spanish secretnanami_6d6ab9c6:

    # "We continued kissing there, more torridly..."
    "Continuamos besándonos ahí, más torridamente..."

# game/dialogs.rpy:2228
translate spanish secretnanami_8534ca42:

    # ". . ."
    ". . ."

# game/dialogs.rpy:2229
translate spanish secretnanami_0e5d8dad:

    # "The idle video game was still making some noise in the room."
    "El ocioso videojuegos aún estaba haciendo ruidos en la habitación."

# game/dialogs.rpy:2230
translate spanish secretnanami_95a4b2f0:

    # "I was on the bed, holding Nanami by my side."
    "Estaba en la cama, sosteniendo a Nanami a mi lado."

# game/dialogs.rpy:2231
translate spanish secretnanami_d89bb97f:

    # "She was hugging me like a plushie."
    "Ella me estaba abrazando como un peluche."

# game/dialogs.rpy:2232
translate spanish secretnanami_45685a47:

    # hero "W-was it okay?"
    hero "¿E-estuvo bien?"

# game/dialogs.rpy:2233
translate spanish secretnanami_d1a94b7f:

    # n "Y-yeah... It hurt a lil' bit at first,... but it felt good at the end..."
    n "S-si... Dolió un poco al principio... Pero fue bueno al final..."

# game/dialogs.rpy:2234
translate spanish secretnanami_5abb8e5f:

    # n "I'm so happy that you were my first, %(stringhero)s-nii..."
    n "Estoy tan feliz que tu fueras mi primera vez, %(stringhero)s-nii..."

# game/dialogs.rpy:2235
translate spanish secretnanami_fa1ef15c:

    # hero "Me too..."
    hero "Yo también..."

# game/dialogs.rpy:2236
translate spanish secretnanami_1d6ad191:

    # n "I feel like a woman now thanks to you..."
    n "¡Ahora me siento como una mujer gracias a ti!"

# game/dialogs.rpy:2237
translate spanish secretnanami_4ce41c79:

    # "I kissed her forehead with a smile."
    "Besé su frente con una sonrisa."

# game/dialogs.rpy:2238
translate spanish secretnanami_e9590123:

    # "Nanami suddenly giggled."
    "Nanami de pronto se rió."

# game/dialogs.rpy:2239
translate spanish secretnanami_08ea1428:

    # n "I can't wait to see Sakura-nee and Rika-nee's faces when they find out about us!"
    n "¡No puedo esperar a ver las caras de Sakura-nee y Rika-nee cuando sepan sobre nosotros!"

# game/dialogs.rpy:2240
translate spanish secretnanami_d3d236dd:

    # hero "Hehe I'm curious to see that too..."
    hero "Jeje tengo curiosidad de verlo también..."

# game/dialogs.rpy:2241
translate spanish secretnanami_523e30b8:

    # "Then I had a mischievous thought..."
    "Entonces de pronto tuve una sonrisa traviesa..."

# game/dialogs.rpy:2242
translate spanish secretnanami_ceb62c81:

    # hero "I have a fun idea:{p}Why not just let them figure it out themselves?"
    hero "Tengo una divertida idea;{p}¿Porqué no dejarlas que adivinen por ellas mismas en vez de contarles?"

# game/dialogs.rpy:2243
translate spanish secretnanami_46f913e1:

    # hero "They might end up even more surprised!"
    hero "¡Ellas podrían hacer todavía una cara más graciosa!"

# game/dialogs.rpy:2244
translate spanish secretnanami_ae388490:

    # "Nanami giggled louder."
    "Nanami se rió con más fuerza."

# game/dialogs.rpy:2245
translate spanish secretnanami_6ab9da22:

    # n "Good idea! Let's do this!"
    n "¡Buena idea! ¡Vamos a hacerlo!"

# game/dialogs.rpy:2246
translate spanish secretnanami_b3625191:

    # hero "Right!"
    hero "¡Bien!"

# game/dialogs.rpy:2247
translate spanish secretnanami_ec3a4b43:

    # "I giggled and kissed her lips again. She embraced me tighter, kissing back..."
    "Me reí y la besé en los labios otra vez. Ella me abraza con fuerza, besándome de vuelta..."

# game/dialogs.rpy:2248
translate spanish secretnanami_82623620:

    # "Before I could even notice, we were on for a second round..."
    "Antes de incluso notarlo, estábamos por una segunda ronda..."

# game/dialogs.rpy:2249
translate spanish secretnanami_8116efac:

    # "Hehe... \"Second round\"...{p}I bet she finds this 'new game' better than any other game she's played!"
    "Jeje...\"Segunda ronda\" ...{p}¡Apuesto que ella encuentra este nuevo juego mejor que cualquier otro videojuego!"

# game/dialogs.rpy:2253
translate spanish secretnanami_3402d7bf:

    # "After that, it was late so it was time for me to go. "
    "Luego de esto, era tan tarde que ya era tiempo de irme."

# game/dialogs.rpy:2254
translate spanish secretnanami_924e253f:

    # hero "Don't forget: Don't tell anyone in the club!"
    hero "¡No lo olvides: no le cuentes a nadie del club!"

# game/dialogs.rpy:2256
translate spanish secretnanami_91f1d653:

    # n "Yes sir!"
    n "¡Si señor!"

# game/dialogs.rpy:2258
translate spanish secretnanami_743da19c:

    # "We kissed again and then I left for home."
    "Nos besamos otra vez y luego me fui a mi casa."

# game/dialogs.rpy:2262
translate spanish secretnanami_ffc14834:

    # "I was so happy. I was hopping around and dancing a bit."
    "Estaba tan feliz que estaba dando saltitos y danzando un poco."

# game/dialogs.rpy:2263
translate spanish secretnanami_bb28eb57:

    # "People probably thought I was crazy but I didn't care."
    "La gente probablemente pensó que estaba loco pero no le di importancia..."

# game/dialogs.rpy:2267
translate spanish secretnanami_50788d9c:

    # "Monday arrived..."
    "Llegó Lunes..."

# game/dialogs.rpy:2268
translate spanish secretnanami_e94279e1:

    # "I was thinking about all the new things I learned this weekend..."
    "Estaba pensando en las nuevas cosas que aprendí este fin de semana..."

# game/dialogs.rpy:2269
translate spanish secretnanami_ff29a530:

    # "I still can't believe Sakura was born as a boy."
    "Aún no puedo creer que Sakura nació como un chico."

# game/dialogs.rpy:2270
translate spanish secretnanami_d6d86bf5:

    # "But I can't wait to see her again anyway. She's my best friend."
    "Pero de todas formas no puedo esperar a verla otra vez. Ella es mi mejor amiga."

# game/dialogs.rpy:2272
translate spanish secretnanami_12a5358b:

    # "I also thought about Nanami... My dear love..."
    "También pensé en Nanami... Mi amor..."

# game/dialogs.rpy:2273
translate spanish secretnanami_55bde2ad:

    # "I can't wait to see her again too... And to make that joke to our friends!"
    "No puedo esperar a verla de nuevo también... ¡Y hacerles esa broma a nuestras amigas!"

# game/dialogs.rpy:2274
translate spanish secretnanami_e6c4d8b3:

    # "Thinking of Sakura and Nanami made me remember my plan for Sakura."
    "Entonces, pensando en Sakura y Nanami me hizo recordar mi plan para Sakura."

# game/dialogs.rpy:2275
translate spanish secretnanami_da787f5b:

    # "I found Rika and told her I knew about Sakura, and that she needs help to tell the truth to Nanami as well."
    "Encontré a Rika y le conté que sé sobre lo de Sakura, y que ella necesita ayuda para decirle la verdad a Nanami también."

# game/dialogs.rpy:2276
translate spanish secretnanami_f5d57b70:

    # "She gladly accepted."
    "Ella aceptó gustosamente."

# game/dialogs.rpy:2277
translate spanish secretnanami_eb899bec:

    # "For the first time, I saw on her a sincere sweet smile."
    "Y por primera vez, vi en ella una sonrisa sincera y dulce."

# game/dialogs.rpy:2278
translate spanish secretnanami_258e716e:

    # "She told me that I'm sure to be a special member of the club now."
    "Ella me dijo que, ahora, de seguro soy un miembro especial del club."

# game/dialogs.rpy:2280
translate spanish secretnanami_4fe7e943:

    # "Of course, I didn't tell her about Nanami and I..."
    "Por supuesto, no le conté sobre Nanami y yo..."

# game/dialogs.rpy:2281
translate spanish secretnanami_e02d74e7:

    # "Yet..."
    "Aún..."

# game/dialogs.rpy:2287
translate spanish secretnanami_3e63050d:

    # "Lunch time came."
    "Entonces, la hora del almuerzo llegó."

# game/dialogs.rpy:2289
translate spanish secretnanami_7d31f878:

    # "I was eating lunch at my desk with Sakura, as we always do."
    "Estaba almorzando en mi escritorio con Sakura, como siempre lo hacemos."

# game/dialogs.rpy:2290
translate spanish secretnanami_96641885:

    # "Usually, Rika and Nanami join us a few minutes after we start."
    "Usualmente, Rika y Nanami se nos unen unos minutos después de que comenzamos."

# game/dialogs.rpy:2291
translate spanish secretnanami_fcd3f037:

    # hero "Oh, guess what? I know about Nanami, now."
    hero "Oh, ¿adivina qué? Ya sé lo de Nanami ahora."

# game/dialogs.rpy:2292
translate spanish secretnanami_7f54d05d:

    # "Sakura nodded, knowing what I meant."
    "Sakura asintió, conociendo lo que estaba diciendo."

# game/dialogs.rpy:2293
translate spanish secretnanami_26f2656b:

    # s "Looks like she really likes and trusts you, now! That's great!"
    s "¡Parece que ahora ella realmente le agradas y confía en ti! ¡Eso es grandioso!"

# game/dialogs.rpy:2294
translate spanish secretnanami_d5961b9f:

    # "I held a laugh. If only she knew how much she likes me now!"
    "Aguante la risa. ¡Si solo ella supiera cuánto le agrado ahora!"

# game/dialogs.rpy:2297
translate spanish secretnanami_ba640f4d:

    # "Nanami came first."
    "Nanami vino primero."

# game/dialogs.rpy:2298
translate spanish secretnanami_eb443bf4:

    # n "Hey, Sakura-nee!"
    n "Oye, ¡Sakura-nee!"

# game/dialogs.rpy:2300
translate spanish secretnanami_0f4878e5:

    # n "Hey,...honey!"
    n "Oye,... ¡Cariño!"

# game/dialogs.rpy:2302
translate spanish secretnanami_349c95fd:

    # "Nanami left a kiss on my cheek, took a free chair and table and joined us like if nothing happened."
    "Nanami dejó un beso en mi mejilla, tomó una silla y un escritorio que estaban libres para unírsenos al almuerzo como si nada pasó."

# game/dialogs.rpy:2303
translate spanish secretnanami_91c5f81c:

    # "Wow, she's good. It's hard to not blush in this situation."
    "Wow, ella es buena. Es difícil no sonrojarse en semejante situación."

# game/dialogs.rpy:2304
translate spanish secretnanami_0083c6b0:

    # "The face Sakura made was priceless."
    "La cara que Sakura hizo no tenía precio."

# game/dialogs.rpy:2305
translate spanish secretnanami_91e03536:

    # "Her face was flustered and expressed something along the lines of \"What the heck?\" and \"That's embarassing!\""
    "Ella se sonrojó intensamente y su rostro estaba diciendo \"¿Qué diablos?\" y \"¡Eso es embarazoso!\""

# game/dialogs.rpy:2306
translate spanish secretnanami_d1b32a3c:

    # "I had a hard time trying to not laugh out loud.{p}I could tell, Nanami was holding it in too."
    "Tuve un tiempo difícil tratando de no reírme.{p}Y como pude observar, Nanami también."

# game/dialogs.rpy:2308
translate spanish secretnanami_5e2e8af1:

    # "Then Rika appeared and joined us."
    "Entonces Rika apareció y se unió al almuerzo."

# game/dialogs.rpy:2309
translate spanish secretnanami_2f6f7af4:

    # r "Hey guys! Did you have a good Sunday?"
    r "¡Hola chicos! ¿Tuvieron un buen Domingo?"

# game/dialogs.rpy:2310
translate spanish secretnanami_b408e187:

    # "Nanami and I almost choked on our meal at the question."
    "Nanami y yo casi nos atragantamos con nuestro almuerzo al escuchar la pregunta."

# game/dialogs.rpy:2312
translate spanish secretnanami_e5fcea13:

    # r "Sakura, you're okay?"
    r "Sakura, ¿estás bien?"

# game/dialogs.rpy:2313
translate spanish secretnanami_5ab8d803:

    # s "I'm... I'm not sure..."
    s "Estoy... No estoy segura..."

# game/dialogs.rpy:2314
translate spanish secretnanami_5ddf9bba:

    # "All during lunch, Nanami and I were exchanging glances, and every time we had to hold a laugh."
    "Durante todo el almuerzo, Nanami y yo estábamos intercambiando miradas, y en todas las veces teníamos que aguantar la risa."

# game/dialogs.rpy:2315
translate spanish secretnanami_3df5cd5f:

    # "All of this was making Sakura and Rika even more confused and uncomfortable."
    "Todo esto estaba haciendo que Sakura y Rika estuvieran incluso más confusas y apenadas."

# game/dialogs.rpy:2316
translate spanish secretnanami_0469525e:

    # "Finally, Rika decided to speak up."
    "Finalmente, Rika decidió aclarar todo."

# game/dialogs.rpy:2318
translate spanish secretnanami_848c0a9a:

    # r "Alright, guys. It looks like Sakura and I missed the memo. What's going on?"
    r "Muy bien, chicos. ¡Parece que yo y Sakura hemos perdido un episodio!"

# game/dialogs.rpy:2319
translate spanish secretnanami_78a3d7bc:

    # r "What happened this weekend?!"
    r "¡¿Qué pasó en el fin de semana?!"

# game/dialogs.rpy:2321
translate spanish secretnanami_a2210385:

    # "Nanami finally cracked. She burst into laughter. Some classmates turned their heads wondering what was going on."
    "Nanami finalmente se rompió. Estalló en una gran carcajada que hizo que algunos compañeros de clase se voltearan sus cabezas, haciéndoles preguntar que había pasado."

# game/dialogs.rpy:2322
translate spanish secretnanami_e7188aa7:

    # "Her laugh instantly unlocked mine and I finished laughing as well!"
    "Su risa instantáneamente liberó a la mía y ¡al final me reí también!"

# game/dialogs.rpy:2324
translate spanish secretnanami_1b6c8610:

    # r "What the!?"
    r "¡Qué rayos!"

# game/dialogs.rpy:2326
translate spanish secretnanami_5bc75703:

    # r "Sakura, tell me! You must know something!"
    r "¡Sakura, dime! ¡Tú debes saber algo!"

# game/dialogs.rpy:2327
translate spanish secretnanami_a3d554dc:

    # s "I... They... I don't know!..."
    s "Yo... Ellos... ¡No lo sé!..."

# game/dialogs.rpy:2328
translate spanish secretnanami_d6d264c2:

    # s "Nana-chan kissed %(stringhero)s-kun on the cheek and they've been acting like this since!"
    s "Nana-chan besó a %(stringhero)s-kun en la mejilla y ¡ellos no pararon de ser así desde entonces!"

# game/dialogs.rpy:2330
translate spanish secretnanami_dba98263:

    # r "No way! {w}You mean..."
    r "¡De ninguna manera! {w}Quieres decir..."

# game/dialogs.rpy:2331
translate spanish secretnanami_8fd173cb:

    # "I calmed down and spoke with my natural voice:"
    "Y, tratando con mi voz más natural."

# game/dialogs.rpy:2332
translate spanish secretnanami_588f721e:

    # hero "Oh yeah, Nanami and I started going out together."
    hero "Oh si, Nanami y yo estamos saliendo juntos desde ayer."

# game/dialogs.rpy:2333
translate spanish secretnanami_83c319f1:

    # hero "We didn't tell you?"
    hero "¿No les dijimos?"

# game/dialogs.rpy:2345
translate spanish secretnanami_e9fe3d46:

    # s "{size=+25}EEEEEEEEHHHHHHHHH???!!!!!{/size}"
    s "{size=+25}¡¡¡¡¡¿¿¿EEEEEEEEHHHHHHHHH???!!!!!{/size}"

# game/dialogs.rpy:2347
translate spanish secretnanami_e1326833:

    # r "{size=+25}I KNEW IT!!!!!{/size}"
    r "{size=+25}¡¡¡¡LO SABÍA!!!!!{/size}"

# game/dialogs.rpy:2348
translate spanish secretnanami_e9aabc9d:

    # "Another burst of laughter occurred between Nanami and I."
    "Hubo otra ronda de risas entre Nanami y yo."

# game/dialogs.rpy:2353
translate spanish secretnanami_597fe66a:

    # "After lunch, I found Sakura speaking with Rika alone."
    "Luego del almuerzo, encontré a Sakura hablando con Rika sola."

# game/dialogs.rpy:2354
translate spanish secretnanami_d72a5ad0:

    # "When Rika saw me, she beckoned me to come over."
    "Cuando Rika me miró, me hizo señas para que viniera."

# game/dialogs.rpy:2358
translate spanish secretnanami_eec796bb:

    # hero "What's going on?"
    hero "¿Qué esta pasando?"

# game/dialogs.rpy:2359
translate spanish secretnanami_7f82104e:

    # r "It's time. Nanami is waiting for us at the club room."
    r "Es la hora. Nanami espera por nosotros en el salón de club."

# game/dialogs.rpy:2360
translate spanish secretnanami_b6e3bb19:

    # r "Are you both ready?"
    r "¿Ambos están listos?"

# game/dialogs.rpy:2361
translate spanish secretnanami_326263c8:

    # hero "I am. As long as Sakura is."
    hero "Lo estoy. Mientras que Sakura-chan lo este."

# game/dialogs.rpy:2363
translate spanish secretnanami_feb04fe2:

    # s "I am... Let's go."
    s "Lo estoy... Vamos."

# game/dialogs.rpy:2369
translate spanish secretnanami_9df2c3c5:

    # n "Hey again, guys!"
    n "¡Hola otra vez, chicos!"

# game/dialogs.rpy:2373
translate spanish secretnanami_4ceec5a6:

    # r "Hey, Nanami."
    r "Oye, Nanami."

# game/dialogs.rpy:2374
translate spanish secretnanami_fe8acb83:

    # "I whispered \"Go on, don't worry!\" into Sakura's ear and she nodded."
    "Le susurré en la oreja de Sakura \"Sigue, ¡no te preocupes!\" y ella asintió."

# game/dialogs.rpy:2376
translate spanish secretnanami_10c08f00:

    # s "Nanami-chan..."
    s "Nanami-chan..."

# game/dialogs.rpy:2378
translate spanish secretnanami_ee407c47:

    # s "There's something I want to tell you."
    s "Hay algo que quiero contarte."

# game/dialogs.rpy:2380
translate spanish secretnanami_54f6b843:

    # n "What is it?...{p}It looks serious... {w}Are you guys okay?"
    n "¿Qué es?...{p}Parece serio... {w}¿Están bien chicos?"

# game/dialogs.rpy:2381
translate spanish secretnanami_1feba992:

    # hero "Yeah, no worries. It's just...something that you need to know..."
    hero "Si, no te preocupes. Es solo... Algo que necesitas saber..."

# game/dialogs.rpy:2382
translate spanish secretnanami_609aaa6d:

    # s "Since Rika-chan and %(stringhero)s-kun are aware of it, I thought it would be unfair for you to not know..."
    s "Puesto que Rika-chan y %(stringhero)s-kun estan conscientes de ello, pensé que sería injusto para ti no saberlo..."

# game/dialogs.rpy:2385
translate spanish secretnanami_827414d5:

    # n "Hey hey, that's okay! Hiding things can be fun sometimes! Remember the lunch?"
    n "Oye oye, ¡eso esta bien! ¡Esconder cosas puede ser divertido a veces! ¿Recuerdas el almuerzo?"

# game/dialogs.rpy:2386
translate spanish secretnanami_1ac65a8e:

    # s "It's not really the same thing, Nana-chan..."
    s "No es realmente la misma cosa, Nana-chan..."

# game/dialogs.rpy:2388
translate spanish secretnanami_73096c17:

    # n "Ah?..."
    n "¿Ah?..."

# game/dialogs.rpy:2389
translate spanish secretnanami_790191ec:

    # s "It's always hard for me to find the right words so it doesn't shock you too much..."
    s "Siempre es difícil para mí encontrar las palabras correctas para que ello no te conmocioné demasiado..."

# game/dialogs.rpy:2390
translate spanish secretnanami_5667ce78:

    # n "It's something that big?"
    n "¿Es algo así de grande?"

# game/dialogs.rpy:2391
translate spanish secretnanami_22a96dc3:

    # "Sakura nodded."
    "Sakura asintió."

# game/dialogs.rpy:2392
translate spanish secretnanami_f8f4fa69:

    # s "And before you ask..."
    s "Y como avance, antes de que preguntes..."

# game/dialogs.rpy:2393
translate spanish secretnanami_6d099b03:

    # s "I'm so sorry I didn't tell you earlier. I didn't know how to tell you."
    s "Lo siento mucho por no habértelo dicho antes. No sabía cómo decirte esto, con las palabras correctas."

# game/dialogs.rpy:2394
translate spanish secretnanami_2a2228a7:

    # s "Before I tell you, can you forgive me for this?"
    s "Antes de decirtelo, ¿puedes perdonarme por esto?"

# game/dialogs.rpy:2396
translate spanish secretnanami_564d9e25:

    # "Nanami smiled softly."
    "Nanami sonrió suavemente."

# game/dialogs.rpy:2397
translate spanish secretnanami_67350be4:

    # n "Of course I can. You're my Sakura-nee!"
    n "Por su puesto que puedo. ¡Tú eres mi Sakura-nee!"

# game/dialogs.rpy:2398
translate spanish secretnanami_c66e8457:

    # "Sakura nodded again with a faint smile. Then, after a moment, she started to reveal her secret."
    "Sakura asintió de nuevo con una leve sonrisa. Entonces, después de un momento, ella comenzó a revelar su secreto."

# game/dialogs.rpy:2399
translate spanish secretnanami_727b9f43:

    # s "Nana-chan..."
    s "Nana-chan..."

# game/dialogs.rpy:2400
translate spanish secretnanami_5b84e198:

    # s "I am...{w}not...{w}technically a girl..."
    s "Yo...{w}No...{w}Soy tecnicamente una chica..."

# game/dialogs.rpy:2402
translate spanish secretnanami_33d36d5f:

    # n "Huh?"
    n "¿Ah?"

# game/dialogs.rpy:2403
translate spanish secretnanami_e8a331c8:

    # s "I..."
    s "Yo..."

# game/dialogs.rpy:2404
translate spanish secretnanami_ce4432b4:

    # "Rika came closer and held Sakura's hand to give her strength. Rika helped Sakura gather herself."
    "Rika se acercó y sostuvo la mano de Sakura para darle más fuerza. Entonces ella le ayudó en encontrar las palabras correctas."

# game/dialogs.rpy:2405
translate spanish secretnanami_d8383a12:

    # r "Sakura...{w}wasn't born as a girl, like you and me."
    r "Sakura...{w}No nació como una chica, como tú o yo."

# game/dialogs.rpy:2406
translate spanish secretnanami_80777581:

    # "I placed my hand on Sakura's shoulder. I helped Rika speak on behalf of Sakura."
    "Colocando mi mano en el hombro de Sakura, terminé la oración de Rika."

# game/dialogs.rpy:2407
translate spanish secretnanami_fd25aa6e:

    # hero "Yeah...She's a girl on the inside, but originally, she was born as a boy. Like me."
    hero "Si... Ella es una chica por dentro, pero originalmente, ella nació como un chico. Como yo."

# game/dialogs.rpy:2409
translate spanish secretnanami_77609d5b:

    # "Nanami didn't say a word. She was trying to process the information. Her face displayed confusion."
    "Nanami no dijo una palabra por un momento, tratando de asimilar la información. Su rostro estaba mostrando sorpresa."

# game/dialogs.rpy:2410
translate spanish secretnanami_8961280a:

    # "Sakura gathered herself. She started to speak about her childhood, almost repeating word by word what she told me."
    "Revelado con nuestra ayuda, Sakura comenzó a hablar sobre su niñez, casi repetiendo palabra por palabra lo que ella me contó hace dos días."

# game/dialogs.rpy:2412
translate spanish secretnanami_d7384a8d:

    # "Nanami listened silently, nodding from time to time.{p}After Sakura finished, Nanami had similar questions to mine at time."
    "Nanami eschuchó silenciosamente, asintiendo de vez en cuando.{p}Después de que Sakura terminó, Nanami tuvo algunas de las mismas preguntas que yo tenía."

# game/dialogs.rpy:2413
translate spanish secretnanami_e145542f:

    # n "I understand now..."
    n "Comprendo ahora..."

# game/dialogs.rpy:2415
translate spanish secretnanami_8d8cc9b7:

    # "Then Nanami grew a cute smile."
    "Entonces Nanami mostró una linda sonrisa."

# game/dialogs.rpy:2416
translate spanish secretnanami_5399f645:

    # n "That's okay for me!"
    n "¡Eso esta bien por mí!"

# game/dialogs.rpy:2420
translate spanish secretnanami_e160708b:

    # n "I'm happy that you told me your secret!"
    n "¡Estoy feliz de que me contaras tu secreto!"

# game/dialogs.rpy:2421
translate spanish secretnanami_93b2b022:

    # n "I know I'm special to you, Sakura-nee, but now I feel it more than ever!"
    n "Sé que soy especial para ti, Sakura-nee; ¡pero ahora siento que lo soy más que nunca!"

# game/dialogs.rpy:2422
translate spanish secretnanami_bfeb43f4:

    # "Sakura smiled, some tears coming from her eyes."
    "Sakura sonrió, algunas lágrimas salieron de sus ojos."

# game/dialogs.rpy:2425
translate spanish secretnanami_957c3f61:

    # "Nanami jumped on Sakura and hugged her tightly with a bright smile and some tears."
    "Nanami salto hacia Sakura y la abrazó fuertemente con una brillante sonrisa y algunas lágrimas."

# game/dialogs.rpy:2426
translate spanish secretnanami_7c8449b8:

    # "That was touching."
    "Eso fue conmovedor."

# game/dialogs.rpy:2427
translate spanish secretnanami_106608f8:

    # n "I understand why it was so hard to tell me that.{p}I'm sure it's not easy to tell a secret like that!"
    n "Entiendo porque fue tan difícil decirme eso.{p}¡Esta claro que no suena fácil contar un secreto como ese!"

# game/dialogs.rpy:2428
translate spanish secretnanami_df955372:

    # "Sakura chuckles."
    "Sakura se ríe un poco."

# game/dialogs.rpy:2429
translate spanish secretnanami_3c734a09:

    # s "It sure isn't!"
    s "¡Esta claro que no!"

# game/dialogs.rpy:2430
translate spanish secretnanami_120f3ff5:

    # s "But now I'm the happiest girl ever..."
    s "Pero ahora soy la chica más feliz..."

# game/dialogs.rpy:2431
translate spanish secretnanami_ba0e9538:

    # s "Because, all the people I love and care about know my secret now."
    s "Porque, ahora, todas las personas que amo y me importan conocen mi secreto."

# game/dialogs.rpy:2432
translate spanish secretnanami_1abc70b9:

    # s "I don't need to hide anything to any of you now."
    s "Ahora ya no necesito esconder nada a ninguno de ustedes."

# game/dialogs.rpy:2433
translate spanish secretnanami_128578da:

    # n "Hiding what?"
    n "¿Esconder qué?"

# game/dialogs.rpy:2434
translate spanish secretnanami_77ee7366:

    # s "Huh?"
    s "¿Ah?"

# game/dialogs.rpy:2435
translate spanish secretnanami_ce51de81:

    # n "You're a girl, Sakura-nee."
    n "Eres una chica, Sakura-nee."

# game/dialogs.rpy:2436
translate spanish secretnanami_20f09b8b:

    # n "You're born as a boy? That's a small detail."
    n "¿Naciste como un chico? Eso es un pequeño detalle."

# game/dialogs.rpy:2437
translate spanish secretnanami_1ca8316e:

    # n "To me, you'll always be my Sakura-nee!"
    n "¡Para mí, tú siempre serás mi Sakura-nee!"

# game/dialogs.rpy:2438
translate spanish secretnanami_e130ea9c:

    # "Sakura kissed Nanami's head and rubbed her hair like for a child."
    "Sakura besó la cabeza de Nanami y acarició su cabello como si fuera una niña."

# game/dialogs.rpy:2439
translate spanish secretnanami_b45c3328:

    # s "Thank you so much... My Nana-chan...{p}My little one..."
    s "Muchas gracias... Mi Nana-chan...{p}Mi pequeña..."

# game/dialogs.rpy:2446
translate spanish secretnanami_9e1d86cb:

    # n "I'm not little!..."
    n "¡No soy pequeña!..."

# game/dialogs.rpy:2471
translate spanish scene_R_13535866:

    # centered "{size=+35}CHAPTER 5\nThe festival{fast}{/size}"
    centered "{size=+35}CÁPITULO 5\nEl festival{fast}{/size}"

# game/dialogs.rpy:2475
translate spanish scene_R_31591cda:

    # "The week went by quickly..."
    "La semana pasó..."

# game/dialogs.rpy:2478
translate spanish scene_R_57141cf2:

    # "Friday evening came. The day before the festival..."
    "Entonces, vino la noche del Viernes. El día antes del festival..."

# game/dialogs.rpy:2481
translate spanish scene_R_1b27c60b:

    # "Preparations for the festival were awfully hard. But I promised Rika I'd help her out."
    "Tuvimos que preparar los puestos y las decoraciones, y, como lo prometí a Rika, la ayudé."

# game/dialogs.rpy:2482
translate spanish scene_R_2d950149:

    # "We had to prepare the stands and the decorations."
    "Fue terriblemente duro."

# game/dialogs.rpy:2483
translate spanish scene_R_9be7bc2a:

    # "Rika came to tell what to do and where to go."
    "Rika estaba solo aquí para decirme dónde debería poner todas las cosas y así."

# game/dialogs.rpy:2484
translate spanish scene_R_b0c123df:

    # hero "Hey, maybe could you give us a hand!?"
    hero "¿¡Oye, podrías darnos una mano!?"

# game/dialogs.rpy:2485
translate spanish scene_R_8a0b645d:

    # r "I can't, I'm guiding you, idiot!"
    r "¡No puedo, te estoy guiando, idiota!"

# game/dialogs.rpy:2486
translate spanish scene_R_139a795e:

    # "We really don't have the same understanding of the word \"help.\" "
    "¡Realmente no tenemos el mismo concepto de la palabra \"ayudar\"!..."

# game/dialogs.rpy:2488
translate spanish scene_R_08547004:

    # "I had to move some boxes inside the temple. I was alone."
    "Tuve que conseguir algunas cosas dentro del templo. Estaba solo."

# game/dialogs.rpy:2489
translate spanish scene_R_166ebec1:

    # "The boxes were inside a big dark room with a lot of old junk."
    "Ellos estaban dentro de un gran y oscuro cuarto con un montón de chatarra vieja."

# game/dialogs.rpy:2490
translate spanish scene_R_9899d4dc:

    # "It's so dark. I can't see a thing..."
    "Tan oscuro que no podía ver nada..."

# game/dialogs.rpy:2491
translate spanish scene_R_7f278c6a:

    # hero "Rika-chan? Are you in there?"
    hero "¿Rika-chan? ¿Estás ahí?"

# game/dialogs.rpy:2492
translate spanish scene_R_a5dde9ae:

    # r "Yes, come over here!"
    r "¡Si, ven por aquí!"

# game/dialogs.rpy:2493
translate spanish scene_R_cd339c0e:

    # "I follow the direction of her voice. But I suddenly stumbled onto something and I fell on the floor."
    "Seguí la dirección de su voz. Pero de pronto me tropecé con algo y caí al piso."

# game/dialogs.rpy:2494
translate spanish scene_R_4aa7bb8a:

    # "Well, not exactly the floor. I fell on something warm and comfy."
    "Bueno, no exactamente al piso. Caí en algo cálido y comfortable."

# game/dialogs.rpy:2495
translate spanish scene_R_e0fa74ca:

    # hero "Ah, I fell on something..."
    hero "Ah, caí en algo..."

# game/dialogs.rpy:2496
translate spanish scene_R_4642c748:

    # "My hand squeezed the mellow thing I was on."
    "Mi mano apretó la suave cosa que estába agarrando.."

# game/dialogs.rpy:2504
translate spanish scene_R_02b7fdcb:

    # hero "What is this... I don't even-"
    hero "¿Qué es esto?.. No puedo ni-"

# game/dialogs.rpy:2505
translate spanish scene_R_d557eb58:

    # r "Y-You {size=+20}IDIOT!!!!!!{/size}"
    r "T-Tú {size=+20}¡¡¡¡¡IDIOTA!!!!!!{/size}"

# game/dialogs.rpy:2508
translate spanish scene_R_73674ca2:

    # "*{i}SMACK!{/i}*"
    "*{i}¡BOFETADA!{/i}*"

# game/dialogs.rpy:2509
translate spanish scene_R_c81c00b8:

    # "Something hit my cheek and I instantly got up by instinct..."
    "Algo golpeó mi mejilla e instantáneamente me levanté por instinto..."

# game/dialogs.rpy:2510
translate spanish scene_R_ece959c9:

    # "I realized what I fell on. Or who I fell on..."
    "Ahora me doy cuenta en qué es lo que caí. O debería decir \"en quien\"..."

# game/dialogs.rpy:2511
translate spanish scene_R_b70add4f:

    # r "You PERVERT!!! Stop touching me!!!"
    r "¡¡¡Tú PERVERTIDO!!! ¡¡¡Deja de tocarme!!!"

# game/dialogs.rpy:2512
translate spanish scene_R_5462e3bc:

    # hero "I stopped! I stopped!"
    hero "¡Ya me detuve! ¡Ya me detuve!"

# game/dialogs.rpy:2513
translate spanish scene_R_5c272729:

    # hero "I'm sorry, it was an accident!"
    hero "¡Perdón, fue un accidente!"

# game/dialogs.rpy:2514
translate spanish scene_R_bd0bcebc:

    # r "Turn the freakin light on and help me lift this box up there!"
    r "¡Enciende la luz y ayúdame a levantar esa gran cosa de ahí!"

# game/dialogs.rpy:2516
translate spanish scene_R_268b921a:

    # "Damn! This girl is nuts!!!"
    "¡Maldición! ¡¡¡Esa chica es realmente insoportable!!!"

# game/dialogs.rpy:2517
translate spanish scene_R_a20cefa7:

    # "..."
    "..."

# game/dialogs.rpy:2518
translate spanish scene_R_4d9f847b:

    # "Although her breast felt pretty nice...{p}It was warm... and mellow... and big..."
    "Pienso que sus pechos se sintieron muy bien...{p}Cálidos... Suaves... Grandes..."

# game/dialogs.rpy:2519
translate spanish scene_R_45a00c24:

    # "Eh!? What the hell am I thinking about!?"
    "¡¡¡Oye, en qué diablos estoy pensando!!!"

# game/dialogs.rpy:2520
translate spanish scene_R_62d361bd:

    # "Rika is definitely not my type of girl. Not at all!"
    "¡Esa chica no es mi tipo!"

# game/dialogs.rpy:2522
translate spanish scene_R_9f1c3606:

    # "Plus I'm already with Sakura-chan!"
    "¡Además ya estoy con Sakura-chan!"

# game/dialogs.rpy:2524
translate spanish scene_R_076bdc09:

    # "Plus I'm already with Nanami-chan!"
    "¡Además ya estoy con Nanami-chan!"

# game/dialogs.rpy:2526
translate spanish scene_R_a20cefa7_1:

    # "..."
    "..."

# game/dialogs.rpy:2532
translate spanish decision_31826e00:

    # "The day of the matsuri arrived."
    "Entonces el día del matsuri llegó."

# game/dialogs.rpy:2533
translate spanish decision_400d0a26:

    # "As the club planned, I will go with Sakura-chan and Rika-chan. And maybe Nanami-chan if she's motivated."
    "Como lo planeó el club, voy a ir con Sakura-chan y Rika-chan. Y tal vez Nanami-chan si ella está motivada."

# game/dialogs.rpy:2534
translate spanish decision_07ca0e28:

    # "I can't wait to see them!"
    "¡No puedo esperar a verlas!"

# game/dialogs.rpy:2545
translate spanish matsuri_S_2ef9ea22:

    # "I was waiting for Sakura in front of her house."
    "Estaba esperando a Sakura enfrente de su casa."

# game/dialogs.rpy:2547
translate spanish matsuri_S_1dcaa12e:

    # "I was wearing my brand new yukata that she gave me."
    "Estaba usando mi nueva yukata que ella me dio."

# game/dialogs.rpy:2549
translate spanish matsuri_S_201b46ce:

    # "I was wearing the yukata I recently bought... One which was awfully expensive..."
    "Estaba usando el yukata que recientemente compré... Uno el cual fue terriblemente caro..."

# game/dialogs.rpy:2550
translate spanish matsuri_S_9c64454a:

    # "She finally appeared, in a wonderful pink and red kimono."
    "Ella apareció finalmente, en un maravilloso kimono rosa y rojo."

# game/dialogs.rpy:2551
translate spanish matsuri_S_c633905a:

    # "She looked like a miko priestess. She was pretty..."
    "Ella se miraba como una sacerdotisa miko. Estaba bonita..."

# game/dialogs.rpy:2553
translate spanish matsuri_S_678a9d48:

    # hero "Wow... Sakura-chan... You look superb!"
    hero "Wow... Sakura-chan... ¡Te ves magnífica!"

# game/dialogs.rpy:2554
translate spanish matsuri_S_4eeac269:

    # s "%(stringhero)s-kun! The yukata suits you very nicely!"
    s "¡%(stringhero)s-kun! ¡La yukata te queda muy bien!"

# game/dialogs.rpy:2556
translate spanish matsuri_S_02c86c6c:

    # "She comes closer and whispers in my ear with a grin."
    "Ella se acercó y me susurra en mi oreja con una sonrisa."

# game/dialogs.rpy:2557
translate spanish matsuri_S_9e55e999:

    # s "I wonder what's hidden inside..."
    s "Me pregunto qué es lo que esconde dentro..."

# game/dialogs.rpy:2558
translate spanish matsuri_S_e44df551:

    # hero "Hehe maybe you'll see later..."
    hero "Jeje tal vez lo verás luego..."

# game/dialogs.rpy:2560
translate spanish matsuri_S_96913011:

    # "I saw a woman come out of the house. That must be Sakura's mother."
    "Vi a su madre salir de la casa."

# game/dialogs.rpy:2561
translate spanish matsuri_S_af9ec1db:

    # "She looks incredibly young for a mother of an 18-year-old. She looks like she's in her early 30s at least!"
    "Ella era increíblemente joven para una madre con una hija de 18 años de edad. ¡Ella parecía tener 35 o algo así!"

# game/dialogs.rpy:2565
translate spanish matsuri_S_6d0cbd15:

    # smom "Oh, you must be %(stringhero)s-san, I presume!"
    smom "¡Oh, tú eres %(stringhero)s-san, supongo!"

# game/dialogs.rpy:2566
translate spanish matsuri_S_fde87b49:

    # smom "My daughter never stops talking about you!"
    smom "¡Mi hija no para de hablar sobre ti!"

# game/dialogs.rpy:2568
translate spanish matsuri_S_273b854b:

    # "Sakura looked embarrassed and blushed."
    "Sakura se veía avergonzada y sonrojada."

# game/dialogs.rpy:2569
translate spanish matsuri_S_2bf9e723:

    # s "Mom!!!"
    s "¡¡¡Mamá!!!"

# game/dialogs.rpy:2570
translate spanish matsuri_S_b8980021:

    # hero "From the yukata you're wearing, I assume that you'll come to the matsuri too, ma'am?"
    hero "Por el yukata que está usando, asumo que vendrá al matsuri también, ¿cierto?"

# game/dialogs.rpy:2572
translate spanish matsuri_S_ed2ca74b:

    # smom "Yes. Once my husband is ready, we'll go as well."
    smom "Si. Una vez que mi esposo esté listo, nosotros iremos también."

# game/dialogs.rpy:2573
translate spanish matsuri_S_5fc7645d:

    # s "We'll be going on ahead, mom. See you later!"
    s "Nosotros iremos primero, mamá. ¡Nos vemos luego!"

# game/dialogs.rpy:2574
translate spanish matsuri_S_0c6a0f45:

    # smom "Have fun, you two!"
    smom "¡Diviértanse, ustedes dos!"

# game/dialogs.rpy:2578
translate spanish matsuri_S_be3185f9:

    # "We were about to leave but then, Sakura's mother called me."
    "Estábamos a punto de irnos pero entonces, la madre de Sakura me llamó."

# game/dialogs.rpy:2580
translate spanish matsuri_S_6c045540:

    # "She spoke quietly."
    "Ella habló con una voz discreta."

# game/dialogs.rpy:2581
translate spanish matsuri_S_8c173d23:

    # smom "I know that you and my daughter are going out."
    smom "Sé que tú y mi hija son amantes."

# game/dialogs.rpy:2582
translate spanish matsuri_S_c0582308:

    # "Yikes!"
    "¡Yikes!"

# game/dialogs.rpy:2583
translate spanish matsuri_S_a5ebdd09:

    # smom "Sakura told me how much you love each other."
    smom "Sakura me cuenta cuánto se aman el uno al otro."

# game/dialogs.rpy:2584
translate spanish matsuri_S_043f4b2a:

    # smom "I'm so happy that my daughter found someone so kind and so understanding."
    smom "Estoy tan feliz de que mi hija encontrara a alguien tan amable y comprensible."

# game/dialogs.rpy:2585
translate spanish matsuri_S_47abc600:

    # hero "I-it's fine, ma'am... I...{p}I do love Sakura-chan. More than anything... No matter how she was born."
    hero "E-esta bien, señora... Yo...{p}Yo amo a Sakura-chan. Más que a nada... No importando cómo haya nacido."

# game/dialogs.rpy:2586
translate spanish matsuri_S_8a914f1f:

    # smom "That's sweet."
    smom "Eso es tierno."

# game/dialogs.rpy:2587
translate spanish matsuri_S_9981c331:

    # smom "Can I count on you to make her the happiest woman she's always wished to be?"
    smom "Cuento contigo para hacer la chica y mujer feliz que ella siempre deseó ser."

# game/dialogs.rpy:2588
translate spanish matsuri_S_707d7f10:

    # hero "I promise I will, ma'am!"
    hero "¡Prometo que lo haré, señora!"

# game/dialogs.rpy:2590
translate spanish matsuri_S_c944f29d:

    # "She smiled and I went back to Sakura. We waved goodbye to Sakura's mom and made our way to the festival."
    "Entonces nos despedimos de la mamá de Sakura mientras que íbamos camino al festival."

# game/dialogs.rpy:2591
translate spanish matsuri_S_463b86c5:

    # "Loud noises and music were becoming audible as we got closer to the festival."
    "Ya había ruido y música viniendo de ahí."

# game/dialogs.rpy:2596
translate spanish matsuri_S_9ab29675:

    # s "What were you talking about with mom?"
    s "¿Qué fue de lo que hablaste con mi mamá?"

# game/dialogs.rpy:2597
translate spanish matsuri_S_b96431ab:

    # hero "Umm... Nothing much..."
    hero "Emm... Nada en especial..."

# game/dialogs.rpy:2598
translate spanish matsuri_S_3edbe663:

    # hero "By the way, your mom looks incredibly young."
    hero "¡Tu mamá luce increíblemente joven, por cierto!"

# game/dialogs.rpy:2599
translate spanish matsuri_S_258d2b74:

    # s "Yeah, she gave birth to me when she was only 16 years old."
    s "Si. Ella me dio luz cuando ella tenía 16."

# game/dialogs.rpy:2600
translate spanish matsuri_S_bac1bc61:

    # s "My father was her Japanese teacher."
    s "Mi padre era su maestro de japonés."

# game/dialogs.rpy:2601
translate spanish matsuri_S_d3f7f082:

    # hero "Haha, sounds like a manga cliché!"
    hero "Ja. ¡El usual cliché de los mangas!"

# game/dialogs.rpy:2602
translate spanish matsuri_S_8dbb02af:

    # s "Teehee! Yes it is!"
    s "¡Teehee! ¡Si lo es!"

# game/dialogs.rpy:2604
translate spanish matsuri_S_f72540d4:

    # "She smiled for a while, but then it grew thin."
    "Ella sonrió por un momento, pero luego desapareció."

# game/dialogs.rpy:2605
translate spanish matsuri_S_e1554201:

    # s "..."
    s "..."

# game/dialogs.rpy:2606
translate spanish matsuri_S_c41191e9:

    # s "I don't like my father very much..."
    s "No me agrada mucho mi padre..."

# game/dialogs.rpy:2607
translate spanish matsuri_S_b2052a3c:

    # hero "Yeah, I remember why, I think. Because he doesn't understand you, right?"
    hero "Si, creo que recuerdo el porqué. Es porque no te entiende, ¿verdad?"

# game/dialogs.rpy:2608
translate spanish matsuri_S_db0a0f02:

    # s "Sometimes he scares me..."
    s "Algunas veces me asusta..."

# game/dialogs.rpy:2609
translate spanish matsuri_S_9465293c:

    # s "Recently he..."
    s "Recientemente él..."

# game/dialogs.rpy:2610
translate spanish matsuri_S_557b9c7c:

    # s ". . ."
    s ". . ."

# game/dialogs.rpy:2611
translate spanish matsuri_S_2639bac0:

    # s "He said he would never accept me as his daughter..."
    s "Él dijo que nunca me aceptaría como su hija..."

# game/dialogs.rpy:2612
translate spanish matsuri_S_337c15c1:

    # "I didn't know what to say."
    "Asentí."

# game/dialogs.rpy:2613
translate spanish matsuri_S_e2c7c8ae:

    # "Her father seems to be confused with the case of his child."
    "Su padre se ve que está confundido con el caso de su hija."

# game/dialogs.rpy:2614
translate spanish matsuri_S_0c7d97ed:

    # s "In fact, he even admitted that he has always preferred having a real boy,"
    s "De hecho, él incluso admitió que simpre prefirió haber tenido un niño real."

# game/dialogs.rpy:2615
translate spanish matsuri_S_9f13d1a6:

    # s "But I'm a boy with everything girly. And it makes him angry..."
    s "Pero soy un chico con todo femenino. Y eso hace que él se enoje..."

# game/dialogs.rpy:2616
translate spanish matsuri_S_66341891:

    # "I nodded again."
    "Asentí otra vez."

# game/dialogs.rpy:2617
translate spanish matsuri_S_cd99f638:

    # hero "And how does your mom feel, about everything?"
    hero "¿Y cómo se siente tu mamá sobre todo esto?"

# game/dialogs.rpy:2619
translate spanish matsuri_S_257a9f41:

    # s "She preferred a girl at the time, but she doesn't care at all, now."
    s "Ella prefirió una chica en su tiempo, pero ahora ya no le importa para nada."

# game/dialogs.rpy:2620
translate spanish matsuri_S_53c7acc4:

    # s "No matter if I'm a boy or a girl, I'm her child first of all..."
    s "No importando si soy un chico o una chica, soy su hija primero que nada..."

# game/dialogs.rpy:2621
translate spanish matsuri_S_08457630:

    # s "She understands me and she takes me for who I am."
    s "Ella me entiende y acepta por quien soy."

# game/dialogs.rpy:2622
translate spanish matsuri_S_05bae488:

    # "I guessed that easily after the chat I had with her earlier."
    "Eso lo supuse fácilmente después de la charla que tuve con ella anteriormente."

# game/dialogs.rpy:2623
translate spanish matsuri_S_636c5687:

    # hero "That's a good thing."
    hero "Eso es bueno."

# game/dialogs.rpy:2624
translate spanish matsuri_S_9600023e:

    # hero "If only your dad saw things like your mom does..."
    hero "Si solo tu papá viera las cosas como lo ve tu mamá..."

# game/dialogs.rpy:2625
translate spanish matsuri_S_88285ade:

    # s "Right."
    s "Cierto."

# game/dialogs.rpy:2626
translate spanish matsuri_S_c43195f9:

    # hero "You can count on your mother if something bad happens..."
    hero "Puedes contar con tu madre si algo malo pasa..."

# game/dialogs.rpy:2627
translate spanish matsuri_S_36e0944d:

    # hero "And you know..."
    hero "Y sabes..."

# game/dialogs.rpy:2628
translate spanish matsuri_S_5303d699:

    # hero "You can count on Rika-chan, Nanami-chan, and me if you need."
    hero "También puedes contar con Rika-chan y conmigo si lo necesitas. Y con Nanami-chan también."

# game/dialogs.rpy:2630
translate spanish matsuri_S_1b982e5f:

    # "Sakura smiled and held my arm tightly."
    "Sakura sonrió y sujetó mi brazo con fuerza."

# game/dialogs.rpy:2631
translate spanish matsuri_S_2775e546:

    # s "I know, %(stringhero)s-kun..."
    s "Lo sé, %(stringhero)s-kun..."

# game/dialogs.rpy:2636
translate spanish matsuri_S_8fa16578:

    # "We arrived at the festival and were joined by Rika-chan."
    "Llegamos al festival y nos reunimos con Rika."

# game/dialogs.rpy:2637
translate spanish matsuri_S_42b57c94:

    # "She was wearing a very pretty purple yukata that showed her shoulders outside."
    "Ella estaba usando una yukata púrpura muy bonita que exhibía sus hombros "

# game/dialogs.rpy:2639
translate spanish matsuri_S_856a9a31:

    # r "Hey hey hey, you lovers!!!"
    r "¡¡¡Hola hola hola, amantes!!!"

# game/dialogs.rpy:2641
translate spanish matsuri_S_aa24547d:

    # s "Rika-chan!"
    s "¡Rika-chan!"

# game/dialogs.rpy:2642
translate spanish matsuri_S_70def7d1:

    # hero "Rika-chan! Your yukata is great!!"
    hero "¡Rika-chan! ¡Tu yukata es grandioso!"

# game/dialogs.rpy:2643
translate spanish matsuri_S_1c3fbf24:

    # r "Pfft, I know what you really mean, you pervert! {image=heart.png}{p}How are you guys?"
    r "Sé lo que quieres decir, pervertido! {image=heart.png}{p}¿Cómo están ustedes?"

# game/dialogs.rpy:2645
translate spanish matsuri_S_b140b475:

    # r "My, my! Looks like our city rat has himself a nice yukata!"
    r "¡Oh! ¡Parece que nuestra rata de la ciudad ha obtenido una buena yukata!"

# game/dialogs.rpy:2646
translate spanish matsuri_S_8051a326:

    # hero "Thanks, Rika-chan- {p}Hey wait! I'm no city rat!!!"
    hero "Gracias, Rika-chan... {p}¡Oye espera! ¡¡¡No soy una rata de la ciudad!!!"

# game/dialogs.rpy:2648
translate spanish matsuri_S_9cb75162:

    # hero "By the way, where is Nanami-chan?"
    hero "Por cierto, ¿dónde está Nanami-chan?"

# game/dialogs.rpy:2649
translate spanish matsuri_S_6683fd53:

    # r "Knowing her, she probably went off to playing the shooting games."
    r "Conociéndola, ella probablemente esta jugando a los juegos de disparos."

# game/dialogs.rpy:2650
translate spanish matsuri_S_0fef1f71:

    # hero "Shooting games?"
    hero "¿Juegos de disparos?"

# game/dialogs.rpy:2651
translate spanish matsuri_S_8644460f:

    # hero "Let's go join her and play too!"
    hero "¿Porqué no nos reunimos con ella ahí y jugamos también?"

# game/dialogs.rpy:2653
translate spanish matsuri_S_5a861c69:

    # s "Oh I would love to!!!"
    s "¡¡¡Oh me encantaría eso!!!"

# game/dialogs.rpy:2654
translate spanish matsuri_S_e0fe552c:

    # r "Beware, %(stringhero)s, Sakura is the best shooter in the whole village!"
    r "Cuidado, %(stringhero)s, ¡Sakura es la mejor tiradora de todo el pueblo! "

# game/dialogs.rpy:2655
translate spanish matsuri_S_64c71cc7:

    # hero "We'll see about that!"
    hero "¡Vamos a ver si ella puede vencerme!"

# game/dialogs.rpy:2656
translate spanish matsuri_S_acc4c1f3:

    # "So we walk to a shooting stand..."
    "Entonces caminamos hacia el puesto de disparos..."

# game/dialogs.rpy:2660
translate spanish matsuri_S_1978a1c0:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2661
translate spanish matsuri_S_449ff6ae:

    # "Sakura made three perfect shots in a row on a big plushie of {i}ByeBye-Neko{/i} which fell on its back."
    "Sakura hizo 3 tiros perfectos en una ronda sobre un gran peluche de {i}ByeBye-Neko{/i} el cual cayó hacia atrás."

# game/dialogs.rpy:2662
translate spanish matsuri_S_5f5c92ba:

    # s "Yay! I love plushies!!!"
    s "¡Yay! ¡¡¡Amo los peluches!!!"

# game/dialogs.rpy:2663
translate spanish matsuri_S_d15d8e67:

    # n "You're great at this, Sakura-nee!"
    n "¡Tú si que eres buena en esto, Sakura-nee!"

# game/dialogs.rpy:2664
translate spanish matsuri_S_db10b573:

    # n "It doesn't feel like it does in the arcade."
    n "Es seguro que no se siente como en el salón recreativo."

# game/dialogs.rpy:2665
translate spanish matsuri_S_b799ab6b:

    # "I smiled."
    "Sonreí."

# game/dialogs.rpy:2666
translate spanish matsuri_S_dd8906ec:

    # "I knew what to do..."
    "Sabía lo tenía que hacer..."

# game/dialogs.rpy:2667
translate spanish matsuri_S_0b28ef8e:

    # "I had to get another plushie for Sakura-chan!"
    "¡Tenía que conseguir otro peluche para Sakura-chan!"

# game/dialogs.rpy:2668
translate spanish matsuri_S_0d1e3f41:

    # hero "My turn to try!"
    hero "¡Mi turno para jugar!"

# game/dialogs.rpy:2669
translate spanish matsuri_S_997b966e:

    # "I gave ¥100 to the guy at the stand, took the gun and targeted a cute crocodile plushie that seemed easy to get."
    "Di ¥100 a la persona del puesto, tomé un arma y apunté a un lindo peluche de cocodrilo que se miraba fácil de obtener."

# game/dialogs.rpy:2670
translate spanish matsuri_S_1978a1c0_1:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2671
translate spanish matsuri_S_c8f69f16:

    # "Missed!"
    "¡Fallé!"

# game/dialogs.rpy:2672
translate spanish matsuri_S_924fcb79:

    # hero "Hold on, I'll try again!"
    hero "¡Espera, trataré de nuevo!"

# game/dialogs.rpy:2673
translate spanish matsuri_S_2ffa84ff:

    # "¥100 more on the table."
    "¥100 más en la mesa."

# game/dialogs.rpy:2674
translate spanish matsuri_S_1978a1c0_2:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2675
translate spanish matsuri_S_df4219e2:

    # "Darn!!! The plushie moved a bit but it wasn't enough for it to fall!"
    "¡¡¡Cielos!!! ¡El peluche se movió un poco pero no fue suficiente para hacerlo caer!"

# game/dialogs.rpy:2676
translate spanish matsuri_S_6cc32b6b:

    # "¥100 more!"
    "¡¥100 más!"

# game/dialogs.rpy:2677
translate spanish matsuri_S_1978a1c0_3:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2678
translate spanish matsuri_S_bea4f24c:

    # "I got it!!! I got the plushie!!!"
    "¡¡¡Lo conseguí !!! ¡¡¡Conseguí el peluche!!!"

# game/dialogs.rpy:2679
translate spanish matsuri_S_67dae2f0:

    # "I was about to give it to Sakura, but I remembered Rika and Nanami."
    "Estaba a punto de dárselo a Sakura cuando de pronto recordé a Rika y Nanami."

# game/dialogs.rpy:2680
translate spanish matsuri_S_ad68e441:

    # "Maybe I should get one for each of them too..."
    "Tal vez debería conseguir uno para ellas también..."

# game/dialogs.rpy:2681
translate spanish matsuri_S_52d0a5e1:

    # "As I was about to pay ¥100 more to the guy, Rika shoved herself through."
    "Pero cuando estaba por pagar ¥100 más al tipo, Rika vino."

# game/dialogs.rpy:2682
translate spanish matsuri_S_602dd045:

    # r "Hold it, city rat!!! It's my turn to play!!!"
    r "¡¡¡Detente, rata de la ciudad!!! ¡¡¡Es mi turno para jugar!!!"

# game/dialogs.rpy:2683
translate spanish matsuri_S_8834e601:

    # r "I'll get the same plushie with only three shots, you'll see!!!"
    r "Conseguiré el mismo peluche con solo 3 tiros, ¡¡¡ya lo verás!!!"

# game/dialogs.rpy:2684
translate spanish matsuri_S_2a606826:

    # "While Rika and Nanami were busy at the stand, I gave the plushie to Sakura."
    "Mientras Rika y Nanami estaban ocupadas en el puesto, le di el peluche a Sakura."

# game/dialogs.rpy:2685
translate spanish matsuri_S_2c879bcb:

    # s "For me?"
    s "¿Para mí?"

# game/dialogs.rpy:2686
translate spanish matsuri_S_c824434e:

    # s "Oh, it's so cute!!!"
    s "¡¡¡Oh, es tan lindo!!!"

# game/dialogs.rpy:2687
translate spanish matsuri_S_20fdfbfd:

    # s "Thank you, %(stringhero)s-kun!!"
    s "¡¡Gracias, %(stringhero)s-kun!!"

# game/dialogs.rpy:2688
translate spanish matsuri_S_f2aa2a51:

    # "She kissed me wildly in front of everybody, in a way that a man is more used to doing than a girl."
    "Ella me besó incontrolablemente enfrente de todos, en un modo que un hombre estaría más acostumbrado que una mujer."

# game/dialogs.rpy:2689
translate spanish matsuri_S_fcea0f28:

    # "I felt embarrassed and shy, but happy..."
    "Me sentí avergonzado y tímido, pero feliz..."

# game/dialogs.rpy:2690
translate spanish matsuri_S_15bd7f0a:

    # r "Ah!!! Missed again, darn!"
    r "¡¡¡Ah!!! ¡Fallé otra vez, maldición!"

# game/dialogs.rpy:2691
translate spanish matsuri_S_b3487258:

    # "Apparently, Rika-chan isn't very good at shooting games..."
    "Aparentemente, Rika-chan no es muy buena en juegos de disparos..."

# game/dialogs.rpy:2692
translate spanish matsuri_S_4ca1680a:

    # "But she finally got a plushie too, after a while..."
    "Pero ella al final consiguió el peluche también, después de un tiempo..."

# game/dialogs.rpy:2693
translate spanish matsuri_S_016f8a4d:

    # "I think it's because the guy was seduced by Rika-chan's sexy yukata..."
    "Pienso que es porque el tipo fue seducido por la sexi yukata de Rika-chan..."

# game/dialogs.rpy:2694
translate spanish matsuri_S_20e94235:

    # "Gah, seriously..."
    "Gah, en serio..."

# game/dialogs.rpy:2695
translate spanish matsuri_S_015dea9a:

    # hero "How about we get some food now? I feel hungry."
    hero "¿Qué tal algo de comer ahora? Me siento hambriento."

# game/dialogs.rpy:2696
translate spanish matsuri_S_80443115:

    # r "Excellent idea!!! Me too!!!"
    r "¡¡¡Excelente idea!!! ¡¡¡Yo también!!!"

# game/dialogs.rpy:2697
translate spanish matsuri_S_3e4fce92:

    # s "Me too, a little."
    s "Yo también, un poco."

# game/dialogs.rpy:2698
translate spanish matsuri_S_278f0f18:

    # r "Let's go! I'll show you a good stand where they make excellent takoyaki!"
    r "¡Vámonos! ¡Les mostraré un buen puesto donde hacen unas excelentes takoyakis!"

# game/dialogs.rpy:2703
translate spanish takoyakis_b486162f:

    # "Our meal was just excellent, as Rika-chan promised!"
    "¡Nuestra comida fue excelente, como Rika-chan prometió!"

# game/dialogs.rpy:2704
translate spanish takoyakis_b36de308:

    # hero "I'm full!"
    hero "¡Estoy lleno!"

# game/dialogs.rpy:2705
translate spanish takoyakis_c5430508:

    # r "Told ya it was good! This is the best place to find them!"
    r "¡Te dije que era bueno! ¡Estamos en el lugar donde fueron creados!"

# game/dialogs.rpy:2706
translate spanish takoyakis_f4e941cb:

    # r "I want more! Hey, more of these please!!!"
    r "¡Quero más! ¡¡¡Oye, más de esos por favor!!!"

# game/dialogs.rpy:2707
translate spanish takoyakis_b42e6c2a:

    # n "Can't... eat... more..."
    n "No puedo... Comer... Más..."

# game/dialogs.rpy:2708
translate spanish takoyakis_032debaa:

    # hero "Rika-chan, how can you eat so much of this stuff??? Do you have a black hole instead of a stomach or what?"
    hero "¿¿¿Rika-chan. cómo puedes comer tanto de esas cosas??? ¿Tienes un hoyo en vez de estómago o qué?"

# game/dialogs.rpy:2709
translate spanish takoyakis_dc489790:

    # s "Actually, Rika-chan can eat a mountain of food without adding on any pounds."
    s "En realidad, Rika-chan puede comer una montaña de comida sin subir ninguna libra."

# game/dialogs.rpy:2710
translate spanish takoyakis_11fca699:

    # hero "Really? She's lucky."
    hero "¿En serio? Es una suertuda."

# game/dialogs.rpy:2711
translate spanish takoyakis_70921009:

    # hero "I have to control my own weight to make sure I don't end up too fat..."
    hero "Debo controlar mi propio peso para estar seguro de no ser demasiado gordo..."

# game/dialogs.rpy:2712
translate spanish takoyakis_e6e2b7e9:

    # s "Teehee! Same for me!"
    s "¡Teehee! ¡igual para mí!"

# game/dialogs.rpy:2717
translate spanish takoyakis_b3e735ce:

    # "We finished the festival by watching the fireworks..."
    "Terminamos el festival mirando los fuegos artificiales..."

# game/dialogs.rpy:2718
translate spanish takoyakis_d988d3e5:

    # "The show was fabulous."
    "El espectáculo fue fabuloso."

# game/dialogs.rpy:2719
translate spanish takoyakis_7e1eaf4f:

    # "At a moment, Sakura and I kissed discreetly in the dark under the fireworks..."
    "En ese momento, Sakura y yo nos besamos discretamente en la oscuridad debajo de los fuegos artificiales..."

# game/dialogs.rpy:2722
translate spanish takoyakis_f06e1942:

    # "Sakura-chan and I were on the way back home."
    "Yo y Sakura-chan estábamos en nuestro camino regreso a casa."

# game/dialogs.rpy:2723
translate spanish takoyakis_5cc5084b:

    # "I'm completely overwhelmed of happiness because of the festival and that night with Sakura-chan."
    "Estoy completamente mareado de felicidad a causa del festival y esa noche con Sakura-chan."

# game/dialogs.rpy:2724
translate spanish takoyakis_6cf004ac:

    # "Without a second thought, I tried to act out a cliché from movies I've seen before."
    "Y por eso dije, sin miedo alguno, una frase que lo he escuchado en una película hace mucho tiempo."

# game/dialogs.rpy:2725
translate spanish takoyakis_60443369:

    # hero "Want to come back to my place for a drink?"
    hero "¿Quieres ir a mi casa para un último trago?"

# game/dialogs.rpy:2726
translate spanish takoyakis_52a57200:

    # s "Huh...?"
    s "¿Hmm...?"

# game/dialogs.rpy:2727
translate spanish takoyakis_676d485e:

    # "I suddenly noticed what I just said. I started to blush and spoke incoherently."
    "De pronto noté lo que acabo de decir. Comencé a sonrojarme y hablar incoherentemente."

# game/dialogs.rpy:2728
translate spanish takoyakis_8d758b57:

    # hero "I-I-I-I mean if... If you... Well I mean y-y--{nw}"
    hero "Y-y-y-yo quiero decir si... Si tú... Bueno quiero decir t-t--{nw}"

# game/dialogs.rpy:2729
translate spanish takoyakis_e043d770:

    # s "Okay."
    s "Si."

# game/dialogs.rpy:2730
translate spanish takoyakis_b16c33f4:

    # "I was surprised."
    "Estaba sorprendido."

# game/dialogs.rpy:2732
translate spanish takoyakis_dc6dab16:

    # "Sakura watched me with a tinge of shyness."
    "Sakura me miró con un matiz de timidez. "

# game/dialogs.rpy:2733
translate spanish takoyakis_74a6bdef:

    # "She looked like a cute begging kitten."
    "Ella se miraba como una tierna gatita."

# game/dialogs.rpy:2734
translate spanish takoyakis_da34f8b5:

    # s "Y-Yes, take me to your home...%(stringhero)s-kun..."
    s "S-si, llévame a tu casa...%(stringhero)s-kun..."

# game/dialogs.rpy:2736
translate spanish takoyakis_ee16eceb:

    # "My heart skipped a beat."
    "Mi corazón comenzó a latir con fuerza."

# game/dialogs.rpy:2737
translate spanish takoyakis_ceb3d2b6:

    # "My own girlfriend... In a kimono...coming to my bedroom...for the night..."
    "Mi propia novia... En un kimono... Llegando a mi cuarto... Por la noche..."

# game/dialogs.rpy:2738
translate spanish takoyakis_ddafe283:

    # "I'm pretty sure that we wouldn't just sleep that night..."
    "Estoy muy seguro que no vamos solo a dormir esta noche..."

# game/dialogs.rpy:2742
translate spanish takoyakis_f412f669:

    # "Apparently my parents weren't back from the festival yet."
    "Aparentemente mis padres aún no estaban de regreso del festival."

# game/dialogs.rpy:2745
translate spanish takoyakis_4bb8cb02:

    # "We entered my house silently and went upstairs and into my bedroom."
    "Entramos silenciosamente en mi casa, subimos las gradas y entramos a mi cuarto."

# game/dialogs.rpy:2747
translate spanish takoyakis_86309b2e:

    # "When I turned on the lights, Sakura opened her eyes widely."
    "Cuando encendí las luces, Sakura abrió ampliamente sus ojos."

# game/dialogs.rpy:2749
translate spanish takoyakis_abbe7785:

    # s "Whoaaa!!!"
    s "¡¡Woooow!!"

# game/dialogs.rpy:2750
translate spanish takoyakis_215547d3:

    # s "It's incredible!!! All of these posters!"
    s "¡¡¡Es increíble!!! ¡Todos estos carteles!"

# game/dialogs.rpy:2751
translate spanish takoyakis_6430e2fa:

    # s "Oh! It's {i}High School Samurai{/i}!"
    s "¡Oh! ¡Es {i}High School Samurai{/i}!"

# game/dialogs.rpy:2752
translate spanish takoyakis_52931ee2:

    # hero "Haha, yeah. I took the habit of collecting posters..."
    hero "Jeje, si. Tomé el hábito de coleccionar carteles..."

# game/dialogs.rpy:2753
translate spanish takoyakis_f00c4d4e:

    # s "You bought them at Tokyo?"
    s "¿Los compraste en Tokyo?"

# game/dialogs.rpy:2754
translate spanish takoyakis_0551d793:

    # hero "Yes. I was living near Gaymerz, the biggest manga shop of Tokyo."
    hero "Si. Estaba viviendo cerca de Gaymerz, la tienda de manga más grande de Tokyo."

# game/dialogs.rpy:2756
translate spanish takoyakis_b4f4239b:

    # s "Oh, I've heard of it!"
    s "¡Oh, escuché sobre ella!"

# game/dialogs.rpy:2757
translate spanish takoyakis_4e9a74e5:

    # s "I hope I can visit it someday."
    s "Espero poder visitarla algún día."

# game/dialogs.rpy:2758
translate spanish takoyakis_05c9701b:

    # "As she was speaking, I hugged her back lovingly."
    "Mientras ella estaba hablando, la abracé por la espalda cariñosamente."

# game/dialogs.rpy:2763
translate spanish takoyakis_ef409314:

    # "We made sweet love to each other."
    "Nos quedamos dormidos rápidamente... Después de hacer un poco el amor."

# game/dialogs.rpy:2764
translate spanish takoyakis_c6b9d465:

    # "We were so tired after this evening...We dozed off..."
    "Estábamos tan cansados después de esa tarde... Que nos dormimos solo después de eso..."

# game/dialogs.rpy:2767
translate spanish takoyakis_0cee0472:

    # "I woke up slowly."
    "Me desperté lentamente."

# game/dialogs.rpy:2768
translate spanish takoyakis_b2766fe2:

    # "Looking at the alarm clock, it's kinda late."
    "Mirando al reloj, es algo tarde."

# game/dialogs.rpy:2769
translate spanish takoyakis_f87aab37:

    # "I felt something warm against me..."
    "Sentí algo cálido tocándome..."

# game/dialogs.rpy:2770
translate spanish takoyakis_58e3a51c:

    # "It was Sakura."
    "Era Sakura."

# game/dialogs.rpy:2771
translate spanish takoyakis_ec9d25fd:

    # "She was sleeping naked in my arms..."
    "Ella estaba durmiendo desnuda en mis brazos..."

# game/dialogs.rpy:2772
translate spanish takoyakis_93a9f4ba:

    # "I smile and watched her sleeping..."
    "Sonreí y la miré durmiendo..."

# game/dialogs.rpy:2773
translate spanish takoyakis_4774de34:

    # "Even after the obvious evidence I've seen, I still can't believe that she's actually a boy..."
    "Ella se mira como una chica... Ella habla con una voz de chica... Ella huele como una chica... Ella tiene una piel suave como una chica..."

# game/dialogs.rpy:2774
translate spanish takoyakis_c2358eed:

    # "She looks like a girl... She speaks with a voice of a girl... She smells like a girl... She has soft skin like a girl..."
    "She looks like a girl... She speaks with a voice of a girl... She smells like a girl... She has soft skin like a girl..."

# game/dialogs.rpy:2775
translate spanish takoyakis_6f1e912a:

    # "I felt my heart beating sweetly as her warm sweet little hand was on my chest..."
    "Sentí mi corazón latir dulcemente mientras que su cálida y dulce mano estaba en mi pecho..."

# game/dialogs.rpy:2776
translate spanish takoyakis_eda104c0:

    # "She finally opened her eyes. As she saw me, she smiled and cuddled me tighter with a sweet sigh... and went back to sleep..."
    "Ella finalmente abrió sus ojos. A medida que me miraba, ella sonrió y se acurrucó más con un dulce suspiro... Y se volvió a dormir..."

# game/dialogs.rpy:2777
translate spanish takoyakis_0deabf9f:

    # "I sighed of happiness..."
    "Suspiré de la felicidad..."

# game/dialogs.rpy:2778
translate spanish takoyakis_a20cefa7:

    # "..."
    "..."

# game/dialogs.rpy:2779
translate spanish takoyakis_8b8ceb72:

    # "............"
    "............"

# game/dialogs.rpy:2780
translate spanish takoyakis_9ed134ce:

    # "Oh..."
    "Oh..."

# game/dialogs.rpy:2781
translate spanish takoyakis_c1399882:

    # "Oh no!!!{p}My parents!!!"
    "¡¡¡Oh no!!!{p}¡¡¡Mis padres!!!"

# game/dialogs.rpy:2782
translate spanish takoyakis_fb40c1b5:

    # "They're surely back from the festival!!!"
    "¡¡¡Ellos seguramente ya volvieron del festival!!!"

# game/dialogs.rpy:2783
translate spanish takoyakis_3b788596:

    # "What are they gonna say if they see me with a girl in my bed???"
    "¿¿¿Qué es lo que van a decir si ellos me ven con una chica en mi cama???"

# game/dialogs.rpy:2784
translate spanish takoyakis_2cffeee3:

    # "What should I do? Dammit, dammit, dammit!!!"
    "¿Qué es lo que debería de hacer? ¡¡¡Maldición, maldición!!!"

# game/dialogs.rpy:2785
translate spanish takoyakis_c1fa5fd9:

    # hero "Huh... Sakura-chan?..."
    hero "Uh... ¿Sakura-chan?..."

# game/dialogs.rpy:2786
translate spanish takoyakis_dfd2530f:

    # s "{size=-5}%(stringhero)s-kun... I love you...{/size}"
    s "{size=-5}%(stringhero)s-kun... Te amo...{/size}"

# game/dialogs.rpy:2787
translate spanish takoyakis_817848c9:

    # "She fell asleep again!?"
    "¡Rayos, se volvió a dormir!"

# game/dialogs.rpy:2788
translate spanish takoyakis_0df5e11e:

    # "I stopped panicking to listen for a moment. I was waiting for an indication that would tell me if my parents were back."
    "Esperé un momento, esperando por un ruido que me diría si mis padres están aquí."

# game/dialogs.rpy:2789
translate spanish takoyakis_9850f53c:

    # "Nothing."
    "Nada."

# game/dialogs.rpy:2790
translate spanish takoyakis_d047fac0:

    # "I slowly moved out from my bed, letting Sakura sleep and looked outside the window."
    "Lentamente me salí de mi cama, dejando a Sakura dormir y miré por la ventana."

# game/dialogs.rpy:2791
translate spanish takoyakis_f6b451ec:

    # "My parent's car wasn't there... Maybe I can do something at last..."
    "El carro de mis padres no estaba ahí... Tal vez pueda hacer algo por fin..."

# game/dialogs.rpy:2792
translate spanish takoyakis_4d679905:

    # s "%(stringhero)s-kun?...."
    s "¿%(stringhero)s-kun?...."

# game/dialogs.rpy:2793
translate spanish takoyakis_08748d66:

    # "Sakura just woke up and sat on the bed."
    "Sakura se acaba de despertar y sentarse en la cama."

# game/dialogs.rpy:2794
translate spanish takoyakis_f25fd7bb:

    # hero "Sakura-chan..."
    hero "Sakura-chan..."

# game/dialogs.rpy:2795
translate spanish takoyakis_ddcb931f:

    # hero "My parents aren't here yet. You should go home now before they come back."
    hero "Mis padres no están aquí. Deberías volver a casa ahora antes de que ellos lleguen."

# game/dialogs.rpy:2796
translate spanish takoyakis_7295fac4:

    # hero "My parents are kinda strict when it comes to relationships."
    hero "¡Mis padres son algo estrictos cuando se trata relaciones amorosas!"

# game/dialogs.rpy:2797
translate spanish takoyakis_b3eeb634:

    # "Sakura nodded. She looked all sleepy."
    "Sakura asintió. Ella se miraba toda adormilada."

# game/dialogs.rpy:2798
translate spanish takoyakis_aee46875:

    # "That was cute. I smiled."
    "Eso fue lindo. Sonreí."

# game/dialogs.rpy:2799
translate spanish takoyakis_8891ed58:

    # hero "Aww. Had too much fun last night?"
    hero "Aww. ¿Tuviste demasiada diversión anoche?"

# game/dialogs.rpy:2800
translate spanish takoyakis_e15b24d1:

    # "Sakura giggled. She stood up and took me in her arms."
    "Sakura se rió. Se levantó y me tomó en sus brazos."

# game/dialogs.rpy:2801
translate spanish takoyakis_67bc7996:

    # "Her warm naked body was against mine."
    "Su cálido y desnudo cuerpo estaba tocando el mío."

# game/dialogs.rpy:2802
translate spanish takoyakis_387596f6:

    # "I kissed her again."
    "La besé otra vez."

# game/dialogs.rpy:2804
translate spanish takoyakis_7dbc9795:

    # "After some time, I helped her put on her kimono and took her to her house..."
    "Después de un tiempo, la ayudé a ponerse su kimono y la llevé a su casa..."

# game/dialogs.rpy:2805
translate spanish takoyakis_5ffe88e0:

    # "We promised to have another date very soon..."
    "Prometimos tener otra cita muy pronto..."

# game/dialogs.rpy:2806
translate spanish takoyakis_17899d6b:

    # "I went back home and turned on my computer."
    "Regresé a casa y encendí mi computadora."

# game/dialogs.rpy:2810
translate spanish takoyakis_2b150c3e:

    # write "Dear sister,"
    write "Querida hermana,"

# game/dialogs.rpy:2812
translate spanish takoyakis_a50c844b:

    # write "How are things? It's been awhile since I wrote."
    write "¿Cómo van las cosas? Ha pasado un tiempo desde que no te he escrito."

# game/dialogs.rpy:2814
translate spanish takoyakis_54b9d70d:

    # write "You'll never guess what happened!"
    write "¡Nunca vas a adivinar lo que me pasó!"

# game/dialogs.rpy:2816
translate spanish takoyakis_67d2ac31:

    # write "I'm going out with someone!"
    write "¡Estoy saliendo con alguien!"

# game/dialogs.rpy:2818
translate spanish takoyakis_df940a1c:

    # write "She's a wonderful person... A unique person...{w} It's kinda hard to tell you more about this...{w}You have to promise that you'll never tell our parents!"
    write "Es una maravillosa persona... Única...{w} Es algo difícil contarte más sobre esto...{w} Debo hacer que prometas que nunca le dirás a nuestros padres primero."

# game/dialogs.rpy:2820
translate spanish takoyakis_bc7a4cdb:

    # write "At least, I'm happy... More happy than I have ever been... It's as if I just got the best Christmas present ever."
    write "Al menos, estoy feliz... Más feliz de lo que nunca he sido... ¡Como si hubiera conseguido el mejor regalo de Navidad que nunca he tenido!"

# game/dialogs.rpy:2821
translate spanish takoyakis_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:2823
translate spanish takoyakis_bf522c92:

    # write "I'll send you a photo of us. I don't know how much longer our story will last, but I'm sure I'll never forget it for the rest of my life. What I'm living is really unique!"
    write "Te mandaré una foto de nosotros. No sé cuánto más va a durar nuestra historia, ¡pero estoy seguro que nunca lo olvidaré en toda mi vida! ¡Lo que estoy viviendo es realmente único!"

# game/dialogs.rpy:2825
translate spanish takoyakis_f52f892c:

    # write "I hope everything is okay for you too and that you are as happy as I am..."
    write "Espero que todo esté bien para ti también y que seas tan feliz como yo lo soy..."

# game/dialogs.rpy:2827
translate spanish takoyakis_f60fe613:

    # write "Your dear brother{p}%(stringhero)s"
    write "Tú querido hermano{p}%(stringhero)s"

# game/dialogs.rpy:2829
translate spanish takoyakis_4387dacf:

    # write "PS: {w}The local summer festival was great! I hope you had a wonderful event like this in Tokyo."
    write "PD: {w}¡El festival de Verano local fue grandioso! Espero que hayas tenido un maravilloso evento como este en Tokyo."

# game/dialogs.rpy:2830
translate spanish takoyakis_76b2fe88_1:

    # nvl clear
    nvl clear

# game/dialogs.rpy:2852
translate spanish matsuri_N_46751f32:

    # "I came to the festival with Sakura."
    "Vine al festival con Sakura."

# game/dialogs.rpy:2853
translate spanish matsuri_N_17894765:

    # "She was wearing a cute yukata. It fit her well."
    "Ella estaba vistiendo una linda yukata. Le quedaba muy bien."

# game/dialogs.rpy:2855
translate spanish matsuri_N_ec440c0a:

    # "I was wearing the brand new yukata that she gave me."
    "Yo estaba usando la nueva yukata que ella me dio."

# game/dialogs.rpy:2857
translate spanish matsuri_N_201b46ce:

    # "I was wearing the yukata I recently bought... One which was awfully expensive..."
    "Estaba usado la yukata que recientemente compre... La cual fue terriblemente cara..."

# game/dialogs.rpy:2858
translate spanish matsuri_N_38154d05:

    # "We quickly found Nanami and Rika despite the big crowd."
    "Rápidamente encontramos a Nanami y Rika a pesar de la gran multitud."

# game/dialogs.rpy:2863
translate spanish matsuri_N_866e240e:

    # "Rika was wearing a very pretty purple yukata that showed her shoulders outside."
    "Rika estaba vistiendo una yukata púrpura muy bonita que mostraba sus hombros."

# game/dialogs.rpy:2864
translate spanish matsuri_N_e57fd5f6:

    # "Nanami was wearing a more simple green yukata but it was incredibly cute on her."
    "Nanami estaba vistiendo una yukata más sencilla de color verde pero era extremadamente linda en ella."

# game/dialogs.rpy:2865
translate spanish matsuri_N_c1358676:

    # r "So, what we do first? I can't wait!"
    r "Entonces, ¿qué hacemos primero? ¡No puedo esperar!"

# game/dialogs.rpy:2867
translate spanish matsuri_N_e60b3319:

    # n "I want to go to the shooting stands!"
    n "¡Quiero ir al puesto de tiros!"

# game/dialogs.rpy:2868
translate spanish matsuri_N_a6a52bc4:

    # s "That sounds nice!"
    s "¡Eso suena bien!"

# game/dialogs.rpy:2869
translate spanish matsuri_N_e8f6b7d0:

    # hero "I'd like that too!"
    hero "¡A mí también me gusta!"

# game/dialogs.rpy:2870
translate spanish matsuri_N_f8974ce9:

    # r "Let's go!"
    r "¡Entonces vámonos!"

# game/dialogs.rpy:2872
translate spanish matsuri_N_228ae33a:

    # "We arrived just in time. The stand was pretty empty."
    "Llegamos justo a tiempo. El puesto estaba vacío."

# game/dialogs.rpy:2874
translate spanish matsuri_N_4cdd6550:

    # n "Hey, %(stringhero)s-nii... I challenge you to make a plushie fall before I do!"
    n "Oye, %(stringhero)s-nii... ¡Te reto a que hagas caer el peluche antes que yo lo haga!"

# game/dialogs.rpy:2875
translate spanish matsuri_N_2635f363:

    # hero "I thought you weren't good at shooting games."
    hero "Pensé que no eras buena en juegos de disparos."

# game/dialogs.rpy:2876
translate spanish matsuri_N_88c489c7:

    # n "I trained hard! You'll see!"
    n "¡Entrené duro! ¡Ya lo verás!"

# game/dialogs.rpy:2881
translate spanish matsuri_N_cd54b02a:

    # "We both payed ¥100 and started to frantically shoot at the targets with our toy rifles."
    "Pagamos ¥100 y comenzamos frenéticamente a disparar a los objetivos con nuestros rifles. "

# game/dialogs.rpy:2882
translate spanish matsuri_N_1978a1c0:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2883
translate spanish matsuri_N_d4bb63e2:

    # "We both missed... We tried again."
    "Ambos fallamos... Tratamos otra vez."

# game/dialogs.rpy:2884
translate spanish matsuri_N_1978a1c0_1:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2885
translate spanish matsuri_N_df8da603:

    # "We hit the same plush but it barely moved."
    "Le dimos al mismo peluche pero apenas se movió."

# game/dialogs.rpy:2886
translate spanish matsuri_N_8d01865c:

    # n "Eeeeh!"
    n "¡Eeeeh!"

# game/dialogs.rpy:2887
translate spanish matsuri_N_1feb4bc0:

    # hero "No fair! It was a critical hit!"
    hero "¡No es justo! ¡Era un golpe crítico!"

# game/dialogs.rpy:2888
translate spanish matsuri_N_cf97790a:

    # n "I'm not done yet!"
    n "¡Aún no termino!"

# game/dialogs.rpy:2889
translate spanish matsuri_N_3093850e:

    # "We tried again once more..."
    "Tratamos una vez más..."

# game/dialogs.rpy:2890
translate spanish matsuri_N_1978a1c0_2:

    # "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"
    "*{i}BANG{/i}*{p}*{i}BANG{/i}*{p}*{i}BANG{/i}*"

# game/dialogs.rpy:2891
translate spanish matsuri_N_ffac77fc:

    # "I got it! I got the plush!"
    "¡Lo conseguí! ¡Conseguí el peluche!"

# game/dialogs.rpy:2895
translate spanish matsuri_N_2530a4ee:

    # hero "Haha I won!"
    hero "¡Jaja gané!"

# game/dialogs.rpy:2896
translate spanish matsuri_N_638da9e6:

    # "It was a cute plush of crocodile."
    "Era un lindo peluche de cocodrilo."

# game/dialogs.rpy:2897
translate spanish matsuri_N_b9dca595:

    # "Nanami started to pout."
    "Nanami comenzó a hacer pucheros."

# game/dialogs.rpy:2898
translate spanish matsuri_N_0d531ead:

    # n "Eeeeh that's unfair, you got it because I hit it a couple times already!"
    n "Eeeeh eso es injusto, ¡lo obtuviste porque lo golpeé primero!"

# game/dialogs.rpy:2899
translate spanish matsuri_N_1bdd906d:

    # n "I wanted that plush!"
    n "¡Quería ese peluche!..."

# game/dialogs.rpy:2900
translate spanish matsuri_N_adab4cae:

    # "I laughed and rubbed her hair."
    "Me reí y acaricié su cabello."

# game/dialogs.rpy:2901
translate spanish matsuri_N_e0e8e5a5:

    # hero "Don't be silly. {p}I got it for you."
    hero "Tonta. Lo conseguí para ti. {p}Ten."

# game/dialogs.rpy:2902
translate spanish matsuri_N_c55aa101:

    # "I gave Nanami the plushie."
    "Le di a Nanami el peluche."

# game/dialogs.rpy:2903
translate spanish matsuri_N_3eb6b6ea:

    # "She looked at me with a cute expression, then she gave me a big hug."
    "Ella me miró con una linda expresión, entonces me abrazó fuertemente."

# game/dialogs.rpy:2905
translate spanish matsuri_N_5693f2ef:

    # n "Yaaay! Thank you, %(stringhero)s-nii!"
    n "¡Yaaay! Gracias, ¡%(stringhero)s-nii!"

# game/dialogs.rpy:2906
translate spanish matsuri_N_162f5687:

    # r "Hey, get a room, you two!"
    r "¡Oigan, consigan un cuarto, ustedes dos!"

# game/dialogs.rpy:2907
translate spanish matsuri_N_9a893c8c:

    # "We all laughed."
    "Todos nos reimos."

# game/dialogs.rpy:2912
translate spanish matsuri_N_9ced4dba:

    # hero "I'm hungry... How about we get some food now?"
    hero "¿Qué tal si comemos algo ahora? Me siento hambriento."

# game/dialogs.rpy:2917
translate spanish matsuri_N_80443115:

    # r "Excellent idea!!! Me too!!!"
    r "¡¡¡Excelente idea!!! ¡¡¡Yo también!!!"

# game/dialogs.rpy:2918
translate spanish matsuri_N_01dbb1d1:

    # s "I'm a little hungry too."
    s "Yo también, un poco."

# game/dialogs.rpy:2919
translate spanish matsuri_N_5e163699:

    # r "Let's go! I'll show you an amazing stand where they make the best takoyaki!"
    r "¡Vamos! ¡Les mostraré un buen puesto donde hacen unos excelentes takoyakis!"

# game/dialogs.rpy:2926
translate spanish matsuri_N2_b3e735ce:

    # "We finished the festival by watching the fireworks..."
    "Terminamos el festival mirando los fuegos artificiales..."

# game/dialogs.rpy:2927
translate spanish matsuri_N2_d988d3e5:

    # "The show was fabulous."
    "El espectáculo fue fabuloso."

# game/dialogs.rpy:2930
translate spanish matsuri_N2_576baed2:

    # "As we we walked around some more, Nanami took my hand and guided me into the forest behind the temple..."
    "En el momento, Nanami tomó mi mano y me guió al bosque detrás del templo..."

# game/dialogs.rpy:2931
translate spanish matsuri_N2_c50c979d:

    # "We made discreet, passionate love behind the bushes..."
    "Hicimos el amor ahí, discretamente, detrás de los arbustos..."

# game/dialogs.rpy:2932
translate spanish matsuri_N2_633fea5f:

    # "Now that's what I call a night!"
    "¡Por Dios, eso es lo que llamó una noche!"

# game/dialogs.rpy:2935
translate spanish matsuri_N2_2ab4bd94:

    # "After escorting Sakura to her house, I went back to my own."
    "Después de escoltar a Sakura a su casa, me regresé a la mía."

# game/dialogs.rpy:2937
translate spanish matsuri_N2_6f974e08:

    # "My parents weren't back yet so I just goofed around and read some manga.{p} I also decided to write to my sister."
    "Mis padres no estaban de vuelta aún, así que estuve de ocio un momento...{p}Y le escribí a mi hermana... "

# game/dialogs.rpy:2940
translate spanish matsuri_N2_2b150c3e:

    # write "Dear sister,"
    write "Querida hermana,"

# game/dialogs.rpy:2942
translate spanish matsuri_N2_92248cbb:

    # write "How are things?"
    write "¿Cómo van las cosas?"

# game/dialogs.rpy:2944
translate spanish matsuri_N2_bb83fcbc:

    # write "You'll never guess what happened to me actually!"
    write "¡Nunca vas a adivinar lo que me ha pasado!"

# game/dialogs.rpy:2946
translate spanish matsuri_N2_4967d059:

    # write "I'm going out with a girl!"
    write "¡Estoy saliendo con una chica!"

# game/dialogs.rpy:2948
translate spanish matsuri_N2_534e4421:

    # write "Her name is Nanami. She's a really cute and energetic girl from school.{w} We play a lot of video games together. We're really into each other!"
    write "Ella se llama Nanami. Ella es una chica realmente linda y energética de la escuela.{w} Nos retamos el uno al otro muy seguido en los videojuegos, pero nos amamos... Nosotros incluso... Tu sabes..."

# game/dialogs.rpy:2950
translate spanish matsuri_N2_4f13aea8:

    # write "I'm happy... Really happy... I can't wait to introduce her to you, someday! {w}I'll send you a photo of us soon!"
    write "Estoy feliz... Realmente feliz... ¡No puedo esperar a presentártela, algún día! {w} Te mandaré una foto de nosotros, pronto, ¡entonces la verás! Ella es muy bonita."

# game/dialogs.rpy:2951
translate spanish matsuri_N2_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:2953
translate spanish matsuri_N2_ab4193c3:

    # write "I hope everything going well for you! I can't wait to see you again!"
    write "Espero que todo esté bien para ti también y que seas tan feliz como yo lo soy..."

# game/dialogs.rpy:2955
translate spanish matsuri_N2_f60fe613:

    # write "Your dear brother{p}%(stringhero)s"
    write "Tu querido hermano{p}%(stringhero)s"

# game/dialogs.rpy:2957
translate spanish matsuri_N2_91609d26:

    # write "PS:{w} The local summer festival was great! I hope you had a wonderful event like this in Tokyo."
    write "PD:{w} ¡El festival de Verano local fue grandioso! Espero que hayas tenido un maravilloso evento como este en Tokyo."

# game/dialogs.rpy:2959
translate spanish matsuri_N2_deb10d39:

    # "I heard my cellphone ring."
    "Repentinamente, mi celular sonó."

# game/dialogs.rpy:2964
translate spanish matsuri_N2_dbd7e6ae:

    # hero "Hello?"
    hero "¿Aló?"

# game/dialogs.rpy:2965
translate spanish matsuri_N2_77c066ad:

    # n "{i}It's me, dude!{/i}"
    n "{i}¡Soy yo, tipo!{/i}"

# game/dialogs.rpy:2966
translate spanish matsuri_N2_f295726b:

    # "I chuckled as I recognized Nanami's voice."
    "Me reí mientras reconocía la voz de Nanami."

# game/dialogs.rpy:2967
translate spanish matsuri_N2_404c955e:

    # hero "We're going out and you still call me dude?"
    hero "Estamos saliendo juntos y ¿tú aún me llamas un tipo?"

# game/dialogs.rpy:2968
translate spanish matsuri_N2_284b7d55:

    # n "{i}Well, you're a guy after all, right?{/i}"
    n "{i}Bueno, tú eres un chico después de todo, ¿cierto?{/i}"

# game/dialogs.rpy:2969
translate spanish matsuri_N2_770ce25a:

    # n "{i}I've seen you from enough close to see it with my own eyes!{/i} {image=heart.png}"
    n "{i}¡Te he visto lo suficientemente cerca para verlo con mis propios ojos! {image=heart.png}{/i}"

# game/dialogs.rpy:2970
translate spanish matsuri_N2_7139659c:

    # "I blushed, remembering what we did earlier in the forest."
    "Me sonrojé, recordando lo que hicimos anteriormente en el bosque."

# game/dialogs.rpy:2971
translate spanish matsuri_N2_e86fa184:

    # hero "Hehe... That was good, yeah..."
    hero "Jeje... Eso fue bueno..."

# game/dialogs.rpy:2972
translate spanish matsuri_N2_7bfb8ae4:

    # n "{i}Teehee! To be honest... the more we do it, the more I want to do it again!{/i} {image=heart.png}"
    n "{i}¡Teehee! Para ser honesta... Cuando más lo hacemos, más quiero hacerlo otra vez!{/i} {image=heart.png}"

# game/dialogs.rpy:2973
translate spanish matsuri_N2_69a9deb4:

    # n "{i}Speaking which, are you busy tomorrow?{/i}"
    n "{i}Hablando de eso, ¿tienes que hacer algo mañana?{/i}"

# game/dialogs.rpy:2974
translate spanish matsuri_N2_2224fcb7:

    # hero "Except coming to see you? No, nothing at all."
    hero "¿Excepto verte? No, nada de nada."

# game/dialogs.rpy:2975
translate spanish matsuri_N2_8da73ec6:

    # "I laughed and heard Nanami laughing on the other end as well."
    "Sonreí. Escuché la risa de Nanami."

# game/dialogs.rpy:2976
translate spanish matsuri_N2_26411a89:

    # n "{i}Alright, %(stringhero)s-nii. {w}I'll wait for you at el-{nw}{/i}"
    n "{i}Esta bien, %(stringhero)s-nii. {w}Te estaré esperando en on-{nw}{/i}"

# game/dialogs.rpy:2977
translate spanish matsuri_N2_587273a6:

    # n "{i}.........{/i}"
    n "{i}.........{/i}"

# game/dialogs.rpy:2978
translate spanish matsuri_N2_fd4b149d:

    # hero "Hello? Nanami-chan?"
    hero "¿Hola? ¿Nanami-chan?"

# game/dialogs.rpy:2979
translate spanish matsuri_N2_97743246:

    # n "{i}{size=-12}It's my boyfriend.....{p}It's none of your business!!......{/size}{/i}"
    n "{i}{size=-12}Es mi novio.....{p}¡¡No es tu asunto!!......{/size}{/i}"

# game/dialogs.rpy:2980
translate spanish matsuri_N2_5027b83e:

    # n "{i}{size=-12}Big brother!!!......{/size}{/i}"
    n "{i}{size=-12}¡¡¡Hermano!!!.......{/size}{/i}"

# game/dialogs.rpy:2981
translate spanish matsuri_N2_2f756018:

    # n "{i}I'll call you back.{/i}"
    n "{i}Te llamaré luego.{/i}"

# game/dialogs.rpy:2983
translate spanish matsuri_N2_6c3f05d2:

    # "*{i}click{/i}*"
    "*{i}click{/i}*"

# game/dialogs.rpy:2984
translate spanish matsuri_N2_2e0a800f:

    # "What was that about...? I shrugged and went back to my computer."
    "Me encogí de hombros y volví a mi computadora."

# game/dialogs.rpy:2985
translate spanish matsuri_N2_73d69e81:

    # "I have a bad feeling about this... {p}But, she said she'll call back."
    "Tenía un mal presentimiento sobre esto... {p}Pero bueno, ella me dijo que me llamaría de vuelta..."

# game/dialogs.rpy:2988
translate spanish matsuri_N2_1569a42e:

    # "Some time passed as I goofed around. All of a sudden, I heard the doorbell."
    "Después de alrededor de media hora, escuché el timbre de la puerta."

# game/dialogs.rpy:2990
translate spanish matsuri_N2_8ef37003:

    # "Suddenly, I got goosebumps. A sense of dread came over me. It pushed me to run downstairs and to open the door as fast I could."
    "De pronto, el mal presentimiento que estaba teniendo anteriormente se convirtió en algo más claro y opresivo."
    "Me impulso a bajar las escaleras y abrir la puerta lo más rápido que podía."

# game/dialogs.rpy:2992
translate spanish matsuri_N2_ee440385:

    # "Nanami was at the door. She was in tears."
    "Nanami estaba en la puerta. Ella estaba en lágrimas."

# game/dialogs.rpy:2993
translate spanish matsuri_N2_e3c36773:

    # "When I opened up, she ran in the living room."
    "Cuando abrí, ella corrió a la sala para ponerse segura."

# game/dialogs.rpy:2994
translate spanish matsuri_N2_fe828537:

    # hero "Nanami-chan?? {p}Are you okay? What's going on?!"
    hero "¿¿Nanami-chan?? {p}¿Estás bien? ¡¿Qué esta pasando?!"

# game/dialogs.rpy:2995
translate spanish matsuri_N2_0b51bf1c:

    # n "He's coming! He's scaring me, %(stringhero)s-nii!"
    n "¡Él esta viniendo! Él me esta asustando, ¡%(stringhero)s-nii!"

# game/dialogs.rpy:2996
translate spanish matsuri_N2_2952dd4d:

    # hero "Who? Who's coming?"
    hero "¿Quién? ¿Quién esta viniendo?"

# game/dialogs.rpy:2997
translate spanish matsuri_N2_817e602f:

    # "I got my answer right after I asked."
    "Obtuve mi respuesta justo después de preguntar."

# game/dialogs.rpy:2998
translate spanish matsuri_N2_d7a874f3:

    # "A guy entered the house with a demented look on his face."
    "Un tipo entró a la casa con una mirada muy enojada y tenebrosa en su rostro."

# game/dialogs.rpy:2999
translate spanish matsuri_N2_5b3076dd:

    # "I recognize him. He was on Nanami's picture frame in her bedroom."
    "Instantáneamente lo reconocí. Él estaba en la foto de Nanami que estaba en su cuarto."

# game/dialogs.rpy:3000
translate spanish matsuri_N2_203812e1:

    # hero "Are you Toshio-san?"
    hero "¿Eres Toshio-san?"

# game/dialogs.rpy:3001
translate spanish matsuri_N2_23531e72:

    # "He ignored my question and barged in straight toward Nanami."
    "Él ignoró mi pregunta y estaba caminando hacia Nanami."

# game/dialogs.rpy:3003
translate spanish matsuri_N2_f15432c1:

    # "I instinctively interposed myself between them."
    "Instintivamente me interpuse entre ellos."

# game/dialogs.rpy:3004
translate spanish matsuri_N2_5d32f865:

    # hero "Whoa! Wait wait wait... What are you going to do to her?!"
    hero "Espera espera espera... ¡¿Qué es lo que vas a hacerle?!"

# game/dialogs.rpy:3005
translate spanish matsuri_N2_91b34485:

    # toshio "Nanami! Come back home right now!"
    toshio "¡Nanami! ¡Vuelve a casa ahora mismo!"

# game/dialogs.rpy:3006
translate spanish matsuri_N2_54fe86a6:

    # n "No! You're scaring me, big brother!"
    n "¡No! ¡Me estás asustando, hermano!"

# game/dialogs.rpy:3007
translate spanish matsuri_N2_9e884cfa:

    # "Then Toshio outrageously pointed at me."
    "Entonces Toshio furiosamente apunto a mi dirección."

# game/dialogs.rpy:3008
translate spanish matsuri_N2_b495b159:

    # toshio "And you...{p}How dare you soil my sister!"
    toshio "Y tú...{p}¡Cómo te atreves a ensuciar a mi hermana!"

# game/dialogs.rpy:3009
translate spanish matsuri_N2_4d869fcc:

    # "What?!{p}Soil her?! He's crazy!"
    "¡¿Qué?!{p}¡¿Ensuciarla?! ¡¡Él está loco!!"

# game/dialogs.rpy:3010
translate spanish matsuri_N2_47b87910:

    # hero "Hey man, calm down!"
    hero "¡Oye hombre, cálmate!"

# game/dialogs.rpy:3011
translate spanish matsuri_N2_5f2f5e83:

    # hero "Calm down and listen, please!"
    hero "¡Cálmate y escucha, por favor!"

# game/dialogs.rpy:3012
translate spanish matsuri_N2_c025cc4c:

    # "Toshio ignored my words completely and approached me menacingly."
    "Toshio no me escuchó y se acercó amenazadoramente."

# game/dialogs.rpy:3013
translate spanish matsuri_N2_d17a127d:

    # "Oh shit, he's going to kill me!"
    "Oh no, ¡me va a matar!"

# game/dialogs.rpy:3014
translate spanish matsuri_N2_8d99340f:

    # "In pure panic, I spoke what came to mind."
    "En puro pánico, traté de hablar lo que estaba en mi mente."

# game/dialogs.rpy:3015
translate spanish matsuri_N2_d2e3bc0f:

    # hero "Just calm down! I didn't soil her! I-"
    hero "¡¡Por favor cálmate!! ¡No la ensucié!"

# game/dialogs.rpy:3017
translate spanish matsuri_N2_499d2845:

    # hero "{size=+15}I love her!!{/size}"
    hero "{size=+15}¡¡Yo la amo!!{/size}"

# game/dialogs.rpy:3019
translate spanish matsuri_N2_e3f58031:

    # "There was an awkward silence in the room."
    "Hubo silencio en el cuarto."

# game/dialogs.rpy:3020
translate spanish matsuri_N2_0ca571d7:

    # "Toshio had stopped walking... He looked more upset than ever."
    "Toshio había parado de caminar... Él se miraba más enojado que nunca."

# game/dialogs.rpy:3021
translate spanish matsuri_N2_cef855da:

    # "Strangely, saying that out loud gave me more courage and strength."
    "Extrañamente, habiendo confesado lo que quería decir a Nanami me dio más coraje y fuerza."

# game/dialogs.rpy:3022
translate spanish matsuri_N2_e6bb1d42:

    # hero "I love her... I love her with all my heart."
    hero "La amo... La amo con todo mi corazón."

# game/dialogs.rpy:3023
translate spanish matsuri_N2_d225817d:

    # hero "I'm in love with her!... All my intentions towards her are from the bottom of my heart!"
    hero "¡Estoy enamorado de ella!... Todas mis intenciones hacia ella son de puro amor..."

# game/dialogs.rpy:3024
translate spanish matsuri_N2_f4b05b05:

    # toshio "How... How dare you..."
    toshio "Cómo... Cómo te atreves..."

# game/dialogs.rpy:3028
translate spanish matsuri_N2_bc61413a:

    # "Nanami came between us, protecting me this time. She was holding me tight."
    "De pronto, Nanami se movió entre yo y Toshio, protegiéndome esta vez. Ella me estaba sujetando con fuerza."

# game/dialogs.rpy:3029
translate spanish matsuri_N2_ff4ee66e:

    # "Toshio stopped again."
    "Toshio se detuvo de nuevo."

# game/dialogs.rpy:3030
translate spanish matsuri_N2_95c7f55c:

    # toshio "Nanami, get the fuck off him!!!"
    toshio "¡¡Nanami, aléjate de él!!"

# game/dialogs.rpy:3031
translate spanish matsuri_N2_5050139a:

    # n "No! Stop it, Big brother!!"
    n "¡No! ¡¡Para ya, hermano!!"

# game/dialogs.rpy:3032
translate spanish matsuri_N2_746a8fc4:

    # n "If you want to hit him, then hit me first!"
    n "¡Si quieres golpearlo, entonces golpéame primero!"

# game/dialogs.rpy:3033
translate spanish matsuri_N2_1053aa7b:

    # n "I love him too, big brother!!{p}{size=+15}I love him!!!{/size}"
    n "¡¡Lo amo también, hermano!!{p}{size=+15}¡¡¡Lo amo!!!{/size}"

# game/dialogs.rpy:3034
translate spanish matsuri_N2_bb6eef02:

    # n "He did nothing wrong! I'm the only culprit!"
    n "¡Él no hizo nada malo! ¡Yo soy la única culpable!"

# game/dialogs.rpy:3035
translate spanish matsuri_N2_7a8a2dfd:

    # "Toshio stood motionless."
    "Toshio estuvo inmóvil."

# game/dialogs.rpy:3036
translate spanish matsuri_N2_9b1a0a97:

    # "Nanami's confession filled me with joy... But I was too scared to savor it fully."
    "Dentro de mí, estaba tan feliz de la confesión de Nanami... Pero estaba demasiado asustado para disfrutarlo por completo."

# game/dialogs.rpy:3037
translate spanish matsuri_N2_a79664fb:

    # "Tears were coming from Nanami's eyes. I knew she meant it. She was just as terrified."
    "De las lágrimas que estaba viendo salir de los ojos de Nanami, sabía que era difícil para ella disfrutarlo también."

# game/dialogs.rpy:3038
translate spanish matsuri_N2_38b0386e:

    # toshio "Nanami..."
    toshio "Nanami..."

# game/dialogs.rpy:3039
translate spanish matsuri_N2_e4239ff7:

    # toshio "You can't...{p}You can't love him. You shouldn't."
    toshio "No puedes...{p}No puedes amarlo. No debes."

# game/dialogs.rpy:3041
translate spanish matsuri_N2_89e8a925:

    # toshio "If you love him, someday he'll go away, just like our parents."
    toshio "Si lo amas, algún día él se irá, como nuestros padres."

# game/dialogs.rpy:3042
translate spanish matsuri_N2_626d5481:

    # toshio "We shouldn't love anyone again. We only have each other!"
    toshio "No debemos amar otra vez a alguien. Debemos quedarnos juntos."

# game/dialogs.rpy:3043
translate spanish matsuri_N2_710fb61d:

    # toshio "In this world, we can only count on ourselves, Nanami."
    toshio "En este mundo, solo podemos contar con nosotros mismos, Nanami."

# game/dialogs.rpy:3044
translate spanish matsuri_N2_c60cf6df:

    # n "No...{p}No you're wrong, big brother..."
    n "No...{p}Estás equivocado, hermano..."

# game/dialogs.rpy:3045
translate spanish matsuri_N2_6112b197:

    # n "When we lost our parents, I was stuck in the same mindset."
    n "Estaba pensando como tú antes, cuando perdimos a nuestros padres."

# game/dialogs.rpy:3046
translate spanish matsuri_N2_3646ab26:

    # n "I thought we shouldn't deeply love anyone anymore."
    n "Pensé que no deberíamos amar profundamente a nadie más."

# game/dialogs.rpy:3047
translate spanish matsuri_N2_7531d9d0:

    # n "And as time passed, I thought I was going more and more crazy because of loneliness."
    n "Y mientras que el tiempo pasó, pensé que me iba a volverme más y más loca de la soledad."

# game/dialogs.rpy:3048
translate spanish matsuri_N2_5366f0e4:

    # n "Until I joined Rika-nee's manga club."
    n "Hasta que me uní al club de manga de Rika-nee."

# game/dialogs.rpy:3049
translate spanish matsuri_N2_2759bd16:

    # n "Now I have two wonderful friends..."
    n "Conseguí dos maravillosas amigas..."

# game/dialogs.rpy:3050
translate spanish matsuri_N2_d47cc30c:

    # n "And %(stringhero)s definitely changed my life..."
    n "Y %(stringhero)s definitivamente me cambió mi vida cuando el llegó..."

# game/dialogs.rpy:3051
translate spanish matsuri_N2_17bb8570:

    # n "I've never been so happy in my life, thanks to them..."
    n "Nunca he estado tan feliz en mi vida, gracias a ellos..."

# game/dialogs.rpy:3052
translate spanish matsuri_N2_1a096aae:

    # n "They all made me realize that we shouldn't turn our back to love."
    n "Todos ellos me hicieron darme cuenta que no deberíamos dejar tirado al amor."

# game/dialogs.rpy:3053
translate spanish matsuri_N2_9afef4f0:

    # n "The only way to heal our hearts is to embrace love and friendship, big brother... Not being stuck in loneliness!"
    n "La única forma de curar nuestros corazones es abrazar al amor y la amistad, hermano... ¡No estar estancados en la soledad!"

# game/dialogs.rpy:3054
translate spanish matsuri_N2_ff64941d:

    # n "If... If you kill %(stringhero)s, I... I will... I..."
    n "Si... Si matas a %(stringhero)s, yo... Yo te... Yo..."

# game/dialogs.rpy:3055
translate spanish matsuri_N2_7f7dd2d3:

    # "Nanami broke into tears again... She probably couldn't bear to live without the manga club."
    "Nanami estalló en lágrimas otra vez mientras que ella estaba imaginando la vida sin mí y las otras."

# game/dialogs.rpy:3057
translate spanish matsuri_N2_7953243c:

    # "Toshio was still motionless. But his face was overflowing with tears now as he realized what he's done."
    "Toshio estaba aún inmóvil. En su rostro ahora estaba fluyendo lágrimas mientas se estaba dando cuenta de su estupidez."

# game/dialogs.rpy:3058
translate spanish matsuri_N2_0b7e8bd9:

    # "He fell on his knees, staring down at the floor... Guilt seemed to overpower him."
    "Él cayó de rodillas, mirando el piso... Probablmente tratando de recuperarse de su locura."

# game/dialogs.rpy:3059
translate spanish matsuri_N2_5e48472d:

    # toshio "You..."
    toshio "Tú..."

# game/dialogs.rpy:3060
translate spanish matsuri_N2_b9326ce8:

    # toshio "You're right, Nanami..."
    toshio "Estas en lo correcto, Nanami..."

# game/dialogs.rpy:3061
translate spanish matsuri_N2_19220ebd:

    # toshio "Oh dear, what have I done..."
    toshio "Oh querida, qué es lo que he hecho..."

# game/dialogs.rpy:3062
translate spanish matsuri_N2_1e2845d6:

    # toshio "I... I thought you... I don't know..."
    toshio "Yo... Yo pensé... No sé..."

# game/dialogs.rpy:3063
translate spanish matsuri_N2_1e3aa01c:

    # toshio "I thought having a boyfriend would make you forget our parents..."
    toshio "Pensé que al tener un novio estaba haciendo que te olvidaras de nuestros padres..."

# game/dialogs.rpy:3064
translate spanish matsuri_N2_ad121cc5:

    # n "How could I forget them?... I love them more than anything...!"
    n "¿Cómo podría olvidarlos?... ¡Los amo más que a nada!..."

# game/dialogs.rpy:3065
translate spanish matsuri_N2_678f6c05:

    # n "And I'm sure that they would be happy to see me with %(stringhero)s and with the others if they were still here..."
    n "Y estoy seguro que ellos estarían felices de verme con %(stringhero)s y con las otras si ellos estuvieran aún aquí..."

# game/dialogs.rpy:3066
translate spanish matsuri_N2_d387126d:

    # toshio "You're right..."
    toshio "Tienes razón..."

# game/dialogs.rpy:3067
translate spanish matsuri_N2_c1e47d68:

    # toshio "I'm an awful brother. A terrible brother..."
    toshio "Soy un terrible hermano. Un horrible hermano..."

# game/dialogs.rpy:3069
translate spanish matsuri_N2_721624ab:

    # "Nanami left my arms to hold her brother."
    "Nanami dejó mis brazos para abrazar a su hermano."

# game/dialogs.rpy:3070
translate spanish matsuri_N2_8e3dd312:

    # n "No, you are not, big brother."
    n "No, no lo eres, hermano."

# game/dialogs.rpy:3071
translate spanish matsuri_N2_98ee3ce3:

    # n "You're just still grieving over what happened to us..."
    n "Tú estás solo perturbado por lo qué nos pasó..."

# game/dialogs.rpy:3072
translate spanish matsuri_N2_e056bbe7:

    # n "Since they died, I've never seen you interact with others as much. You were always thinking of working to help us survive."
    n "Desde que ellos se fueron, nunca te he visto con tus amigos o con una novia. Tú estabas siempre pensando en el trabajo para ayudarnos a sobrevivir."

# game/dialogs.rpy:3073
translate spanish matsuri_N2_2f0b2303:

    # n "That's not healthy... {p}Loneliness isn't healthy..."
    n "Eso no es sano... {p}La soledad no es sana..."

# game/dialogs.rpy:3074
translate spanish matsuri_N2_0a9e2c99:

    # "I watched the scene without a word."
    "Miré la escena sin decir una palabra."

# game/dialogs.rpy:3077
translate spanish matsuri_N2_f5d91d1c:

    # "When Toshio felt better, Nanami released her embrace."
    "Cuando Toshio se sintió mejor, Nanami liberó su abrazo."

# game/dialogs.rpy:3078
translate spanish matsuri_N2_2ef38f10:

    # hero "You both need some fresh water. I'll get you some."
    hero "Ambos necesitan algo de agua fresca. Les conseguiré un poco."

# game/dialogs.rpy:3079
translate spanish matsuri_N2_01068bf5:

    # toshio "S-sure... Thank you..."
    toshio "C-claro... Gracias..."

# game/dialogs.rpy:3080
translate spanish matsuri_N2_56a7dd0c:

    # toshio "I'm... I'm sorry for..."
    toshio "Lo... Lo siento por..."

# game/dialogs.rpy:3081
translate spanish matsuri_N2_f3ee0ddb:

    # hero "It's okay..."
    hero "Esta bien..."

# game/dialogs.rpy:3082
translate spanish matsuri_N2_a44ad6f3:

    # hero "I don't have a little sister. But I might be a bit worried if I knew she was seeing some random dude I didn't know about."
    hero "No tengo una hermana menor, pero tal vez estaría un poco enojado si ella estuviera viendo a algún tipo que no conozco."

# game/dialogs.rpy:3083
translate spanish matsuri_N2_1e293463:

    # n "Hey, I'm the one saying you're a dude, not you!"
    n "Oye, ¡Soy yo la que dice que eres un tipo, no tú!"

# game/dialogs.rpy:3084
translate spanish matsuri_N2_c02bf505:

    # "I giggled softly."
    "Me reí suavemente."

# game/dialogs.rpy:3085
translate spanish matsuri_N2_e1ca6ffd:

    # "I went to the kitchen and came back with three glasses of fresh water."
    "Fuí a la cocina y regresé con 3 vasos de agua fresca. Todos lo necesitábamos para recuperarnos de esas emociones."

# game/dialogs.rpy:3086
translate spanish matsuri_N2_9d66422e:

    # hero "Toshio-san, I promise you that I want Nanami-chan to be happy and that I'll do my best to make her happy."
    hero "Toshio-san, te prometo que quiero que Nanami-chan sea feliz y haré mi mejor esfuerzo para hacer que esté feliz."

# game/dialogs.rpy:3087
translate spanish matsuri_N2_b183c263:

    # toshio "T-thank you..."
    toshio "G-gracias..."

# game/dialogs.rpy:3088
translate spanish matsuri_N2_407f6320:

    # "Nanami smiled at me..."
    "Nanami me sonrió..."

# game/dialogs.rpy:3090
translate spanish matsuri_N2_5b99a7c8:

    # "I escorted both Nanami and Toshio back to their house."
    "Los escolté a su casa."

# game/dialogs.rpy:3091
translate spanish matsuri_N2_750a3cb5:

    # "I handshaked Toshio and we smiled to each other."
    "Le di un apretón de manos a Toshio y nos sonreímos el uno al otro."

# game/dialogs.rpy:3092
translate spanish matsuri_N2_471cd43e:

    # toshio "You better not make her sad, %(stringhero)s-san!"
    toshio "Será mejor que no la hagas llorar, ¡%(stringhero)s-san!"

# game/dialogs.rpy:3093
translate spanish matsuri_N2_3fd749c7:

    # hero "I won't. I promise."
    hero "No lo haré. Lo prometo."

# game/dialogs.rpy:3094
translate spanish matsuri_N2_611bd54f:

    # "He entered the house."
    "Él entró a la casa."

# game/dialogs.rpy:3095
translate spanish matsuri_N2_0f0ea53d:

    # hero "Nanami, wait..."
    hero "Nanami, espera..."

# game/dialogs.rpy:3096
translate spanish matsuri_N2_943066fb:

    # n "{size=-5}I'm coming, big brother, hold on.{/size}"
    n "{size=-5}Voy entrando, hermano, espera un momento.{/size}"

# game/dialogs.rpy:3098
translate spanish matsuri_N2_b0f85b49:

    # n "Yes, %(stringhero)s-nii?"
    n "Si, ¿%(stringhero)s-nii?"

# game/dialogs.rpy:3099
translate spanish matsuri_N2_d6c71472:

    # hero "What I said in that moment... {w}It wasn't very enjoyable... {p}So I want to try again, here, right now."
    hero "Diciendo eso en el momento que hicimos...{w}No fue muy agradable...{p}Así que quiero tratar de nuevo, aquí, ahora mismo."

# game/dialogs.rpy:3100
translate spanish matsuri_N2_ce0929b4:

    # "I held her small hands."
    "Sostuve sus pequeñas manos."

# game/dialogs.rpy:3101
translate spanish matsuri_N2_6baf7cff:

    # hero "I love you, Nanami-chan."
    hero "Te amo, Nanami-chan."

# game/dialogs.rpy:3103
translate spanish matsuri_N2_f18aa4de:

    # "Nanami smiled and lovingly kissed my lips."
    "Nanami sonrió y me besó en mis labios cariñosamente."

# game/dialogs.rpy:3104
translate spanish matsuri_N2_30fea132:

    # n "I love you too, %(stringhero)s-nii."
    n "Te amo también, %(stringhero)s-nii."

# game/dialogs.rpy:3106
translate spanish matsuri_N2_6f7d9b3e:

    # "Then she went into her house."
    "Entonces ella entró a su casa."

# game/dialogs.rpy:3107
translate spanish matsuri_N2_75c7dfac:

    # "I sighed from relief... What a night..."
    "Suspiré del alivio... Que noche..."

# game/dialogs.rpy:3108
translate spanish matsuri_N2_abd512ab:

    # "My emotions were put to the test tonight."
    "Mis emociones han sido puestos a prueba,  esta noche."

# game/dialogs.rpy:3109
translate spanish matsuri_N2_a7393e9c:

    # "But I get the strange feeling that I'm not done yet..."
    "Pero siento que no he terminado con ellos..."

# game/dialogs.rpy:3110
translate spanish matsuri_N2_230f323d:

    # "That sense of dread was still in the back of my mind. Something else is about to come soon..."
    "Y que algo más esta por venir pronto..."

# game/dialogs.rpy:3130
translate spanish matsuri_R_040d4c37:

    # "When I arrived at the festival, I was joined by Rika-chan."
    "Cuando llegué al festival, me junté con Rika."

# game/dialogs.rpy:3131
translate spanish matsuri_R_42b57c94:

    # "She was wearing a very pretty purple yukata that showed her shoulders outside."
    "Ella estaba vistiendo una yukata púrpura muy bonito que mostraba sus hombros."

# game/dialogs.rpy:3132
translate spanish matsuri_R_a636fbfa:

    # "She was insanely sexy!"
    "Ella estaba especialmente sexi."

# game/dialogs.rpy:3134
translate spanish matsuri_R_6db6001a:

    # hero "Ah, Rika-chan! That yukata looks great on you!"
    hero "¡Ah, Rika-chan! ¡Estás muy sexi!"

# game/dialogs.rpy:3135
translate spanish matsuri_R_4ec8e433:

    # r "Hmph! Don't stare too much, pervert!"
    r "¡Hmph! ¡Pervertido!"

# game/dialogs.rpy:3136
translate spanish matsuri_R_8686ea2b:

    # hero "Where are the others?"
    hero "¿Dónde están las otras?"

# game/dialogs.rpy:3138
translate spanish matsuri_R_e7b4be7c:

    # r "Well... Sakura couldn't come."
    r "Bueno... Sakura no pudo venir."

# game/dialogs.rpy:3139
translate spanish matsuri_R_1457c98c:

    # r "She felt sick so she stayed home."
    r "Ella se sentía enferma así que se quedó en casa."

# game/dialogs.rpy:3140
translate spanish matsuri_R_f0ca7698:

    # r "And Nanami wasn't around either. Festivals aren't really her thing. Too much of a crowd."
    r "Y Nanami no esta por los alrededores tampoco. El... Festival nunca fue de su interés."

# game/dialogs.rpy:3141
translate spanish matsuri_R_4299e4c0:

    # hero "Aw damn, that really sucks... Guess it's just us two..."
    hero "Aww, eso realmente apesta..."

# game/dialogs.rpy:3142
translate spanish matsuri_R_42954815:

    # hero "Do you think we should pay them a visit after the festival?"
    hero "¿Piensas que es una buena idea que los visitemos después del festival?"

# game/dialogs.rpy:3143
translate spanish matsuri_R_ae397942:

    # r "Hmm..."
    r "Hmm..."

# game/dialogs.rpy:3145
translate spanish matsuri_R_67d6aa19:

    # r "That's a good idea."
    r "Buena idea."

# game/dialogs.rpy:3146
translate spanish matsuri_R_4a7cd128:

    # r "So, shall we grab a souvenir for them at the festival?"
    r "¡Entonces deberíamos de tomar unos recuerdos para ellas en el festival! "

# game/dialogs.rpy:3147
translate spanish matsuri_R_2bd890ea:

    # "I nodded in agreement."
    "Asentí."

# game/dialogs.rpy:3148
translate spanish matsuri_R_068f330c:

    # "We started to walk around the festival."
    "Entonces comenzamos a festejar..."

# game/dialogs.rpy:3152
translate spanish matsuri_R_19be20ef:

    # "We went to almost every stall, from the fishing game stands to every single food stand."
    "Hicimos casi todo, desde el puesto de pesca hasta los puestos de comida..."

# game/dialogs.rpy:3153
translate spanish matsuri_R_ff5174ca:

    # "Rika got pretty pissed off that I was better than her at some of the games."
    "Rika siempre estaba enojada de que yo era mejor que ella en algunos juegos."

# game/dialogs.rpy:3154
translate spanish matsuri_R_8a3d2cd4:

    # "When she eventually gave up, we continued to roam around. Finally, an outburst."
    "Eso comenzó a tornarse aburrido y finalmente, mientras deambulábamos alrededor de los puestos, ella finalmente alegó."

# game/dialogs.rpy:3156
translate spanish matsuri_R_8693e2bc:

    # r "Ugh, you know, you really don't know how to be a gentleman!"
    r "¡Tú realmente no sabes cómo ser un caballero!"

# game/dialogs.rpy:3157
translate spanish matsuri_R_6b9f9eed:

    # r "Didn't your mom ever teach you to respect women, city rat!?"
    r "¿¡¡Tu madre nunca te dijo que debes ser amable con las damas, rata de la ciudad!!?"

# game/dialogs.rpy:3158
translate spanish matsuri_R_d78833cc:

    # "What the heck is she talking about?! I got irritated with her."
    "Ahora estoy enfadado."

# game/dialogs.rpy:3159
translate spanish matsuri_R_23fae77a:

    # hero "Shut up! If you don't want to be with me then get lost. Nobody asked you to come!"
    hero "¡Oye, deja eso! ¡Si no querías tener esta cita, nadie te preguntó que vinieras!"

# game/dialogs.rpy:3160
translate spanish matsuri_R_848651e9:

    # r "I can leave anytime! I just decided to stay because I felt bad that Sakura-chan and Nanami-chan ditched a loser like you!"
    r "¡Lo mismo par ti, jum!"

# game/dialogs.rpy:3161
translate spanish matsuri_R_c4e42251:

    # r "You're the worst kind of guy! You're not even my type!"
    r "¡Tú no eres ni siquiera mi tipo de hombre de cualquier manera!"

# game/dialogs.rpy:3162
translate spanish matsuri_R_1527ed79:

    # hero "Fine! Whatever! You're not my type of woman either!!!"
    hero "¡Bien! ¡¡¡Tú no eres mi tipo de mujer tampoco!!!"

# game/dialogs.rpy:3163
translate spanish matsuri_R_08fc819f:

    # r "Fine!!!"
    r "¡¡¡Bien!!!"

# game/dialogs.rpy:3164
translate spanish matsuri_R_bb288886:

    # hero "Fine!!!"
    hero "¡¡¡Bien!!!"

# game/dialogs.rpy:3165
translate spanish matsuri_R_1a15f35b:

    # "We turned back against each other, sulking..."
    "Nos dimos las espaldas el uno al otro, enfurruñando..."

# game/dialogs.rpy:3166
translate spanish matsuri_R_a20cefa7:

    # "..."
    "..."

# game/dialogs.rpy:3167
translate spanish matsuri_R_b9e5c171:

    # "And after a few seconds..."
    "Y luego de solo 5 segundos..."

# game/dialogs.rpy:3168
translate spanish matsuri_R_ed6f129a:

    # "Something came over me. I don't know why...but..."
    "No sé porqué..."

# game/dialogs.rpy:3171
translate spanish matsuri_R_d9b9652e:

    # "We both turned around to face each other and we kissed wildly!"
    "¡Nos dimos la cara y nos besamos incontrolablemente!"

# game/dialogs.rpy:3172
translate spanish matsuri_R_0af6321e:

    # "It was a passionate, torrid kiss in front of a whole crowd of strangers!"
    "¡Un beso tórrido enfrente de todo el mundo!"

# game/dialogs.rpy:3173
translate spanish matsuri_R_5fbe52ef:

    # "When we stopped, she was looking at me with her pretty bi-colored eyes. She was blushing but she still looked pretty pissed off."
    "Cuando paramos, ella me estaba mirando con sus bonitos ojos bi-coloreados. Su expresión aún se miraba enojada, pero estaba sonrojándose."

# game/dialogs.rpy:3174
translate spanish matsuri_R_76e3f3b2:

    # hero "Ri... Rika-chan... I..."
    hero "Ri... Rika-chan... Yo..."

# game/dialogs.rpy:3175
translate spanish matsuri_R_cd9391ee:

    # r "Shut up! Just shut up and kiss me again!"
    r "¡Silencio! ¡Cállate y bésame!"

# game/dialogs.rpy:3183
translate spanish matsuri_R_83960144:

    # "And we kissed again."
    "Y nos besamos otra vez."

# game/dialogs.rpy:3184
translate spanish matsuri_R_0d89a6a1:

    # "...Rika took held my hand and guided me into the forest behind the temple."
    "Cuando términamos, ella sostuvo mi mano y me guió al bosque entorno al templo."

# game/dialogs.rpy:3188
translate spanish matsuri_R_02658004:

    # "Hidden by the bushes... we made intense love there."
    "Hicimos el amor en el bosque, escondidos en los arbustos..."

# game/dialogs.rpy:3189
translate spanish matsuri_R_862f50c1:

    # "It was crazy..."
    "Eso fue una locura..."

# game/dialogs.rpy:3190
translate spanish matsuri_R_91cf2fbf:

    # "I didn't think Rika was into me at all."
    "Como que ella no se miraba como que me amara."

# game/dialogs.rpy:3191
translate spanish matsuri_R_f1d355d1:

    # "And as strange as it looks, I thought I didn't like her at all either."
    "Y tan extraño como parezca, pensé que yo tampoco estaba amándola de esa manera."

# game/dialogs.rpy:3192
translate spanish matsuri_R_9540939d:

    # "Tsundere love is a really strange thing..."
    "Amor tsundere es realmente una cosa extraña..."

# game/dialogs.rpy:3193
translate spanish matsuri_R_9d8d28e3:

    # "I never thought that Rika, of all the people in the world, would be my first..."
    "Nunca adivinaría que haría esto con Rika..."

# game/dialogs.rpy:3194
translate spanish matsuri_R_0256c5ee:

    # "...Actually, even though it was both our first time, she was pretty good at it..."
    "Y en realidad, para una primera vez, ella es bastante buena en eso."

# game/dialogs.rpy:3198
translate spanish matsuri_R_bff9fa0e:

    # "We were planning to go back to our respective homes."
    "Estábamos a punto de volver a nuestras respectivas casas."

# game/dialogs.rpy:3200
translate spanish matsuri_R_975037cd:

    # r "Hey, I almost forgot!"
    r "Oye, ¡casi lo olvido!"

# game/dialogs.rpy:3201
translate spanish matsuri_R_09ea128c:

    # r "We should get a present and visit Sakura-chan and Nanami-chan."
    r "¡Deberíamos conseguir un regalo y visitar a Sakura ahora!"

# game/dialogs.rpy:3202
translate spanish matsuri_R_23aa15f6:

    # hero "Oh yeah, I almost forgot! Let's go!"
    hero "Je, es verdad, ¡vamos!"

# game/dialogs.rpy:3203
translate spanish matsuri_R_1f36ff66:

    # hero "I wonder how she'll react when she finds out about us!"
    hero "¡Me pregunto cómo va a reaccionar cuando ella sepa sobre nosotros!"

# game/dialogs.rpy:3205
translate spanish matsuri_R_c90d887e:

    # r "Haha, I bet she'll just think we're pulling a prank on her or-"
    r "Jeje, lo mismo digo."

# game/dialogs.rpy:3209
translate spanish matsuri_R_22d23a85:

    # hero "Huh? My cellphone? Who could that be?"
    hero "¿Mi teléfono celular? ¿quién podría ser?"

# game/dialogs.rpy:3213
translate spanish matsuri_R_e91d81d4:

    # hero "Hello? Who is it?"
    hero "¿Quién es?"

# game/dialogs.rpy:3214
translate spanish matsuri_R_0c279eeb:

    # s "{i}It's me, Sakura. How are you?{/i}"
    s "{i}Soy yo, Sakura, ¿cómo estás?{/i}"

# game/dialogs.rpy:3215
translate spanish matsuri_R_baf56ae6:

    # hero "Sakura-chan! Are you okay? I heard you were sick."
    hero "¡Sakura-chan! ¿Estás bien? escuché que estábas enferma."

# game/dialogs.rpy:3216
translate spanish matsuri_R_16bb0b38:

    # s "{i}I'm feeling better. Thank you, don't worry.{/i}"
    s "{i}Me estoy sintiendo mejor. Gracias, no te preocupes.{/i}"

# game/dialogs.rpy:3217
translate spanish matsuri_R_cafbc553:

    # s "{i}I'm actually at Nana-chan's house. We were playing some video games together.{/i}"
    s "{i}En realidad estoy en la casa de Nana-chan. Jugamos algunos juegos juntas.{/i}"

# game/dialogs.rpy:3218
translate spanish matsuri_R_e322e7d4:

    # hero "Oh alright! I'm glad you feel better!"
    hero "Esta bien."

# game/dialogs.rpy:3219
translate spanish matsuri_R_311bdbc8:

    # hero "Well hey, we were about to come visit you. So I guess we'll meet you at Nanami-chan's?"
    hero "Bueno, estábamos por visitarte. ¿Entonces supongo que nos veremos en la casa de Nanami-chan?"

# game/dialogs.rpy:3220
translate spanish matsuri_R_cb76d6a6:

    # s "{i}Sure! We'll wait right here for you!{/i}"
    s "{i}¡Claro! ¡Los esperaremos aquí!{/i}"

# game/dialogs.rpy:3222
translate spanish matsuri_R_1713379a:

    # r "Sakura's feeling better?"
    r "¿Era Sakura?"

# game/dialogs.rpy:3223
translate spanish matsuri_R_ea44e64c:

    # hero "Yeah. She's feeling better and she's at Nanami-chan's house."
    hero "Si. Ella se está sintiendo mejor y está en la casa de Nanami-chan."

# game/dialogs.rpy:3224
translate spanish matsuri_R_17259851:

    # r "Let's go visit them then!"
    r "¡Entonces vamos ahí!"

# game/dialogs.rpy:3229
translate spanish matsuri_R_2f5a201e:

    # "Nanami's room was a bit of a mess."
    "El cuarto de Nanami estaba viéndose un poco desordenado. "

# game/dialogs.rpy:3230
translate spanish matsuri_R_53129eea:

    # "Her computer setup took up most of the space, besides her bed."
    "Excepto por su cama, la parte más grande de su cuarto era su computadora. Algo de la última generación."

# game/dialogs.rpy:3231
translate spanish matsuri_R_992fbe8e:

    # "There was also an old TV with several gaming consoles plugged in it."
    "También había una vieja TV con varias consolas de videojuegos conectados a ella."

# game/dialogs.rpy:3232
translate spanish matsuri_R_3b146827:

    # "It was cramped but, it seemed homely for Nanami."
    "Eso fue un desorden pero un impresionante desorden."

# game/dialogs.rpy:3233
translate spanish matsuri_R_61c51bff:

    # "I told the both of them about Rika and I. Sakura couldn't believe it when she heard the news!"
    "¡Sakura no podía creerlo cuando escuchó las noticias!"

# game/dialogs.rpy:3234
translate spanish matsuri_R_c281b0de:

    # "Nanami was in disbelief and was looking at us with a funny face."
    "Nanami no podía ni creerlo y estaba mirándonos con una cara graciosa."

# game/dialogs.rpy:3238
translate spanish matsuri_R_586a170e:

    # s "That's incredible!"
    s "¡Es increíble!"

# game/dialogs.rpy:3239
translate spanish matsuri_R_72ae68c4:

    # n "Seriously, guys!!"
    n "¡¡¡En serio, chicos!!!"

# game/dialogs.rpy:3240
translate spanish matsuri_R_5bfe62d0:

    # s "I'm so happy for both of you!"
    s "¡Estoy tan feliz por ambos!"

# game/dialogs.rpy:3241
translate spanish matsuri_R_391891ff:

    # s "I'd never guess you two would end up together! Teehee!"
    s "¡Nunca adivinaría que ustedes estuvieran juntos! ¡Teehee!"

# game/dialogs.rpy:3242
translate spanish matsuri_R_04b5e9a4:

    # hero "Haha, I have to admit, me neither!"
    hero "Jeje, admito, ¡que yo tampoco!"

# game/dialogs.rpy:3243
translate spanish matsuri_R_2a5f51c2:

    # r "Hmph, it's true. I didn't think I'd fall for him either...He's still an idiot though..."
    r "¡Eso es verdad, con un idiota como él!"

# game/dialogs.rpy:3244
translate spanish matsuri_R_73e2cbfb:

    # hero "Whatever, you're the one whose blushing..."
    hero "¡¡¡Oye!!!"

# game/dialogs.rpy:3245
translate spanish matsuri_R_34e95488:

    # "Sakura giggled as she watched us bicker as usual."
    "Sakura se rió mientras nos miraba peleando como usualmente lo hacemos."

# game/dialogs.rpy:3246
translate spanish matsuri_R_885f1f06:

    # "Looks like our new relationship didn't change anything about the way we communicate."
    "Parece que nuestra nueva relación no cambió nada sobre la forma en que nos comunicamos."

# game/dialogs.rpy:3247
translate spanish matsuri_R_7a965734:

    # "But that's okay with me..."
    "Eso está bien conmigo. Estoy acostumbrado a ello..."

# game/dialogs.rpy:3254
translate spanish matsuri_R_0cee0472:

    # "I woke up slowly."
    "Desperté lentamente."

# game/dialogs.rpy:3255
translate spanish matsuri_R_b2766fe2:

    # "Looking at the alarm clock, it's kinda late."
    "Mirando al reloj, es algo tarde."

# game/dialogs.rpy:3256
translate spanish matsuri_R_f87aab37:

    # "I felt something warm against me..."
    "Sentí algo cálido en contra mía..."

# game/dialogs.rpy:3257
translate spanish matsuri_R_2d8ea27f:

    # "It was Rika."
    "Era Rika."

# game/dialogs.rpy:3258
translate spanish matsuri_R_8d3f93d7:

    # "She was fast asleep, naked in my arms."
    "Ella estaba durmiendo desnuda en mis brazos..."

# game/dialogs.rpy:3259
translate spanish matsuri_R_b91ebafe:

    # "I let out a smile as I held her tight."
    "Sonreí y la miré dormir..."

# game/dialogs.rpy:3260
translate spanish matsuri_R_880d8986:

    # "After visiting Sakura and Nanami, I offered her \"to have a drink.\""
    "Después de visitar a Sakura y Nanami, le propuse a \"Tomar un último trago\" en mi casa y ella aceptó."

# game/dialogs.rpy:3261
translate spanish matsuri_R_87283b9b:

    # "She responded in an incredibly seductive way, \"It's not that I want to sleep at your place...I'm just thirsty...\""
    "Y ella añadió, \"¡No es que quiera dormir en tu casa; solo estoy sedienta!\"."

# game/dialogs.rpy:3262
translate spanish matsuri_R_d65a0334:

    # "We made intense and passionate love twice during the night."
    "Entonces hicimos el amor durante la noche. Dos veces."

# game/dialogs.rpy:3263
translate spanish matsuri_R_3e893f32:

    # "It's funny how much of a tsundere cliché she can be sometimes. But I like it a lot."
    "Es gracioso cuánto de un cliché tsundere puede ser a veces. Pero me gusta."

# game/dialogs.rpy:3264
translate spanish matsuri_R_0726d127:

    # "I pet Rika's hair tenderly. Her hair was longer than I thought with her pigtails removed."
    "Acaricié el cabello de Rika tiernamente. Su cabello parecía más largo de lo que pensé, cuando se desató sus coletas..."

# game/dialogs.rpy:3265
translate spanish matsuri_R_c6b79cfc:

    # "She finally opened her eyes. As she saw me, she smiled and sat on the bed..."
    "Ella finalmente abrió sus ojos. Mientras me miraba, sonrió y se sentó en la cama..."

# game/dialogs.rpy:3266
translate spanish matsuri_R_044a4f8a:

    # "She was still half asleep when she spoke."
    "Su voz aún estaba un poco somnolienta."

# game/dialogs.rpy:3267
translate spanish matsuri_R_ddaea844:

    # r "Hmmmm... Did you have a good nap, city rat?"
    r "Hmmmm... ¿Has dormido bien, rata de la ciudad?"

# game/dialogs.rpy:3268
translate spanish matsuri_R_ffcb2dd8:

    # hero "Geez, even if we're in a relationship, even with you naked in my own bed, you'll still keep calling me that for the rest of your life huh?"
    hero "Geez, ¿incluso si somos una pareja de amantes, incluso contigo desnuda en mi propia cama, me sigues llamando así?"

# game/dialogs.rpy:3269
translate spanish matsuri_R_51e9effd:

    # r "Mmmm stop complaining... You should be thankful to have a girl like me as your lover..."
    r "Mmm deja de quejarte por eso. Deberías de estar agradecido de tener una chica como yo como amante..."

# game/dialogs.rpy:3270
translate spanish matsuri_R_13bdddc3:

    # "I gave a small laugh and kissed her. She began to embrace me..."
    "Me reí un poco y la besé en sus labios. Ella respondió al beso, abrazándome..."

# game/dialogs.rpy:3271
translate spanish matsuri_R_a20cefa7_1:

    # "..."
    "..."

# game/dialogs.rpy:3272
translate spanish matsuri_R_b072efdd:

    # "I suddenly realized something:{p}My parents!!!"
    "De pronto, un pensamiento pasó por mi mente:{p}¡¡¡Mis padres!!!"

# game/dialogs.rpy:3273
translate spanish matsuri_R_361d12e9:

    # "Oh shit, they're surely back from the festival!!!"
    "¡¡¡Seguramente ellos han vuelto del festival!!!"

# game/dialogs.rpy:3274
translate spanish matsuri_R_37e91653:

    # "They'll kill me if they see that I brought a girl home, let alone in my bed!!!"
    "¿¡Qué es lo que van a decir si me ven con una chica en mi cama!?"

# game/dialogs.rpy:3275
translate spanish matsuri_R_ead2c864:

    # "What should I do? Dammit, dammit!!!"
    "¿Qué debería hacer? ¡¡¡Maldición, maldición!!!"

# game/dialogs.rpy:3276
translate spanish matsuri_R_a7bc91fd:

    # "I listened for a moment, waiting for an indication that would tell me if my parents were here."
    "Esperé por un momento, esperando por un ruido que me diría que mis padres están aquí."

# game/dialogs.rpy:3277
translate spanish matsuri_R_06130763:

    # "Nothing..."
    "Nada."

# game/dialogs.rpy:3278
translate spanish matsuri_R_bb28b60c:

    # "Rika looked a bit confused...that or she was still half asleep."
    pass

# game/dialogs.rpy:3279
translate spanish matsuri_R_c4b1d086:

    # hero "Rika-chan..."
    hero "Rika-chan..."

# game/dialogs.rpy:3280
translate spanish matsuri_R_9e07d6af:

    # hero "My parents aren't there. You should go home now before they come back."
    hero "Mis padres no están aquí. Deberías ir a casa ahora antes de que ellos lleguen."

# game/dialogs.rpy:3281
translate spanish matsuri_R_272a8b83:

    # hero "My parents are kinda strict when it comes to love relationships."
    hero "¡Mis padres son algo estrictos cuando se trata de relaciones amorosas!"

# game/dialogs.rpy:3282
translate spanish matsuri_R_7bf71025:

    # "Rika nodded."
    "Rika asintió."

# game/dialogs.rpy:3283
translate spanish matsuri_R_22236077:

    # r "Alright... I understand. My parents would react the same in this kinda situation..."
    r "Esta bien... Entiendo eso. Mis padres estarían igual en esta situación..."

# game/dialogs.rpy:3285
translate spanish matsuri_R_fb307b0d:

    # "I helped her put on her yukata and took her to her house..."
    "Luego de un tiempo, la ayudé a ponerse su yukata y la llevé a su casa..."

# game/dialogs.rpy:3286
translate spanish matsuri_R_5ffe88e0:

    # "We promised to have another date very soon..."
    "Nos prometimos que tendríamos otra cita muy pronto..."

# game/dialogs.rpy:3287
translate spanish matsuri_R_17899d6b:

    # "I went back home and turned on my computer."
    "Regresé a mi casa y encendí mi computadora."

# game/dialogs.rpy:3291
translate spanish matsuri_R_2b150c3e:

    # write "Dear sister,"
    write "Querida hermana,"

# game/dialogs.rpy:3293
translate spanish matsuri_R_92248cbb:

    # write "How are things?"
    write "¿Cómo van las cosas?"

# game/dialogs.rpy:3295
translate spanish matsuri_R_54b9d70d:

    # write "You'll never guess what happened!"
    write "¡No podrás adivinar lo que me pasó!"

# game/dialogs.rpy:3297
translate spanish matsuri_R_4967d059:

    # write "I'm going out with a girl!"
    write "¡Estoy saliendo con una chica!"

# game/dialogs.rpy:3299
translate spanish matsuri_R_c293cd3c:

    # write "Her name is Rika. She's a strange girl. It's a bit hard to understand she's really thinking.{w} She seems to mock me all the time, but we're really into each other."
    write "Ella se llama Rika. Ella es una chica extraña. Es un poco difícil entender lo que realmente piensa.{w} Ella parece que se burla de mí todo el tiempo, pero nos amamos..."

# game/dialogs.rpy:3301
translate spanish matsuri_R_1773278b:

    # write "I'm happy... Really happy... I can't wait to introduce her to you someday! {w}I'll send you a photo of us. She's incredibly gorgeous."
    write "Estoy feliz... Realmente feliz... ¡No puedo esperar a presentártela, algún día!{w}Te mandaré una foto de nosotros, pronto, ¡entonces lo verás! Ella es muy bonita."

# game/dialogs.rpy:3302
translate spanish matsuri_R_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:3304
translate spanish matsuri_R_c42ad76e:

    # write "I hope everything is going great for you as well!"
    write "Espero que todo esté bien para ti también y que seas tan feliz como yo lo soy..."

# game/dialogs.rpy:3306
translate spanish matsuri_R_f60fe613:

    # write "Your dear brother{p}%(stringhero)s"
    write "Tu querido hermano{p}%(stringhero)s"

# game/dialogs.rpy:3308
translate spanish matsuri_R_91609d26:

    # write "PS:{w} The local summer festival was great! I hope you had a wonderful event like this in Tokyo."
    write "PD:{w} ¡El festival de Verano local fue grandioso! Espero que hayas tenido un maravilloso evento como este en Tokyo."

# game/dialogs.rpy:3309
translate spanish matsuri_R_76b2fe88_1:

    # nvl clear
    nvl clear

# game/dialogs.rpy:3330
translate spanish scene8_66fd8474:

    # centered "{size=+35}FINAL CHAPTER\nI wish I was a real girl{fast}{/size}"
    centered "{size=+35}CÁPITULO FINAL\nDesearía haber sido una chica real{fast}{/size}"

# game/dialogs.rpy:3336
translate spanish scene8_26da46e6:

    # centered "Three days after the festival..."
    centered "Tres días después."

# game/dialogs.rpy:3339
translate spanish scene8_3193f804:

    # "It was late in the evening. I was in front of my computer, surfing on the web..."
    "Esa noche, estaba enfrente de mi computadora, navegando aleatoriamente en la red..."

# game/dialogs.rpy:3341
translate spanish scene8_216308f6:

    # "My cellphone rang."
    "Repentinamente, mi teléfono celular sonó."

# game/dialogs.rpy:3345
translate spanish scene8_dbd7e6ae:

    # hero "Hello?"
    hero "¿Si?"

# game/dialogs.rpy:3346
translate spanish scene8_5354350a:

    # r "{i}%(stringhero)s?{/i}"
    r "{i}¿%(stringhero)s?{/i}"

# game/dialogs.rpy:3348
translate spanish scene8_30e755be:

    # hero "What's cooking, love?"
    hero "¿Qué se está cociendo, amor?"

# game/dialogs.rpy:3350
translate spanish scene8_6d9c1d81:

    # hero "Yeah, Rika-chan?"
    hero "¿Si, Rika-chan?"

# game/dialogs.rpy:3351
translate spanish scene8_237675bd:

    # r "{i}Come to my house right now! It's very important!{/i}"
    r "{i}¡Ven a mi casa ahora mismo! ¡Es muy importante!{/i}"

# game/dialogs.rpy:3353
translate spanish scene8_0bf1cc8f:

    # r "{i}It's about Sakura!{/i}"
    r "{i}¡Es sobre Sakura!{/i}"

# game/dialogs.rpy:3354
translate spanish scene8_eec796bb:

    # hero "What's going on?"
    hero "¿Qué está pasando?"

# game/dialogs.rpy:3355
translate spanish scene8_87546516:

    # r "{i}Just come!{/i}"
    r "{i}¡Solo ven!{/i}"

# game/dialogs.rpy:3357
translate spanish scene8_495869e3:

    # "*{i}clic{/i}*"
    "*{i}clic{/i}*"

# game/dialogs.rpy:3359
translate spanish scene8_4f4c3c02:

    # "What could have happened to Sakura? Oh man, I hope she's okay."
    "¡Oh Dios, no Sakura también!"

# game/dialogs.rpy:3361
translate spanish scene8_8a4ee6ed:

    # "I switched off my computer, took my bicycle and went to her house, worried to death."
    "Apagué mi computadora, tomé mi bicicleta y me fuí a su casa, extremadamete preocupado."

# game/dialogs.rpy:3362
translate spanish scene8_a60f3c18:

    # "It was late in the night and the little rural streets of the village were a bit scary in the dark."
    "Era tarde en la noche y la pequeña calle rural del pueblo era un poco escalofriante en la oscuridad."

# game/dialogs.rpy:3363
translate spanish scene8_85e64de3:

    # "Now isn't the time to be scared of the small things."
    "Pero no me importó en lo absoluto."

# game/dialogs.rpy:3365
translate spanish scene8_b7c54990:

    # "My love needs help!"
    "¡Mi amor me necesita!"

# game/dialogs.rpy:3367
translate spanish scene8_ab18a7cd:

    # "My friend needs help!"
    "¡Mi amiga me necesita!"

# game/dialogs.rpy:3370
translate spanish scene8_993a8f06:

    # "*{i}Knock knock{/i}*"
    "*{i}Knock knock{/i}*"

# game/dialogs.rpy:3372
translate spanish scene8_6d6e7ebc:

    # "The door of the house opened and Rika-chan appeared."
    "La puerta de la casa se abrió y Rika apareció."

# game/dialogs.rpy:3373
translate spanish scene8_68bac721:

    # "She looked very worried and upset, as I imagined."
    "Ella se veía muy preocupada y enojada, como me imaginé."

# game/dialogs.rpy:3375
translate spanish scene8_47dbdd39:

    # r "Ah, there you are. Come i-{nw}"
    r "Ah, ahí estás. Ven a-{nw}"

# game/dialogs.rpy:3377
translate spanish scene8_27651eaf:

    # "I didn't give her any time to show me where Sakura-chan was."
    "No le di ninguna oportunidad de mostrarme dónde estaba Sakura."

# game/dialogs.rpy:3378
translate spanish scene8_0a632bcf:

    # "I instinctively ran into the house and searched for her."
    "Instintivamente corrí hacia dentro de la casa y la busqué."

# game/dialogs.rpy:3380
translate spanish scene8_c676b3c1:

    # "I found her in the living room."
    "La encontré en la sala."

# game/dialogs.rpy:3382
translate spanish scene8_37ffbdd8:

    # r "Ah, there you are. Come in, quickly."
    r "Ah, Ahí estás. Ven adentro, rápido."

# game/dialogs.rpy:3384
translate spanish scene8_616170d4:

    # "I followed Rika-chan into her house, to the living room."
    "Seguí a Rika-chan hacia dentro de la casa, a la sala."

# game/dialogs.rpy:3386
translate spanish scene8_b8ce5033:

    # "Near the kotatsu, there was Sakura sitting on her knees, wearing her pajamas."
    "Cerca del kotatsu, ahí estaba Sakura sentada en sus rodillas, usando sus pijamas."

# game/dialogs.rpy:3387
translate spanish scene8_c44364bc:

    # "Her long hair was hiding her face, but she looked shocked."
    "Su largo cabello estaba ocultando su rostro, pero se veía consternada."

# game/dialogs.rpy:3388
translate spanish scene8_d47b58c5:

    # "Thank heavens, she's alive."
    "Gracias a Dios, ella está viva."

# game/dialogs.rpy:3389
translate spanish scene8_c89376fe:

    # hero "Sakura-chan! Are you okay?"
    hero "¡Sakura-chan! ¿Estás bien?"

# game/dialogs.rpy:3390
translate spanish scene8_1740f374:

    # "She turned her face to me, speechless."
    "Ella volteó su rostro hacia mí, perpleja."

# game/dialogs.rpy:3391
translate spanish scene8_15ef4c6c:

    # "She had a large bruise on her cheek and tears were flowing slowly from her sweet blue eyes."
    "Ella tenía un gran moretón en su mejilla y lágrimas estaban flujendo lentamente de sus dulces ojos azules."

# game/dialogs.rpy:3392
translate spanish scene8_d0d64bc6:

    # hero "Oh..."
    hero "Oh..."

# game/dialogs.rpy:3393
translate spanish scene8_74d2762c:

    # hero "Oh no..."
    hero "Oh no..."

# game/dialogs.rpy:3394
translate spanish scene8_6d9e86b7:

    # hero "Who the heck did this to you?!"
    hero "¿¡¡Quién es el cabrón que le hizo esto!!?"

# game/dialogs.rpy:3396
translate spanish scene8_67ac6839:

    # "Rika arrived at the living room."
    "Rika llegó a la sala."

# game/dialogs.rpy:3397
translate spanish scene8_4fc2e4fc:

    # hero "Rika-chan, what happened? Was it the gang from school again?"
    hero "Rika-chan, ¿Qué pasó? ¿Fue el matón de la escuela otra vez?"

# game/dialogs.rpy:3398
translate spanish scene8_391ed42e:

    # "Rika shook her head."
    "Rika sacudió su cabeza."

# game/dialogs.rpy:3399
translate spanish scene8_93c707d1:

    # r "Her father did this."
    r "Su padre le hizo esto."

# game/dialogs.rpy:3400
translate spanish scene8_03cc985d:

    # hero "What!?{p}Her father!?"
    hero "¿¡Qué!?{p}¿¡Su padre!?"

# game/dialogs.rpy:3401
translate spanish scene8_7bf71025:

    # "Rika nodded."
    "Rika asintió."

# game/dialogs.rpy:3402
translate spanish scene8_6da77c2c:

    # r "He was drunk, he hit her and even broke her violin..."
    r "Él estaba borracho, él le pegó e incluso le quebró su violin..."

# game/dialogs.rpy:3403
translate spanish scene8_75cef802:

    # r "Her mother helped her to flee the house. So she came to me. When she told me what happened, I called you."
    r "Su madre la ayudó a escapar de la casa. Así que ella vino aquí. Cuando ella me contó lo que pasó, te llamé."

# game/dialogs.rpy:3404
translate spanish scene8_5820756e:

    # r "She needs both of us, more than anything..."
    r "Ella nos necesita a ambos, más que nada..."

# game/dialogs.rpy:3405
translate spanish scene8_f4f4126a:

    # hero "Does Nanami-chan know?"
    hero "¿Nanami-chan lo sabe?"

# game/dialogs.rpy:3406
translate spanish scene8_65b003bb:

    # r "Not yet. I prefer not to worry her."
    r "Aún no. Prefiero no preocuparla."

# game/dialogs.rpy:3407
translate spanish scene8_8f451a85:

    # r "She cares a lot for Sakura but she's very fragile."
    r "Ella le importa mucho Sakura pero ella es muy frágil."

# game/dialogs.rpy:3408
translate spanish scene8_35bf5a71:

    # "I nodded."
    "Asentí."

# game/dialogs.rpy:3409
translate spanish scene8_5bd541e7:

    # "From what I saw, I could see that she really considers Sakura and Rika like her sisters."
    "Aprendí por lo que he visto que ella realmente considera a Sakura y Rika como sus hermanas."

# game/dialogs.rpy:3410
translate spanish scene8_a20cefa7:

    # "..."
    "..."

# game/dialogs.rpy:3411
translate spanish scene8_380894b8:

    # "Anyway, Sakura's father surely did this because he was still pissed that his boy is a girl inside."
    "De cualquier modo, el padre de Sakura seguramente hizo esto porque él aún estaba molesto de que su hijo es una chica por dentro."

# game/dialogs.rpy:3413
translate spanish scene8_63e75ac3:

    # "Or maybe he was pissed because Sakura gave away her yukata for men. I've heard that he bought it for her in the hopes of making her a man."
    "O tal vez él estaba molesto porque Sakura regaló su yukata para hombres. He escuchado que él lo compró para ella con la esperanza de hacerla un hombre."

# game/dialogs.rpy:3414
translate spanish scene8_0745cc53:

    # "I felt enraged."
    "Sentí un fuerte enojo."

# game/dialogs.rpy:3415
translate spanish scene8_1a6c86d8:

    # "But actually, I had no idea of what to do."
    "Pero en realidad, no tenía idea de qué hacer."

# game/dialogs.rpy:3420
translate spanish scene8_91aef672:

    # "We stayed at Rika-chan's home for the night."
    "Estuvimos en la casa de Rika-chan por la noche."

# game/dialogs.rpy:3421
translate spanish scene8_7267642d:

    # "But I wanted to avenge my friend."
    "Pero quería vengar a mi amiga."

# game/dialogs.rpy:3422
translate spanish scene8_0057c44c:

    # "I had to go there and talk to her father!"
    "¡Tengo que ir ahí y hablar con su padre!"

# game/dialogs.rpy:3423
translate spanish scene8_8e59b631:

    # "While they were both asleep, I stood up and went outside with my bicycle, on my way to Sakura's house."
    "Mientras que ellas estaban dormidas, me levanté y me fuí con mi bicicleta, en mi camino a la casa de Sakura."

# game/dialogs.rpy:3426
translate spanish scene8_b1145ef5:

    # "In front of the house, there was her father."
    "Enfrente de la casa, estaba su padre."

# game/dialogs.rpy:3427
translate spanish scene8_8fb982f8:

    # "He was mumbling some gibberish and was holding a bottle of sake."
    "Él estaba murmurando algunas incoherencias y estaba sosteniendo un botella de sake."

# game/dialogs.rpy:3428
translate spanish scene8_d9426232:

    # "As I could guess, he was still drunk."
    "Como lo podía suponer, él aún estaba borracho."

# game/dialogs.rpy:3429
translate spanish scene8_185ef70f:

    # hero "Hey you!"
    hero "¡Oye tú!"

# game/dialogs.rpy:3430
translate spanish scene8_cb6740c6:

    # sdad "Wh--- Who are you?!"
    sdad "Qui---¡¿Quién eres?!"

# game/dialogs.rpy:3432
translate spanish scene8_032d6f9c:

    # hero "Name's %(stringhero)s... I'm the boyfriend of your daughter! Her boyfriend!!!"
    hero "Mi nombre es %(stringhero)s... ¡Soy el novio de tu hija! ¡¡¡Su novio!!!"

# game/dialogs.rpy:3434
translate spanish scene8_e3dcc113:

    # hero "Name's %(stringhero)s... I'm a friend of your daughter!"
    hero "Mi nombre es %(stringhero)s... ¡Soy un amigo de tu hija!"

# game/dialogs.rpy:3435
translate spanish scene8_18e3dade:

    # sdad "*{i}hiccups{/i}* I don't have any d-d-daughter... I only have a son!..."
    sdad "*{i}hiccups{/i}* No tengo ninguna hi-hi-hija... ¡Solo tengo un hijo!..."

# game/dialogs.rpy:3436
translate spanish scene8_9fd9385d:

    # sdad "And my son is a freaking pervert who dresses like a girl, that's all!!!"
    sdad "Y mi hijo es un engendro pervertido que se viste como una chica, ¡¡¡eso es todo!!!"

# game/dialogs.rpy:3437
translate spanish scene8_4576c2b6:

    # hero "SHUT UP!!!"
    hero "¡¡¡CÁLLATE!!!"

# game/dialogs.rpy:3438
translate spanish scene8_f199e2fa:

    # "I shouted so loud, some dogs in the neighborhood started to bark."
    "Grité tan fuerte que algunos perros en el vecindario comenzaron a ladrar."

# game/dialogs.rpy:3439
translate spanish scene8_bb4f1d08:

    # hero "Whether he is a girl or a boy,... he's your child!! Your own flesh and blood!!!"
    hero "Si él es una chica o un chico... ¡¡¡Él es tu hijo!!! ¡¡¡Tu propia carne y sangre!!!"

# game/dialogs.rpy:3440
translate spanish scene8_90a161cb:

    # hero "You must accept him as he is!"
    hero "¡Debes aceptarlo como es!"

# game/dialogs.rpy:3441
translate spanish scene8_042b0d72:

    # hero "Sakura accepts her father like he is, so why don't you do the same!!?"
    hero "Sakura acepta a su padre como es, ¿¡¡entonces porqué no haces lo mismo!!?"

# game/dialogs.rpy:3442
translate spanish scene8_16e9224f:

    # sdad "*{i}hiccups{/i}*"
    sdad "*{i}hiccups{/i}*"

# game/dialogs.rpy:3443
translate spanish scene8_a67faa19:

    # sdad "I d-d-d-on't have any lessons to take from a kid!"
    sdad "¡N-n-n-no tengo que tomar ninguna lección de un niño!"

# game/dialogs.rpy:3444
translate spanish scene8_5f87fa70:

    # "He started to shout too. Some lights turned on in the neighborhood, awakened by the argument."
    "Él comenzó a gritar también. Algunas luces se encendieron en el vecindario, activadas por la discusión."

# game/dialogs.rpy:3445
translate spanish scene8_4d26840c:

    # "Sakura's mother came out from the house and stood by the porch. She had some bruises on her body and her eyes were red... she must have been crying a lot."
    "La madre de Sakura salió de su casa y se quedó parada por la entrada. Ella tenía algunos moretones en su cuerpo y se miraba que ha llorado mucho."

# game/dialogs.rpy:3446
translate spanish scene8_3a555d15:

    # "She seemed scared and was watching the scene quietly."
    "Ella se miraba asustada y estaba mirando la escena calladamente."

# game/dialogs.rpy:3447
translate spanish scene8_380ead38:

    # "Looks like Sakura wasn't the only one who got hurt..."
    "Parece que Sakura no fue la única que fue lastimada..."

# game/dialogs.rpy:3448
translate spanish scene8_4e5e9fd5:

    # "Her father broke his empty bottle, held it like a knife and grinned."
    "Su padre rompió su botella vacía, lo agarró como un cuchillo y sonrió."

# game/dialogs.rpy:3449
translate spanish scene8_23076b47:

    # sdad "Now get out or I'm gonna stab you... you gay brat!"
    sdad "Ahora vete o te apuñalaré... ¡Tú mocoso gay!"

# game/dialogs.rpy:3450
translate spanish scene8_415034f0:

    # "Shit, is this guy for real?!"
    "Oye, ¡¡¡este tipo me está asustando ahora!!!"

# game/dialogs.rpy:3451
translate spanish scene8_5ac5be9a:

    # "He began to charge at me brandishing the broken glass. I was paralyzed with fear."
    "Él corrió hacia mí rápidamente. Estaba impresionado de que un borracho como él pudiera ser tan rápido."

# game/dialogs.rpy:3452
translate spanish scene8_22ee59f4:

    # sdad "{size=+10}Go to hell!!{/size}"
    sdad "{size=+10}¡¡!Vete al infierno!!{/size}"

# game/dialogs.rpy:3453
translate spanish scene8_f5b011b4:

    # "Just as he closed in, someone jumped in front of me."
    "Justo en el momento que estaba cerca de él, vi una sombra enfrente de mí."

# game/dialogs.rpy:3456
translate spanish scene8_1d392ab1:

    # "It was a deafening sound.{p}I heard a painful moan.{p}It resonated in my head even after it was done.."
    "Un sonido ensordecedor.{p}Un gemido de dolor cerca de mí.{p}Un grito a lo lejos."

# game/dialogs.rpy:3457
translate spanish scene8_6ead108f:

    # "Time stopped. I just remember being pushed to the floor."
    "Sentí un empujón y caí en el suelo."

# game/dialogs.rpy:3460
translate spanish scene8_1bd19034:

    # "When I came back to my senses, I saw Sakura's father flinched back, shaken and shocked. "
    "Cuando regresé a mis sentidos, vi a su padre retroceder, temblando y perplejo."

# game/dialogs.rpy:3461
translate spanish scene8_770343cb:

    # "The broken bottle he was holding fell on the ground and shattered."
    "La botella rota que él estaba sosteniendo cayó en el suelo en un ruido de vidrio roto."

# game/dialogs.rpy:3462
translate spanish scene8_2cf1e3ba:

    # "The pieces were stained with blood."
    "Algunas partes del arma fueron manchadas de sangre."

# game/dialogs.rpy:3464
translate spanish scene8_43934e89:

    # "On top of me, there was the body of Sakura."
    "En mi regazo, estaba el cuerpo de Sakura."

# game/dialogs.rpy:3465
translate spanish scene8_9e470804:

    # "Her stomach was bleeding profusely."
    "Su abdomen estaba sangrando profusamente."

# game/dialogs.rpy:3466
translate spanish scene8_fc381d0a:

    # hero "Sakura!!!"
    hero "¡¡¡Sakura!!!"

# game/dialogs.rpy:3467
translate spanish scene8_c04fe468:

    # hero "{size=+15}Sakura!!! Hang on!!! Sakura!!!{/size}"
    hero "{size=+15}¡¡¡Sakura!!! ¡¡¡Resiste!!! ¡¡¡Sakura!!!{/size}"

# game/dialogs.rpy:3468
translate spanish scene8_281954cf:

    # "I started to feel tears in my eyes."
    "Comencé a sentir lágrimas en mis ojos."

# game/dialogs.rpy:3469
translate spanish scene8_b46a96ab:

    # "She saved me by sacrificing herself."
    "Ella me salvó sacrificándose ella misma."

# game/dialogs.rpy:3470
translate spanish scene8_2e1ee92a:

    # hero "{size=+15}Please, don't die, Sakura!!!{/size}"
    hero "{size=+15}Por favor, no te mueras, ¡¡¡Sakura!!!{/size}"

# game/dialogs.rpy:3471
translate spanish scene8_6f1c6352:

    # "Sakura weakly turned her head to me."
    "Sakura débilmente volteó su cabeza hacia mí."

# game/dialogs.rpy:3473
translate spanish scene8_22286f8f:

    # "I caressed her cheek tenderly, removing a tear, and kissing her lips softly, my own tears preventing me to see how beautiful she looked, even in that state."
    "Acaricié su mejilla tiernamente, removiendo una lágrima, y besando sus labios suavemente, mis propias lágrimas me impedian ver lo hermosa que se miraba, incluso en ese estado."

# game/dialogs.rpy:3474
translate spanish scene8_1204b721:

    # s "..........{p}......%(stringhero)s....kun....."
    s "..........{p}......%(stringhero)s....kun....."

# game/dialogs.rpy:3476
translate spanish scene8_001b31dc:

    # s "I'm... so happy to... be... your girlfriend......"
    s "Estoy... Tan feliz de... Ser... Tu novia......"

# game/dialogs.rpy:3477
translate spanish scene8_d6ab4c63:

    # s "I... love you... so much,...%(stringhero)s....kun...."
    s "Te... Amo... Tanto...%(stringhero)s....kun...."

# game/dialogs.rpy:3478
translate spanish scene8_a2f900d8:

    # hero "And I love you, Sakura, more than anything!"
    hero "Y yo te amo, Sakura, ¡más que a nada!"

# game/dialogs.rpy:3479
translate spanish scene8_04745b3b:

    # hero "That's why you must stay alive!! Stay with me!!"
    hero "¡¡Y por eso debes estar viva!! ¡¡Quédate conmigo!!"

# game/dialogs.rpy:3480
translate spanish scene8_e8a331c8:

    # s "I..."
    s "Yo..."

# game/dialogs.rpy:3482
translate spanish scene8_379966c6:

    # s "T... Too bad it...{w}didn't worked...between us..."
    s "Q...Qué mal...{w}que no funcionó... entre nosotros..."

# game/dialogs.rpy:3483
translate spanish scene8_362ff53c:

    # s "But that's okay...{p}You... and the others...{w} took care of me so nicely..."
    s "Pero esta bien...{p}Tú... Y las otras...{w} Me cuidaron muy bien..."

# game/dialogs.rpy:3484
translate spanish scene8_0868d776:

    # s "I'm...so happy to... be... {w}your friend......"
    s "Estoy... Tan feliz de... Ser...{w}tu amiga......"

# game/dialogs.rpy:3485
translate spanish scene8_f3331226:

    # s "T... Thank...... you..."
    s "G...Gra......cias...."

# game/dialogs.rpy:3487
translate spanish scene8_a58e1fda:

    # s "I regret I... didn't had the time... to tell Nanami-chan... the truth..."
    s "Lamento que yo... No haya tenido el tiempo... de contarle a Nanami-chan... La verdad..."

# game/dialogs.rpy:3488
translate spanish scene8_f8ac1185:

    # s "You know..."
    s "Sabes..."

# game/dialogs.rpy:3490
translate spanish scene8_cb894c8f:

    # s "I wish......{p}I was...{p}a real girl..."
    s "Desearía....{p}Haber sido...{p}Una chica real..."

# game/dialogs.rpy:3492
translate spanish scene8_ccf33f5d:

    # "She smiled at me, then she fainted in my arms."
    "Ella me sonrió, entonces se desmayó en mis brazos."

# game/dialogs.rpy:3496
translate spanish scene8_8e8bdb99:

    # hero "Sakura!!!!!!{p}{size=+15}Sakura!!! Answer me!!!{/size}"
    hero "¡¡¡¡¡¡¡Sakura!!!!!!{p}{size=+15}¡¡¡Sakura!!! ¡¡¡Respóndeme!!!{/size}"

# game/dialogs.rpy:3497
translate spanish scene8_43e0fc44:

    # hero "{size=+20}SAKURAAAAAAAAAAAAAAA!!!!!!{/size}"
    hero "{size=+20}¡¡¡¡¡SAKURAAAAAAAAAAAAAAA!!!!!!{/size}"

# game/dialogs.rpy:3501
translate spanish scene8_7a909493:

    # "The police and the ambulance arrived shortly after, with Rika-chan."
    "La policía y la ambulancia llegaron un poco depués de eso, con Rika-chan."

# game/dialogs.rpy:3502
translate spanish scene8_dd0ebba0:

    # "There was still a pulse inside Sakura's body, but she was badly hurt."
    "Había aún pulso dentro del cuerpo de Sakura, pero estaba muy malherida."

# game/dialogs.rpy:3503
translate spanish scene8_913d018c:

    # "The police arrested her father for attempted homicide and domestic violence."
    "La policía arrestó a su padre por intento de homicidio y violencia."

# game/dialogs.rpy:3504
translate spanish scene8_53b8960a:

    # "Sakura's mom went with her in the ambulance."
    "La madre de Sakura estuvo con ella en la ambulancia."

# game/dialogs.rpy:3506
translate spanish scene8_8f2b17c9:

    # "I stayed there with Rika-chan for a moment... when everyone was gone."
    "Estuve ahí con Rika-chan por un momento... Cuando todos se fueron."

# game/dialogs.rpy:3508
translate spanish scene8_e09c696e:

    # "I cried in Rika-chan's arms, uncontrollably."
    "Lloré en los brazos de Rika-chan, incontrolablemente."

# game/dialogs.rpy:3509
translate spanish scene8_c44c2f72:

    # hero "Sakura... No, damnit, not Sakura..."
    hero "Sakura... No, querido Dios, no Sakura..."

# game/dialogs.rpy:3510
translate spanish scene8_68b58967:

    # "Rika was sobbing as well."
    "Rika estaba llorando un poco también."

# game/dialogs.rpy:3511
translate spanish scene8_c6f7844b:

    # "When we calmed down a little, she spoke."
    "Cuando finalmente paramos, ella habló."

# game/dialogs.rpy:3512
translate spanish scene8_d30a40a6:

    # r "You know... {p}I don't know how to say it..."
    r "Sabes... {p}No sé cómo decirlo..."

# game/dialogs.rpy:3513
translate spanish scene8_906477c5:

    # r "But I think it's the right time to tell you..."
    r "Pero pienso que es el tiempo correcto para decirtelo..."

# game/dialogs.rpy:3514
translate spanish scene8_ea880f58:

    # r "Before you came to our club... I was in love with Sakura..."
    r "Antes de que vinieras a nuestro club... Estaba enamorado de Sakura..."

# game/dialogs.rpy:3515
translate spanish scene8_6c7d6f4e:

    # hero "You were? With Sakura-chan?"
    hero "¿Estabas? ¿De Sakura-chan?"

# game/dialogs.rpy:3516
translate spanish scene8_73d36031:

    # r "Yes..."
    r "Si..."

# game/dialogs.rpy:3517
translate spanish scene8_1a4eb0ff:

    # r "I was attracted by this boy who is so girly..."
    r "Estaba atraída por este chico quien es tan femenina..."

# game/dialogs.rpy:3518
translate spanish scene8_e94b0b53:

    # r "After that, you came,... {size=-15}and it changed...{/size}"
    r "Luego de eso, tu viniste... {size=-15}y eso lo cambió...{/size}"

# game/dialogs.rpy:3519
translate spanish scene8_402f030d:

    # r "By the way, you know why I consider Sakura as my best friend, now..."
    r "Por cierto, ya conoces porqué considero a Sakura como mi mejor amiga ahora..."

# game/dialogs.rpy:3520
translate spanish scene8_e668a3b6:

    # hero "I see..."
    hero "Ya veo..."

# game/dialogs.rpy:3521
translate spanish scene8_7030c013:

    # r "But don't worry... I was shocked when I heard about you two, but..."
    r "Pero no te preocupes... Estaba perpleja cuando escuché sobre lo de ustedes dos, pero..."

# game/dialogs.rpy:3522
translate spanish scene8_09ad0db3:

    # r "I was genuinely happy for the both of you."
    r "Estaba genuinamente feliz por ambos."

# game/dialogs.rpy:3523
translate spanish scene8_39b70c91:

    # r "I never seen Sakura so happy before. You changed her life so much..."
    r "Nunca vi a Sakura tan feliz antes. Tú cambiaste mucho su vida..."

# game/dialogs.rpy:3524
translate spanish scene8_c0eddcd4:

    # r "It's why I'm not mad at you... Sakura's happiness is all that matters..."
    r "Por esa razón es que no estoy enojada contigo... La felicidad de Sakura es todo lo que importa..."

# game/dialogs.rpy:3525
translate spanish scene8_190df487:

    # r "And now..."
    r "Y ahora..."

# game/dialogs.rpy:3526
translate spanish scene8_9a33713f:

    # r "I just hope...{w}That she will be alright!"
    r "Solo espero...{w}¡Que ella esté bien!"

# game/dialogs.rpy:3527
translate spanish scene8_0bd0684e:

    # "She started to weep too. I had never seen her like that before."
    "Ella comenzó a llorar también. Nunca la he visto así antes."

# game/dialogs.rpy:3528
translate spanish scene8_0fb4909e:

    # "I held her in my arms gently."
    "La abracé en mis brazos gentilmente."

# game/dialogs.rpy:3532
translate spanish scene8_f4f6d6da:

    # r "Are you okay, hun?"
    r "¿Estás bien, cariño?"

# game/dialogs.rpy:3534
translate spanish scene8_422d11ba:

    # r "Are you okay?"
    r "¿Estás bien?"

# game/dialogs.rpy:3535
translate spanish scene8_e8331494:

    # hero "I'm fine... Horribly anxious but fine..."
    hero "Estoy bien... Horriblemente ansioso pero bien..."

# game/dialogs.rpy:3536
translate spanish scene8_f8b52a34:

    # "Rika stayed silent for a moment. But finally, she cried in my arms."
    "Rika estuvo en silencio por un momento. Pero finalmente, ella lloró en mis brazos."

# game/dialogs.rpy:3537
translate spanish scene8_a4d9e75c:

    # r "I'm so scared... I don't want to loose Sakura...!"
    r "Estoy tan asustada... ¡No quiero perder a Sakura...!"

# game/dialogs.rpy:3539
translate spanish scene8_103c3486:

    # hero "I know, babe... Me neither..."
    hero "Lo sé, bebé... Yo tampoco..."

# game/dialogs.rpy:3541
translate spanish scene8_bfb9febc:

    # hero "I know... Me neither..."
    hero "Lo sé... Yo tampoco..."

# game/dialogs.rpy:3542
translate spanish scene8_5546a02e:

    # "She cried for a long time in my arms."
    "Ella lloró por un largo tiempo en mis brazos."

# game/dialogs.rpy:3543
translate spanish scene8_46acf1af:

    # "When she stopped, she spoke."
    "Cuando se detuvo, ella habló."

# game/dialogs.rpy:3544
translate spanish scene8_72e004e1:

    # r "You know... She told me that..."
    r "Sabes... Ella me dijo que..."

# game/dialogs.rpy:3545
translate spanish scene8_72694f8f:

    # r "Sakura told me that she loved you..."
    r "Sakura me dijo que ella te amaba..."

# game/dialogs.rpy:3546
translate spanish scene8_1ae45319:

    # hero "Ah?"
    hero "¿Ah?"

# game/dialogs.rpy:3547
translate spanish scene8_6ea028e4:

    # "I had guessed it since the car incident, some days ago."
    "Lo supuse desde el incidente del carro, hace algunos días."

# game/dialogs.rpy:3548
translate spanish scene8_4e396aba:

    # "I had the feeling that she was in love with me, sort-of..."
    "Tenía el sentimiento de que ella estaba enamorada de mí, algo..."

# game/dialogs.rpy:3549
translate spanish scene8_77b8faa3:

    # r "She knew that it was an impossible love since you're straight...{p}But she had always loved you with all her heart..."
    r "Ella sabía que eso era un amor imposible puesto que eres heterosexual...{p}Pero siempre te ha amado con todo su corazón..."

# game/dialogs.rpy:3551
translate spanish scene8_5f050a71:

    # r "And to be honest... It's thanks to her that we are going out together now."
    r "Y para ser honesta... Es gracias a ella que estamos saliendo juntos ahora."

# game/dialogs.rpy:3552
translate spanish scene8_9fcae4dd:

    # hero "What?"
    hero "¿Qué?"

# game/dialogs.rpy:3553
translate spanish scene8_601c81c7:

    # hero "But...{p}How? {w}Wh-{nw}"
    hero "Pero...{p}¿Cómo? {w}Qu-{nw}"

# game/dialogs.rpy:3554
translate spanish scene8_b37d1c72:

    # r "She was the one who organized everything in our first date, at the festival, with the help of Nanami."
    r "Ella fue la que organizó todo en nuestra primera cita, en el festival, con la ayuda de Nanami."

# game/dialogs.rpy:3555
translate spanish scene8_4a5bf62a:

    # r "It didn't happen as she expected it, but the result was the same, in the end."
    r "No pasó como ella lo esperaba, pero el resultado fue el mismo, al final."

# game/dialogs.rpy:3556
translate spanish scene8_1900c1b3:

    # hero "That's why they weren't there at the festival?"
    hero "¿Esa fue la razón de que ellas no estuvieran ahí en el festival?"

# game/dialogs.rpy:3557
translate spanish scene8_bec3b4af:

    # r "Yes."
    r "Si."

# game/dialogs.rpy:3558
translate spanish scene8_6220eee7:

    # r "I wasn't very okay about the idea first, but she knew that..."
    r "No estaba muy de acuerdo con la idea al principio, pero ella sabía que..."

# game/dialogs.rpy:3559
translate spanish scene8_a0391de0:

    # "She blushed."
    "Ella se sonrojó."

# game/dialogs.rpy:3560
translate spanish scene8_4232035f:

    # r "That I had... feelings for you too..."
    r "Que tenía... Sentimientos por ti también..."

# game/dialogs.rpy:3561
translate spanish scene8_65088067:

    # "I smiled. Tears started to come again from my eyes."
    "Sonreí. Lágrimas comenzaron a brotar otra vez de mis ojos."

# game/dialogs.rpy:3562
translate spanish scene8_33789624:

    # hero "She wanted us to be all together."
    hero "Ella quería que todos nosotros estuviéramos juntos."

# game/dialogs.rpy:3563
translate spanish scene8_b16b0eb8:

    # hero "That she will be in our relationship like a best unique friend... Like a sister to each of us..."
    hero "Que ella estaría en nuestra relación como una única mejor amiga... Como una hermana para cada uno de nosotros..."

# game/dialogs.rpy:3564
translate spanish scene8_e66f523f:

    # hero "The sister who is ready to abandon everything...just for the happiness of her best friends..."
    hero "La hermana quien estaba lista para abandonar todo... Solo por la felicidad de sus mejores amigos..."

# game/dialogs.rpy:3565
translate spanish scene8_f1b0dae1:

    # "She was in love with me... Being lovers was impossible but her friendship and love for me never stopped..."
    "Ella estaba enamorada de mí... Ser amantes era imposible pero su amistad y amor por mí nunca se detuvo..."

# game/dialogs.rpy:3566
translate spanish scene8_cfdac421:

    # "And she even went to create happiness between me and Rika just because she loved us..."
    "Y ella incluso creó felicidad entre yo y Rika solo porque ella nos amaba..."

# game/dialogs.rpy:3567
translate spanish scene8_0bd26214:

    # "It was my turn to cry, thinking about that adorable angel Sakura."
    "Era mi turno para llorar, pensando sobre esa adorable ángel Sakura."

# game/dialogs.rpy:3569
translate spanish scene8_f1b0dae1_1:

    # "She was in love with me... Being lovers was impossible but her friendship and love for me never stopped..."
    "Ella estaba enamorada de mí... Ser amantes era imposible pero su amistad y amor por mí nunca se detuvo..."

# game/dialogs.rpy:3570
translate spanish scene8_86a7a846:

    # "It was my turn to cry, thinking about Sakura's kind heart."
    "Era mi turno para llorar, pensando en el amable corazón de Sakura."

# game/dialogs.rpy:3571
translate spanish scene8_01146bd8:

    # "At the same time, I felt like a jerk..."
    "Al mismo tiempo, me sentí como un cretino..."

# game/dialogs.rpy:3572
translate spanish scene8_23b3af63:

    # "Rika-chan held me tight in her arms, rubbing my hair."
    "Rika me sostuvó entre sus brazos, acariciando mi cabello."

# game/dialogs.rpy:3573
translate spanish scene8_f0b3fd2d:

    # "I was confused. It's not something she would do normally..."
    "Estaba confuso. No es algo que ella haría normalmente..."

# game/dialogs.rpy:3574
translate spanish scene8_2e4a7bba:

    # "But I felt better in her arms..."
    "Pero me sentí mejor en sus brazos..."

# game/dialogs.rpy:3575
translate spanish scene8_d1802fa3:

    # "The night was silent..."
    "La noche era silenciosa..."

# game/dialogs.rpy:3582
translate spanish scene9_e9b7540d:

    # "The next day at school was gloomy for me, Nanami and Rika."
    "Al día siguiente en la escuela fue sombrío para mí, Nanami y Rika."

# game/dialogs.rpy:3584
translate spanish scene9_43a31284:

    # "At the club, we were just sitting... doing nothing, saying nothing."
    "En el club, estábamos solo sentados... Haciendo nada, diciendo nada."

# game/dialogs.rpy:3585
translate spanish scene9_d9d4a59c:

    # "The only noise we could hear was Nanami's crying."
    "El único ruido que escuchamos era el llanto de Nanami."

# game/dialogs.rpy:3586
translate spanish scene9_de531cea:

    # "She couldn't stop crying when she heard the news..."
    "Ella no podía parar de llorar cuando escuchó las noticias..."

# game/dialogs.rpy:3587
translate spanish scene9_748065c0:

    # "The club was silent without Sakura..."
    "El club estaba en silencio sin Sakura..."

# game/dialogs.rpy:3589
translate spanish scene9_be3ea4ed:

    # "Finally I spoke as I stood up, smashing my fist on the table."
    "Finalmente hablé mientras me paraba, azotando mi puño en la mesa."

# game/dialogs.rpy:3591
translate spanish scene9_7a7f745d:

    # hero "I can't stay here like this! I have to the hospital and get news about Sakura!"
    hero "¡No puedo estar aquí así! ¡Debo ir al hospital y tener noticas sobre Sakura!"

# game/dialogs.rpy:3592
translate spanish scene9_cf4c0510:

    # "Rika-chan smiled, determined, and stood up as well."
    "Rika-chan sonrió, determinada, y se levantó también."

# game/dialogs.rpy:3593
translate spanish scene9_00b28479:

    # r "You're right, kid! Let's go there right now!"
    r "¡Tienes razón, chico! ¡Vamos ahora mismo!"

# game/dialogs.rpy:3594
translate spanish scene9_611208b8:

    # r "Nobody said that the manga club would leave one of its members alone!"
    r "¡Nadie dijo que el club de manga dejará a uno de sus miembros solos!"

# game/dialogs.rpy:3595
translate spanish scene9_3966d3ed:

    # "I smiled. I at last found the Rika-chan I knew."
    "Sonreí. Finalmente encontré a la Rika-chan que conocía."

# game/dialogs.rpy:3597
translate spanish scene9_551012d0:

    # "Finally, Rika-chan stood up, determined."
    "Finalmente, Rika-chan se levantó, determinada."

# game/dialogs.rpy:3599
translate spanish scene9_3cf11638:

    # r "I can't stay here like this! I must go to the hospital and get news about Sakura-chan!"
    r "¡No puedo estar aquí así! ¡Debo de ir al hospital y conseguir noticias sobre Sakura-chan!"

# game/dialogs.rpy:3600
translate spanish scene9_567a77f3:

    # "I nodded and stood up as well."
    "Asentí y me levanté también."

# game/dialogs.rpy:3601
translate spanish scene9_00c27679:

    # hero "You're right! Let's go right now!"
    hero "¡Tienes razón! ¡Vámonos ahora mismo!"

# game/dialogs.rpy:3602
translate spanish scene9_2fe99060:

    # "Nanami didn't say a word, but she stopped crying, dried her tears and followed us with a cute determined face."
    "Nanami no dijo ni una palabra, pero dejó de llorar, secó sus lágrimas y nos siguió con una linda cara determinada."

# game/dialogs.rpy:3604
translate spanish scene9_243a70b8:

    # hero "So, Doctor, how is Sakura?"
    hero "Entonces, Doctor, ¿cómo está Sakura?"

# game/dialogs.rpy:3606
translate spanish scene9_9eefb10b:

    # "Doctor" "Oh, don't worry, kids. Your friend is fine, now!"
    "Doctor" "Oh, no se preocupen, niños. ¡Su amigo está bien ahora!"

# game/dialogs.rpy:3607
translate spanish scene9_063c6c0a:

    # "Doctor" "We managed to save him right on time, thanks to his mother's call!"
    "Doctor" "Pudimos salvarlo justo a tiempo, ¡gracias a la llamada de su madre!"

# game/dialogs.rpy:3608
translate spanish scene9_24b1777f:

    # "We couldn't believe it! We heaved a sigh of relief."
    "¡No lo podíamos creerlo! Suspiramos todos del alivio."

# game/dialogs.rpy:3609
translate spanish scene9_f6dd7e91:

    # "Doctor" "He should be ready in a few days."
    "Doctor" "Él debería estar listo en unos días."

# game/dialogs.rpy:3610
translate spanish scene9_e7741f1a:

    # "Doctor" "He can have visitors, if you want to see him."
    "Doctor" "Él puede tener visitantes, si lo quieren ver."

# game/dialogs.rpy:3611
translate spanish scene9_61585f71:

    # "We nodded and followed the doctor to Sakura's hospital room."
    "Asentimos y seguimos al doctor hacia el cuarto del hospital donde estaba Sakura."

# game/dialogs.rpy:3613
translate spanish scene9_81c4b68b:

    # "Her face shone as she saw us."
    "Su rostro brillo mientras que nos veía."

# game/dialogs.rpy:3614
translate spanish scene9_9b4114a6:

    # s "%(stringhero)s-kun!! Rika-chan!! Nana-chan!!"
    s "¡¡%(stringhero)s-kun!! ¡¡Rika-chan!! ¡¡Nana-chan!!"

# game/dialogs.rpy:3615
translate spanish scene9_417ded14:

    # n "Sakura-nee!!!"
    n "¡¡¡Sakura-nee!!!"

# game/dialogs.rpy:3616
translate spanish scene9_c0cdba56:

    # "Nanami jumped on the bed to hug Sakura."
    "Nanami saltó a la cama para abrazar a Sakura."

# game/dialogs.rpy:3617
translate spanish scene9_5fcae85e:

    # "She flinched a little bit by the pain but hugged Nanami back, rubbing her head like child."
    "Se estremeció un poco por el dolor pero abrazó a Nanami de vuelta, acariciando su cabeza como si se tratara de una niña."

# game/dialogs.rpy:3618
translate spanish scene9_06d60896:

    # s "Easy, little one... I need time for the wound to heal."
    s "Calma, pequeña... Necesito tiempo para que la herida sane."

# game/dialogs.rpy:3619
translate spanish scene9_52c6a554:

    # n "I'm not little!"
    n "¡No soy pequeña!"

# game/dialogs.rpy:3620
translate spanish scene9_00bc78d2:

    # "She said that while going back to crying. But of joy this time."
    "Ella dijo eso mientras volvía a llorar. Pero esta vez de alegria."

# game/dialogs.rpy:3621
translate spanish scene9_f46ea2ef:

    # hero "Oh my gosh... Sakura-chan...{p}I'm so happy you survived!"
    hero "Oh mi Dios... Sakura-chan...{p}¡Estoy tan feliz de que sobrevivieras!"

# game/dialogs.rpy:3622
translate spanish scene9_b9968b56:

    # hero "I was so scared that you...!"
    hero "¡Estaba tan asustado de que tú...!"

# game/dialogs.rpy:3623
translate spanish scene9_1508b046:

    # "She seemed to be really okay, as if nothing ever happened."
    "Ella se veía bien realmente, como si nada hubiera pasado."

# game/dialogs.rpy:3625
translate spanish scene9_b8aeb0be:

    # s "I will not leave this planet where I can have the best boyfriend ever!"
    s "¡No dejaré el planeta donde puedo tener al mejor novio!"

# game/dialogs.rpy:3626
translate spanish scene9_0d3e5a44:

    # s "As well as the best friends!"
    s "¡También como a las mejores amigas!"

# game/dialogs.rpy:3628
translate spanish scene9_0ac19270:

    # s "I will not leave this planet where I can have my best friends!"
    s "¡No dejaré el planeta donde puedo tener a los mejores amigos!"

# game/dialogs.rpy:3629
translate spanish scene9_fadc1602:

    # r "We all love you, Sakura! {image=heart.png}"
    r "¡Todos te amamos, Sakura! {image=heart.png}"

# game/dialogs.rpy:3630
translate spanish scene9_ab6bc56f:

    # "Sakura smiled to us."
    "Sakura nos sonrió."

# game/dialogs.rpy:3631
translate spanish scene9_c7659595:

    # s "I love you all too... So much..."
    s "Yo también los amo... Tanto..."

# game/dialogs.rpy:3633
translate spanish scene9_dad43788:

    # hero "You know, Sakura-chan..."
    hero "Sabes, Sakura-chan..."

# game/dialogs.rpy:3634
translate spanish scene9_1acb2356:

    # hero "I feel I was a terrible friend to you..."
    hero "Siento que fui un terrible amigo para ti..."

# game/dialogs.rpy:3635
translate spanish scene9_8d593ce9:

    # hero "Please forgive me..."
    hero "Por favor perdóname..."

# game/dialogs.rpy:3636
translate spanish scene9_79003247:

    # "Sakura put a finger on my lips."
    "Sakura puso un dedo en mis labios."

# game/dialogs.rpy:3637
translate spanish scene9_69c6fa27:

    # s "Hush! Stop talking nonsense."
    s "¡Hush! deja de estar hablando disparates."

# game/dialogs.rpy:3638
translate spanish scene9_e9594450:

    # s "You faced my father to defend me and you were almost ready to die for that!"
    s "¡Te enfrentaste a mi padre para defenderme y casi estabas listo para morir por ello!"

# game/dialogs.rpy:3639
translate spanish scene9_7b24500a:

    # s "How can you possibly think you're an awful friend?"
    s "¿Cómo puedes pensar que eres un terrible amigo?"

# game/dialogs.rpy:3640
translate spanish scene9_0cde7c21:

    # s "You're my best friend!"
    s "¡Eres mi mejor amigo!"

# game/dialogs.rpy:3641
translate spanish scene9_993b25b5:

    # "Tears rolled down my eyes."
    "Algunas lágrimas estaban brotando de mis ojos. Sonreí."

# game/dialogs.rpy:3642
translate spanish scene9_1cce67e1:

    # hero "You're an angel, Sakura-chan."
    hero "Eres un ángel, Sakura-chan."

# game/dialogs.rpy:3644
translate spanish scene9_57b35236:

    # r "Did your mom visit you?"
    r "¿Tu madre te ha visitado?"

# game/dialogs.rpy:3645
translate spanish scene9_85c4e509:

    # s "Yes she did."
    s "Si lo hizo."

# game/dialogs.rpy:3646
translate spanish scene9_a3b8f1b9:

    # s "She's a bit sad that father was taken to jail,... {w}But I think she can't love him anymore after what he did."
    s "Ella está un poco triste de que mi padre haya sido llevado a la cárcel...{w}Pero pienso que ella ya no puede amarlo después de lo que hizo."

# game/dialogs.rpy:3647
translate spanish scene9_f1990ad2:

    # s "I don't know what to think either. {w}It's a relief that he won't bother me and mom anymore...{w}but he's still my father..."
    s "No sé en qué pensar tampoco. {w}Es un alivio que él ya no vaya a molestar a mamá y a mí...{w}Pero aún es mi padre..."

# game/dialogs.rpy:3648
translate spanish scene9_05ffe314:

    # hero "Don't worry, Sakura-chan. We'll all be here for you!"
    hero "No te preocupes, Sakura-chan. Todos te ayudaremos con ello."

# game/dialogs.rpy:3649
translate spanish scene9_0c126df5:

    # hero "I don't know how yet, but we'll find out!"
    hero "¡Aún no sé cómo, pero lo descubriremos!"

# game/dialogs.rpy:3650
translate spanish scene9_363e4438:

    # r "Yes, we will!"
    r "¡Si, lo haremos!"

# game/dialogs.rpy:3651
translate spanish scene9_6a9ed22c:

    # n "Yeah!"
    n "¡Si!"

# game/dialogs.rpy:3652
translate spanish scene9_eecde369:

    # "A tear of joy rolled down Sakura's cheek as she smiled."
    "Una lágrima de alegría recorrió en la mejilla de Sakura mientras sonreía."

# game/dialogs.rpy:3653
translate spanish scene9_cb78f15e:

    # s "I know you all do."
    s "Sé que lo harán."

# game/dialogs.rpy:3654
translate spanish scene9_e926c122:

    # s "Thank you..."
    s "Gracias..."

# game/dialogs.rpy:3655
translate spanish scene9_81a1d957:

    # "Since Rika-chan brought some manga with us, we did our club activities with Sakura in her room."
    "Puesto que Rika-chan llevó algunos mangas con nosotros, finalmente hicimos nuestras actividades del club con Sakura-chan en su cuarto."

# game/dialogs.rpy:3656
translate spanish scene9_87845d82:

    # "That was fun."
    "Eso fue divertido."

# game/dialogs.rpy:3660
translate spanish scene9_b859f96a:

    # "I was so happy..."
    "Estaba tan feliz..."

# game/dialogs.rpy:3661
translate spanish scene9_4055f551:

    # "I think my luck helped me again..."
    "Pienso que mi suerte me ayudó otra vez..."

# game/dialogs.rpy:3662
translate spanish scene9_9095e36e:

    # "But this time, it helped Sakura too."
    "Pero esta vez, ayudó a Sakura también."

# game/dialogs.rpy:3665
translate spanish scene9_8dbb56a9:

    # n "Why the doctor said \"he\" to Sakura?"
    n "¿Porqué el doctor dijo \"él\" a Sakura?"

# game/dialogs.rpy:3666
translate spanish scene9_ad0764fc:

    # "Uh-oh..."
    "Uh-oh..."

# game/dialogs.rpy:3667
translate spanish scene9_ff11d5e2:

    # "Well, better late than never."
    "Bueno, mejor tarde que nunca."

# game/dialogs.rpy:3668
translate spanish scene9_984c292d:

    # "Since there probably wouldn't be any other better moment for that, We told Nanami about Sakura's secret."
    "Puesto que probablemente no habría otra mejor oportunidad para ello, le contamos a Nanami sobre el secreto de Sakura."

# game/dialogs.rpy:3669
translate spanish scene9_fcc6280e:

    # "She took it well and understood. Her feelings for Sakura didn't changed at all. Just as I expected of her."
    "Ello se lo tomó bien y entendió. Sus sentimientos por Sakura no cambiaron para nada. Justo lo que esperé de ella."

# game/dialogs.rpy:3670
translate spanish scene9_a20cefa7:

    # "..."
    "..."

# game/dialogs.rpy:3671
translate spanish scene9_19a38d50:

    # "Though, for a whole week, she randomly said \"Seriously, guys!!\" multiple times!"
    "Aunque, por una semana entera, ella aleatoriamente decía \"¡¡En serio, chicos!!\" ¡múltiples veces!"

# game/dialogs.rpy:3672
translate spanish scene9_7c3cef17:

    # "I think she was a bit upset that we kept the secret to us until now."
    "Pienso que ella estaba un poco molesta que le hayamos guardado el secreto hasta ahora."

# game/dialogs.rpy:3673
translate spanish scene9_0bfbc1f4:

    # "We promised it was the last time we kept a secret from Nanami."
    "Prometimos que era la última vez que guardamos un secreto a Nanami."

# game/dialogs.rpy:3677
translate spanish end_d9a3a0bf:

    # "Summer vacation came."
    "Las vacaciones de Verano terminaron."

# game/dialogs.rpy:3678
translate spanish end_d7bb0ba9:

    # "To all of us, school was over until next September."
    "Para todos nosotros, la escuela estaba finalizada hasta el próximo Septiembre."

# game/dialogs.rpy:3683
translate spanish end_d95c02cb:

    # "Love can be really strange..."
    "El amor puede ser realmente un extraño sentimiento..."

# game/dialogs.rpy:3684
translate spanish end_c409fabd:

    # "You think you know what kind of person you'll end up with but..."
    "Piensas que sabes qué clase de compañera amarías."

# game/dialogs.rpy:3685
translate spanish end_5eba593d:

    # "Love at first sight can hit anybody..."
    "Pero el amor a primera vista puede golpear a cualquiera..."

# game/dialogs.rpy:3686
translate spanish end_617e0a41:

    # "I was pretty sure I would only love real girls..."
    "Estaba muy seguro que solo amaría a chicas reales..."

# game/dialogs.rpy:3687
translate spanish end_e3e4774b:

    # "And my first girlfriend is actually a very girlish boy!"
    "¡Y mi primera novia es actualmente un chico muy femenino!"

# game/dialogs.rpy:3688
translate spanish end_6292a02f:

    # "Hehe, who could have predicted this?{p}Not me, for sure..."
    "Jeje, ¿quién podría haber predicho esto?{p}Yo no, eso es seguro..."

# game/dialogs.rpy:3689
translate spanish end_e4e1f13a:

    # "I don't know how much time we will stay together, Sakura and I."
    "No sé cuánto tiempo estaremos juntos, Sakura y yo."

# game/dialogs.rpy:3690
translate spanish end_32aa38be:

    # "Months? Years? Maybe for the rest of our lives...?"
    "¿Meses? ¿Años? ¿Tal vez por el resto de nuestras vidas...?"

# game/dialogs.rpy:3691
translate spanish end_478778fd:

    # "I'm not sure of what the future holds for us..."
    "No estoy seguro de qué es lo que el futuro nos aguarda..."

# game/dialogs.rpy:3692
translate spanish end_d076867a:

    # "But if my story with Sakura must end someday,{w} I'll never forget the wonderful times I passed with her."
    "Pero si mi historia con Sakura debe terminar algún día,{w} nunca olvidaré el maravilloso tiempo que pasé con ella."

# game/dialogs.rpy:3693
translate spanish end_7dbb760e:

    # "Or him..."
    "O él..."

# game/dialogs.rpy:3694
translate spanish end_54a608c6:

    # "Who cares..."
    "A quién le importa..."

# game/dialogs.rpy:3695
translate spanish end_85655125:

    # s "Honey! What are you doing? Come see the view!"
    s "¡Cariño! ¿Qué estás haciendo? ¡Ven a ver la vista!"

# game/dialogs.rpy:3696
translate spanish end_7c43af43:

    # hero "I'm coming, sweetie!"
    hero "¡Ya voy, dulzura!"

# game/dialogs.rpy:3703
translate spanish end_59a65d95:

    # "We're waiting at our plane at the airport."
    "Estamos esperando nuestro vuelo en el aeropuerto."

# game/dialogs.rpy:3704
translate spanish end_ae3dd256:

    # "Nanami and I decided to take some vacations at Nanami's homeland, Okinawa."
    "Nanami y yo decidimos tomar vacaciones en la tierra natal de Nanami, Okinawa."

# game/dialogs.rpy:3705
translate spanish end_d12ce3e4:

    # "My head was full of ondo and traditional music, like at the festival."
    "Mi cabeza estaba lleno de ondos y música tradicional, como en el festival."

# game/dialogs.rpy:3706
translate spanish end_4452fe89:

    # "Toshio escorted us to the airport.{p}Rika and Sakura were here too."
    "Toshio nos escoltó al aeropuerto.{p}Rika y Sakura estaban aquí también."

# game/dialogs.rpy:3707
translate spanish end_8b58bec9:

    # "It's been a while that Rika and Sakura started to going out together, now."
    "Ha pasado un tiempo desde que Rika y Sakura comenzaron a salir juntas."

# game/dialogs.rpy:3708
translate spanish end_e30bea45:

    # "They look like a cute couple of girls."
    "Ellas se miran como una linda pareja de chicas."

# game/dialogs.rpy:3709
translate spanish end_ca017532:

    # toshio "Tell our grand-parents that I love them."
    toshio "Diles a nuestros abuelos que los amo."

# game/dialogs.rpy:3710
translate spanish end_a7442fed:

    # n "I will, don't worry."
    n "Lo haré, no te preocupes."

# game/dialogs.rpy:3711
translate spanish end_d3330439:

    # toshio "%(stringhero)s-san... Take good care of my sister."
    toshio "%(stringhero)s-san... Cuida de mi hermana."

# game/dialogs.rpy:3712
translate spanish end_a831d3fe:

    # hero "I will."
    hero "Lo haré."

# game/dialogs.rpy:3713
translate spanish end_17ebd900:

    # hero "And you, good luck with your new job."
    hero "Y tú, buena suerte con tu nuevo trabajo."

# game/dialogs.rpy:3714
translate spanish end_5b348dc8:

    # "My parents hired him at the grocery shop."
    "Mis padres lo contrataron en la tienda de abarrotes."

# game/dialogs.rpy:3715
translate spanish end_16f4a6f4:

    # "That way, he can renew with his past while keeping going straight forward to his new adult life."
    "De esa forma, él puede renovarse con su pasado mientras va hacia adelante con su nueva vida adulta."

# game/dialogs.rpy:3716
translate spanish end_95d64dcd:

    # s "Take a lot of photos, %(stringhero)s-kun! The whole place is beautiful, it deserve photographs!"
    s "Toma muchas fotos, ¡%(stringhero)s-kun! ¡El lugar entero es hermoso, se merece fotografías!"

# game/dialogs.rpy:3717
translate spanish end_8f710e49:

    # r "And bring back some souvenirs!{p}Like, an okinawa manga or something!"
    r "¡Y tráenos algunos recuerdos!{p}¡Como, un manga okinawan o algo!"

# game/dialogs.rpy:3718
translate spanish end_9cf1c80f:

    # "I chuckled at Rika's request."
    "Me reí de la solicitud de Rika."

# game/dialogs.rpy:3719
translate spanish end_a725dde6:

    # "Then, I heard the call for our plane."
    "Entonces, escuché la llamada de nuestro vuelo."

# game/dialogs.rpy:3720
translate spanish end_bb0d139e:

    # "Nanami and I went for our departure."
    "Nanami y yo nos fuimos para el despegue de nuestro vuelo."

# game/dialogs.rpy:3721
translate spanish end_8ea8e17b:

    # "We waved at our friends and they waved back."
    "Nos despedimos de nuestras amigas y ellas nos la devolvieron."

# game/dialogs.rpy:3722
translate spanish end_ff8fb8bb:

    # "One thing is certain,..."
    "Una cosa es certera..."

# game/dialogs.rpy:3723
translate spanish end_06a038b7:

    # "It's in moments like this that having friends like that is a true luck."
    "Es en momentos como este que teniendo amigas como ellas es una verdadera suerte."

# game/dialogs.rpy:3724
translate spanish end_099873e9:

    # "And with a girlfriend like Nanami,...{w}and friends like I have,..."
    "Y con una novia como Nanami...{w}Y amigas como los que tengo..."

# game/dialogs.rpy:3725
translate spanish end_3f02d717:

    # "I can tell I'm the luckiest dude ever."
    "Puedo decir que soy el tipo más suertudo."

# game/dialogs.rpy:3726
translate spanish end_bc578e9b:

    # s "Don't forget to come back!"
    s "¡No se olviden de regresar!"

# game/dialogs.rpy:3727
translate spanish end_867c4c03:

    # n "We will, Sakura-nee!"
    n "¡Lo haremos, Sakura-nee!"

# game/dialogs.rpy:3734
translate spanish end_2d2fc7dd:

    # "I finally asked Rika-chan to marry me."
    "Finalmente le pedí a Rika-chan casarse conmigo."

# game/dialogs.rpy:3735
translate spanish end_7d486c68:

    # "And I promised Sakura-chan that she will be the godparent of our children."
    "Y prometí a Sakura-chan que ella sería la madrina de nuestros hijos."

# game/dialogs.rpy:3736
translate spanish end_16d52d33:

    # "I hope Sakura will find love someday. She really deserves it."
    "Espero que Sakura encuentre el amor algún día. Ella realmente se lo merece."

# game/dialogs.rpy:3737
translate spanish end_8052eee9:

    # "I was actually ready for the ceremony."
    "En realidad estaba listo para la ceremonia."

# game/dialogs.rpy:3738
translate spanish end_d8e35e56:

    # "In the village, we usually do traditional weddings, but Rika wanted an occidental wedding."
    "En el pueblo, nosotros usualmente hacemos bodas tradicionales, pero Rika quería una boda occidental."

# game/dialogs.rpy:3739
translate spanish end_e5ffcd4a:

    # "I guess it's for the dress..."
    "Supongo que es por el vestido..."

# game/dialogs.rpy:3740
translate spanish end_c371d9bb:

    # "It's the best cosplay she ever did. Except, this time, it wasn't a cosplay. It was real."
    "Es el mejor cosplay que ella jamás ha hecho. Excepto, que esta vez, no era un cosplay. Era real."

# game/dialogs.rpy:3741
translate spanish end_ecc6ece6:

    # "Priest" "Dear %(stringhero)s-san, do you take Rika-san to be your wedded wife, to live together in marriage?"
    "Sacerdote" "Querido %(stringhero)s-san, ¿tomas a Rika-san como tu esposa, para vivir juntos en matrimonio?"

# game/dialogs.rpy:3742
translate spanish end_02471825:

    # hero "You bet I do!"
    hero "¡Apuesta a que si lo hago!"

# game/dialogs.rpy:3743
translate spanish end_34741d03:

    # r "Can't you just say \"I do.\", you city rat?!"
    r "¡¿No puedes solo decir \"acepto\" rata de la ciudad?!"

# game/dialogs.rpy:3744
translate spanish end_96f20b66:

    # "*{i}laughs{/i}*"
    "*{i}Risas{/i}*"

# game/dialogs.rpy:3745
translate spanish end_c2e91305:

    # hero "Hey do you plan to call me city rat even after being married!"
    hero "¡Oye planeas llamarme rata de la ciudad incluso después de estar casados!"

# game/dialogs.rpy:3746
translate spanish end_5848829c:

    # r "Yes I will!"
    r "¡Si lo haré!"

# game/dialogs.rpy:3747
translate spanish end_96f20b66_1:

    # "*{i}laughs{/i}*"
    "*{i}Risas{/i}*"

# game/dialogs.rpy:3748
translate spanish end_6ddd1312:

    # "Priest" "Hem hem..."
    "Sacerdote" "Ajem ajem..."

# game/dialogs.rpy:3749
translate spanish end_aea646e4:

    # "Priest" "And you, dear Rika-san,...do you take %(stringhero)s-san to be your wedded husband, to live together in marriage?"
    "Sacerdote" "Y tú, querida Rika-san... ¿Tomas a %(stringhero)s-san a ser tu esposo, para vivir juntos en matrimonio?"

# game/dialogs.rpy:3750
translate spanish end_742d691c:

    # "Then, Rika made a smile.{p}A smile I never seen on her before."
    "Entonces, Rika hizo una sonrisa.{p}Una sonrisa que nunca he visto en ella antes."

# game/dialogs.rpy:3751
translate spanish end_2665aacc:

    # "Not that naughty smile of when she prepares a joke or anything."
    "No esa sonrisa traviesa de cuando ella prepara una broma o algo."

# game/dialogs.rpy:3752
translate spanish end_dfe9a447:

    # "A shining, bright smile. The cutest smile she ever made."
    "Una reluciente, brillante sonrisa. La sonrisa más linda que jamás ha hecho."

# game/dialogs.rpy:3753
translate spanish end_b8af1a6a:

    # r "... I do!"
    r "... ¡Acepto!"

# game/dialogs.rpy:3806
translate spanish trueend_906ebb63:

    # centered "You finished the game for the first time!\nGo check the Bonus menu to see what you have unlocked!"
    centered "¡Haz terminado el juego por primera vez!\n¡Ve a chequear el menú Bonus para ver qué has desbloqueado!"

# game/dialogs.rpy:3811
translate spanish trueend_66eedbda:

    # centered "You finished the game with Sakura's route for the first time!"
    centered "¡Haz terminado el juego con la ruta de Sakura por primera vez!"

# game/dialogs.rpy:3816
translate spanish trueend_1561cc61:

    # centered "You finished the game with Rika's route for the first time!"
    centered "¡Haz terminado el juego con la ruta de Rika por primera vez!"

# game/dialogs.rpy:3821
translate spanish trueend_bb63381f:

    # centered "You finished the game with Nanami's route for the first time!"
    centered "¡Haz terminado el juego con la ruta de Nanami por primera vez!"

# game/dialogs.rpy:3838
translate spanish bonus1_25742137:

    # hero "Hey girls, do you remember the first anime you ever watched?"
    hero "Oigan chicas, ¿recuerdan el primer anime que miraron jamás?"

# game/dialogs.rpy:3840
translate spanish bonus1_6e60ac8f:

    # s "Hmm... That's a hard one..."
    s "Hmm... Es una difícil..."

# game/dialogs.rpy:3841
translate spanish bonus1_25a3fcbc:

    # r "Hmm, I think my first one was {i}Sanae-san{/i}."
    r "Hmm, pienso que el primero fue {i}Sanae-san{/i}."

# game/dialogs.rpy:3842
translate spanish bonus1_1ac8f05b:

    # hero "{i}Sanae-san{/i}?{p}That anime that about politics and stuff?"
    hero "¿{i}Sanae-san{/i}?{p}¿Ese anime algo viejo que hablaba sobre políticas y cosas así?"

# game/dialogs.rpy:3843
translate spanish bonus1_1e55e98f:

    # r "Yeah! That one was nice!"
    r "¡Si! ¡Ese era uno bueno!"

# game/dialogs.rpy:3844
translate spanish bonus1_89c89c91:

    # hero "But it was aired when we were around four or five years old! You could understand what they were talking about?"
    hero "¡Pero estaba en transmisión cuando teníamos alrededor de 4 o 5 años! ¿Podías entender sus diálogos?"

# game/dialogs.rpy:3846
translate spanish bonus1_b9b8111c:

    # "Rika gave me a disgusted look."
    "Rika me miraba con una pequeña cara enojada."

# game/dialogs.rpy:3847
translate spanish bonus1_17789b93:

    # r "Whatever! Well what about you? What was your first anime?!"
    r "¿Y tú, qué mirabas?"

# game/dialogs.rpy:3848
translate spanish bonus1_517679f1:

    # hero "{i}Panpanman{/i}. You know, the one with the donut-headed guy..."
    hero "{i}Panpanman{/i}. Ya sabes, el que tenía un tipo con cabeza de dona..."

# game/dialogs.rpy:3849
translate spanish bonus1_c5d8e4d4:

    # r "And could you understand everything in that show?"
    r "¿Y tú podías entender el diálogo?"

# game/dialogs.rpy:3850
translate spanish bonus1_8b365fc3:

    # "Ah damn, she's got a point there."
    "Rayos, ella tenía un punto ahí."

# game/dialogs.rpy:3851
translate spanish bonus1_85a0d1bb:

    # "{i}Panpanman{/i} wasn't as complicated as {i}Sanae-san{/i}, but it's just as old. I was young so...I just followed along the animation."
    "{i}Panpanman{/i} no era tan complicado como {i}Sanae-san{/i}, pero era igual de viejo. Era joven así que solo podía entender por los imágenes."

# game/dialogs.rpy:3852
translate spanish bonus1_61a5b6c2:

    # hero "And you, Nanami-san?"
    hero "¿Y tú, Nanami-san?"

# game/dialogs.rpy:3856
translate spanish bonus1_b075ad7f:

    # n "Hmm..."
    n "Hmm..."

# game/dialogs.rpy:3857
translate spanish bonus1_2d685261:

    # n "I think my first one was {i}Galaxyboy{/i}..."
    n "Pienso que el primero fue {i}Galaxyboy{/i}..."

# game/dialogs.rpy:3858
translate spanish bonus1_88ba5484:

    # r "The one with that robot kid?"
    r "¿El que tenía ese chico robot?"

# game/dialogs.rpy:3859
translate spanish bonus1_31293345:

    # n "Yeah! I remember it being really colorful and fun to watch."
    n "Si."

# game/dialogs.rpy:3860
translate spanish bonus1_cd1e46e5:

    # n "There was a game called {i}Punkman{/i} with a similar looking robot."
    n "Entonces más tarde, había un juego llamado {i}Punkman{/i} con un robot similar..."

# game/dialogs.rpy:3861
translate spanish bonus1_f81a7f2c:

    # n "Maybe that's why I remember {i}Galaxyboy{/i} a little."
    n "Tal vez ese es el porqué recuerdo un poco a {i}Galaxyboy{/i}..."

# game/dialogs.rpy:3863
translate spanish bonus1_e11f5261:

    # n "Although, I don't remember the story at all!"
    n "¡Porque no recuerdo para nada cual era la historia!"

# game/dialogs.rpy:3865
translate spanish bonus1_d7bf3979:

    # hero "What about you, Sakura-san?{p}What was your first anime?"
    hero "¿Qué hay sobre ti, Sakura-san?{p}¿Cuál fue tu primer anime?"

# game/dialogs.rpy:3867
translate spanish bonus1_85feadfd:

    # s "Hmm... Well... I think my first one was {i}La fleur de Paris{/i}..."
    s "Hmm... Bueno... Pienso que el primero fue {i}La fleur de Paris{/i}..."

# game/dialogs.rpy:3868
translate spanish bonus1_7076984c:

    # hero "The one based on a book with that girl from France?"
    hero "¿El que se basó en un libro con esa chica de Francia?"

# game/dialogs.rpy:3869
translate spanish bonus1_e929395f:

    # hero "That one is more recent... It only aired around six years ago."
    hero "Ese es el más reciente... Comenzó a transmitir hace 6 años."

# game/dialogs.rpy:3870
translate spanish bonus1_4343cc28:

    # s "Yeah. My love for anime started a little later. I was 12 when I started watching it."
    s "Si. Mi amor por los animes comenzó un poco más tarde. Yo tenía 12 cuando comencé a verla."

# game/dialogs.rpy:3871
translate spanish bonus1_5a7baf3a:

    # s "I loved the story. It was about a girl who disguises herself as a man just to get closer to her king."
    s "Amé la historia sobre esta chica quien se disfraza como un hombre solo para estar más cerca de su rey."

# game/dialogs.rpy:3872
translate spanish bonus1_64c82b58:

    # hero "{i}La fleur de Paris{/i} sounds like a shoujo... I think my sister tried to read it once..."
    hero "{i}La fleur de Paris{/i} parece como un shoujo real... Pienso que mi hermana trató de leerla una vez..."

# game/dialogs.rpy:3873
translate spanish bonus1_9c8d2196:

    # s "I didn't know you had a sister!"
    s "¡No sabía que tenías una hermana!"

# game/dialogs.rpy:3874
translate spanish bonus1_376fc77d:

    # hero "Well, yeah."
    hero "Bueno, si..."

# game/dialogs.rpy:3875
translate spanish bonus1_a4020f07:

    # hero "But she's way older than me. She's already married."
    hero "Pero ella es mucho mayor que yo. Ella ya está casada."

# game/dialogs.rpy:3876
translate spanish bonus1_5d47aa0b:

    # hero "She's in Tokyo with her husband."
    hero "Ella se quedó en Tokyo con su esposo."

# game/dialogs.rpy:3877
translate spanish bonus1_3e2e6e7f:

    # r "That's sounds nice!"
    r "¡Eso esta bien por ella!"

# game/dialogs.rpy:3879
translate spanish bonus1_5a889d44:

    # s "Don't you miss having your sister around?"
    s "Me siento un poco triste de que estés separado de tu hermana."

# game/dialogs.rpy:3880
translate spanish bonus1_38aa09d8:

    # hero "Oh well, she's always been independent. I got used to it after awhile, so it's okay! I still write to her from time to time."
    hero "Oh bueno, ella siempre fue una mujer independiente y estaba acostumbrado a ello, así que esta bien..."

# game/dialogs.rpy:3881
translate spanish bonus1_2e2dc283:

    # hero "By the way, do you guys have siblings?"
    hero "Por cierto, ¿ustedes dos tienen hermanos o hermanas?"

# game/dialogs.rpy:3882
translate spanish bonus1_ceca0652:

    # n "I have an older brother."
    n "Yo tengo un hermano mayor."

# game/dialogs.rpy:3883
translate spanish bonus1_1e3491ce:

    # s "I don't."
    s "Yo no tengo."

# game/dialogs.rpy:3884
translate spanish bonus1_7c225574:

    # r "Me neither."
    r "Yo tampoco."

# game/dialogs.rpy:3885
translate spanish bonus1_bc19b4bf:

    # hero "Ever wish you had one?"
    hero "¿Alguna vez desearon tener uno?"

# game/dialogs.rpy:3886
translate spanish bonus1_bdb5b07a:

    # r "Nah, I never did. I'm happy with my current life."
    r "Nunca lo hice. Estoy feliz con mi vida actual."

# game/dialogs.rpy:3887
translate spanish bonus1_58cc7ba4:

    # s "Well for me... I sometimes dream of having a big brother."
    s "Bueno para mí... A veces he soñado de tener un hermano mayor..."

# game/dialogs.rpy:3888
translate spanish bonus1_dc1a8fcd:

    # n "I can understand that! My big brother is awesome!"
    n "Comprendo eso. ¡Mi hermano mayor es asombroso!"

# game/dialogs.rpy:3889
translate spanish bonus1_f18686d4:

    # s "But I'll never have any siblings. My parents don't want to..."
    s "Pero yo nunca tendré algún hermano, pienso yo. Mis padres no quieren..."

# game/dialogs.rpy:3890
translate spanish bonus1_7568d34b:

    # hero "It's sad...{p}But maybe it's better like this."
    hero "Es triste...{p}Pero tal vez sea mejor así."

# game/dialogs.rpy:3891
translate spanish bonus1_453d2365:

    # s "Why is that?"
    s "¿Porqué es eso?"

# game/dialogs.rpy:3892
translate spanish bonus1_d027b066:

    # hero "Usually big brothers are mean to their little sisters in the first years."
    hero "Usualmente hermanos mayores son malos con sus hermanas menores en los primeros años."

# game/dialogs.rpy:3893
translate spanish bonus1_3fae45d0:

    # n "Hmm... My big brother was a jerk to me when I was younger... {p}But not anymore..."
    n "Hmm... Mi hermano mayor era malo conmigo cuando era más joven...{p}Pero ya no más..."

# game/dialogs.rpy:3894
translate spanish bonus1_6d59e0e0:

    # hero "What do you mean?"
    hero "¿Qué quieres decir?"

# game/dialogs.rpy:3895
translate spanish bonus1_e148f620:

    # n "Well... {w}Nevermind... {p}I guess it's normal for siblings after some time..."
    n "Bueno... {w}Olvidalo... {p}Supongo que es normal para los hermanos después de un tiempo..."

# game/dialogs.rpy:3896
translate spanish bonus1_207b93d2:

    # hero "You must be right...{p}Anyway,..."
    hero "Debes de tener razón...{p}De cualquier forma..."

# game/dialogs.rpy:3897
translate spanish bonus1_0ee56e3f:

    # hero "What I mean is, at least nobody can harm you, Sakura-san."
    hero "Lo que quiero decir es, al menos nadie puede hacerte daño, realmente, Sakura-san."

# game/dialogs.rpy:3898
translate spanish bonus1_557b9c7c:

    # s ". . ."
    s ". . ."

# game/dialogs.rpy:3899
translate spanish bonus1_b3b9c0e8:

    # s "Hmm..."
    s "Hmm..."

# game/dialogs.rpy:3901
translate spanish bonus1_743a5cb3:

    # s "Yes, you're probably right, %(stringhero)s-san."
    s "Si, probablemente estés en lo correcto, %(stringhero)s-san."

# game/dialogs.rpy:3902
translate spanish bonus1_d501d7aa:

    # "That hesitation again...{p}I don't find it very natural..."
    "Ese titubeo otra vez...{p}No lo encuentro muy natural..."

# game/dialogs.rpy:3903
translate spanish bonus1_b9ad9038:

    # "Is she hiding something?..."
    "¿Está ella ocultando algo?"

# game/dialogs.rpy:3904
translate spanish bonus1_7d45cddc:

    # "And Nanami's acting weird too. Maybe I brought up a sore subject..."
    "¿Y porqué Nanami hizo lo mismo?"

# game/dialogs.rpy:3905
translate spanish bonus1_8534ca42:

    # ". . ."
    ". . ."

# game/dialogs.rpy:3906
translate spanish bonus1_a191a0bc:

    # "Nah, I'm just being paranoid..."
    "Nah, tal vez estoy solo paranóico..."

# game/dialogs.rpy:3927
translate spanish bonus2_8d40556d:

    # "I was watching Sakura as she was looking for her yukata."
    "Estaba viendo a Sakura buscando por su yukata."

# game/dialogs.rpy:3928
translate spanish bonus2_2eb7bc22:

    # "She noticed that I was staring and she smiled and stopped."
    "Ella notó que yo estaba sonriendo y se detuvo."

# game/dialogs.rpy:3929
translate spanish bonus2_1ab585f3:

    # s "Why the smile, honey?"
    s "¿Porqué la sonrisa, cariño?"

# game/dialogs.rpy:3930
translate spanish bonus2_f27ba686:

    # hero "Well, now that I've seen you nude... I realized something..."
    hero "Ahora que te veo totalmente desnuda... Me di cuenta que..."

# game/dialogs.rpy:3931
translate spanish bonus2_28c06110:

    # hero "You look so girly with that hair and thin body..."
    hero "Te ves tan femenina con ese cabello, y ese cuerpo delgado..."

# game/dialogs.rpy:3932
translate spanish bonus2_1740af69:

    # hero "But you have the chest of a thin guy... and a penis too..."
    hero "Pero tienes un pecho de un chico delgado... Y esa cosa ahí abajo..."

# game/dialogs.rpy:3933
translate spanish bonus2_cf68db7f:

    # hero "I have the evidence that you're born as a boy... And I was pretty sure I would only like girls."
    hero "Tengo la evidencia de que naciste como un chico... Y yo estaba muy seguro que solo me gustarían las chicas."

# game/dialogs.rpy:3934
translate spanish bonus2_a2c879ff:

    # hero "But in my eyes, you're way prettier than any woman or girl I've ever seen. And I can't stop loving you anyway..."
    hero "Pero para mis ojos, tú eres mucho más bonita que cualquier mujer o chica que haya visto. Y no puedo parar de amarte de ninguna manera..."

# game/dialogs.rpy:3935
translate spanish bonus2_bc3f3461:

    # "She smiled. Then she sat beside me on the bed."
    "Ella sonrió. Entonces se sentó al lado mío en la cama."

# game/dialogs.rpy:3936
translate spanish bonus2_6e930048:

    # s "Do you love me because I look like a girl?"
    s "¿Me amas porque me veo como una chica?"

# game/dialogs.rpy:3937
translate spanish bonus2_32aff81a:

    # s "You think it would be different if I was a real boy?"
    s "¿Piensas que sería diferente si yo fuera un chico de verdad?"

# game/dialogs.rpy:3938
translate spanish bonus2_b543397f:

    # hero "Well..."
    hero "Bueno..."

# game/dialogs.rpy:3939
translate spanish bonus2_aa6128c8:

    # hero "To be honest, I..."
    hero "Para ser honesto, yo..."

# game/dialogs.rpy:3940
translate spanish bonus2_2983804d:

    # "I felt a headache."
    "Sentí un dolor de cabeza."

# game/dialogs.rpy:3941
translate spanish bonus2_f7a250f4:

    # hero "Darn, actually, I have no idea. I don't know at all..."
    hero "Maldición, en realidad, no tengo idea. No lo sé de ninguna manera..."

# game/dialogs.rpy:3942
translate spanish bonus2_4c90784c:

    # hero "The first thing that attracted me to you was your feminine features and personality...{w} I didn't realize you were male at the time."
    hero "La primera cosa que me atrajo de ti fue tu cuerpo tan femenino...{w}Y luego, tu personalidad cuando lo descubrí."

# game/dialogs.rpy:3943
translate spanish bonus2_7a20d415:

    # hero "I fell in love with you at first sight...But surprisingly, your secret didn't stop me from loving you."
    hero "Me enamoré de ti a primera vista... Y tu secreto no me detuvo de estar enamorado de ti..."

# game/dialogs.rpy:3944
translate spanish bonus2_7fb40657:

    # hero "I'm wondering about my own sexuality... Does this mean I'm \"biologically gay\" or something?"
    hero "Me estoy preguntando quién soy...¿Eso significa que soy, como \"biológicamente homosexual\" o algo? "

# game/dialogs.rpy:3945
translate spanish bonus2_8b8a7483:

    # hero "Maybe it's not gay since you are a girl inside and that I love you..."
    hero "Tal vez no es homosexual ya que eres una chica por dentro y por lo que te amo..."

# game/dialogs.rpy:3946
translate spanish bonus2_7ae5be64:

    # hero "I have so many questions about myself and I don't have any answers..."
    hero "Tengo muchas preguntas sobre mí mismo y no encuentro ninguna respuesta..."

# game/dialogs.rpy:3947
translate spanish bonus2_0a5fc0a9:

    # "Sakura gently leaned her head against mine and looked into my eyes."
    "Sakura gentilmente toma mi cabeza, me apoye sobre la suya, mirando a mis ojos, profundamente."

# game/dialogs.rpy:3948
translate spanish bonus2_4951c5b2:

    # s "The important thing isn't what your mind wants."
    s "Lo importante no es lo que tu mente quiere."

# game/dialogs.rpy:3949
translate spanish bonus2_91e5ab91:

    # s "But what your heart wants."
    s "Sino lo que quiere tu corazón."

# game/dialogs.rpy:3950
translate spanish bonus2_c269d052:

    # s "You know... sometimes I ask that about myself too."
    s "Sabes... Me pregunté sobre eso también, algunas veces en mi vida."

# game/dialogs.rpy:3951
translate spanish bonus2_77498e04:

    # s "My father was always telling me that I should like girls. And I actually do, to be honest..."
    s "Mi padre siempre estaba diciéndome que debería gustarme las chicas. Y en realidad me gustan, para ser honesta..."

# game/dialogs.rpy:3952
translate spanish bonus2_891bc3a6:

    # s "But as I tried to become more comfortable with who I was..."
    s "Pero mi padre no es muy amable conmigo... Además el hecho de que no tengo un hermano, como Nanami-chan tiene..."

# game/dialogs.rpy:3953
translate spanish bonus2_dbb3d044:

    # s "I started to be attracted to boys as well."
    s "Comencé a ser atraída a los chicos también."

# game/dialogs.rpy:3954
translate spanish bonus2_97ff28cd:

    # s "I was completely undecided during this time. I was feeling the same exact way you are right now. But I finally figured out..."
    s "Estaba completamente indecisa durante ese tiempo. Estaba teniendo exactamente las mismas preguntas que tú tienes... Pero finalmente lo descubrí."

# game/dialogs.rpy:3955
translate spanish bonus2_f9789510:

    # s "Girls? Boys? It doesn't matter."
    s "¿Chicas? ¿Chicos? No importa."

# game/dialogs.rpy:3956
translate spanish bonus2_6041b77d:

    # s "The most important is: {w}Does the person you love, return as much love as you do?"
    s "Lo más importante es: {w}¿La persona que amas responde de la misma manera?"

# game/dialogs.rpy:3957
translate spanish bonus2_7d1800b4:

    # "Looking at her cute blue eyes made my heart beat louder."
    "Mirando en sus lindos ojos azules hizo que mi corazón latiera más fuerte."

# game/dialogs.rpy:3958
translate spanish bonus2_49959dea:

    # "Yeah, she's right. Why does gender matter...?"
    "Si, ¿Cuál es el problema, después de todo?"

# game/dialogs.rpy:3959
translate spanish bonus2_473681a8:

    # hero "Sakura-chan... My love... My heart wants to stay with you..."
    hero "Sakura-chan... Mi amor... Mi corazón quiere estar contigo..."

# game/dialogs.rpy:3960
translate spanish bonus2_8482b65d:

    # hero "I don't care about about your body and what others may think."
    hero "Como dije, la última vez, no me importa sobre lo que tu cuerpo y los otros piensen."

# game/dialogs.rpy:3961
translate spanish bonus2_e9d4d741:

    # hero "No matter what happens, my feelings for you won't change. You're the one I want to be with."
    hero "No importando lo que me pase, no cambiaré mis sentimientos por ti. Es a ti quien elegí para estar juntos."

# game/dialogs.rpy:3962
translate spanish bonus2_b8d074ce:

    # hero "You're right... What's gender, anyway? It's just a box to check on a paper."
    hero "Tienes razón... ¿Qué es un género, de cualquier manera? Solo un caja para chequear en un perfil de currículum."

# game/dialogs.rpy:3963
translate spanish bonus2_9df6f08e:

    # s "Exactly... Genders are just a label..."
    s "Exactamente... Géneros son solo una etiqueta..."

# game/dialogs.rpy:3964
translate spanish bonus2_d9b204fb:

    # hero "I love you, Sakura-chan... I love you..."
    hero "Te amo, Sakura-chan... Te amo..."

# game/dialogs.rpy:3965
translate spanish bonus2_96516aa7:

    # hero "And my only hope is that you love me too..."
    hero "Y mi única esperanza es que tú me ames también..."

# game/dialogs.rpy:3966
translate spanish bonus2_05a7a895:

    # "She cuddled me tighter."
    "Ella se me acurrucó fuertemente."

# game/dialogs.rpy:3967
translate spanish bonus2_66ae317a:

    # s "I do, %(stringhero)s-kun. I love you, more than anything..."
    s "Lo hago, %(stringhero)s-kun. Te amo, más que a nada... "

# game/dialogs.rpy:3968
translate spanish bonus2_25c4ff40:

    # s "I fell in love for you the first time we met, as well."
    s "Me enamoré de ti la primera vez que nos conocimos, también."

# game/dialogs.rpy:3969
translate spanish bonus2_a7a9aeaa:

    # s "You are the man of my life."
    s "Tú eres el hombre de mi vida."

# game/dialogs.rpy:3970
translate spanish bonus2_20830efe:

    # hero "And you are the girl of mine, Sakura-chan. Forever."
    hero "Y tú eres la mujer de la mía, Sakura-chan. Para siempre."

# game/dialogs.rpy:3971
translate spanish bonus2_6b6e7e46:

    # hero "And that secret of yours... Actually, it makes you pretty unique..."
    hero "Y ese secreto tuyo... En realidad, te hace bastante única..."

# game/dialogs.rpy:3972
translate spanish bonus2_27fcf534:

    # hero "And I feel so proud and happy to have a so unique girlfriend."
    hero "Y me siento tan orgulloso y feliz de tener a tan única novia."

# game/dialogs.rpy:3973
translate spanish bonus2_57845a53:

    # "I kissed her tenderly."
    "La besé tiernamente."

# game/dialogs.rpy:3974
translate spanish bonus2_3db00aad:

    # "I leaned on her naked body as I carried on."
    "Me incliné a su cuerpo desnudo mientras proseguía."

# game/dialogs.rpy:3975
translate spanish bonus2_0949b3d0:

    # "I stopped a moment to look at her beautiful face."
    "Me detuve un momento para mirar su hermoso rostro."

# game/dialogs.rpy:3976
translate spanish bonus2_ad103b2f:

    # "Some tears were falling."
    "Algunas lágrimas estaban brotando."

# game/dialogs.rpy:3977
translate spanish bonus2_2b9b486c:

    # hero "Why are you crying, hun?"
    hero "¿Porqué estás llorando, cariño?"

# game/dialogs.rpy:3978
translate spanish bonus2_193b5b96:

    # s "I'm just... so happy... %(stringhero)s-kun..."
    s "Solo estoy... Tan feliz... %(stringhero)s-kun..."

# game/dialogs.rpy:3979
translate spanish bonus2_a9227760:

    # s "I'm so happy that you love me, no matter what I am."
    s "Estoy tan feliz de que me ames, no importando qué soy."

# game/dialogs.rpy:3980
translate spanish bonus2_09611ce1:

    # s "I love you so much, %(stringhero)s-kun... So much..."
    s "Te amo tanto, %(stringhero)s-kun... Tanto..."

# game/dialogs.rpy:3981
translate spanish bonus2_c1ae5d6f:

    # "I cuddled her lovingly. Her body was warming mine."
    "La abracé cariñosamente. Su cuerpo estaba calentando el mío."

# game/dialogs.rpy:3982
translate spanish bonus2_8d1c6006:

    # "Then I suddenly remembered... My parents! They must not find Sakura here and naked!"
    "Entonces de pronto recordé... ¡Mis padres! ¡Ellos no deben encontrar a Sakura aquí y desnuda!"

# game/dialogs.rpy:3983
translate spanish bonus2_0f61a346:

    # "After a moment, we stood up and I helped Sakura putting her yukata again..."
    "Después de un momento, nos levantamos y ayudé a Sakura a ponerse su yukata otra vez..."

# game/dialogs.rpy:3987
translate spanish bonus2_38df4441:

    # "In front of her house, I felt my stomach twisting. I don't want to be without her..."
    "Enfrente de su casa, sentí mi estómago retorcerse. No quería estar sin ella."

# game/dialogs.rpy:3988
translate spanish bonus2_82ba49f2:

    # hero "I don't want to leave you, I want to stay with you!"
    hero "¡No quiero dejarte, quiero estar contigo!"

# game/dialogs.rpy:3989
translate spanish bonus2_1a74f79c:

    # s "Me too... I miss your arms already... {image=heart.png}"
    s "Yo también... Ya extraño tus brazos... {image=heart.png}"

# game/dialogs.rpy:3990
translate spanish bonus2_4a161b65:

    # s "It's okay, love, we'll meet again tomorrow at school, remember?"
    s "Esta bien, amor, ¿nos veremos de nuevo mañana en la escuela, recuerdas?"

# game/dialogs.rpy:3991
translate spanish bonus2_2b99de4b:

    # s "It will be the last week until the summer vacation."
    s "Va a ser la última semana hasta las vacaciones de Verano."

# game/dialogs.rpy:3992
translate spanish bonus2_f405c4f8:

    # s "And I will be free during this period."
    s "Y estaré libre durante ese tiempo."

# game/dialogs.rpy:3993
translate spanish bonus2_c5fd93a1:

    # hero "So will I..."
    hero "También lo estaré..."

# game/dialogs.rpy:3994
translate spanish bonus2_4ff85ef0:

    # hero "And you'll be sure that I'll see you everyday during the vacations."
    hero "Y es seguro de que te veré todos los días durante las vacaciones."

# game/dialogs.rpy:3995
translate spanish bonus2_903c8c1d:

    # "She smiled and pecks my cheek."
    "Ella sonrió y me pellizco mi mejilla."

# game/dialogs.rpy:3996
translate spanish bonus2_f3fb3329:

    # s "I can't wait for it, %(stringhero)s-honey-bun! {image=heart.png}"
    s "¡No puedo esperar por ello, %(stringhero)s-bollito-de-miel! {image=heart.png}"

# game/dialogs.rpy:4015
translate spanish bonus3_5f502601:

    # write "{b}Headline news : A father tried to murder his daughter.{/b}"
    write "{b}Encabezado: Un padre trata de asesinar a su hija.{/b}"

# game/dialogs.rpy:4016
translate spanish bonus3_eae14b39:

    # write "Last night, in the little village of N. in the Osaka prefecture at 1am, a drunken father, 44 years old, attempted to murder the young %(stringhero)s, 19 years old.{w} The father's daughter protected him and got stabbed in the abdomen."
    write "Anoche, en el pequeño pueblo de N. en la prefectura de Osaka a la 1am, un padre ebrio, 44 años de edad, intentó asesinar al joven %(stringhero)s, 19 años de edad.{w} La hija del padre lo protegió y fue apuñalada en el abdomen."

# game/dialogs.rpy:4017
translate spanish bonus3_1fc20115:

    # write "The father was arrested and the girl, 18 years old, has been taken to the hospital.{w} The reasons of the fight are unknown, but the Police suspect it was due to the effects of the alcohol and the offender's criminal record."
    write "El padre fue arrestado y la chica, 18 años de edad, ha sido llevada al hospital.{w} Las razones de la pelea son desconocidos, pero la policía sospecha que fue debido a los efectos del alcohol y registro criminal del agresor."

# game/dialogs.rpy:4018
translate spanish bonus3_76b2fe88:

    # nvl clear
    nvl clear

# game/dialogs.rpy:4053
translate spanish bonus4_244a1f9c:

    # "It was Sunday. Our first Sunday of summer vacation."
    "Era un Domingo. Nuestro primer Domingo de las vacaciones de Verano."

# game/dialogs.rpy:4054
translate spanish bonus4_d04b10e5:

    # "We decided to hold a picnic near the rice field."
    "Decidimos hacer un día de campo cerca de un arrozal."

# game/dialogs.rpy:4055
translate spanish bonus4_41f4816b:

    # "I remembered it was here where Sakura revealed her secret."
    "Sonreí mientras recordaba que aquí fue dónde Sakura me contó su profundo secreto."

# game/dialogs.rpy:4057
translate spanish bonus4_c69f2e23:

    # "It's also here that we kissed for the first time."
    "También fue aquí donde nos besamos por primera vez."

# game/dialogs.rpy:4058
translate spanish bonus4_edd1ee56:

    # "Rika organized a little game for us and we started playing after lunch."
    "Rika organizó un pequeño juego para nosotros y comenzamos a jugar luego del almuerzo."

# game/dialogs.rpy:4063
translate spanish bonus4_c4dbd1b7:

    # r "Okay, this is a game based on manga, anime and videogames."
    r "Bien, es un juego basado en mangas, animes y videojuegos."

# game/dialogs.rpy:4064
translate spanish bonus4_e067d3ab:

    # r "It's a simple quiz where the losers will have to throw dice and the punishment is decided by the who rolls the highest."
    r "Es un simple cuestionario dónde los perdedores van a tirar un dado y el castigo es decidido por la persona con el punteo más alto."

# game/dialogs.rpy:4065
translate spanish bonus4_3d239b9b:

    # hero "Uhhh... What kind of punishment...?"
    hero "Hmm... ¿Qué clase de castigos?"

# game/dialogs.rpy:4067
translate spanish bonus4_5b7ca773:

    # r "Ohohoho. Scared to lose, city rat?"
    r "¿Porqué esa pregunta? ¿Miedo de perder, rata de la ciudad?"

# game/dialogs.rpy:4069
translate spanish bonus4_53fc8dd1:

    # hero "I'm not a city rat! I'm your boyfriend!{p}Bring on the questions, I'm not afraid!"
    hero "¡No soy una rata de la ciudad! ¡Soy tu novio!{p}¡Venga las preguntas, no estoy asustado!"

# game/dialogs.rpy:4071
translate spanish bonus4_785461af:

    # hero "I'm not a city rat! Bring on the questions, I'm not afraid!"
    hero "¡No soy una rata de la ciudad! ¡Venga las preguntas, no estoy asustado!"

# game/dialogs.rpy:4072
translate spanish bonus4_0a90e627:

    # s "Please be gentle with %(stringhero)s-kun, it's his first time."
    s "Por favor se gentil con %(stringhero)s-kun, es su primera vez."

# game/dialogs.rpy:4073
translate spanish bonus4_5e33fd5b:

    # r "Don't worry, Sakura-chan... I'll be just fine..."
    r "No te preocupes, Sakura-chan... Eso será justo lo suficiente..."

# game/dialogs.rpy:4074
translate spanish bonus4_57287ff9:

    # "I don't like that smirk on Rika's face..."
    "No me gustó la sonrisa que Rika acaba de hacer..."

# game/dialogs.rpy:4075
translate spanish bonus4_1f4b7948:

    # "Nanami noticed the smirk as well."
    "Nanami notó la sonrisa también."

# game/dialogs.rpy:4077
translate spanish bonus4_bb701bb3:

    # n "Ooooh, you will not like this, %(stringhero)s-nii!"
    n "Ooooh, ¡no te va a gustar esto, %(stringhero)s-nii!"

# game/dialogs.rpy:4079
translate spanish bonus4_03d1d4c1:

    # n "Ooooh, you will not like this, %(stringhero)s-senpai!"
    n "Ooooh, ¡no te va a gustar esto, %(stringhero)s-senpai!"

# game/dialogs.rpy:4080
translate spanish bonus4_ae65064e:

    # "Rika took some cards in her bag."
    "Rika tomó unas cartas en su bolsa."

# game/dialogs.rpy:4083
translate spanish bonus4_0158abfa:

    # r "Okay guys, Here's the first question..."
    r "Bien chicos, aquí está la primera pregunta..."

# game/dialogs.rpy:4084
translate spanish bonus4_c9e531e1:

    # r "\"What is the name of the tsundere girl in the game {i}Real Love '95{/i}?\""
    r "\"¿Cuál es el nombre de la chica tsundere en el juego {i}Real Love '95{/i}?\""

# game/dialogs.rpy:4085
translate spanish bonus4_9f44cf6d:

    # "Ouch, this first question is hard..."
    "Ouch, esta primera pregunta es difícil... Pero me siento con suerte ya que recuerdo este juego."

# game/dialogs.rpy:4086
translate spanish bonus4_db066cbc:

    # "Knowing Rika, I'm surprised about this question, since {i}Real Love '95{/i} is actually an eroge for PC."
    "Conociendo a Rika, estoy sorprendido sobre esta pregunta, ya que {i}Real Love '95{/i} es en realidad un eroge para PC."

# game/dialogs.rpy:4087
translate spanish bonus4_0e0a402b:

    # "And I remember playing it a few months ago, when I still was in Tokyo."
    "Y recuerdo haberlo jugado, hace meses, cuando aún estaba en Tokyo."

# game/dialogs.rpy:4088
translate spanish bonus4_d39d805f:

    # "So... The tsundere girl... Hmmmm...{p}Yes! I remember!"
    "Entonces... La chica tsundere... Hmmmm...{p}¡Si! ¡Lo recuerdo!"

# game/dialogs.rpy:4089
translate spanish bonus4_e4df0c97:

    # hero "It was Mayumi!"
    hero "¡Era Mayumi!"

# game/dialogs.rpy:4092
translate spanish bonus4_0a0fb39e:

    # r "Good answer, honey!"
    r "¡Buena respuesta, cariño!"

# game/dialogs.rpy:4094
translate spanish bonus4_74267ea4:

    # r "Good answer, city rat!"
    r "¡Buena respuesta, rata de la ciudad!"

# game/dialogs.rpy:4095
translate spanish bonus4_eab1e51f:

    # r "Sakura, Nanami, roll the dice!"
    r "¡Sakura, Nanami, tiren el dado!"

# game/dialogs.rpy:4096
translate spanish bonus4_b1e1b336:

    # "Sakura got a 2.{w} Nanami got a 3."
    "Sakura obtuvo un 2.{w} Nanami obtuvo un 3."

# game/dialogs.rpy:4097
translate spanish bonus4_50bfd4d4:

    # r "Sakura-chan, you lost this round, so Nanami-chan must choose a punishment for you!"
    r "Sakura-chan, perdiste esta ronda, ¡así que Nanami-chan debe elegir un castigo para ti!"

# game/dialogs.rpy:4099
translate spanish bonus4_86b16bed:

    # "Sakura started to feel shy."
    "Sakura comenzó a sentirse tímida."

# game/dialogs.rpy:4100
translate spanish bonus4_4a9a8b0c:

    # "Nanami made a funny grin."
    "Nanami hizo una sonrisa graciosa."

# game/dialogs.rpy:4102
translate spanish bonus4_724f715d:

    # n "Sakura-nee, you must call us by a cute anime-style name for the rest of the day!"
    n "Sakura-nee, ¡debes llamarnos por un nombre lindo estilo-anime por el resto del día!"

# game/dialogs.rpy:4103
translate spanish bonus4_a42514e8:

    # n "I want to be called \"Senpai\",... Senpai! *{i}giggles{/i}*"
    n "Quiero ser llamada \"Senpai\"....¡Senpai! *{i}risitas{/i}*"

# game/dialogs.rpy:4104
translate spanish bonus4_997b66a7:

    # r "I want to be called \"Big sister\"!"
    r "Quiero ser llamada ¡\"hermana mayor\"!"

# game/dialogs.rpy:4106
translate spanish bonus4_f47c567a:

    # s "Okay!"
    s "¡Esta bien!"

# game/dialogs.rpy:4107
translate spanish bonus4_4b1e12da:

    # s "And you, %(stringhero)s-kun? What should I call you?" nointeract
    s "¿Y tú, %(stringhero)s-kun? ¿Cómo debería llamarte?" nointeract

# game/dialogs.rpy:4115
translate spanish bonus4_aab8ef6a:

    # hero "Sakura-chan...{w}I want you to call me \"[hnick!t]\"!"
    hero "Sakura-chan...{w}¡Quiero que me llames \"[hnick!t]\"!"

# game/dialogs.rpy:4117
translate spanish bonus4_7d57ea39:

    # s "E... Eeeeeeeh!!!"
    s "¡¡¡E... Eeeeeeeh!!!"

# game/dialogs.rpy:4118
translate spanish bonus4_5541ddc2:

    # "She became embarrassed, hiding her little mouth behind her fist... So cute..."
    "Ella se avergonzó, ocultando su pequeña boca detrás de su puño... Tan linda..."

# game/dialogs.rpy:4119
translate spanish bonus4_cebdf164:

    # "I blushed myself, waiting for her answer, knowing what she was about to say already..."
    "Me sonrojé, esperando por su respuesta, ya conociendo qué es lo que estaba a punto de decir..."

# game/dialogs.rpy:4121
translate spanish bonus4_c5c21450:

    # s "A-Alright... I'll d-do it... [hnick!t]..."
    s "E-Esta bien... Lo ha-haré... [hnick!t]..."

# game/dialogs.rpy:4123
translate spanish bonus4_70397aff:

    # hero "Gah! The cuteness!"
    hero "¡Gah! ¡La lindura!"

# game/dialogs.rpy:4124
translate spanish bonus4_b312e964:

    # "If this was an anime, my nose would be bleeding."
    "¡Sangré por la nariz y caí al suelo!"

# game/dialogs.rpy:4126
translate spanish bonus4_92f46d2c:

    # r "Hey, wake up, city rat! Quit daydreaming and get ready for the next question. It's Sakura's turn to ask the question!"
    r "Oye, despierta, rata de la ciudad, preparate para la próxima pregunta. Y según las reglas, ¡es el turno de Sakura para hacer la pregunta!"

# game/dialogs.rpy:4127
translate spanish bonus4_24294182:

    # "Yikes! It means that it will be me against Rika and Nanami! I must not fail!"
    "¡Yikes! ¡Eso significa que me enfrentaré a Rika y Nanami! ¡No debo fallar!"

# game/dialogs.rpy:4131
translate spanish bonus4_731520a2:

    # s "O... Okay... So..."
    s "E... Esta bien... Entonces..."

# game/dialogs.rpy:4132
translate spanish bonus4_1011202f:

    # s "The question is:{p}\"How many episodes are in the anime {i}Dragon Sphere{/i}?\""
    s "La pregunta es:{p}\"¿Cuántos episodios tiene el anime {i}Dragon Sphere{/i}?\""

# game/dialogs.rpy:4133
translate spanish bonus4_37f34882:

    # "I knew this anime well. I was about to answer but Rika was faster."
    "Me sabía este anime muy bien. Estaba a punto de responder pero Rika fue más rápida."

# game/dialogs.rpy:4135
translate spanish bonus4_60b1dd5c:

    # r "26! {w}And 3 OVAs!"
    r "¡26! {w}¡Y 3 OVAs!"

# game/dialogs.rpy:4137
translate spanish bonus4_ed2c49db:

    # s "Correct, Big sister!"
    s "Correcto, ¡Rika-chan!"

# game/dialogs.rpy:4138
translate spanish bonus4_c9aee644:

    # s "[hnick!t], Nanami-senpai, roll the dice."
    s "[hnick!t], Nanami-senpai, tiren el dado."

# game/dialogs.rpy:4139
translate spanish bonus4_906b5890:

    # "I got a five! Yes!"
    "¡Obtuve un 5! ¡Si!"

# game/dialogs.rpy:4140
translate spanish bonus4_1581741a:

    # "Nanami rolls...{w}and got a 6! Oh come on!"
    "Nanami tira... {w}¡Y obtiene un 6! ¡Oh vamos!"

# game/dialogs.rpy:4141
translate spanish bonus4_db50cb13:

    # "Nanami directed an evil smile at me. Now I'm pretty scared..."
    "Nanami me mando una sonrisa malvada. Ahora estoy muy asustado..."

# game/dialogs.rpy:4142
translate spanish bonus4_021acfb6:

    # "She took something in Rika's bag and gave it to me."
    "Ella tomó algo en la bolsa de Rika y me lo dio."

# game/dialogs.rpy:4143
translate spanish bonus4_d1658509:

    # "Cat-ears!"
    "¡Orejas de gato!"

# game/dialogs.rpy:4144
translate spanish bonus4_3d592873:

    # n "Here, put this on your head and say 'Nyan nyan~'!"
    n "Aquí tienes, ponte esto en tu cabeza y di 'Nyan nyan~'!"

# game/dialogs.rpy:4145
translate spanish bonus4_e4a0fb20:

    # hero "Heeeeey!{p}I'm pretty sure this was Rika-chan's idea, huh!"
    hero "¡Oyeeeee!{p}¡Estoy muy seguro que fue una idea de Rika-chan, huh!"

# game/dialogs.rpy:4146
translate spanish bonus4_eb7fa7f2:

    # r "Who knows?... Get to work, city rat! Or I should say 'City cat'!"
    r "¿Quién sabe?... ¡A trabajar, rata de la ciudad! ¡O debería decir 'gato de la ciudad'!"

# game/dialogs.rpy:4147
translate spanish bonus4_8bc9d837:

    # n "Get to work, you slacker! *{i}with a fake strong male voice from some video game{/i}*"
    n "¡A trabajar, haragán! *{i}Con una falsa y fuerte voz masculina de algún videojuego{/i}*"

# game/dialogs.rpy:4148
translate spanish bonus4_ff3a6290:

    # "I sighed and I put the ears on my head."
    "Suspiré y me puse las orejas en mi cabeza."

# game/dialogs.rpy:4149
translate spanish bonus4_cf248989:

    # "We didn't have any mirrors around so I couldn't see how I looked. But I'm sure it was ridiculous."
    "No teniamos ningún espejo alrededor así que no podía ver como me veía. Pero estoy seguro que era ridículo."

# game/dialogs.rpy:4150
translate spanish bonus4_7ae9ab60:

    # "I put my fists in front of me, kitty-like."
    "Puse mis puños enfrente de mí, como un gato."

# game/dialogs.rpy:4151
translate spanish bonus4_ad791bf6:

    # hero "N... Ny... Nyan~ nyan~!"
    hero "N... Ny... ¡Nyan~ nyan~!"

# game/dialogs.rpy:4157
translate spanish bonus4_3686e5b0:

    # "Sakura made a big charming smile that instantly made me feel better."
    "Sakura hizo una gran y encantadora sonrisa que instantáneamente me hizo sentir mejor."

# game/dialogs.rpy:4159
translate spanish bonus4_af9b813d:

    # "Sakura made a big charming smile."
    "Sakura hizo una gran y encantadora sonrisa."

# game/dialogs.rpy:4160
translate spanish bonus4_17625773:

    # s "Awwww it's so cuuuuute!!!"
    s "¡¡¡Awwww es tan liinnnnndo!!!"

# game/dialogs.rpy:4161
translate spanish bonus4_6328f30b:

    # "Rika smiled too, but her smile was way more sinister."
    "Rika sonrió también, pero su sonrisa era de un modo más siniestro."

# game/dialogs.rpy:4162
translate spanish bonus4_bda42d96:

    # "Nanami was just laughing out loud. I definitely should look ridiculous."
    "Nanami solo estaba riéndose a carcajadas. Definitivamente debía verme ridículo."

# game/dialogs.rpy:4163
translate spanish bonus4_8d7688c3:

    # r "Hehe, doesn't fit you at all!"
    r "Jeje, ¡no te va para nada!"

# game/dialogs.rpy:4164
translate spanish bonus4_12c6774c:

    # r "Since you lost, it's your turn to ask the question!"
    r "Puesto que perdiste, ¡es tu turno de hacer la pregunta!"

# game/dialogs.rpy:4169
translate spanish bonus4_ede3e123:

    # "Rika gave me another card and I read the question."
    "Rika me dio otra carta y leí la pregunta."

# game/dialogs.rpy:4170
translate spanish bonus4_0e18d25a:

    # "I was also thinking about the pledge they could imagine."
    "También estaba pensando sobre el compromiso que se podían estar imaginando."

# game/dialogs.rpy:4171
translate spanish bonus4_9fe745b2:

    # hero "Okay so...{p}\"What is the name of the seiyuu who plays Haruki in {i}Panty, Bra und Strumpfgürtel{/i}?\""
    hero "Esta bien...{p}\"¿Cuál es el nombre de la seiyuu quien toca Haruki in {i}Panty, Bra und Strumpfgürtel{/i}?\""

# game/dialogs.rpy:4172
translate spanish bonus4_9c8a3481:

    # "Nanami answered immediately, just before Rika could answer."
    "Nanami respondió inmediatamente, justo antes de que Rika pudiera contestar."

# game/dialogs.rpy:4174
translate spanish bonus4_5cc59595:

    # n "Hidaka Megumi!"
    n "¡Hidaka Megumi!"

# game/dialogs.rpy:4175
translate spanish bonus4_a7215825:

    # hero "You got it, Nanami-chan!"
    hero "¡Acertaste, Nanami-chan!"

# game/dialogs.rpy:4177
translate spanish bonus4_fc07c2f9:

    # hero "Rika-chan, Sakura-chan, roll the dice!"
    hero "Rika-chan, Sakura-chan, ¡tiren el dado!"

# game/dialogs.rpy:4178
translate spanish bonus4_1a6d10bf:

    # "Sakura rolled a 3."
    "Sakura obtuvo un 3."

# game/dialogs.rpy:4180
translate spanish bonus4_d821c96c:

    # "Rika made a naughty grin and rolled the dice..."
    "Rika hizo una mueca traviesa y lanzó el dado..."

# game/dialogs.rpy:4182
translate spanish bonus4_619191be:

    # "She pouted when she got a one..."
    "Ella hizo pucheritos cuando obtuvo un 1..."

# game/dialogs.rpy:4184
translate spanish bonus4_667bfdcf:

    # s "Hmmm... Let's see..."
    s "Hmmm... Vamos a ver..."

# game/dialogs.rpy:4187
translate spanish bonus4_ddb63045:

    # "She thought for a moment but finally made a smile... One worse than Rika-chan's!"
    "Ella lo pensó por un momento pero finalmente hizo una sonrisa... ¡Una peor que la de Rika-chan!"

# game/dialogs.rpy:4188
translate spanish bonus4_b721b3d2:

    # "It's so unusual to see such a smile on Sakura's face."
    "Es tan inusual ver tal sonrisa en el rostro de Sakura."

# game/dialogs.rpy:4190
translate spanish bonus4_226ccb5a:

    # s "Big sister, I want you to kiss [hnick!t] on the mouth, in front of us!"
    s "Hermana mayor, ¡quiero que beses a [hnick!t] en la boca, enfrente de nosotros!"

# game/dialogs.rpy:4192
translate spanish bonus4_288e9d30:

    # s "Big sister, I want you to kiss [hnick!t] on the cheek!"
    s "Hermana mayor, ¡quiero que beses a [hnick!t] en la mejilla!"

# game/dialogs.rpy:4194
translate spanish bonus4_54428ce0:

    # r "Whaaaaaaat???"
    r "¿¿¿Quééééééé???"

# game/dialogs.rpy:4195
translate spanish bonus4_3efa7eb4:

    # hero "Eeeeeeeeh!!!"
    hero "¡¡¡Eeeeeeeeh!!!"

# game/dialogs.rpy:4198
translate spanish bonus4_329e5a99:

    # n "Eeeeh but it's my boyfriend!!"
    n "¡¡Eeeeh pero es mi novio!!"

# game/dialogs.rpy:4200
translate spanish bonus4_7529a0c6:

    # r "In front of you all?!"
    r "¡¿Enfrente de todas ustedes?!"

# game/dialogs.rpy:4202
translate spanish bonus4_2f9c2ac6:

    # r "I don't want to, Sakura-chan!{p}It's mean!"
    r "¡No quiero hacerlo, Sakura-chan!{p}¡Eso es malvado!"

# game/dialogs.rpy:4203
translate spanish bonus4_d7114010:

    # s "I'm sorry but it's in the rules. You told us earlier, Rika-chan!{p}And it wouldn't be a punishment if it was too easy!"
    s "Lo siento pero esas son las reglas. ¡Tú nos lo dijiste antes, Rika-chan!{p}¡Y no sería un castigo si fuera demasiado fácil!"

# game/dialogs.rpy:4206
translate spanish bonus4_eb063792:

    # n "I feel like punished too, even if I won..."
    n "Siento como que soy castigada también, incluso si gané..."

# game/dialogs.rpy:4207
translate spanish bonus4_a2b2dbfe:

    # "Sakura can be scary sometimes..."
    "Sakura puede ser aterradora a veces..."

# game/dialogs.rpy:4209
translate spanish bonus4_b9f6e14e:

    # r "Hmpf!... {w}A... Alright... I'll do it!"
    r "¡Hmpf!... {w}E... Esta bien... ¡Lo haré!"

# game/dialogs.rpy:4210
translate spanish bonus4_6213e31d:

    # "My eyes wide open, I saw Rika-chan crawling slowly to me with her pissed expression."
    "Mis ojos se abrieron ampliamente, vi a Rika gatear lentamente hacia mí con su expresión irritada."

# game/dialogs.rpy:4213
translate spanish bonus4_7062c20b:

    # "Then we closed our eyes and we kissed, after some hesitation."
    "Entonces cerramos nuestros ojos y nos besamos, luego de vacilar un poco."

# game/dialogs.rpy:4214
translate spanish bonus4_23c6d1be:

    # "At the moment our lips touched, I think we both forgot the girls were here because the kiss was long and lovey."
    "En el momento que nuestros labios se tocaron, pienso que ambos olvidamos que las chicas estaban aquí porque el beso fue largo y encantador."

# game/dialogs.rpy:4216
translate spanish bonus4_6e084a46:

    # "My heart was beating loudly..."
    "Mi corazón estaba latiendo fuertemente..."

# game/dialogs.rpy:4218
translate spanish bonus4_0defb206:

    # "Then she closed her eyes and neared her face to my cheek."
    "Entonces ella cerró sus ojos y acercó su rostro hacia mi mejilla."

# game/dialogs.rpy:4220
translate spanish bonus4_f50c2971:

    # "My heart was beating loudly as she draws near..."
    "Mi corazón estaba latiendo fuertemente mientras se acercaba..."

# game/dialogs.rpy:4221
translate spanish bonus4_fcdd662b:

    # ".........{i}*chu~*{/i}......"
    ".........{i}*chu~*{/i}......"

# game/dialogs.rpy:4222
translate spanish bonus4_e916cd9d:

    # s "Awww, so cute!!!"
    s "¡¡¡Awww, tan lindo!!!"

# game/dialogs.rpy:4224
translate spanish bonus4_4ef9b2c5:

    # n "You better wash your cheek and kiss me at once after this!"
    n "¡Será mejor que te laves tu mejilla y me beses luego de esto!"

# game/dialogs.rpy:4226
translate spanish bonus4_79a8814d:

    # n "Hey, get a room, you two!"
    n "¡Oigan, consigan un cuarto, ustedes dos!"

# game/dialogs.rpy:4228
translate spanish bonus4_d218ebbe:

    # "Then Rika got back to where she was sitting."
    "Entonces Rika regresó donde estaba sentada."

# game/dialogs.rpy:4230
translate spanish bonus4_e986db9f:

    # "She was embarrassed but her face was still looking pissed."
    "Ella estaba avergonzada pero su rostro aún se miraba molesta."

# game/dialogs.rpy:4232
translate spanish bonus4_98a2e64c:

    # r "T-t-that's private stuff! You're not supposed to watch that!..."
    r "¡E-e-eso es una cosa privada! ¡No se supone que veas eso!..."

# game/dialogs.rpy:4233
translate spanish bonus4_20c841e6:

    # "I silently agreed, blushing red."
    "Silenciosamente estuve de acuerdo, sonrojándome."

# game/dialogs.rpy:4234
translate spanish bonus4_479b77b6:

    # "But well...as Sakura said, it was a punishment..."
    "Pero bueno... Como Sakura dijo, era un castigo..."

# game/dialogs.rpy:4236
translate spanish bonus4_8dbc83a8:

    # r "D... Don't expect anything, you pervert, you city rat!... It's just a punishment!"
    r "No...No esperes nada, tú pervertido, ¡tú rata de la ciudad!... ¡Es solo un castigo!"

# game/dialogs.rpy:4238
translate spanish bonus4_e77cd0a8:

    # r "It should be Sakura's job, anyway!"
    r "¡Debería ser el trabajo de Sakura, de todas maneras!"

# game/dialogs.rpy:4240
translate spanish bonus4_b9726a19:

    # r "It should be Nanami's job, anyway!"
    r "¡Debería ser el trabajo de Nanami, de todas maneras!"

# game/dialogs.rpy:4241
translate spanish bonus4_929d41bb:

    # n "Right!"
    n "¡Cierto!"

# game/dialogs.rpy:4242
translate spanish bonus4_d0abd9fc:

    # hero "Hmph! Fine with me!"
    hero "¡Hmph! ¡Bien por mí!"

# game/dialogs.rpy:4243
translate spanish bonus4_9b79ecac:

    # "I felt that I'm turning red too..."
    "Sentí que me estaba tornando rojizo también..."

# game/dialogs.rpy:4245
translate spanish bonus4_8fc82e11:

    # "Sakura woke me up in my confusion."
    "Sakura me despertó en mi confusión."

# game/dialogs.rpy:4246
translate spanish bonus4_ad51f715:

    # s "Hey, I'm your girlfriend, don't you forget!"
    s "¡Oye, soy tu novia, no lo olvides!"

# game/dialogs.rpy:4247
translate spanish bonus4_2aa2b9a6:

    # "I smiled and kissed Sakura fondly on the cheek."
    "Sonreí y besé a Sakura cariñosamente en la mejilla."

# game/dialogs.rpy:4248
translate spanish bonus4_a2e4f8f1:

    # n "Saku and %(stringhero)s, sitting in a tree, K. I. S. S. I. N. G.!..."
    n "Saku y %(stringhero)s, sentados en un árbol, ¡B.E.S.Á.N.D.O.S.E.!..."

# game/dialogs.rpy:4250
translate spanish bonus4_4420fe81:

    # "Nanami woke me up in my confusion."
    "Nanami me despertó de mi confusión."

# game/dialogs.rpy:4251
translate spanish bonus4_ad51f715_1:

    # s "Hey, I'm your girlfriend, don't you forget!"
    s "Oye, soy tu novia, ¡no lo olvides!"

# game/dialogs.rpy:4252
translate spanish bonus4_c6260d04:

    # "I smiled and kissed Nanami fondly on the cheek."
    "Sonreí y besé a Nanami cariñosamente en la mejilla."

# game/dialogs.rpy:4253
translate spanish bonus4_a5c16330:

    # hero "By the way... It's your turn for the next question, Rika-chan..."
    hero "Por cierto... Es tu turno para la siguiente pregunta, Rika-chan..."

# game/dialogs.rpy:4259
translate spanish bonus4_fc3d4898:

    # "The game was long and the punishments was fun... Sometimes evil but fun..."
    "El juego fue largo y los castigos divertidos... Algunas veces malvados pero divertidos..."

# game/dialogs.rpy:4260
translate spanish bonus4_4c6939cf:

    # "When evening came, we started to go back to our homes."
    "Cuando la noche llegó, comenzamos a regresar a nuestras casas."

# game/dialogs.rpy:4266
translate spanish bonus4_f83000c7:

    # "I was escorting Sakura-chan to her house as usual and we were chatting about the picnic."
    "Estaba escoltando a Sakura a su casa como simpre y estábamos platicando sobre el día de campo."

# game/dialogs.rpy:4267
translate spanish bonus4_ca69579e:

    # s "That was fun... [hnick!t]!"
    s "Eso fue divertido... ¡[hnick!t]!"

# game/dialogs.rpy:4268
translate spanish bonus4_313e8d85:

    # hero "You know, you don't need to keep calling me [hnick!t], now. The girls aren't with us anymore."
    hero "Sabes, no tienes que seguir llamándome [hnick!t]. Las chicas ya no están con nosotros."

# game/dialogs.rpy:4269
translate spanish bonus4_c63b8929:

    # s "It's okay, I like taking the punishment seriously...{p}Unless you don't like me calling you that?"
    s "Esta bien, me gusta tomarme el castigo seriamente...{p}¿A menos que no te agrade que te llame así?"

# game/dialogs.rpy:4270
translate spanish bonus4_e5dc6483:

    # hero "No, it's okay, I enjoy it!"
    hero "No, esta bien, ¡lo disfruto!"

# game/dialogs.rpy:4273
translate spanish bonus4_90714309:

    # hero "Even if it might sound weird between lovers..."
    hero "Incluso si suena raro entre amantes..."

# game/dialogs.rpy:4274
translate spanish bonus4_b9b5a607:

    # "We smiled together."
    "Sonreímos juntos."

# game/dialogs.rpy:4277
translate spanish bonus4_756dbf3c:

    # s "[hnick!t], do you...{p}Do you think I would look pretty in a maid outfit?"
    s "[hnick!t], piensas...{p}¿Piensas que me vería bonita en un traje de sirvienta?"

# game/dialogs.rpy:4279
translate spanish bonus4_6dc8daef:

    # "I was close to nose bleeding as I was imagining Sakura in a maid outfit."
    "Estaba cerca de sangrar por la nariz mientras me imaginaba a Sakura en un traje de sirvienta."

# game/dialogs.rpy:4280
translate spanish bonus4_6049d7bb:

    # hero "I... I'm pretty sure of it, Sakura-chan!"
    hero "E... ¡Estoy muy seguro de eso, Sakura-chan!"

# game/dialogs.rpy:4281
translate spanish bonus4_e234cd17:

    # hero "In fact... No matter what you'll wear, you always will be cute to me."
    hero "De hecho... No importa lo que uses, tú siempre serás linda para mí."

# game/dialogs.rpy:4282
translate spanish bonus4_138db92c:

    # "We blushed together."
    "Nos sonrojamos juntos."

# game/dialogs.rpy:4284
translate spanish bonus4_554402fd:

    # hero "Well... I guess you would..."
    hero "Bueno... Supongo que lo serías..."

# game/dialogs.rpy:4285
translate spanish bonus4_03905b91:

    # hero "You know how cute you look already."
    hero "Tú ya sabes lo linda que te ves."

# game/dialogs.rpy:4286
translate spanish bonus4_a0391de0:

    # "She blushed."
    "Ella se sonrojó."

# game/dialogs.rpy:4289
translate spanish bonus4_4722aed7:

    # s "You know... I like to call you [hnick!t]..."
    s "Sabes... Me gusta llamarte [hnick!t]..."

# game/dialogs.rpy:4290
translate spanish bonus4_1ea38385:

    # s "I always wanted to have a big brother but I didn't got this chance."
    s "Siempre quise tener un hermano mayor pero no tuve la oportunidad."

# game/dialogs.rpy:4291
translate spanish bonus4_dcd60152:

    # s "So, calling you like this... It's almost as if... If I have one..."
    s "Así que, llamándote así... Es casi como si... si tuviera uno..."

# game/dialogs.rpy:4292
translate spanish bonus4_acc2309b:

    # "We blushed together, deeply."
    "Nos sonrojamos juntos, intensamente."

# game/dialogs.rpy:4294
translate spanish bonus4_119e7f5e:

    # "Well, I guess it would be nice for her to have a brother. As long as he doesn't turn crazy easily like Nanami's brother..."
    "Bueno, supongo que sería bonito para ella tener un hermano. Mientras que no se vuelva loco fácilmente como el hermano de Nanami..."

# game/dialogs.rpy:4295
translate spanish bonus4_1f716602:

    # hero "Well... If you still want to call me like this all the time... I won't mind at all, Sakura-chan."
    hero "Bueno... Si aún quieres llamarme así todo el tiempo... No me importaría para nada, Sakura-chan."

# game/dialogs.rpy:4296
translate spanish bonus4_a465601d:

    # hero "To be honest, I always wanted to have a little sister."
    hero "Para ser honesto, siempre quise tener una hermana pequeña."

# game/dialogs.rpy:4297
translate spanish bonus4_b5b85398:

    # s "But you have a big sister, don't you?"
    s "¿Pero tú tienes una hermana mayor, cierto?"

# game/dialogs.rpy:4298
translate spanish bonus4_3f93cb80:

    # hero "Yeah, but it's not the same thing. Plus, she doesn't live with me anymore..."
    hero "Si, pero no es lo mismo. Además, ella ya no vive conmigo."

# game/dialogs.rpy:4299
translate spanish bonus4_6fc9312b:

    # "Sakura nodded..."
    "Sakura asintió..."

# game/dialogs.rpy:4303
translate spanish bonus4_bd6e96bc:

    # s "So, how's your couple with Rika-chan?"
    s "Entonces, ¿cómo van las cosas con Rika-chan?"

# game/dialogs.rpy:4304
translate spanish bonus4_a9623e40:

    # hero "Pretty good! I think I'm going to marry her!"
    hero "¡Bastante bien! ¡Pienso que me voy a casar con ella!"

# game/dialogs.rpy:4306
translate spanish bonus4_dd27c7e7:

    # s "Really?! That's great!!"
    s "¡¿En serio?! ¡¡Eso es grandioso!!"

# game/dialogs.rpy:4307
translate spanish bonus4_699e47c9:

    # hero "You think she will say yes?"
    hero "¿Piensas que ella dirá que si?"

# game/dialogs.rpy:4309
translate spanish bonus4_df94e128:

    # s "I'm sure she will."
    s "Estoy segura que lo hará."

# game/dialogs.rpy:4310
translate spanish bonus4_c15523d1:

    # s "You know, she really loves you more than it looks."
    s "Sabes, ella realmente te ama más de lo que aparenta."

# game/dialogs.rpy:4311
translate spanish bonus4_f0c8c519:

    # s "It's just her way of being herself."
    s "Es solo la manera de ser ella misma."

# game/dialogs.rpy:4312
translate spanish bonus4_10b77e4e:

    # hero "Yeah, I know..."
    hero "Si, lo sé..."

# game/dialogs.rpy:4313
translate spanish bonus4_7f081c6b:

    # "Then I stopped and I asked Sakura."
    "Entonces me detuve y le pregunté a Sakura."

# game/dialogs.rpy:4314
translate spanish bonus4_f25fd7bb:

    # hero "Sakura-chan..."
    hero "Sakura-chan..."

# game/dialogs.rpy:4315
translate spanish bonus4_7347e2f5:

    # hero "I would be so honored that you become our future children's godparent!"
    hero "¡Estaría tan honrado de que te conviertas en la madrina de nuestros futuros hijos!"

# game/dialogs.rpy:4316
translate spanish bonus4_2662060c:

    # hero "Would you like to?"
    hero "¿Te gustaría?"

# game/dialogs.rpy:4318
translate spanish bonus4_197853e0:

    # "Sakura got surprised and didn't said a word for a while."
    "Sakura se sorprendió y no dijo ni una palabra por un tiempo."

# game/dialogs.rpy:4319
translate spanish bonus4_08e5cd3b:

    # "But her face made a shiny smile, finally."
    "Pero su rostro hizo una reluciente sonrisa, al final."

# game/dialogs.rpy:4321
translate spanish bonus4_a89ea5da:

    # s "Oh yes! Yes, I would love to!"
    s "¡Oh si! ¡Si, me encantaría!"

# game/dialogs.rpy:4322
translate spanish bonus4_95d8a37f:

    # hero "Yes! Thank you, Sakura-chan!"
    hero "¡Si! ¡Gracias, Sakura-chan!"

# game/dialogs.rpy:4323
translate spanish bonus4_189cb5d4:

    # s "I'm so happy you asked me that!"
    s "¡Estoy tan feliz de que me preguntaras eso!"

# game/dialogs.rpy:4324
translate spanish bonus4_1170dcb4:

    # hero "You really are a close friend, Sakura-chan..."
    hero "Tú realmente eres una amiga cercana, Sakura-chan..."

# game/dialogs.rpy:4326
translate spanish bonus4_2fff80d3:

    # hero "I know we can't technically be brothers and sisters... But then I want you to be a family friend!"
    hero "Sé que técnicamente no podemos ser hermanos y hermanas... ¡Pero quiero que seas una amiga de la familia!"

# game/dialogs.rpy:4328
translate spanish bonus4_c58e083a:

    # hero "I would like so much to have you as a family friend!"
    hero "¡Me gustaría tanto tenerte como una amiga de la familia!"

# game/dialogs.rpy:4329
translate spanish bonus4_02601609:

    # "Sakura suddenly hugged me."
    "Sakura de pronto me abrazó."

# game/dialogs.rpy:4330
translate spanish bonus4_8b5ff4e0:

    # s "Thank you so much, %(stringhero)s!... I mean, [hnick!t]!"
    s "¡Muchisimas gracias, %(stringhero)s!... Digo, ¡[hnick!t]!"

# game/dialogs.rpy:4331
translate spanish bonus4_bdc5b511:

    # "I held her a moment with a smile."
    "La abracé por un momento con una sonrisa."

# game/dialogs.rpy:4334
translate spanish bonus4_7b0de0eb:

    # s "So, how's your couple with Nana-chan?"
    s "Entonces, ¿cómo van las cosas con Nana-chan?"

# game/dialogs.rpy:4335
translate spanish bonus4_22478f8e:

    # hero "It's great! She's an amazing girl."
    hero "¡Es grandioso! Ella es una asombrosa chica."

# game/dialogs.rpy:4336
translate spanish bonus4_5ba932be:

    # hero "We plan to go to her grand-parents at Okinawa for the vacations, for a couple of weeks."
    hero "Planeamos ir con sus abuelos en Okinawa para las vacaciones, por un par de semanas."

# game/dialogs.rpy:4337
translate spanish bonus4_a54e7c2a:

    # s "That's great!"
    s "¡Eso es grandioso!"

# game/dialogs.rpy:4338
translate spanish bonus4_d33cd195:

    # s "What you will do, there?"
    s "¿Qué es lo que harán ahí?"

# game/dialogs.rpy:4339
translate spanish bonus4_15d9f409:

    # hero "I don't know yet."
    hero "Aún no sé."

# game/dialogs.rpy:4340
translate spanish bonus4_b396e8be:

    # hero "Since it's Nanami-chan's birth land, she probably knows what's the best."
    hero "Puesto que es la tierra natal de Nanami-chan, ella probablemente conozca qué es lo mejor."

# game/dialogs.rpy:4341
translate spanish bonus4_fb2732b4:

    # hero "So far, she said there's a lot of festivals at this period of the year."
    hero "Hasta ahora, ella dijo que habían muchos festivales en este periodo del año."

# game/dialogs.rpy:4342
translate spanish bonus4_aff5d3d2:

    # s "You're so lucky!"
    s "¡Eres tan afortunado!"

# game/dialogs.rpy:4343
translate spanish bonus4_a3265a2a:

    # s "I wish I could go there."
    s "Desería poder ir ahí."

# game/dialogs.rpy:4344
translate spanish bonus4_9597c256:

    # "I nodded, undestanding her feeling."
    "Asentí, comprendiendo su sentimiento."

# game/dialogs.rpy:4345
translate spanish bonus4_d8972d72:

    # "Then I got an idea."
    "Entonces tuve una idea."

# game/dialogs.rpy:4346
translate spanish bonus4_3d45bfe0:

    # hero "Hey... Why not trying to organize a trip for all of us when we will come back?"
    hero "Oye... ¿Porqué no tratamos de organizar un viaje para todos nosotros cuando volvamos?"

# game/dialogs.rpy:4347
translate spanish bonus4_7e322d73:

    # hero "Like, I'll go there with Nanami in recognition, then when we will come back, we will know what are the great spots we can go."
    hero "Como, voy ahí con Nanami en reconocimiento, entonces cuando regresemos, sabremos cuáles son los lugares grandiosos que podemos ir."

# game/dialogs.rpy:4348
translate spanish bonus4_6796bcb2:

    # hero "And while that, we'll save money all together and Rika will organize the trip for all of us!"
    hero "Y mientras tanto, ¡ahorraremos dinero todos juntos y Rika organizaría el viaje para todos nosotros!"

# game/dialogs.rpy:4350
translate spanish bonus4_0d18a236:

    # s "That sounds so great!"
    s "¡Eso suena tan bien!"

# game/dialogs.rpy:4351
translate spanish bonus4_52addb72:

    # hero "I'll call Rika tomorrow to propose it!"
    hero "¡Voy a llamar a Rika mañana para proponérselo!"

# game/dialogs.rpy:4352
translate spanish bonus4_8cd12022:

    # hero "I'm sure it will be alot of fun!"
    hero "¡Estoy seguro de que será un montón de diversión!"

# game/dialogs.rpy:4357
translate spanish bonus4_e1a7c00a:

    # "We finally reached Sakura's house."
    "Finalmente llegamos a la casa de Sakura."

# game/dialogs.rpy:4358
translate spanish bonus4_94a9ed42:

    # s "See you soon and take care... [hnick!t]."
    s "Te veo en la escuela y ten cuidado... [hnick!t]."

# game/dialogs.rpy:4360
translate spanish bonus4_50eaa374:

    # "Sakura does a maid-like reverence with her little summer dress."
    "Sakura hace una reverencia tipo sirvienta con su pequeño vestido veraniego."

# game/dialogs.rpy:4361
translate spanish bonus4_0f54fbae:

    # hero "Take care, Sakura-chan!"
    hero "¡Cuidate, Sakura-chan!"

# game/dialogs.rpy:4363
translate spanish bonus4_68a39d42:

    # hero "Take care, little sister!"
    hero "¡Cuidate, hermanita!"

# game/dialogs.rpy:4365
translate spanish bonus4_8dcab874:

    # "She giggled and disappeared in her house."
    "Ella se rió y desapareció dentro de su casa."

# game/dialogs.rpy:4366
translate spanish bonus4_6b242521:

    # "That was a fine sunday..."
    "Ese fue un buen Domingo..."

# game/dialogs.rpy:4367
translate spanish bonus4_5206326a:

    # "A good start for the vacations..."
    "Un buen comienzo para las vacaciones..."

# game/dialogs.rpy:4380
translate spanish previewmsg_5de028fa:

    # centered "{size=+12}The story starts but the preview ends here...{/size}"
    centered "{size=+12}The story starts but the preview ends here...{/size}"

# game/dialogs.rpy:4381
translate spanish previewmsg_f6d47b23:

    # centered "{size=+12}Stay tuned and keep the app in your device to get the free complete update of Kare wa Kanojo in early 2018!{/size}"
    centered "{size=+12}Stay tuned and keep the app in your device to get the free complete update of Kare wa Kanojo in early 2018!{/size}"

translate spanish strings:

    # dialogs.rpy:34
    old "What should I do?"
    new "¿Qué debería hacer?"

    # dialogs.rpy:34
    old "Let's play some online game."
    new "Vamos a jugar unos juegos en línea."

    # dialogs.rpy:34
    old "Let's rewatch some anime VHS."
    new "Vamos a ver de nuevo algún VHS de anime."

    # dialogs.rpy:34
    old "Let's study a bit."
    new "Vamos a estudiar un poco."

    # dialogs.rpy:438
    old "What should I say?"
    new "¿Qué debería de decir?"

    # dialogs.rpy:438
    old "The truth: {i}High School Samurai{/i}, an ecchi shonen manga"
    new "La verdad: {i}High School Samurai{/i}, un manga ecchi shonen"

    # dialogs.rpy:438
    old "Not completely the truth: {i}Rosario Maiden{/i}, a seinen manga about vampire dolls"
    new "No completamente la verdad: {i}Rosario Maiden{/i}, un manga seinen sobre muñecas vampiro"

    # dialogs.rpy:940
    old "What should I do?..."
    new "¿Qué debería de hacer?"

    # dialogs.rpy:940
    old "Tell the truth to Sakura"
    new "Decir la verdad a Sakura"

    # dialogs.rpy:940
    old "Keep the secret and buy it another day"
    new "Mantener el secreto y comprarlo otro día"

    # dialogs.rpy:1609
    old "Please help me! What should I do??"
    new "¡Por favor ayudame! ¿¿Qué debería de hacer??"

    # dialogs.rpy:1609
    old "No!... It's a boy! I can't love him, I'm not gay!"
    new "¡No!... ¡Es un chico! No puedo amarlo, ¡No soy gay!"

    # dialogs.rpy:1609
    old "Whatever! Love doesn't have gender!"
    new "¡Como sea! ¡El amor no tiene géneros!"

    # dialogs.rpy:4107
    old "Call me \"Master\"!"
    new "¡Llámame \"Amo\"!"

    # dialogs.rpy:4107
    old "Call me \"Big brother\"!"
    new "¡Llámame \"Hermano mayor\"!"
