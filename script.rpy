﻿# TODO: Translation updated at 2017-11-05 17:34

# game/script.rpy:220
translate spanish splashscreen_73841dfc:

    # centered "THIS IS AN ALPHA RELEASE\n\nIt's not meant for public.\nPlease do not distribute!{fast}"
    centered "ESTE ES UN LANZAMIENTO ALFA\n\nNo está destinada para el público.\n¡Por favor no lo distribuyan!{fast}"

# game/script.rpy:251
translate spanish konami_code_e5b259c9:

    # "Max le Fou" "What the heck am I doing here?..."
    "Max le Fou" "¿Qué diablos estoy haciendo aquí?..."

# game/script.rpy:287
translate spanish gjconnect_4ae60ea0:

    # "Disconnected."
    "Desconectado."

# game/script.rpy:316
translate spanish gjconnect_e254a406:

    # "Connection to Game Jolt succesful.\nWARNING: The connection doesn't count for previously saved games."
    "Exitosa conexión a Game Jolt.\nADVERTENCIA: La conexión no cuenta para juegos previamnete guardados. "

translate spanish strings:

    # script.rpy:201
    old "Master"
    new "Amo"

    # script.rpy:202
    old "Big brother"
    new "Hermano mayor"

    # script.rpy:272
    old "Disconnect from Game Jolt?"
    new "¿Desconectar de Game Jolt?"

    # script.rpy:272
    old "Yes, disconnect."
    new "Si, desconectar."

    # script.rpy:272
    old "No, return to menu."
    new "No, regresar al menú."

    # script.rpy:334
    old "A problem occured. Maybe your connection has a problem. Or maybe it's Game Jolt derping...\n\nTry again?"
    new "Un problema ocurrió. Tal vez su conexión tiene un problema. O tal vez es Game Jolt fallando...\n\n¿Intentar de nuevo?"

    # script.rpy:334
    old "Yes, try again."
    new "Si, tratar de nuevo."

    # script.rpy:352
    old "It seems the authentication failed. Maybe you didn't write correctly the username and token...\n\nTry again?"
    new "Parece que la autenticación falló. Tal vez no escribió correctamente el nombre de usuario y la autenticación...\n\n¿Intentar de nuevo?"

    # script.rpy:228
    old "Please enter your name and press Enter:"
    new "Por favor ingrese su nombre y presione Enter."

    # script.rpy:295
    old "Type here your username and press Enter."
    new "Teclee su nombre de usuario y presione Enter."

    # script.rpy:296
    old "Now, type here your game token and press Enter."
    new "Ahora, teclee aquí su autenticación de juego y presione Enter."

    # script.rpy:207
    old "Game Jolt trophy obtained!"
    new "¡Trofeo de Game Jolt obtenido!"

    # script.rpy:177
    old "Sakura"
    new "Sakura"

    # script.rpy:178
    old "Rika"
    new "Rika"

    # script.rpy:179
    old "Nanami"
    new "Nanami"

    # script.rpy:180
    old "Sakura's mom"
    new "Mamá de S."

    # script.rpy:181
    old "Sakura's dad"
    new "Papá de S."

    # script.rpy:182
    old "Toshio"
    new "Toshio"

    # script.rpy:188
    old "Taichi"
    new "Taichi"

    # script.rpy:13
    old "{size=80}Thanks for playing!"
    new "{size=80}¡Gracias por jugar!"
# TODO: Translation updated at 2019-04-23 10:19

# game/script.rpy:286
translate spanish update_menu_dc3e4e02:

    # "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"
    "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"

# game/script.rpy:300
translate spanish gjconnect_dc3e4e02:

    # "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"
    "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"

# TODO: Translation updated at 2019-04-27 10:49

translate spanish strings:

    # script.rpy:226
    old "Achievement obtained!"
    new "¡Logro obtenido!"
